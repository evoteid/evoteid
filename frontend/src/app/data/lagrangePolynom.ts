import {LagrangePoint} from './lagrangePoint';

export class LagrangePolynom {

    private points: LagrangePoint[];

    constructor() {
        this.points = new Array();
    }

    addPoint(position: string, value: string) {
        this.points.push(new LagrangePoint(position, value));
    }

    getPoints(): LagrangePoint[] {
        return this.points;
    }
}
