export class AsymmetricKey {

    constructor(private publicKey: string, private privateKey: string) {
    }

    getPrivateKey() {
        return this.privateKey;
    }

    getPublicKey() {
        return this.publicKey;
    }
}
