export class LagrangePoint {

    private readonly position: string;
    private readonly value: string;

    constructor(position: string, value: string) {
        this.position = position;
        this.value = value;
    }

    getPosition(): string {
        return this.position;
    }

    getValue(): string {
        return this.value;
    }
}
