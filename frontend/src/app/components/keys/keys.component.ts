import {Maybe } from './../../utils/maybe.utils';
import {AsymmetricKey } from './../../data/asymmetricKey';
import {CryptoService } from 'src/app/services/crypto/crypto.service';
import {Component, Inject, OnInit} from '@angular/core';
import {SaveFileService } from 'src/app/services/saveFile/saveFile.service';
import {CurrentUserService } from 'src/app/services/currentUser/currentUser.service';
import {LocalStorageService } from 'src/app/services/localStorage/localStorage.service';
import {Router } from '@angular/router';
import {KeysService} from '../../services/communication/keys/keys.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-keys',
  templateUrl: './keys.component.html',
  styleUrls: ['./keys.component.css']
})
export class KeysComponent implements OnInit {

  isGeneratingKey = false;
  votingId: number;

  privateKey = '';
  publicKey = '';

  constructor(@Inject(MAT_DIALOG_DATA) private data: { votingId: number },
              private router: Router,
              private cryptoService: CryptoService,
              private saveFileService: SaveFileService,
              private currentUserService: CurrentUserService,
              private keysService: KeysService,
              private localStorageService: LocalStorageService,
              private dialogRef: MatDialogRef<KeysComponent>,
              private snackBar: MatSnackBar) {
    this.votingId = data.votingId;
  }

  ngOnInit() {
  }

  generateKey() {
    this.privateKey = '';
    this.publicKey = '';
    this.isGeneratingKey = true;
    this.cryptoService.generateKeyPair().then(maybeKeyPair => this.setKeys(maybeKeyPair));
  }

  savePrivateKey() {
    this.saveFileService.saveFile(this.privateKey, 'key.secret');
  }

  private processResponse(resp: boolean) {
    if (resp) {
      this.dialogRef.close(true);
    }
    else {
      this.showErrorSnackBar('Adding the key failed');
    }
  }

  sendPublicKey() {
    this.keysService.storePublicKeyForVoting(this.currentUserService.getUserId().getData(), this.votingId, this.publicKey).subscribe(resp => this.processResponse(resp));
  }

  private setKeys(maybeKey: Maybe<AsymmetricKey> ) {
    if (maybeKey.isEmpty()) {
    }
    else {
      this.isGeneratingKey = false;
      this.privateKey = maybeKey.getData().getPrivateKey();
      this.publicKey = maybeKey.getData().getPublicKey();
    }
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
