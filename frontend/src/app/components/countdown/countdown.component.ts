import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.css']
})
export class CountdownComponent implements OnInit {

  @Input() endTime: string;

  days: number;
  hours: number;
  minutes: number;
  seconds: number;

  leftTime: number;

  constructor() {
  }

  ngOnInit() {
    setInterval(() => {
      this.updateTime();
    }, 1000);
  }

  private updateTime() {
    let time =  new Date(this.endTime).getTime() - new Date().getTime();
    this.leftTime = time;
    this.days = Math.floor(time / (1000 * 60 * 60 * 24));
    time -= (this.days * 1000 * 60 * 60 * 24);

    this.hours = Math.floor(time / (1000 * 60 * 60));
    time -= (this.hours * 1000 * 60 * 60);

    this.minutes = Math.floor(time / (1000 * 60));
    time -= (this.minutes * 1000 * 60);

    this.seconds = Math.floor(time / 1000);
  }

}
