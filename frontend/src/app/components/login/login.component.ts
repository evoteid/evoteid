import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/communication/authentication/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import { LocalStorageService } from '../../services/localStorage/localStorage.service';
import {AuthenticatedVoterService} from '../../services/communication/authenticatedVoter/authenticatedVoter.service';
import {Maybe} from '../../utils/maybe.utils';
import {AuthenticatedVoter} from '../../models/authenticatedVoter/authenticatedVoter.json.model';
import {environment} from '../../../environments/environment';
import {VotersService} from '../../services/communication/voters/voters.service';
import {Pair} from '../../utils/pair.utils';
import {MatSnackBar} from '@angular/material';
import {SubscriberService} from '../../services/subscription/subscriber.service';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService,
              private authenticatedVoterService: AuthenticatedVoterService,
              private votersService: VotersService,
              private storageService: LocalStorageService,
              private subscriberService: SubscriberService,
              private currentUserService: CurrentUserService,
              private snackBar: MatSnackBar) { }

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  private returnUrl;

  isLoggedIn = false;
  isRegistered = false;
  environmentName = environment.NAME;

  voterName = '';

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
    this.setup();
  }

  setup() {
    this.authenticatedVoterService.getAuthenticatedVoter().subscribe(data => this.onAuthenticatedVoterGet(data));
  }

  onAuthenticatedVoterGet(authenticatedVoter: Maybe<AuthenticatedVoter>) {
    if (authenticatedVoter.isEmpty()) {
      this.isLoggedIn = false;
      this.isRegistered = false;
      return;
    }
    else {
      this.isLoggedIn = true;
      this.isRegistered = authenticatedVoter.getData().getIsRegistered();
      this.voterName = authenticatedVoter.getData().getVoterName();
    }

    this.currentUserService.setIsCurrentUserRegistered(this.isRegistered);

    this.subscriberService.notifyTokenChanged();

    if (this.isRegistered) {
      if (this.returnUrl === undefined) {
        this.router.navigate(['/home']);
      }
      else {
        this.router.navigate([this.returnUrl]);
      }
    }
  }


  register() {
    this.votersService.registerCurrentVoter().subscribe(ret => this.onRegistered(ret));
  }

  onRegistered(ret: Pair<boolean, string>) {
    if (ret.getFirst()) {
      this.showSuccessfulSnackBar(ret.getSecond());
      this.setup();
    }
    else {
      this.showErrorSnackBar(ret.getSecond());
    }
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
