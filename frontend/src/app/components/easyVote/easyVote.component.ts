import {Component, Input, OnInit} from '@angular/core';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {MatDialog, MatSnackBar} from '@angular/material';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';
import {VoteService} from '../../services/communication/vote/vote.service';
import {CommitmentsService} from '../../services/communication/commitments/commitments.service';
import {SecretSharesService} from '../../services/communication/secretShares/secretShares.service';
import {forkJoin, Observable, of} from 'rxjs';
import {Maybe} from '../../utils/maybe.utils';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {Commitments} from '../../models/commitments/commitments.model';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';
import {LocalStorageService} from '../../services/localStorage/localStorage.service';
import {VoteHelperService} from '../../services/vote/voteHelper.service';
import {SingleVote} from '../../models/vote/singleVote.json.model';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {ActivatedRoute} from '@angular/router';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';
import {VotingVoterProgress} from '../../models/votings/votingVoterProgress';
import {CryptoService} from '../../services/crypto/crypto.service';
import {KeysService} from '../../services/communication/keys/keys.service';
import {Pair} from '../../utils/pair.utils';
import {VoteNIZKProofService} from '../../services/communication/voteNIZKProofs/voteNIZKProofs.service';
import {ZeroKnowledgeProofHelperService} from '../../services/zeroKnowledgeProofs/zeroKnowledgeProofHelper.service';
import {flatMap} from 'rxjs/operators';
import {PrivateKeyService} from '../../services/privateKey/privateKey.service';
import {VoteNIZKProof} from '../../models/voteNIZKProof/voteNIZKProofs';

class CalculationData {
  indexOfVote: number;
  dataForKeyCalculation: DataForKeyCalculation;
  privateKey: string;
  currentVoterId: number;
  currentVotingId: number;
  currentVoterName: string;
  selectedVotingOption: string;
  commitments: Commitments;
  secretShares: SecretSharesForVoter;
  validated: Boolean;
  vote: SingleVote;
  r: string;
  publicKeyToEncryptVote: string;
  proof: VoteNIZKProof;
  errorMessage: string;
  successful = true;
}

@Component({
  selector: 'app-easy-vote',
  templateUrl: './easyVote.component.html',
  styleUrls: ['./easyVote.component.css']
})
export class EasyVoteComponent implements OnInit {

  @Input() parentComponentName: string;

  title: string;
  noVotingsMessage: string;
  isInTrusteeTab: boolean;

  displayedVotings: Array<VotingOverview> = [];

  statusMap: { [id: number]: VotingVoterProgress; } = {};

  selectedVotingOption: string[];

  showSpinner = false;
  spinnerDescription = '';

  voterIdToDisplay: number;
  votingIdToDisplay: number;

  constructor(
      private currentUserService: CurrentUserService,
      private voteService: VoteService,
      private voteNIZKProofService: VoteNIZKProofService,
      private votingsDataService: VotingsDataService,
      private commitmentService: CommitmentsService,
      private secretShareService: SecretSharesService,
      private storageService: LocalStorageService,
      private voteHelperService: VoteHelperService,
      private zeroKnowledgeProofHelperService: ZeroKnowledgeProofHelperService,
      private keyService: KeysService,
      private cryptoService: CryptoService,
      private privateKeyService: PrivateKeyService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog,
      private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const routeParams = this.activatedRoute.queryParams;

    const params = this.activatedRoute.snapshot.params;
    const voterId = params['voterId'];
    const votingId = params['votingId'];

    this.voterIdToDisplay = voterId;
    this.votingIdToDisplay = votingId;
    this.isInTrusteeTab = this.parentComponentName === 'trustee';

    if (!this.isInTrusteeTab) {
      this.title = 'Current votings';
      this.noVotingsMessage = 'At the moment there are no votings available.';
    }
    else {
      this.title = 'Ongoing votings';
      this.noVotingsMessage = 'At the moment there are no ongoing votings available.';
    }

    this.votingsDataService.getCurrentVotings(2).subscribe(votings => this.onInitDataFetched(votings, voterId, votingId));
  }

  onInitDataFetched(votings: Maybe<VotingsWithProgressArray>, voterId: number, votingId: number) {
    this.evaluateVotingsProgressData(votings.getData().getVotingsVoterProgress());

    if (voterId === undefined || votingId === undefined) {
      this.displayedVotings = votings.getData().getVotings();
      this.selectedVotingOption = new Array(this.displayedVotings.length);
      return;
    }

    const currentUserId = this.currentUserService.getUserId().getData();

    if (Number(currentUserId) !== Number(voterId)) {
      console.log('invalid url');
    }

    for (const votingOverview of votings.getData().getVotings()) {
      if (Number(votingOverview.getId()) === Number(votingId)) {
        this.displayedVotings = [votingOverview];
        this.selectedVotingOption = new Array(1);
        return;
      }
    }
  }

  evaluateVotingsProgressData(progress: VotingVoterProgress[]) {
    for (const singleProgress of progress) {
      this.statusMap[singleProgress.getVotingId().valueOf()] = singleProgress;
    }
  }

  submit(votingId: number, indexOfVote: number) {
    const selectedVotingOption = this.selectedVotingOption[indexOfVote];

    if (selectedVotingOption === undefined) {
      this.snackBar.open('You did not choose an option.', '', {
        duration: 2000,
        panelClass: ['red-snackbar']
      });
      return;
    }

    const maybeCurrentUserId = this.currentUserService.getUserId();
    if (maybeCurrentUserId.isEmpty()) {
      this.showErrorSnackBar('Inconsistent state. Please refresh the page to solve the issue.');
      return;
    }

    this.spinnerDescription = 'Loading shares ...';
    this.showSpinner = true;

    const currentVoterId = maybeCurrentUserId.getData();
    const currentVoterName = this.currentUserService.getVoterName().getData();

    const retData = new CalculationData();
    retData.indexOfVote = indexOfVote;
    retData.currentVoterId = currentVoterId;
    retData.currentVotingId = votingId;
    retData.selectedVotingOption = selectedVotingOption;
    retData.currentVoterName = currentVoterName;

    const votingsResult = this.voteService.getDataForKeyCalculation(votingId)
        .pipe(flatMap(res => this.getPrivateKey(res, retData)))
        .pipe(flatMap(res => this.loadSharesAndCommitments(res)))
        .pipe(flatMap(res => this.validateShares(res)))
        .pipe(flatMap(res => this.encryptVote(res)))
        .pipe(flatMap(res => this.calculateVoteNIZKProof(res)))
        .pipe(flatMap(res => this.sendVoteAndProof(res)));

    votingsResult.subscribe(res => this.afterVoter(res));

  }

  afterVoter(res: Maybe<CalculationData>) {
    this.showSpinner = false;
    if (res.isEmpty()) {
      this.showErrorSnackBar('Error');
    }

    const data = res.getData();
    if (data.successful) {
      this.showSuccessfulSnackBar('Successfully added the vote');
    }
    else  {
      this.showErrorSnackBar(data.errorMessage);
    }

    this.votingsDataService.getCurrentVotings(2)
        .subscribe(votings => this.onInitDataFetched(votings, this.voterIdToDisplay, this.votingIdToDisplay));
  }

  private getPrivateKey(maybeKeyCalculationsData: Maybe<DataForKeyCalculation>, retData): Observable<Maybe<CalculationData>> {
    if (maybeKeyCalculationsData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Loading key ...';

    retData.dataForKeyCalculation = maybeKeyCalculationsData.getData();

    const ret = this.privateKeyService.getPrivateKey(retData.dataForKeyCalculation, retData.currentVoterId).pipe(flatMap(res => this.aggregate(retData, res)));
    return ret;
  }

  private aggregate(data: CalculationData, maybePrivateKey: Maybe<string>): Observable<Maybe<CalculationData>> {
    if (maybePrivateKey.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    data.privateKey = maybePrivateKey.getData();
    return of(Maybe.fromData(data));
  }

  private loadSharesAndCommitments(maybeData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Loading shares ...';

    const data: CalculationData = maybeData.getData();

    const commitmentsObservable = this.commitmentService.getAllCommitments(data.currentVotingId);
    const secretShareObservable = this.secretShareService.getAllSharesForVoter(data.currentVotingId, data.currentVoterId);

    return forkJoin(
        commitmentsObservable,
        secretShareObservable
    ).pipe(flatMap(res => this.onSharesAndCommitmentsLoaded(res, data)));
  }

  private onSharesAndCommitmentsLoaded(responses: any[], data: CalculationData): Observable<Maybe<CalculationData>> {
    const maybeCommitments: Maybe<Commitments> = responses[0];
    const maybeSecretShares: Maybe<SecretSharesForVoter> = responses[1];

    if (maybeCommitments.isEmpty() || maybeSecretShares.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    data.commitments = maybeCommitments.getData();
    data.secretShares = maybeSecretShares.getData();

    return of(Maybe.fromData(data));
  }

  private validateShares(maybeData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Validating shares ...';

    const data = maybeData.getData();

    let isTrustee = false;
    for (const eligibleVoter of data.dataForKeyCalculation.getEligibleVoters()) {
      if ( eligibleVoter.getId() === this.currentUserService.getUserId().getData()) {
        isTrustee = eligibleVoter.getIsTrustee();
        break;
      }
    }

    if (!isTrustee) {
      return of(maybeData);
    }

    const ret: [number, boolean][] = this.voteHelperService.validateShares(
        data.dataForKeyCalculation,
        data.commitments,
        data.secretShares,
        data.privateKey);

    for (const val of ret) {
      if (!val[1]) {
        return of(Maybe.fromEmpty());
      }
    }

    data.validated = true;
    return of(Maybe.fromData(data));
  }

  private encryptVote(maybeData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Encrypting vote ...';

    const data = maybeData.getData();

    const publicKeyToEncryptVote = this.voteHelperService.calculatePublicKey(
        data.dataForKeyCalculation,
        data.commitments
    );

    const voteData: [string, string, string] = this.voteHelperService.encryptVote(
        data.dataForKeyCalculation,
        publicKeyToEncryptVote,
        '' + data.selectedVotingOption
    );

    const alpha = voteData[0];
    const beta = voteData[1];
    const r = voteData[2];

    const messageToSign = alpha + ',' + beta;
    const signature = this.cryptoService.signSHA256(data.privateKey, messageToSign).getData();

    const vote = new SingleVote(data.currentVoterId, data.currentVoterName, alpha, beta, signature);
    data.publicKeyToEncryptVote = publicKeyToEncryptVote;
    data.vote = vote;
    data.r = r;

    return of(Maybe.fromData(data));
  }

  private calculateVoteNIZKProof(maybeData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Calculating proof ...';

    const data = maybeData.getData();

    const voteNIZKProof = this.zeroKnowledgeProofHelperService.calculateVoteZeroKnowledgeProof(
        data.dataForKeyCalculation.getCalculationParameters(),
        data.vote.getAlpha(),
        data.vote.getBeta(),
        data.r,
        data.publicKeyToEncryptVote,
        +data.selectedVotingOption,
        this.displayedVotings[data.indexOfVote].getOptions().map(o => o.getOptionPrimeNumber().valueOf()));

    for (const singleProof of voteNIZKProof.getZeroKnowledgeProofs()) {
      const messageToSign = singleProof.getA() + ',' + singleProof.getB() + ',' + singleProof.getC() + ',' + singleProof.getR();
      const signature = this.cryptoService.signSHA256(data.privateKey, messageToSign).getData();
      singleProof.setSignature(signature);
    }

    data.proof = voteNIZKProof;

    return of(Maybe.fromData(data));
  }

  private sendVoteAndProof(maybeData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Sending data ...';

    const data = maybeData.getData();

    const voteObservable = this.voteService.vote(data.currentVotingId, data.currentVoterId, data.vote);
    const zeroKnowledgeProofObservable = this.voteNIZKProofService.sendNIZKProofs(data.currentVotingId, data.currentVoterId, data.proof);

    return forkJoin(
        voteObservable,
        zeroKnowledgeProofObservable,
    ).pipe(flatMap(res => this.onVoted(res, data)));

  }

  onVoted(res, data: CalculationData): Observable<Maybe<CalculationData>> {
    const voteResp: Pair<boolean, string> = res[0];
    const proofResp: Pair<boolean, string> = res[1];

    this.spinnerDescription = 'Checking result ...';

    if (!voteResp.getFirst()) {
      data.successful = false;
      data.errorMessage = voteResp.getSecond();
    }
    else if (!proofResp.getFirst()) {
      data.successful = false;
      data.errorMessage = proofResp.getSecond();
    }
    else {
      data.successful = true;
    }

    return of(Maybe.fromData(data));
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
