import { EligibleVoter } from './../../models/votings/eligibleVoter.json.model';
import { Maybe } from './../../utils/maybe.utils';
import { VotingOption } from 'src/app/models/votings/votingOption.json.model';
import { UserPublicKey } from './../../models/userPublicKey/userPublicKey.model';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { VotingsDataService } from 'src/app/services/communication/votingsData/votingsData.service';
import { CryptoService } from 'src/app/services/crypto/crypto.service';
import { VotingCalculationParameters } from 'src/app/models/votings/votingsCalculationParameters.json.model';
import { NewCreatedVoting } from '../../models/votings/newCreatedVoting.json.model';
import {PrimesService} from '../../services/primes/primes.service';
import {SafePrimePair} from '../../data/safePrimePair';
import {KeysService} from '../../services/communication/keys/keys.service';
import {MatSnackBar, MatStepper} from '@angular/material';
import {VotingsArray} from '../../models/votings/votingsArray.json.model';
import {RolesService} from '../../services/communication/roles/roles.service';
import {forkJoin} from 'rxjs';
import {SpecifyVotersComponent} from '../dialogs/specifyVoters/specifyVoters.component';
import {OptionsValidator} from '../validators/optionsValidator';

@Component({
  selector: 'app-add-voting',
  templateUrl: './addVoting.component.html',
  styleUrls: ['./addVoting.component.css'],
})
export class AddVotingComponent implements OnInit {

  constructor(private votingsDataService: VotingsDataService,
              private keyService: KeysService,
              private roleService: RolesService,
              private cryptoService: CryptoService,
              private primesService: PrimesService,
              private snackBar: MatSnackBar) { }

  @Output()
  createdVotingEvent: EventEmitter<Number> = new EventEmitter<Number>();

  @ViewChild('trustees') trusteesStep: SpecifyVotersComponent;
  @ViewChild('voters') votersStep: SpecifyVotersComponent;

  trusteesCanSpecifyKeys = true;
  trusteesGetNotified = true;

  allVoters: UserPublicKey[] = [];

  votingOptionsNames = [
    '',
    ''
  ];

  userPublicKeys: UserPublicKey[] = new Array();
  selectedSecurityThreshold = 42;

  votingsDataStep = new FormGroup({
    votingTitle: new FormControl('', Validators.required),
    votingDescription: new FormControl('', Validators.required),
    votingOptions: new FormArray([], OptionsValidator),
  });

  ngOnInit() {
    const options: FormArray = this.getOptionsFormArray();
    for (let i = 0; i < this.votingOptionsNames.length; i++) {
        options.push(new FormControl(this.votingOptionsNames[i], Validators.required));
    }

    const votersDataObservable = this.keyService.getAllKeys();
    const rolesDataObservable = this.roleService.getAllVotersWithRoles();

    forkJoin(
        votersDataObservable,
        rolesDataObservable
    ).subscribe(data => this.onReceiveVotersData(data));
  }

  private onReceiveVotersData(responses: any[]) {
    this.allVoters = responses[0];
    this.trusteesStep.setPossibleVoters(this.allVoters);
    this.votersStep.setPossibleVoters(this.allVoters);
  }

  // ----- Functions for Phase 1 ------

  getVotingTitle(): string {
      return this.votingsDataStep.get('votingTitle').value;
  }

  getVotingDescription(): string {
    return this.votingsDataStep.get('votingDescription').value;
  }

  drop(event: CdkDragDrop<string[]>) {
    const optionsArray: FormArray = this.getOptionsFormArray();
    const controls: AbstractControl[] = optionsArray.controls;
    moveItemInArray(controls, event.previousIndex, event.currentIndex);
  }

  addOption() {
    const optionsArray: FormArray = this.getOptionsFormArray();
    optionsArray.push(new FormControl('', Validators.required));
  }

  getOptionsFormArray(): FormArray {
    return (this.votingsDataStep.get('votingOptions') as FormArray);
  }

  deleteOption(i: number) {
    (this.votingsDataStep.get('votingOptions') as FormArray).controls.splice(i, 1);
  }

  // ----- Functions for Phase 3 ------

  updateMaxSecurityThreshold() {
    this.selectedSecurityThreshold = this.trusteesStep.selectedSecurityThreshold;
  }

  /*calculateMaxThreshold(): number {
    let maxThreshold = 0;
    for (let i = 0; i < this.userPublicKeys.length; i++) {
      if (this.userPublicKeys[i].getCanParticipate() && this.userPublicKeys[i].getCanBePowerVoter()) {
        maxThreshold++;
      }
    }
    return maxThreshold;
  }*/

  onSubmit() {
    this.userPublicKeys = this.trusteesStep.voters;

    const allOptionNames = [];

    const optionsFromControl = this.getOptionsFormArray();
    const numberOfOptions = optionsFromControl.length;
    const options: VotingOption[] = new Array(numberOfOptions);
    for (let i = 0; i < numberOfOptions; i++) {
      const optionName = optionsFromControl.controls[i].value;

      if (allOptionNames.indexOf(optionName) !== -1) {
        this.showErrorSnackBar('Duplicate option "' + optionName + '".');
        return;
      }

      allOptionNames.push(optionName);

      const optionPrimeNumber = this.primesService.getNthPrime(i);
      options[i] = new VotingOption().setOptionName(optionName).setOptionPrimeNumber(optionPrimeNumber);
    }

    if (this.selectedSecurityThreshold === 0) {
      this.showErrorSnackBar('The security threshold cannot be zero.');
      return;
    }

    const eligibleVoters = new Array();
    let position = 0;
    for (let i = 0; i < this.userPublicKeys.length; i++) {
      if (this.userPublicKeys[i].getCanParticipate() || this.userPublicKeys[i].getCanBeTrustee()) {
        const name = this.userPublicKeys[i].getName();
        const id = this.userPublicKeys[i].getId();
        const canBeTrustee = this.userPublicKeys[i].getCanBeTrustee();
        const canBeVoter = this.userPublicKeys[i].getCanParticipate();
        const voterKey: EligibleVoter = new EligibleVoter()
            .setId(id)
            .setName(name)
            .setPosition(position)
            .setIsTrustee(canBeTrustee)
            .setCanVote(canBeVoter);
        position++;
        eligibleVoters.push(voterKey);
      }
    }

    this.primesService.getSafePrimePair().subscribe(maybeSafePrimes =>
        this.sendCreateVoting(maybeSafePrimes, eligibleVoters, options));
  }

  private sendCreateVoting(safePrimes: Maybe<SafePrimePair>, eligibleVoters: EligibleVoter[], options: VotingOption[]) {
    const primeP = safePrimes.getData().getP();
    const primeQ = safePrimes.getData().getQ();
    const generatorG = '65536';

    const calculationParameters = new  VotingCalculationParameters()
        .setPrimeP(primeP)
        .setPrimeQ(primeQ)
        .setGeneratorG(generatorG);

    const newVoting: NewCreatedVoting = new NewCreatedVoting()
        .setTitle(this.getVotingTitle())
        .setDescription(this.getVotingDescription())
        .setOptions(options)
        .setNumberOfEligibleVoters(eligibleVoters.length)
        .setEligibleVoters(eligibleVoters)
        .setSecurityThreshold(this.selectedSecurityThreshold)
        .setVotingCalculationParameters(calculationParameters)
        .setPhaseNumber(-1);

    this.votingsDataService.addVoting(newVoting).subscribe(ret => this.onVotingStored(ret));
  }

  onVotingStored(savedVoting: Maybe<VotingsArray>) {
    const storedVotingId = savedVoting.getData().getVotings()[0].getId().valueOf();

    let sendMail = this.trusteesGetNotified;
    if (!this.trusteesCanSpecifyKeys) {
      sendMail = false;
    }

    this.votingsDataService.setPhaseNumber(storedVotingId, 0, sendMail).subscribe(() => this.onPhaseNumberZeroSet(storedVotingId));
  }

  onPhaseNumberZeroSet(votingId: number) {
    if (!this.trusteesCanSpecifyKeys) {
      this.votingsDataService.setPhaseNumber(votingId, 1, this.trusteesGetNotified).subscribe(() => this.showSuccess(votingId));
    }
    else {
      this.showSuccess(votingId);
    }
  }

  showSuccess(votingId: number) {
    this.showSuccessfulSnackBar('The new voting was successfully created.');
    this.createdVotingEvent.emit(votingId);
  }

  goBack(stepper: MatStepper) {
    stepper.previous();
  }

  goForward(stepper: MatStepper, stage: Number) {
    if (stage === 1) {
      const securityThreshold = this.trusteesStep.selectedSecurityThreshold;
      if (this.allVoters.filter(v => v.getCanBeTrustee()).length === 0) {
        alert('You did not select any trustees.');
      }
      else if (securityThreshold === 0) {
        alert('Security threshold cannot be 0.');
      }
      else {
        stepper.selected.completed = true;
        stepper.next();
      }
    }
    else if (stage === 2) {
      if (this.allVoters.filter(v => v.getCanParticipate()).length === 0) {
        alert('You did not select any voter.');
      }
      else {
        stepper.selected.completed = true;
        stepper.next();
      }
    }
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }
}
