import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LocalStorageService} from '../../services/localStorage/localStorage.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/communication/authentication/authentication.service';
import {MatSnackBar} from '@angular/material';
import {Maybe} from '../../utils/maybe.utils';
import {AuthenticatedVoter} from '../../models/authenticatedVoter/authenticatedVoter.json.model';
import {AuthenticatedVoterService} from '../../services/communication/authenticatedVoter/authenticatedVoter.service';
import {CustomObserver} from '../../services/subscription/customObserver';
import {SubscriberService} from '../../services/subscription/subscriber.service';
import {EnvironmentService} from '../../services/environment/environment.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, CustomObserver {

  isCurrentUserLoggedIn: boolean;
  isCurrentUserAdmin: boolean;
  isCurrentUserVotingAdministrator: boolean;
  isCurrentUserRegistrar: boolean;
  isCurrentUserTrustee: boolean;

  isProdEnvironment: boolean;
  isCurrentVoterRegistered: boolean;

  currentVoterId: string;
  currentVoterName: string;

  infoFormVisible = false;

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(private currentUserService: CurrentUserService,
              private localStorageService: LocalStorageService,
              private authenticationService: AuthenticationService,
              private authenticatedVoterService: AuthenticatedVoterService,
              private storageService: LocalStorageService,
              private subscriberService: SubscriberService,
              private snackBar: MatSnackBar,
              private environmentService: EnvironmentService,
              private router: Router) {
  }

  ngOnInit() {
    this.subscriberService.subscribeOnTokenChange(this);
    this.updateCurrentVoter();
    this.authenticatedVoterService.getAuthenticatedVoter().subscribe(data => this.onAuthenticatedVoterGet(data));
  }

  updateCurrentVoter() {
    this.isCurrentUserLoggedIn = this.currentUserService.isCurrentUserLoggedIn();
    this.isCurrentUserAdmin = this.currentUserService.isCurrentUserAdmin();
    this.isCurrentUserVotingAdministrator = this.currentUserService.isCurrentUserVotingAdministrator();
    this.isCurrentUserRegistrar = this.currentUserService.isCurrentUserRegistrar();
    this.currentVoterName = this.currentUserService.getVoterName().getData();
    this.currentVoterId = this.currentUserService.getVoterName().getData();
    this.isProdEnvironment = this.environmentService.isProd();
    this.isCurrentVoterRegistered = this.currentUserService.isCurrentUserRegistered();
    this.isCurrentUserTrustee = this.currentUserService.isCurrentUserTrusteeAtTheMoment();
  }

  onAuthenticatedVoterGet(authenticatedVoter: Maybe<AuthenticatedVoter>) {
    if (!authenticatedVoter.isEmpty() && authenticatedVoter.getData().getIsRegistered()) {
      this.isCurrentVoterRegistered = true;
      return;
    }
    this.isCurrentVoterRegistered = false;
  }

  onLogoutClicked() {
    this.localStorageService.delete('token');
    this.updateCurrentVoter();
    this.authenticatedVoterService.getAuthenticatedVoter().subscribe(data => this.onAuthenticatedVoterGet(data));
    this.router.navigate(['/login']);
  }

  get username(): string {
    return this.loginForm.get('username').value;
  }

  get password(): string {
    return this.loginForm.get('password').value;
  }

  get valid(): boolean {
    return this.username !== '' && this.password !== '';
  }

  notify() {
    this.updateCurrentVoter();
  }

  onLoginClicked() {
    this.infoFormVisible = !this.infoFormVisible;
  }


}
