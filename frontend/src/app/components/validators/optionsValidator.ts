import {AbstractControl, FormArray} from '@angular/forms';

export function OptionsValidator(control: FormArray) {
    const values = control.controls.map(v => v.value);
    const setOfValues = new Set(values);

    for (const controlField of control.controls) {
        if (controlField.value !== 0) {
            controlField.setErrors(null);
        }
    }

    const hasDuplicates = (values.length !== setOfValues.size);
    if (!hasDuplicates) {
        return null;
    }

    for (const controlField of control.controls) {
        const value = controlField.value;
        const numberOfOccurrences = values.filter(n => n === value).length;
        if (numberOfOccurrences !== 1) {
            controlField.setErrors({duplicateOption: true});
        }
    }

    return { validOptions: true };
}

