import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSort, MatTableDataSource} from '@angular/material';
import {UserPublicKey} from '../../../models/userPublicKey/userPublicKey.model';

@Component({
  selector: 'app-specify-voters',
  templateUrl: './specifyVoters.component.html',
  styleUrls: ['./specifyVoters.component.css']
})
export class SpecifyVotersComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('editor') editor;

  @Input() mode: string;

  content = '';

  codeMirror = null;

  voters: UserPublicKey[] = [];
  private backUp: UserPublicKey[];

  allVotersToggleChecked = false;

  selectedSecurityThreshold = 0;

  numberOfEligibleVoters = 0;
  numberOfTrustees = 0;

  dataSource: MatTableDataSource<UserPublicKey> = new MatTableDataSource([]);

  displayedColumns: string[] = ['Voter Name', 'Tickbox'];

  voterMap: Map<String, UserPublicKey>;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data: UserPublicKey, filter: string) => data.getName().toLowerCase().indexOf(filter) !== -1;

    this.codeMirror = this.editor.codeMirror;
    this.codeMirror.on('change', (obj, data) => {
      this.onEditorChange(obj, data);
    });
     /*this.dataSource.data[0].setCodeMirror(this.codeMirror).setParser(this.voterParserService);*/
  }



  setPossibleVoters(voters: UserPublicKey[]) {
    this.voters = voters;
    this.dataSource.data = this.voters;

    this.voterMap = new Map<String, UserPublicKey>();
    for (const voter of voters) {
      this.voterMap.set(voter.getName(), voter);
    }
    this.backUp = this.voters.slice();

    this.updateVoterNumber();
    this.updatePosition();
  }

  onTickBoxChange(event, index: number) {
    if (this.mode === 'trustees') {
      this.dataSource.data[index].setCanBeTrustee(event.checked);
    }
    else {
      this.dataSource.data[index].setCanParticipate(event.checked);
    }

    this.updateEditorText();
    this.updateVoterNumber();
  }

  updateVoterNumber() {
    this.numberOfEligibleVoters = this.voters.filter(u1 => u1.getCanParticipate()).length;
    this.numberOfTrustees = this.voters.filter(u1 => u1.getCanBeTrustee()).length;
  }

  onToggleAll(event) {
    if (this.mode === 'trustees') {
      this.voters.forEach(v => v.setCanBeTrustee(event.checked));
    }
    else {
      this.voters.forEach(v => v.setCanParticipate(event.checked));
    }

    this.updateEditorText();
    this.updateVoterNumber();
  }

  onFilter(event) {
    this.dataSource.filter = event.target.value.trim().toLowerCase();
    this.updatePosition();
  }

  updatePosition() {
    for (let i = 0; i < this.voters.length; i++) {
      this.voters[i].setTablePosition(i);
    }
  }

  sortData() {
    switch (this.sort.active) {
      case 'Voter Name': {
        if (this.sort.direction === 'asc') {
          this.voters.sort((v1, v2) => v1.getName().localeCompare(v2.getName()));
        } else if (this.sort.direction === 'desc') {
          this.voters.sort((v1, v2) => v2.getName().localeCompare(v1.getName()));
        } else {
          this.voters = this.backUp.slice();
          this.dataSource.data = this.voters;
        }
        break;
      }
      case 'Tickbox': {
        if (this.sort.direction === 'asc') {
          if (this.mode === 'trustees') {
            this.voters.sort((v1, v2) => String(v2.getCanBeTrustee()).localeCompare(String(v1.getCanBeTrustee())));
          }
          else {
            this.voters.sort((v1, v2) => String(v2.getCanParticipate()).localeCompare(String(v1.getCanParticipate())));
          }
        } else if (this.sort.direction === 'desc') {
          if (this.mode === 'trustees') {
            this.voters.sort((v1, v2) => String(v1.getCanBeTrustee()).localeCompare(String(v2.getCanBeTrustee())));
          }
          else {
            this.voters.sort((v1, v2) => String(v1.getCanParticipate()).localeCompare(String(v2.getCanParticipate())));
          }
        } else {
          this.voters = this.backUp.slice();
          this.dataSource.data = this.voters;
        }
        break;
      }
    }
    this.updatePosition();
  }

  onEditorChange(obj, data) {
    const numberOfVoters = this.voters.length;
    const isInEditor: boolean[] = new Array();
    for (let i = 0; i < numberOfVoters; i++) {
      isInEditor[i] = false;
    }

    const numberOfLines = obj.lineCount();
    for (let i = 0; i < numberOfLines; i++) {
      const voterName = obj.getLine(i);
      const voter = this.voterMap.get(voterName);
      if (voter !== undefined) {
        const pos = voter.getTablePosition();
        isInEditor[pos] = true;
        this.codeMirror.setGutterMarker(i, 'breakpoints', this.makeGreenMarker());
      }
      else {
        this.codeMirror.setGutterMarker(i, 'breakpoints', this.makeRedMarker());
      }
    }

    for (let i = 0; i < numberOfVoters; i++) {
      const voter = this.voters[i];
      if (this.mode === 'trustees') {
        voter.setCanBeTrustee(isInEditor[i]);
      }
      else {
        voter.setCanParticipate(isInEditor[i]);
      }
    }
    this.updateVoterNumber();
  }

  onThresholdInputChange(event) {
    this.selectedSecurityThreshold = event.value;
  }

  private updateEditorText() {
    let newText = '';
    for (const voter of this.voters) {
      if (this.mode === 'trustees' && voter.getCanBeTrustee()) {
        newText += voter.getName() + '\n';
      }
      else if (this.mode === 'voters' && voter.getCanParticipate()) {
        newText += voter.getName() + '\n';
      }
    }

    if (newText.endsWith('\n')) {
      newText = newText.substr(0, newText.length - 1);
    }

    this.codeMirror.setValue(newText);
  }

  private makeGreenMarker() {
    const marker = document.createElement('div');
    marker.style.color = 'green';
    marker.innerHTML = '●';
    return marker;
  }

  private makeRedMarker() {
    const marker = document.createElement('div');
    marker.style.color = '#822';
    marker.innerHTML = '●';
    return marker;
  }

}

