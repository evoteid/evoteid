import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';

@Component({
  selector: 'app-votings',
  templateUrl: './votings.component.html',
  styleUrls: ['./votings.component.css']
})
export class VotingsComponent implements OnInit {

  constructor(private currentUserService: CurrentUserService) { }

  isVotingAdministrator: boolean;

  ngOnInit() {
      this.isVotingAdministrator = this.currentUserService.isCurrentUserVotingAdministrator();
  }

}
