import {Component, Input, OnInit, Optional} from '@angular/core';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {Maybe} from '../../utils/maybe.utils';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';
import {MatDialog, MatDialogConfig, MatSnackBar, MatTableDataSource} from '@angular/material';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';
import {VotingVoterProgress} from '../../models/votings/votingVoterProgress';
import {VotingInformationComponent} from '../dialogs/votingInformation/votingInformation.component';
import {KeysComponent} from '../keys/keys.component';
import {KeysService} from '../../services/communication/keys/keys.service';
import {UploadPrivateKeyComponent} from '../dialogs/uploadPrivateKey/uploadPrivateKey.component';
import {VotingOfficials} from '../../models/votings/votingOfficials.json.model';

@Component({
  selector: 'app-pki',
  templateUrl: './pki.component.html',
  styleUrls: ['./pki.component.css']
})
export class PkiComponent implements OnInit {

  showSpinner = false;
  spinnerDescription = '';

  displayedVotings: Array<VotingOverview> = [];
  statusMap: { [id: number]: VotingVoterProgress; } = {};
  votingOfficialsMap: { [id: number]: VotingOfficials; } = {};
  textMap: { [id: number]: String; } = {};

  displayedColumns: string[] = ['Title', 'Description', 'Generate'];
  dataSource: MatTableDataSource<VotingOverview>;

  constructor(private votingsDataService: VotingsDataService,
              private currentUserService: CurrentUserService,
              private keysService: KeysService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.votingsDataService.getCurrentVotings(0).subscribe(data => this.onVotingsDataReceived(data));
  }

  onVotingsDataReceived(votings: Maybe<VotingsWithProgressArray>) {
    this.displayedVotings = votings.getData().getVotings();
    this.dataSource.data = votings.getData().getVotings();

    for (const singleVoting of this.displayedVotings) {
      let text: String = singleVoting.getDescription();
      text += '\n\nPossible choices:\n\n';

      const optionTextArray = singleVoting.getOptions().map(v => v.getOptionName());
      const optionText = optionTextArray.join(' | ');
      text += optionText;

      this.textMap[singleVoting.getId().valueOf()] = text;
    }

    this.evaluateVotingsProgressData(votings.getData().getVotingsVoterProgress());
    this.evaluateVotingOfficialsData(votings.getData().getVotingOfficials());
  }

  evaluateVotingsProgressData(progress: VotingVoterProgress[]) {
    for (const singleProgress of progress) {
      this.statusMap[singleProgress.getVotingId().valueOf()] = singleProgress;
    }
  }

  evaluateVotingOfficialsData(votingOfficials: VotingOfficials[]) {
    for (const votingOffical of votingOfficials) {
      this.votingOfficialsMap[votingOffical.getVotingId().valueOf()] = votingOffical;
    }
  }

  onAddCustomKeyClicked(votingId: number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '600px';
    dialogConfig.maxHeight = '80vh';
    dialogConfig.data = {
      votingId: votingId
    };

    const dialogRef = this.dialog.open(KeysComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => this.onSpecifyKeyDialogClosed(result));
  }

  private onSpecifyKeyDialogClosed(result: boolean) {
    if (result) {
      this.showSuccessfulSnackBar('The public key was added');
      this.votingsDataService.getCurrentVotings(0).subscribe(data => this.onVotingsDataReceived(data));
    }
  }

  onViewKeyClicked(votingId: number) {
    const voterId = this.currentUserService.getUserId().getData();
    this.keysService.getPublicKeyForVoting(voterId, votingId).subscribe(key => this.onPublicKeyGet(key, votingId));
  }

  onPublicKeyGet(maybePublicKey: Maybe<string>, votingId: number) {
    if (maybePublicKey.isEmpty()) {
      this.showErrorSnackBar('The key could not be fetched');
      return;
    }

    const publicKey = maybePublicKey.getData();
    const dialogConfig = new MatDialogConfig();

    dialogConfig.height = '590px';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      publicKey: publicKey,
      fromPKIComponent: true,
    };

    this.dialog.open(UploadPrivateKeyComponent, dialogConfig)
        .afterClosed().subscribe(res => this.onDialogClosed(res, votingId));
  }

  onDialogClosed(result: string, votingId: number) {
    if (result === 'generate new key') {
      this.onAddCustomKeyClicked(votingId);
    }
  }

  showMoreInformation(voting: VotingOverview) {
    const dialogConfig = new MatDialogConfig();

    const votingOfficials: VotingOfficials = this.votingOfficialsMap[voting.getId().valueOf()];

    dialogConfig.width = '90vw';
    dialogConfig.maxHeight = '80vh';
    dialogConfig.data = {
      voting: voting,
      votingOfficials: votingOfficials
    };

    this.dialog.open(VotingInformationComponent, dialogConfig);
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
