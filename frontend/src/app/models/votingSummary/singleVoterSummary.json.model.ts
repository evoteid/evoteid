import { JsonObject, JsonProperty } from 'json2typescript';
import {SingleVote} from '../vote/singleVote.json.model';
import {SingleVoteNIZKProof} from '../voteNIZKProof/singleVoteNIZKProof';

@JsonObject('SingleVoteSummary')
export class SingleVoteSummary {

    @JsonProperty('data', SingleVote)
    private vote: SingleVote = null;

    @JsonProperty('proofs', [SingleVoteNIZKProof])
    private proofs: SingleVoteNIZKProof[]  = null;

    getVote(): SingleVote {
        return this.vote;
    }

    getProofs(): SingleVoteNIZKProof[] {
        return this.proofs;
    }

}
