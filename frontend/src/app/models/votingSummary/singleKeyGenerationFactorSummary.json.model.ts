import { JsonObject, JsonProperty } from 'json2typescript';
import {SingleCommitment} from '../commitments/singleCommitment.model';
import {SingleSecretShareForVoter} from '../secretShares/singleSecretShareForVoter';

@JsonObject('SingleKeyGenerationFactorSummary')
export class SingleKeyGenerationFactorSummary {

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number  = null;

    @JsonProperty('commitments', [SingleCommitment])
    private commitments: SingleCommitment[] = new Array();

    @JsonProperty('secretShares', [SingleSecretShareForVoter])
    private secretShares: SingleSecretShareForVoter[]  = null;

    getFromVoterId(): number {
        return this.fromVoterId;
    }

    getCommitments(): SingleCommitment[] {
        return this.commitments;
    }

    getSecretShares(): SingleSecretShareForVoter[] {
        return this.secretShares;
    }

}
