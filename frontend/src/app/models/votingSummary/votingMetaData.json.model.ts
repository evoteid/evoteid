import { JsonObject, JsonProperty } from 'json2typescript';
import {VotingCalculationParameters} from '../votings/votingsCalculationParameters.json.model';
import {VotingOption} from '../votings/votingOption.json.model';
import {VotingInfo} from './votingInfo.json.model';

@JsonObject('VotingMetaData')
export class VotingMetaData {

    @JsonProperty('calculationParameters', VotingCalculationParameters)
    private votingCalculationParameters: VotingCalculationParameters = null;

    @JsonProperty('options', [VotingOption])
    private options: VotingOption[]  = null;

    @JsonProperty('info', VotingInfo)
    private info: VotingInfo  = null;

    getVotingCalculationParameters(): VotingCalculationParameters {
        return this.votingCalculationParameters;
    }

    getOptions(): VotingOption[] {
        return this.options;
    }

    getInfo(): VotingInfo {
        return this.info;
    }

}
