import { JsonObject, JsonProperty } from 'json2typescript';
import {EligibleVoterWithKey} from '../dataForKeyCalculation/eligibleVoterWithKey.model';
import {SingleDecryptionFactorSummary} from './singleDecryptionFactorSummary.json.model';
import {VotingMetaData} from './votingMetaData.json.model';
import {SingleKeyGenerationFactorSummary} from './singleKeyGenerationFactorSummary.json.model';
import {SingleVoteSummary} from './singleVoterSummary.json.model';

@JsonObject('VotingSummary')
export class VotingSummary {

    @JsonProperty('eligibleVoters', [EligibleVoterWithKey])
    private eligibleVoters: EligibleVoterWithKey[] = new Array();

    @JsonProperty('decryptionPhase', [SingleDecryptionFactorSummary])
    private decryptionFactors: SingleDecryptionFactorSummary[] = new Array();

    @JsonProperty('meta', VotingMetaData)
    private meta: VotingMetaData = undefined;

    @JsonProperty('keyGenerationPhase', [SingleKeyGenerationFactorSummary])
    private keyGenerationPhase: SingleKeyGenerationFactorSummary[] = new Array();

    @JsonProperty('votingPhase', [SingleVoteSummary])
    private votePhase: SingleVoteSummary[] = new Array();

    getEligibleVoters(): EligibleVoterWithKey[] {
        return this.eligibleVoters;
    }

    getDecryptionFactors(): SingleDecryptionFactorSummary[] {
        return this.decryptionFactors;
    }

    getMetaData(): VotingMetaData {
        return this.meta;
    }

    getKeyGenerationPhase(): SingleKeyGenerationFactorSummary[] {
        return this.keyGenerationPhase;
    }

    getVotePhase(): SingleVoteSummary[] {
        return this.votePhase;
    }

}
