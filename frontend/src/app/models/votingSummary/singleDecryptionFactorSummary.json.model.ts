import { JsonObject, JsonProperty } from 'json2typescript';
import { Voting } from '../votings/voting.json.model';
import {VotingOverview} from '../votings/votingOverview.json.model';
import {EligibleVoter} from '../votings/eligibleVoter.json.model';
import {EligibleVoterWithKey} from '../dataForKeyCalculation/eligibleVoterWithKey.model';
import {SingleDecryptionFactor} from '../decryptionFactors/singleDecryptionFactor.model';
import {DecryptionNIZKProof} from '../decryptionNIZKProof/decryptionNIZKProof';

@JsonObject('SingleDecryptionFactorSummary')
export class SingleDecryptionFactorSummary {

    @JsonProperty('data', SingleDecryptionFactor)
    private decryptionFactor: SingleDecryptionFactor = null;

    @JsonProperty('proof', DecryptionNIZKProof)
    private proof: DecryptionNIZKProof  = null;

    getDecryptionFactor(): SingleDecryptionFactor {
        return this.decryptionFactor;
    }

    getProof(): DecryptionNIZKProof {
        return this.proof;
    }

}
