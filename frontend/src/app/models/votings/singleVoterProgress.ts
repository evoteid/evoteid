import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('SingleVoterProgress')
export class SingleVoterProgress {

    @JsonProperty('position', Number)
    private position: number = undefined;

    @JsonProperty('voterName', String)
    private voterName: string = undefined;

    @JsonProperty('participatedInKeyGeneration', Boolean)
    private participatedInKeyGeneration: boolean = undefined;

    @JsonProperty('participatedInVoting', Boolean)
    private participatedInVoting: boolean = undefined;

    @JsonProperty('participatedInDecryption', Boolean)
    private participatedInDecryption: boolean = undefined;

    @JsonProperty('isTrustee', Boolean)
    private isTrustee: boolean = undefined;

    @JsonProperty('canVote', Boolean)
    private canVote: boolean = undefined;

    @JsonProperty('usesCustomKey', Boolean)
    private usesCustomKey: boolean = undefined;

    getPosition(): number {
        return this.position;
    }

    getVoterName(): string {
        return this.voterName;
    }

    getParticipatedInKeyGeneration(): boolean {
        return this.participatedInKeyGeneration;
    }

    getParticipatedInVoting(): boolean {
        return this.participatedInVoting;
    }

    getParticipatedInDecryption(): boolean {
        return this.participatedInDecryption;
    }

    getIsTrustee(): boolean {
        return this.isTrustee;
    }

    getCanVote(): boolean {
        return this.canVote;
    }

    getUsesCustomKey(): boolean {
        return this.usesCustomKey;
    }
}
