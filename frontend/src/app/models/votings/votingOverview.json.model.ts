import {Voting} from './voting.json.model';
import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('VotingOverview')
export class VotingOverview extends Voting<VotingOverview> {
}
