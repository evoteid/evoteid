import { VotingOption } from './votingOption.json.model';
import { JsonObject, JsonProperty } from 'json2typescript';
import {VotingInterface} from './votingInterface.model';

@JsonObject('Voting')
export class Voting<T extends VotingInterface> {

    @JsonProperty('id', Number)
    private id: number = undefined;

    @JsonProperty('title', String)
    private title: string = null;

    @JsonProperty('description', String)
    private description: string = null;

    @JsonProperty('options', [VotingOption])
    private options: VotingOption[] = null;

    @JsonProperty('phase', Number)
    private phase: number = null;

    @JsonProperty('timeVotingIsFinished', Number)
    private timeVotingIsFinished: number = null;

    @JsonProperty('transactionId', String)
    private transactionId: string = null;

    getId(): Number {
        return this.id;
    }

    setId(id: number): T {
        this.id = id;
        return <T><any>this;
    }

    getTitle(): String {
        return this.title;
    }

    setTitle(title: string): T {
        this.title = title;
        return <T><any>this;
    }

    getDescription(): String {
        return this.description;
    }

    setDescription(description: string): T {
        this.description = description;
        return <T><any>this;
    }

    setOptions(options: VotingOption[]): T {
        this.options = options;
        return <T><any>this;
    }

    getOptions(): VotingOption[] {
        return this.options;
    }

    getPhaseNumber(): number {
        return this.phase;
    }

    setPhaseNumber(phase: number): T {
        this.phase = phase;
        return <T><any>this;
    }

    getTimeVotingIsFinished(): number {
        return this.timeVotingIsFinished;
    }

    setTimeVotingIsFinished(value: number): T {
        this.timeVotingIsFinished = value;
        return <T><any>this;
    }

    getTransactionId(): string {
        return this.transactionId;
    }

    setTransactionId(transactionId: string): T {
        this.transactionId = transactionId;
        return <T><any>this;
    }
}
