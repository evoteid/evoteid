import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SingleCommitment')
export class SingleCommitment {

    @JsonProperty('commitment', String)
    private commitment: string = undefined;

    @JsonProperty('coefficientNumber', Number)
    private coefficientNumber: number = undefined;

    @JsonProperty('signature', String)
    private signature: string = undefined;

    public getCommitment(): string {
        return this.commitment;
    }

    public setCommitment(commitment: string): SingleCommitment {
        this.commitment = commitment;
        return this;
    }

    public getCoefficientNumber(): number {
        return this.coefficientNumber;
    }

    public setCoefficientNumber(coefficientNumber: number): SingleCommitment {
        this.coefficientNumber = coefficientNumber;
        return this;
    }

    public getSignature(): string {
        return this.signature;
    }

    public setSignature(signature: string): SingleCommitment {
        this.signature = signature;
        return this;
    }

}
