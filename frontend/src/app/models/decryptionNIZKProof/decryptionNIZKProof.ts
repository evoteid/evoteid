import {JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('DecryptionNIZKProof')
export class DecryptionNIZKProof {

    @JsonProperty('a', String)
    private a: string = undefined;

    @JsonProperty('b', String)
    private b: string = undefined;

    @JsonProperty('r', String)
    private r: string = undefined;

    @JsonProperty('signature', String)
    private signature: string = undefined;

    public getA(): string {
        return this.a;
    }

    public setA(a: string): DecryptionNIZKProof {
        this.a = a;
        return this;
    }

    public getB(): string {
        return this.b;
    }

    public setB(b: string): DecryptionNIZKProof {
        this.b = b;
        return this;
    }

    public getR(): string {
        return this.r;
    }

    public setR(r: string): DecryptionNIZKProof {
        this.r = r;
        return this;
    }

    public getSignature(): string {
        return this.signature;
    }

    public setSignature(signature: string): DecryptionNIZKProof {
        this.signature = signature;
        return this;
    }
}
