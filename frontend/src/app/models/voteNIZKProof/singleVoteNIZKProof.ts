import {JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SingleVoteNIZKProof')
export class SingleVoteNIZKProof {

    @JsonProperty('messageIndex', Number)
    private messageIndex: number;

    @JsonProperty('a', String)
    private a: string;

    @JsonProperty('b', String)
    private b: string;

    @JsonProperty('c', String)
    private c: string;

    @JsonProperty('r', String)
    private r: string;

    @JsonProperty('signature', String)
    private signature: string = null;

    constructor(messageIndex: number, a: string, b: string, c: string, r: string) {
        this.messageIndex = messageIndex;
        this.a = a;
        this.b = b;
        this.c = c;
        this.r = r;
    }

    public getMessageIndex(): number {
        return this.messageIndex;
    }

    public getA(): string {
        return this.a;
    }

    public getB(): string {
        return this.b;
    }

    public getC(): string {
        return this.c;
    }

    public getR(): string {
        return this.r;
    }

    public getSignature(): string {
        return this.signature;
    }

    public setSignature(signature: string): SingleVoteNIZKProof {
        this.signature = signature;
        return this;
    }
}
