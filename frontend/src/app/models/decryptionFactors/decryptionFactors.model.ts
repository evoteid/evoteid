import { JsonProperty, JsonObject } from 'json2typescript';
import {SingleDecryptionFactor} from './singleDecryptionFactor.model';

@JsonObject('DecryptionFactors')
export class DecryptionFactors {

    @JsonProperty('decryptionFactors', [SingleDecryptionFactor])
    private decryptionFactors: SingleDecryptionFactor[] = undefined;

    public setDecryptionFactors(decryptionFactors: SingleDecryptionFactor[]): DecryptionFactors {
        this.decryptionFactors = decryptionFactors;
        return this;
    }

    public getDecryptionFactors(): SingleDecryptionFactor[] {
        return this.decryptionFactors;
    }

}
