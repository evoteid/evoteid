import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SingleSecretShareForVoter')
export class SingleSecretShareForVoter {

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number = undefined;

    @JsonProperty('fromVoterName', String)
    private fromVoterName: string = undefined;

    @JsonProperty('secretShare', [String])
    private secretShare: string[] = undefined;

    @JsonProperty('signature', String)
    private signature: string = undefined;

    public getFromVoterId(): number {
        return this.fromVoterId;
    }

    public getFromVoterName(): string {
        return this.fromVoterName;
    }

    public getSecretShare(): string[] {
        return this.secretShare;
    }

    public getSignature(): string {
        return this.signature;
    }
}
