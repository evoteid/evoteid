export class User {
    private username: string;
    private token: string;

    constructor(username: string, token: string) {
        this.username = username;
        this.token = token;
    }

    public getUsername(): string {
        return this.username;
    }

    public getToken(): string {
        return this.token;
    }
  }
