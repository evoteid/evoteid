import { JsonObject, JsonProperty } from 'json2typescript';
import { UserPublicKey } from './userPublicKey.model';

@JsonObject('UserPublicKeyArray')
export class UserPublicKeyArray {

    @JsonProperty('voters', [UserPublicKey])
    voters: UserPublicKey[] = new Array();

    getUserPublicKeys(): UserPublicKey[] {
        return this.voters;
    }

}
