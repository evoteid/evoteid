import {Injectable} from '@angular/core';

@Injectable()
export abstract class PrimeFactorizationService {

    abstract factorize(number: string): number[];
}
