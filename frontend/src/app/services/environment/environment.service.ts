import {Injectable} from '@angular/core';

@Injectable()
export abstract class EnvironmentService {

    abstract isLocal(): boolean;

    abstract isDemo(): boolean;

    abstract isProd(): boolean;
}
