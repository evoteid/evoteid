import {EnvironmentService} from './environment.service';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable()
export class EnvironmentServiceImpl implements EnvironmentService {

    isLocal(): boolean {
        return environment.NAME === 'local';
    }

    isDemo(): boolean {
        return environment.NAME === 'demo';
    }

    isProd(): boolean {
        return environment.NAME === 'prod';
    }
}
