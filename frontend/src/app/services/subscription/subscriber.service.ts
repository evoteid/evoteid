import {Injectable} from '@angular/core';
import {CustomObserver} from './customObserver';

@Injectable({
    providedIn: 'root'
})
export abstract class SubscriberService {

    abstract subscribeOnTokenChange(observer: CustomObserver);

    abstract notifyTokenChanged();
}
