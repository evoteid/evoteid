import {Injectable} from '@angular/core';
import {CustomObserver} from './customObserver';
import {SubscriberService} from './subscriber.service';

@Injectable()
export class SubscriberServiceImpl implements SubscriberService {

    private onTokenChangeSubscribers: CustomObserver[] = [];

    subscribeOnTokenChange(observer: CustomObserver) {
        this.onTokenChangeSubscribers.push(observer);
    }

    notifyTokenChanged() {
        for (const observer of this.onTokenChangeSubscribers) {
            observer.notify();
        }
    }
}
