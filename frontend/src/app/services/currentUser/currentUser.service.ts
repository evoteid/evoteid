import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class CurrentUserService {

    abstract isCurrentUserLoggedIn(): boolean;

    abstract isCurrentUserRegistered(): boolean;

    abstract isCurrentUserAdmin(): boolean;

    abstract isCurrentUserRegistrar(): boolean;

    abstract isCurrentUserVotingAdministrator(): boolean;

    abstract isCurrentUserTrusteeAtTheMoment(): boolean;

    abstract getUserId(): Maybe<number>;

    abstract getVoterName(): Maybe<string>;

    abstract getPublicKey(): Maybe<string>;

    abstract getRoles(): Maybe<string[]>;

    abstract setIsCurrentUserRegistered(isCurrentUserRegistered: boolean);
}
