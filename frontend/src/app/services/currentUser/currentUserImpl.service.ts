import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import { LocalStorageService } from './../localStorage/localStorage.service';
import { CurrentUserService } from './currentUser.service';
import { JwtTokenDecoderService } from '../jwtTokenDecoder/jwtTokenDecoder.service';

@Injectable()
export class CurrentUserServiceImpl implements CurrentUserService {

    private publicKeyNameField = 'publicKey';
    private voterIdField = 'subId';
    private voterNameField = 'sub';
    private voterRoles = 'subRoles';
    private numberOfVotingsWithTrusteeRoleField = 'numberOfVotingsWithTrusteeRole';

    private administratorRole = 'Administrator';
    private registrarRole = 'Registrar';
    private votingAdministratorRole = 'Voting_Administrator';

    private currentUserRegistered = false;

    constructor(private storageService: LocalStorageService,
                private jwtTokenDecoderService: JwtTokenDecoderService) {
    }

    private tokenFieldName = 'token';

    isCurrentUserLoggedIn(): boolean {
        return !this.getJWTToken().isEmpty();
    }

    getUserId(): Maybe<number> {
        const idString = this.getTokenSingleField(this.voterIdField).getData();
        const id = parseInt(idString, 10);
        if (isNaN(id)) {
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(id);
    }

    getVoterName(): Maybe<string> {
        return this.getTokenSingleField(this.voterNameField);
    }

    getPublicKey(): Maybe<string> {
        return this.getTokenSingleField(this.publicKeyNameField);
    }

    getRoles(): Maybe<string[]> {
        return this.getTokenFieldArray(this.voterRoles);
    }

    isCurrentUserAdmin(): boolean {
        if (this.getRoles().isEmpty()) {
            return false;
        }
        return this.getRoles().getData().indexOf(this.administratorRole) !== -1;
    }

    isCurrentUserRegistrar(): boolean {
        if (this.getRoles().isEmpty()) {
            return false;
        }
        return this.getRoles().getData().indexOf(this.registrarRole) !== -1;
    }

    isCurrentUserVotingAdministrator(): boolean {
        if (this.getRoles().isEmpty()) {
            return false;
        }
        return this.getRoles().getData().indexOf(this.votingAdministratorRole) !== -1;
    }


    isCurrentUserRegistered(): boolean {
        return (
            !this.storageService.get('isVoterRegistered').isEmpty() &&
            this.storageService.get('isVoterRegistered').getData() === 'yes');
    }

    isCurrentUserTrusteeAtTheMoment(): boolean {
        const maybeNumberOfVotingsInWhichVoterIsTrustee = this.getTokenSingleField(this.numberOfVotingsWithTrusteeRoleField);
        if (maybeNumberOfVotingsInWhichVoterIsTrustee.isEmpty()) {
            return false;
        }
        return maybeNumberOfVotingsInWhichVoterIsTrustee.getData() !== '0';
    }

    setIsCurrentUserRegistered(currentUserRegistered: boolean): CurrentUserServiceImpl {
        if (currentUserRegistered) {
            this.storageService.upsert('isVoterRegistered', 'yes');
        }
        else {
            this.storageService.upsert('isVoterRegistered', 'no');
        }
        return this;
    }

    private getTokenSingleField(fieldName: string): Maybe<string> {
        const maybeToken = this.getJWTToken();
        if (maybeToken.isEmpty()) {
            return Maybe.fromEmpty();
        }
        const token = maybeToken.getData();

        return this.jwtTokenDecoderService.extractSingleField(fieldName, token);
    }

    private getTokenFieldArray(fieldName: string): Maybe<string[]> {
        const maybeToken = this.getJWTToken();
        if (maybeToken.isEmpty()) {
            return Maybe.fromEmpty();
        }
        const token = maybeToken.getData();

        return this.jwtTokenDecoderService.extractFieldArray(fieldName, token);
    }

    private getJWTToken(): Maybe<string> {
        return this.storageService.get(this.tokenFieldName);
    }

}
