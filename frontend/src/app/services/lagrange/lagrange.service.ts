import {Injectable} from '@angular/core';
import {LagrangePolynom} from '../../data/lagrangePolynom';

@Injectable()
export abstract class LagrangeService {

    abstract getValueAtPosition(position: string, polynom: LagrangePolynom, p: string, q: string): string;

}
