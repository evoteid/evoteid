import {Injectable} from '@angular/core';
import {LagrangeService} from './lagrange.service';
import {LagrangePolynom} from '../../data/lagrangePolynom';
import {MathService} from '../math/math.service';

@Injectable()
export class LagrangeServiceImpl implements LagrangeService {

    constructor(private mathService: MathService) {
    }

    getValueAtPosition(position: string, polynom: LagrangePolynom, p: string, q: string): string {

        let res = '1';

        for (let i = 0; i < polynom.getPoints().length; i++) {

            const currentPoint = polynom.getPoints()[i];

            let lambda = '1';
            const prodNumerator = '1';
            const prodDenomiator = '1';

            for (let j = 0; j < polynom.getPoints().length; j++) {
                if (i === j) {
                    continue;
                }

                const currentPos = polynom.getPoints()[j];

                let numerator = currentPos.getPosition();
                let denominator = this.mathService.subtract(currentPos.getPosition(), currentPoint.getPosition());

                numerator = this.mathService.mod(numerator, q);
                denominator = this.mathService.mod(denominator, q);

                const whole = this.mathService.multiplyWithModulo(numerator, this.mathService.modInverse(denominator, q), q);

                lambda = this.mathService.multiplyWithModulo(lambda, whole, q);
            }

            const dec = this.mathService.powerWithModulo(currentPoint.getValue(), lambda, p);

            res = this.mathService.multiplyWithModulo(res, dec, p);

        }

        return res;
    }

}
