import { Injectable } from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {VoterToCreate} from '../../data/voterToCreate';

@Injectable({
  providedIn: 'root'
})
export abstract class VoterParserService {

  abstract setWithRoles(withRoles: boolean);

  abstract parseLine(line: string): Maybe<VoterToCreate>;
}
