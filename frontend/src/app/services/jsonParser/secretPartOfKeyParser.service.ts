import {Maybe} from '../../utils/maybe.utils';
import {SecretPartOfKey} from '../../models/secretPartOfKey/secretPartOfKey.json.model';

export abstract class SecretPartOfKeyParser {
    abstract parseJson(jsonString: string): Maybe<SecretPartOfKey>;
}
