import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import {VotingSummary} from '../../models/votingSummary/votingSummary.json.model';

@Injectable()
export abstract class VotingSummaryJsonParser {

    abstract parseJson(jsonString: string): Maybe<VotingSummary>;
}
