import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {AllVoteNIZKProofs} from '../../models/voteNIZKProof/allVoteNIZKProofs';

@Injectable()
export abstract class VotesNIZKProofJsonParser {

    abstract parseJson(jsonString: string): Maybe<AllVoteNIZKProofs>;

}
