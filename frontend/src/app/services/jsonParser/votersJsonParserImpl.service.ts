import { Maybe } from './../../utils/maybe.utils';

import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import { Injectable } from '@angular/core';
import {VotersJsonParser} from './votersJsonParser.service';
import {VoterArray} from '../../models/voters/voterArray.model';

@Injectable()
export class VotersJsonParserImpl implements VotersJsonParser {

    parseJson(jsonString: string): Maybe<VoterArray> {
        let voters: VoterArray;
        try {
        const jsonObj: object = JSON.parse(jsonString);
        const jsonConvert: JsonConvert = new JsonConvert();
        jsonConvert.ignorePrimitiveChecks = false;
        jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            voters = jsonConvert.deserializeObject(jsonObj, VoterArray);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(voters);
    }
}
