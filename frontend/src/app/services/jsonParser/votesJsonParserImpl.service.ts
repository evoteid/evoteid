import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {Votes} from '../../models/vote/votes.json.model';
import {VotesJsonParser} from './votesJsonParser.service';
import { JsonConvert, ValueCheckingMode } from 'json2typescript';

@Injectable()
export class VotesJsonParserImpl implements VotesJsonParser {

    parseJson(jsonString: string): Maybe<Votes> {
        let votes: Votes;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votes = jsonConvert.deserializeObject(jsonObj, Votes);
        } catch (e) {
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votes);
    }
}
