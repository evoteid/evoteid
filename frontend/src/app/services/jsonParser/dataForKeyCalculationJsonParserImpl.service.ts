import { Injectable } from '@angular/core';
import { Maybe } from '../../utils/maybe.utils';
import { DataForKeyCalculation } from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import { DataForKeyCalculationJsonParser } from './dataForKeyCalculationJsonParser.service';
import { JsonConvert, ValueCheckingMode } from 'json2typescript';

@Injectable()
export class DataForKeyCalculationJsonParserImpl implements DataForKeyCalculationJsonParser {

    parseJson(jsonString: string): Maybe<DataForKeyCalculation> {
        let dataForKeyCalculation: DataForKeyCalculation;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            dataForKeyCalculation = jsonConvert.deserializeObject(jsonObj, DataForKeyCalculation);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(dataForKeyCalculation);
    }
}
