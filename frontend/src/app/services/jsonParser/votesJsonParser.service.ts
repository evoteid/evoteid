import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {Votes} from '../../models/vote/votes.json.model';

@Injectable()
export abstract class VotesJsonParser {

    abstract parseJson(jsonString: string): Maybe<Votes>;

}
