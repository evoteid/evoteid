import {Maybe} from '../../utils/maybe.utils';
import {DecryptionFactors} from '../../models/decryptionFactors/decryptionFactors.model';
import {DecryptionFactorsJsonParser} from './decryptionFactorsJsonParser.service';
import { JsonConvert, ValueCheckingMode } from 'json2typescript';

export class DecryptionFactorsJsonParserImpl implements DecryptionFactorsJsonParser {

    parseJson(jsonString: string): Maybe<DecryptionFactors> {
        let decryptionFactors: DecryptionFactors;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            decryptionFactors = jsonConvert.deserializeObject(jsonObj, DecryptionFactors);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(decryptionFactors);
    }
}
