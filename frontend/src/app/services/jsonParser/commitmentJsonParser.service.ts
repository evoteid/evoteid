import { Injectable } from '@angular/core';
import { Maybe } from '../../utils/maybe.utils';
import {Commitments} from '../../models/commitments/commitments.model';


@Injectable()
export abstract class CommitmentJsonParser {

    abstract parseJson(jsonString: string): Maybe<Commitments>;

}
