import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';

@Injectable()
export abstract class VotingsWithProgressJsonParser {

    abstract parseJson(jsonString: string): Maybe<VotingsWithProgressArray>;

}
