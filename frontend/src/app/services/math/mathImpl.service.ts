import { Injectable } from '@angular/core';
import { MathService } from './math.service';
import { Polynom } from 'src/app/data/polynom';

@Injectable({
  providedIn: 'root'
})
export class MathServiceImpl implements MathService {

  constructor() { }

  generateRandom(upperBound: string): string {
    const maxNumberOfBytes = this.toByteLengthRoundedUp(upperBound);
    const randomInteger = this.generateNBytesRandomNumber(maxNumberOfBytes);
    return this.mod(randomInteger, upperBound).toString();
  }

  generateNBytesRandomNumber(numberOfBytes: number): string {
      const BigInteger = require('jsbn').BigInteger;
      const forge = require('node-forge');
      const randomBytes = forge.random.getBytesSync(numberOfBytes);
      const randomHex = forge.util.bytesToHex(randomBytes);
      return new BigInteger(randomHex, 16).toString();
  }

  toBitLength(n: string): number {
      const BigInteger = require('jsbn').BigInteger;
      const number = new BigInteger(n);
      return number.bitLength();
  }

  toByteLengthRoundedUp(n: string): number {
      const bitLength = this.toBitLength(n);
      const remainder = bitLength % 8;
      const byteLengthWithOutRemainder = (bitLength - remainder) / 8;
      if (remainder !== 0) {
          return byteLengthWithOutRemainder + 1;
      }
      return byteLengthWithOutRemainder;
  }

  isPositive(n: string): boolean {
    const BigInteger = require('jsbn').BigInteger;
    const number = new BigInteger(n);
    return number.equals(number.abs());
  }

  generateRandomPolynomialWithNBytes(numberOfExponents: number, numberOfBytes: number): Polynom {
      const exponents: string[] = new Array();
      for (let i = 0; i < numberOfExponents; i++) {
          exponents.push(this.generateNBytesRandomNumber(numberOfBytes));
      }
      return new Polynom(exponents);
  }

  generateRandomPolynomial(numberOfExponents: number, upperBoundOfExponents: string): Polynom {
      const exponents: string[] = new Array();
      for (let i = 0; i < numberOfExponents; i++) {
            exponents.push(this.generateRandom(upperBoundOfExponents));
      }
      return new Polynom(exponents);
  }

  evaluatePolynomial(poly: Polynom, pos: string): string {
    const BigInteger = require('jsbn').BigInteger;
    const position = new BigInteger(pos);

    const coefficients = poly.getCoefficients();

    let res = new BigInteger('0');
    for (let i = coefficients.length - 1; i >= 0; i--) {
        const c = new BigInteger(coefficients[i]);
        res = res.multiply(position).add(c);
    }

    return res.toString();
  }

  powerWithModulo(base: string, exponent: string, mod: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const baseInt = new BigInteger(base);
      const exponentInt = new BigInteger(exponent);
      const moduloInt = new BigInteger(mod);

      const res = baseInt.modPow(exponentInt, moduloInt);
      return res.toString();
  }

  multiplyAllWithModulo(numbers: string[], mod: string): string {
      let res = '1';
      for (const number of numbers) {
          res = this.multiply(res, number);
      }
      return this.mod(res, mod);
  }

  multiplyWithModulo(factor1: string, factor2: string, modulo: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const factorOneInt = new BigInteger(factor1);
      const factorTwoInt = new BigInteger(factor2);
      const moduloInt = new BigInteger(modulo);

      const product = factorOneInt.multiply(factorTwoInt);
      const res = product.mod(moduloInt);
      return res.toString();
  }

  multiply(factor1: string, factor2: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const factorOneInt = new BigInteger(factor1);
      const factorTwoInt = new BigInteger(factor2);
      return factorOneInt.multiply(factorTwoInt).toString();
  }

  add(summand1: string, summand2: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const summandOneInt = new BigInteger(summand1);
      const summandTwoInt = new BigInteger(summand2);
      return summandOneInt.add(summandTwoInt).toString();
  }

  addWithModulo(summand1: string, summand2: string, modulo: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const summandOneInt = new BigInteger(summand1);
      const summandTwoInt = new BigInteger(summand2);
      const modInt = new BigInteger(modulo);
      return summandOneInt.add(summandTwoInt).mod(modInt).toString();
  }

  addAllWithModulo(numbers: string[], mod: string): string {
      let res = '0';
      for (const number of numbers) {
          res = this.add(res, number);
      }
      return this.mod(res, mod);
  }

  subtractWithModulo(subtly: string, subtle: string, modulo: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const subtlyInt = new BigInteger(subtly);
      const subtleInt = new BigInteger(subtle);
      const modInt = new BigInteger(modulo);
      return subtlyInt.subtract(subtleInt).mod(modInt).toString();
  }

  subtract(subtly: string, subtle: string): string {
        const BigInteger = require('jsbn').BigInteger;
        const subtlyInt = new BigInteger(subtly);
        const subtleInt = new BigInteger(subtle);
        return subtlyInt.subtract(subtleInt).toString();
  }

  divide(dividend: string, divisor: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const dividendInt = new BigInteger(dividend);
      const divisorInt = new BigInteger(divisor);
      return dividendInt.divide(divisorInt).toString();
  }

  modInverse(i: string, n: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const iInt = new BigInteger(i);
      const nInt = new BigInteger(n);
      return iInt.modInverse(nInt).toString();
  }

  mod(n: string, mod: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const nInt = new BigInteger(n);
      const modInt = new BigInteger(mod);
      return nInt.mod(modInt).toString();
  }

  abs(n: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const nInt = new BigInteger(n);
      return nInt.abs().toString();
  }

  hexToNumber(n: string): string {
      const BigInteger = require('jsbn').BigInteger;
      const nInt = new BigInteger(n, 16);
      return nInt.toString();
  }

}
