import {Injectable} from '@angular/core';
import {CommitmentsService} from './commitments.service';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {CommitmentFromVoter} from '../../../models/commitments/commitmentFromVoter.model';
import {Commitments} from '../../../models/commitments/commitments.model';
import {Maybe} from '../../../utils/maybe.utils';
import {CommitmentJsonParser} from '../../jsonParser/commitmentJsonParser.service';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export class CommitmentsServiceImpl implements CommitmentsService {

    private startUrl = '/api/votings/';

    constructor(private http: HttpClient,
                private parser: CommitmentJsonParser) {
    }

    public storeCommitments(votingId: Number, voterId: Number, commitments: CommitmentFromVoter): Observable<Pair<boolean, string>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Pair<boolean, string>>;

        obsResponse = this.http.post(
            this.startUrl + votingId + '/commitments/voters/' + voterId,
            JSON.stringify(commitments),
            {
                observe: 'response',
                responseType: 'text'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.checkIfSuccessfull(data)),
            catchError(err => this.handleError(err))
        );

        return obsReturn;
    }

    private checkIfSuccessfull(resp: HttpResponse<String>): Pair<boolean, string> {
        const successful = resp.status === 201;
        if (successful) {
            return new Pair<boolean, string>(true, 'Successful');
        }
        else {
            return new Pair<boolean, string>(false, 'Unknown failure');
        }
    }

    private handleError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }

    public getAllCommitments(votingId: Number): Observable<Maybe<Commitments>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Maybe<Commitments>>;

        obsResponse = this.http.get<String>(
            this.startUrl + votingId + '/commitments',
            {
                observe: 'response'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToCommitments(data)),
            catchError(err => this.handleGetCommitmentsError(err))
        );

        return obsReturn;
    }

    private mapToCommitments(resp: HttpResponse<String>): Maybe<Commitments> {
        if (resp && resp.status === 200 && resp.body) {
            return this.parser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetCommitmentsError(error: HttpErrorResponse): Observable<Maybe<Commitments>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

}
