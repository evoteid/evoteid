import {Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Maybe} from '../../../utils/maybe.utils';
import {VotingVoterProgress} from '../../../models/votings/votingVoterProgress';

@Injectable()
export abstract class VotingVoterProgressService {

    abstract getVotingVoterProgress(votingId: Number, voterId: Number): Observable<Maybe<VotingVoterProgress>> ;

}
