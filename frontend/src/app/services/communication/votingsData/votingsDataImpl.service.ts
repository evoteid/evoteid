import { Observable, of } from 'rxjs';
import { Maybe } from './../../../utils/maybe.utils';
import { VotingsArray } from './../../../models/votings/votingsArray.json.model';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VotingsDataService } from './votingsData.service';
import { map, catchError } from 'rxjs/operators';
import {NewCreatedVoting} from '../../../models/votings/newCreatedVoting.json.model';
import {VotingsJsonParser} from '../../jsonParser/votingsJsonParser.service';
import {VotingsWithProgressArray} from '../../../models/votings/votingsWithProgressArray.json.model';
import {VotingsWithProgressJsonParser} from '../../jsonParser/votingsWithProgressJsonParser.service';
import {VotingOverview} from '../../../models/votings/votingOverview.json.model';
import {Pair} from '../../../utils/pair.utils';

interface VotingsResponse {
    votings: string;
}

@Injectable()
export class VotingsDataServiceImpl implements VotingsDataService {

    private url = '/api/votings';
    private createdVotingsUrl = '/api/createdVotings';

    constructor(private http: HttpClient,
                private parser: VotingsJsonParser,
                private votingWithProgressParser: VotingsWithProgressJsonParser) {
    }

    private checkIfSuccessfull(resp: HttpResponse<any>): Boolean {
        return resp.status === 201;
    }

    private handleAddError(error: HttpErrorResponse): Observable<Boolean> {
        return of(false);
    }

    getCurrentVotings(phaseNumber: number): Observable<Maybe<VotingsWithProgressArray>> {
        let obsVotingResponse: Observable<HttpResponse<string>>;
        let obsVoting: Observable<Maybe<VotingsWithProgressArray>>;

        obsVotingResponse = this.http.get(this.url + '/phaseNumber/' + phaseNumber,
            {
                observe: 'response',
                responseType: 'text'
            });
        obsVoting = obsVotingResponse.pipe(
            map(data => this.mapToVotingsWithProgressArray(data)),
            catchError(err => this.handleGetWithProgressError(err))
        );
        return obsVoting;
    }

    private mapToVotingsWithProgressArray(resp: HttpResponse<string>): Maybe<VotingsWithProgressArray> {
        if (resp && resp.status === 200 && resp.body) {
            return this.votingWithProgressParser.parseJson(resp.body);
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetWithProgressError(error: HttpErrorResponse): Observable<Maybe<VotingsWithProgressArray>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    getCreatedVotings(): Observable<Maybe<VotingsArray>> {
        let obsVotingResponse: Observable<HttpResponse<VotingsResponse>>;
        let obsVoting: Observable<Maybe<VotingsArray>>;

        obsVotingResponse = this.http.get<VotingsResponse>(this.createdVotingsUrl, { observe: 'response'});
        obsVoting = obsVotingResponse.pipe(
            map(data => this.mapToVotingsArray(data)),
            catchError(err => this.handleGetError(err))
        );
        return obsVoting;
    }

    private mapToVotingsArray(resp: HttpResponse<VotingsResponse>): Maybe<VotingsArray> {
        if (resp && (resp.status === 200 || resp.status === 201) && resp.body.votings) {
            return this.parser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetError(error: HttpErrorResponse): Observable<Maybe<VotingsArray>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    private mapToSingleVoting(resp: HttpResponse<string>): Maybe<VotingOverview> {
        if (resp && (resp.status === 200 || resp.status === 201) && resp.body) {
            return this.parser.parseSingleVoting(resp.body);
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetSingleError(error: HttpErrorResponse): Observable<Maybe<VotingOverview>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    addVoting(newVoting: NewCreatedVoting): Observable<Maybe<VotingsArray>> {
        const jsonString = JSON.stringify(newVoting);
        const addVotingResponse = this.http.post<any>(this.url, jsonString, {observe: 'response'});

        const insertSuccessfull = addVotingResponse.pipe(
                map(data => this.mapToVotingsArray(data)),
                catchError(err => this.handleGetError(err))
        );
        return insertSuccessfull;
    }

    setPhaseNumber(votingId: number, phaseNumber: number, sendNotificationMail: boolean): Observable<Maybe<VotingOverview>> {
        let obsResponse: Observable<HttpResponse<string>>;
        let obsReturn: Observable<Maybe<VotingOverview>>;

        obsResponse = this.http.put(this.url + '/' + votingId + '/phaseNumber', { phaseNumber: phaseNumber, sendNotificationMail: sendNotificationMail  }, {
            observe: 'response',
            responseType: 'text'
        });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToSingleVoting(data)),
            catchError(err => this.handleGetSingleError(err))
        );

        return obsReturn;
    }

    setVotingEndTime(votingId: number, votingEndTime: number): Observable<Maybe<VotingOverview>> {
        let obsResponse: Observable<HttpResponse<string>>;
        let obsReturn: Observable<Maybe<VotingOverview>>;

        obsResponse = this.http.put(this.url + '/' + votingId + '/timeVotingFinishes', { votingEndTime: votingEndTime }, {
            observe: 'response',
            responseType: 'text'
        });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToSingleVoting(data)),
            catchError(err => this.handleGetSingleError(err))
        );

        return obsReturn;
    }

    deleteVoting(votingId: Number): Observable<Pair<boolean, string>> {
        let obsVoterResponse: Observable<HttpResponse<string>>;
        let returnMessage: Observable<Pair<boolean, string>>;

        obsVoterResponse = this.http.delete(this.url + '/' + votingId,
            {
                observe: 'response',
                responseType: 'text'
            });

        returnMessage = obsVoterResponse.pipe(
            map(data => this.mapToDeleteMessage(data)),
            catchError(err => this.catchDeleteVotingError(err))
        );

        return returnMessage;
    }

    private mapToDeleteMessage(resp: HttpResponse<string>): Pair<boolean, string> {
        if (resp && resp.status === 200 && resp.body) {
            return new Pair(true, resp.body);
        }
        return new Pair(false, resp.body);
    }

    private catchDeleteVotingError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }

    getVotingSummary(votingId: Number): Observable<Maybe<string>> {
        let obsVoterResponse: Observable<HttpResponse<string>>;
        let returnMessage: Observable<Maybe<string>>;

        obsVoterResponse = this.http.get(this.url + '/' + votingId + '/votingSummary',
            {
                observe: 'response',
                responseType: 'text'
            });

        returnMessage = obsVoterResponse.pipe(
            map(data => this.mapToVotingSummary(data)),
            catchError(err => this.catchVotingSummaryError(err))
        );

        return returnMessage;
    }

    private mapToVotingSummary(resp: HttpResponse<string>): Maybe<string> {
        if (resp && resp.status === 200 && resp.body) {
            return Maybe.fromData(resp.body);
        }
        return Maybe.fromEmpty();
    }

    private catchVotingSummaryError(error: HttpErrorResponse): Observable<Maybe<string>> {
        return of(Maybe.fromEmpty());
    }
}
