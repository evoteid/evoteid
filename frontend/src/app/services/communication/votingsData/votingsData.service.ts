import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Maybe } from './../../../utils/maybe.utils';
import { VotingsArray } from 'src/app/models/votings/votingsArray.json.model';
import { NewCreatedVoting } from '../../../models/votings/newCreatedVoting.json.model';
import {VotingsWithProgressArray} from '../../../models/votings/votingsWithProgressArray.json.model';
import {VotingOverview} from '../../../models/votings/votingOverview.json.model';
import {Pair} from '../../../utils/pair.utils';


@Injectable()
export abstract class VotingsDataService {

    abstract getCurrentVotings(phaseNumber: number): Observable<Maybe<VotingsWithProgressArray>>;

    abstract getCreatedVotings(): Observable<Maybe<VotingsArray>>;

    abstract addVoting(voting: NewCreatedVoting): Observable<Maybe<VotingsArray>>;

    abstract setPhaseNumber(votingId: number, phaseNumber: number, sendNotificationMail: boolean): Observable<Maybe<VotingOverview>>;

    abstract setVotingEndTime(votingId: number, votingEndTime: number): Observable<Maybe<VotingOverview>>;

    abstract deleteVoting(votingId: Number): Observable<Pair<boolean, string>>;

    abstract getVotingSummary(votingId: Number): Observable<Maybe<string>>;
}
