import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../localStorage/localStorage.service';
import { Maybe } from '../../../utils/maybe.utils';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

    constructor(private storageService: LocalStorageService) {
    }

    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {

        let maybeToken: Maybe<String>;
        maybeToken = this.storageService.get('token');

        if (maybeToken.isEmpty()) {
            return next.handle(req);
        }
        else {
            const authField = 'Bearer ' + maybeToken.getData();
            const copy = req.clone({
                headers: req.headers.set('Authorization',
                    authField)
            });
            return next.handle(copy);
        }
    }
}
