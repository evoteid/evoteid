import {Injectable} from '@angular/core';
import {RolesService} from './roles.service';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Roles} from '../../../models/roles/roles.model';
import {Maybe} from '../../../utils/maybe.utils';
import {catchError, map} from 'rxjs/operators';
import {RolesJsonParser} from '../../jsonParser/rolesJsonParser.service';
import {VotersWithRoles} from '../../../models/roles/votersWithRoles.model';

@Injectable()
export class RolesServiceImpl implements RolesService {

    constructor(private http: HttpClient,
                private parser: RolesJsonParser) {
    }

    getAllAvailableRoles(): Observable<Maybe<Roles>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Maybe<Roles>>;

        obsResponse = this.http.get<String>(
            '/api/roles',
            {
                observe: 'response'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToRoles(data)),
            catchError(err => this.handleError(err))
        );
        return obsReturn;
    }

    private mapToRoles(resp: HttpResponse<String>): Maybe<Roles> {
        if (resp && resp.status === 200 && resp.body) {
            return this.parser.parseAvailableRolesJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleError(error: HttpErrorResponse): Observable<Maybe<Roles>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    getAllVotersWithRoles(): Observable<Maybe<VotersWithRoles>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Maybe<VotersWithRoles>>;

        obsResponse = this.http.get<String>(
            '/api/voters/roles',
            {
                observe: 'response'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToVotersWithRoles(data)),
            catchError(err => this.handleVotersWithRolesError(err))
        );
        return obsReturn;
    }

    private mapToVotersWithRoles(resp: HttpResponse<String>): Maybe<VotersWithRoles> {
        if (resp && resp.status === 200 && resp.body) {
            return this.parser.parseVotersWithRoles(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleVotersWithRolesError(error: HttpErrorResponse): Observable<Maybe<VotersWithRoles>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    addRole(voterId: number, roleName: string): Observable<boolean> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<boolean>;

        obsResponse = this.http.put(
            '/api/voters/' + voterId + '/roles/' + roleName,
            {},
            {
                observe: 'response',
                responseType: 'text'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapAddRoleAnswer(data)),
            catchError(err => this.handleAddRoleError(err))
        );
        return obsReturn;
    }

    private mapAddRoleAnswer(resp: HttpResponse<String>): boolean {
        return resp.status.valueOf() === 200;
    }

    private handleAddRoleError(error: HttpErrorResponse): Observable<boolean> {
        console.log(error);
        return of(false);
    }

    removeRole(voterId: number, roleName: string): Observable<boolean> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<boolean>;

        obsResponse = this.http.delete(
            '/api/voters/' + voterId + '/roles/' + roleName,
            {
                observe: 'response',
                responseType: 'text'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapDeleteRoleAnswer(data)),
            catchError(err => this.handleDeleteRoleError(err))
        );
        return obsReturn;
    }

    private mapDeleteRoleAnswer(resp: HttpResponse<String>): boolean {
        return resp.status.valueOf() === 200;
    }

    private handleDeleteRoleError(error: HttpErrorResponse): Observable<boolean> {
        console.log(error);
        return of(false);
    }

}
