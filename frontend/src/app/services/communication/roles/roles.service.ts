import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {Roles} from '../../../models/roles/roles.model';
import {Maybe} from '../../../utils/maybe.utils';
import {VotersWithRoles} from '../../../models/roles/votersWithRoles.model';

@Injectable()
export abstract class RolesService {

    abstract getAllAvailableRoles(): Observable<Maybe<Roles>>;

    abstract getAllVotersWithRoles(): Observable<Maybe<VotersWithRoles>>;

    abstract addRole(voterId: number, roleName: string): Observable<boolean>;

    abstract removeRole(voterId: number, roleName: string): Observable<boolean>;

}
