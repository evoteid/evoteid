import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VoteService } from './vote.service';
import { Observable, of } from 'rxjs';
import {DataForKeyCalculation} from '../../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {DataForKeyCalculationJsonParser} from '../../jsonParser/dataForKeyCalculationJsonParser.service';
import {Maybe} from '../../../utils/maybe.utils';
import {SingleVote} from '../../../models/vote/singleVote.json.model';
import {Votes} from '../../../models/vote/votes.json.model';
import {VotesJsonParser} from '../../jsonParser/votesJsonParser.service';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export class VoteServiceImpl implements VoteService {

    private startUrl = '/api/votings/';
    private endUrl = '/votes';

    constructor(private http: HttpClient,
                private dataForKeyCalculationParser: DataForKeyCalculationJsonParser,
                private votesParser: VotesJsonParser) {
    }

    vote(voteId: Number, voterId: Number, vote: SingleVote): Observable<Pair<boolean, string>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Pair<boolean, string>>;

        obsResponse = this.http.post(this.startUrl + voteId + '/votes/voters/' + voterId, vote, {
            observe: 'response',
            responseType: 'text'
        });

        obsReturn = obsResponse.pipe(
            map(data => this.checkIfVoteWasSetSuccessfull(data)),
            catchError(err => this.handleVoteSetError(err))
        );

        return obsReturn;
    }

    private checkIfVoteWasSetSuccessfull(resp: HttpResponse<any>): Pair<boolean, string> {
        const successful = resp.status === 201;
        if (successful) {
            return new Pair<boolean, string>(true, 'Successful');
        }
        else {
            return new Pair<boolean, string>(false, 'Unknown failure');
        }
    }

    private handleVoteSetError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }

    getVotes(voteId: number): Observable<Maybe<Votes>> {
        let obsVotesResponse: Observable<HttpResponse<String>>;
        let obsVotes: Observable<Maybe<Votes>>;

        obsVotesResponse = this.http.get<String>(this.startUrl + voteId + '/votes', {
            observe: 'response'
        });

        obsVotes = obsVotesResponse.pipe(
            map(data => this.mapToVote(data)),
            catchError(err => this.handleErrorForVote(err))
        );

        return obsVotes;
    }

    private mapToVote(resp: HttpResponse<String>): Maybe<Votes> {
        if (resp && resp.status === 200 && resp.body) {
            return this.votesParser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleErrorForVote(error: HttpErrorResponse): Observable<Maybe<Votes>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    getDataForKeyCalculation(voteId: Number): Observable<Maybe<DataForKeyCalculation>> {
        let obsDataForKeyCalculationResponse: Observable<HttpResponse<String>>;
        let obsDataForKeyCalculation: Observable<Maybe<DataForKeyCalculation>>;

        obsDataForKeyCalculationResponse = this.http.get<String>(this.startUrl + voteId + '/keyCalculationData', {
            observe: 'response'
        });

        obsDataForKeyCalculation = obsDataForKeyCalculationResponse.pipe(
            map(data => this.mapToCalculationObject(data)),
            catchError(err => this.handleErrorForCalculationData(err))
        );

        return obsDataForKeyCalculation;
    }

    private mapToCalculationObject(resp: HttpResponse<String>): Maybe<DataForKeyCalculation> {
        if (resp && resp.status === 200 && resp.body) {
            return this.dataForKeyCalculationParser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleErrorForCalculationData(error: HttpErrorResponse): Observable<Maybe<DataForKeyCalculation>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    private handlePhaseNumberSetError(error: HttpErrorResponse): Observable<boolean> {
        console.log(error);
        return of(false);
    }
}
