import { Injectable } from '@angular/core';
import { Maybe } from '../../../utils/maybe.utils';
import { User } from './../../../models/user.model';
import { Observable } from 'rxjs';

@Injectable()
export abstract class AuthenticationService {

  abstract authenticate(username: string, password: string): Observable<Maybe<User>>;
}
