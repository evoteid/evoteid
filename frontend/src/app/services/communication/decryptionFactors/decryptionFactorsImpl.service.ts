import {Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Maybe} from '../../../utils/maybe.utils';
import {DecryptionFactors} from '../../../models/decryptionFactors/decryptionFactors.model';
import {SingleDecryptionFactor} from '../../../models/decryptionFactors/singleDecryptionFactor.model';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {DecryptionFactorsJsonParser} from '../../jsonParser/decryptionFactorsJsonParser.service';
import {catchError, map} from 'rxjs/operators';
import {DecryptionFactorsService} from './decryptionFactors.service';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export class DecryptionFactorsServiceImpl implements DecryptionFactorsService {

    private startUrl = '/api/votings/';

    constructor(private http: HttpClient,
                private parser: DecryptionFactorsJsonParser) {
    }

    storeDecryptionFactor(votingId: Number, voterId: Number, decryptionFactor: SingleDecryptionFactor): Observable<Pair<boolean, string>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Pair<boolean, string>>;

        obsResponse = this.http.post(
            this.startUrl + votingId + '/decryptionFactors/voters/' + voterId,
            JSON.stringify(decryptionFactor),
            {
                observe: 'response',
                responseType: 'text'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.checkIfSuccessfull(data)),
            catchError(err => this.handleError(err))
        );

        return obsReturn;
    }

    private checkIfSuccessfull(resp: HttpResponse<String>): Pair<boolean, string> {
        const successful = resp.status === 201;
        if (successful) {
            return new Pair<boolean, string>(true, 'Successful');
        }
        else {
            return new Pair<boolean, string>(false, 'Unknown failure');
        }
    }

    private handleError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }

    getAllDecryptionFactors(votingId: Number): Observable<Maybe<DecryptionFactors>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Maybe<DecryptionFactors>>;

        obsResponse = this.http.get<String>(
            this.startUrl + votingId + '/decryptionFactors',
            {
                observe: 'response'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToDecryptionFactors(data)),
            catchError(err => this.handleGetDecryptionFactorsError(err))
        );

        return obsReturn;
    }

    private mapToDecryptionFactors(resp: HttpResponse<String>): Maybe<DecryptionFactors> {
        if (resp && resp.status === 200 && resp.body) {
            return this.parser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetDecryptionFactorsError(error: HttpErrorResponse): Observable<Maybe<DecryptionFactors>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

}
