import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class LocalStorageService {

    abstract get(key: string): Maybe<string>;

    abstract upsert(key: string, value: string);

    abstract delete(key: string): boolean;
}
