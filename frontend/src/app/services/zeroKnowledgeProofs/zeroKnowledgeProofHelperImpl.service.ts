import {Injectable} from '@angular/core';
import {CryptoService} from '../crypto/crypto.service';
import {MathService} from '../math/math.service';
import {Votes} from '../../models/vote/votes.json.model';
import {VoteNIZKProof} from '../../models/voteNIZKProof/voteNIZKProofs';
import {SingleVoteNIZKProof} from '../../models/voteNIZKProof/singleVoteNIZKProof';
import {DecryptionNIZKProof} from '../../models/decryptionNIZKProof/decryptionNIZKProof';
import {ZeroKnowledgeProofHelperService} from './zeroKnowledgeProofHelper.service';
import {VotingCalculationParameters} from '../../models/votings/votingsCalculationParameters.json.model';
import {AllVoteNIZKProofs} from '../../models/voteNIZKProof/allVoteNIZKProofs';
import {VotingOption} from '../../models/votings/votingOption.json.model';
import {SingleVote} from '../../models/vote/singleVote.json.model';

@Injectable()
export class ZeroKnowledgeProofHelperServiceImpl implements ZeroKnowledgeProofHelperService {

    constructor(private cryptoService: CryptoService,
                private mathService: MathService) {
    }

    calculateDecryptionFactorZeroKnowledgeProof(params: VotingCalculationParameters, secretShare, votes: Votes): DecryptionNIZKProof {
        const primeP = params.getPrimeP();
        const primeQ = params.getPrimeQ();
        const generatorG = params.getGeneratorG();

        const alphas = new Array();

        for (const singleVote of votes.getVotes()) {
            const alpha = singleVote.getAlpha();
            alphas.push(alpha);
        }

        const multipliedAlphas = this.mathService.multiplyAllWithModulo(alphas, primeP);
        const w = this.mathService.generateRandom(primeQ);

        const a = this.mathService.powerWithModulo(generatorG, w, primeP);
        const b = this.mathService.powerWithModulo(multipliedAlphas, w, primeP);

        const stringToHash = a + ',' + b;
        const hash = this.cryptoService.hashSHA256(stringToHash);
        const challenge = this.mathService.hexToNumber(hash);

        const shareTimeChallenge = this.mathService.multiplyWithModulo(secretShare, challenge, primeQ);
        const response = this.mathService.addWithModulo(w, shareTimeChallenge, primeQ);
        return new DecryptionNIZKProof().setA(a).setB(b).setR(response);
    }

    calculateVoteZeroKnowledgeProof(params: VotingCalculationParameters, alpha: string, beta: string, r: string, y: string, chosenOptionNumber: number, allOptions: number[]): VoteNIZKProof {
        const generator = params.getGeneratorG();
        const primeP = params.getPrimeP();
        const primeQ = params.getPrimeQ();

        const proofsBeforeChosenOption: SingleVoteNIZKProof[] = [];
        const proofsAfterChosenOption: SingleVoteNIZKProof[] = [];

        let chosenOptionIndex = -1;

        for (let j = 0; j < allOptions.length; j++) {
            if (allOptions[j] !== chosenOptionNumber) {
                const c_j = this.mathService.generateRandom(primeQ);
                const r_j = this.mathService.generateRandom(primeQ);

                const alpha_j = this.calculateA(params, alpha, c_j, r_j);

                const fake_message = '' + allOptions[j];
                const beta_j = this.calculateB(params, beta, y, c_j, r_j, fake_message);

                const proof = new SingleVoteNIZKProof(j, alpha_j, beta_j, c_j, r_j);

                if (allOptions[j] < chosenOptionNumber) {
                    proofsBeforeChosenOption.push(proof);
                }
                else {
                    proofsAfterChosenOption.push(proof);
                }
            }
            else {
                chosenOptionIndex = j;
            }
        }

        const w = this.mathService.generateRandom(primeQ);
        const alpha_i = this.mathService.powerWithModulo(generator, w, primeP);
        const beta_i = this.mathService.powerWithModulo(y, w, primeP);

        const values = [];
        let cs = '0';

        for (const proof of proofsBeforeChosenOption) {
            values.push(proof.getA());
            values.push(proof.getB());
            cs = this.mathService.add(cs, proof.getC());
        }

        values.push(alpha_i);
        values.push(beta_i);

        for (const proof of proofsAfterChosenOption) {
            values.push(proof.getA());
            values.push(proof.getB());
            cs = this.mathService.add(cs, proof.getC());
        }

        const stringToHash = values.join(',');
        const hash = this.cryptoService.hashSHA256(stringToHash);
        const number = this.mathService.hexToNumber(hash);

        const c_i = this.mathService.subtractWithModulo(number, cs, primeQ);
        const rTimesC = this.mathService.multiply(r, c_i);
        const r_i = this.mathService.addWithModulo(w, rTimesC, primeQ);

        const realProof = new SingleVoteNIZKProof(chosenOptionIndex, alpha_i, beta_i, c_i, r_i);

        const allProofs = new VoteNIZKProof()
            .setZeroKnowledgeProofs(proofsBeforeChosenOption.concat(realProof).concat(proofsAfterChosenOption));

        return allProofs;
    }

    verifyAllProofs(params: VotingCalculationParameters, votes: Votes, proofs: AllVoteNIZKProofs, votingOptions: VotingOption[], keyToEncrypt: string): SingleVote[] {
        const proofMap: { [id: number]: VoteNIZKProof; } = {};
        for (const proof of proofs.getAllZeroKnowledgeProofs()) {
            proofMap[proof.getFromVoterId()] = proof;
        }

        const options = votingOptions.map(option => option.getOptionPrimeNumber().valueOf());
        const invalidVotes = [];

        for (const singleVote of votes.getVotes()) {
            const fromVoterId = singleVote.getFromVoterId();
            const proof: VoteNIZKProof = proofMap[fromVoterId];

            if (proof === undefined) {
                invalidVotes.push(singleVote);
                continue;
            }

            const valid = this.verifyVoteZeroKnowledgeProof(params, singleVote.getAlpha(), singleVote.getBeta(), keyToEncrypt, options, proof);
            if (!valid) {
                invalidVotes.push(singleVote);
            }
        }
        return invalidVotes;
    }

    verifyVoteZeroKnowledgeProof(params: VotingCalculationParameters, alpha: string, beta: string, keyToEncrypt: string, options: number[], proof: VoteNIZKProof): boolean {
        const primeQ = params.getPrimeQ();

        for (let i = 0; i < options.length; i++) {
            const j = proof.getZeroKnowledgeProofs()[i].getMessageIndex();

            const singleProof = proof.getZeroKnowledgeProofs()[j];
            const r_j = singleProof.getR();
            const c_j = singleProof.getC();

            const expectedAlpha_j = this.calculateA(params, alpha, c_j, r_j);
            const alphaValid = (expectedAlpha_j === singleProof.getA());

            const chosenOption = '' + options[j];
            const expectedBeta_j = this.calculateB(params, beta, keyToEncrypt, c_j, r_j, chosenOption);
            const betaValid = (expectedBeta_j === singleProof.getB());

            if (!alphaValid || !betaValid) {
                return false;
            }
        }

        const values = [];
        for (const singleProof of proof.getZeroKnowledgeProofs()) {
            values.push(singleProof.getA());
            values.push(singleProof.getB());
        }

        const actualChallenge = this.addChallenges(proof);
        const expectedChallenge = this.calculateChallenge(proof);

        const challengesValid = (this.mathService.mod(expectedChallenge, primeQ) === this.mathService.mod(actualChallenge, primeQ));
        return challengesValid;
    }

    private calculateA(params: VotingCalculationParameters, alpha: string, c_j: string, r_j: string): string {
        const generatorG = params.getGeneratorG();
        const primeP = params.getPrimeP();

        const nom_alpha = this.mathService.powerWithModulo(generatorG, r_j, primeP);
        const den_alpha = this.mathService.powerWithModulo(alpha, c_j, primeP);
        const den_alpha_inv = this.mathService.modInverse(den_alpha, primeP);

        return this.mathService.multiplyWithModulo(nom_alpha, den_alpha_inv, primeP);
    }

    private calculateB(params: VotingCalculationParameters, beta: string, keyToEncrypt: string, c_j: string, r_j: string, chosenOption: string): string {
        const primeP = params.getPrimeP();

        const nom_beta = this.mathService.powerWithModulo(keyToEncrypt, r_j, primeP);

        const chosenOption_inv = this.mathService.modInverse(chosenOption, primeP);
        const betaDividedByChosenOption = this.mathService.multiplyWithModulo(beta, chosenOption_inv, primeP);
        const den_beta = this.mathService.powerWithModulo(betaDividedByChosenOption, c_j, primeP);
        const den_beta_inv = this.mathService.modInverse(den_beta, primeP);

        return this.mathService.multiplyWithModulo(nom_beta, den_beta_inv, primeP);
    }

    private addChallenges(proof: VoteNIZKProof): string {
        let cs = '0';
        for (const singleProof of proof.getZeroKnowledgeProofs()) {
            cs = this.mathService.add(cs, singleProof.getC());
        }
        return cs;
    }

    private calculateChallenge(proof: VoteNIZKProof): string {
        const values = [];
        for (const singleProof of proof.getZeroKnowledgeProofs()) {
            values.push(singleProof.getA());
            values.push(singleProof.getB());
        }

        const stringToHash = values.join(',');
        const hash = this.cryptoService.hashSHA256(stringToHash);
        return this.mathService.hexToNumber(hash);
    }

}
