import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import { JwtTokenDecoderService } from './jwtTokenDecoder.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class JwtTokenDecoderServiceImpl implements JwtTokenDecoderService {

    private extract(field: string, token: string): any {
        const helper = this.getJwtHelper();
        const decoded = helper.decodeToken(token);
        return decoded[field];
    }

    extractSingleField(field: string, token: string): Maybe<string> {
        const value = this.extract(field, token);
        if (!value) {
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(value);
    }

    extractFieldArray(field: string, token: string): Maybe<string[]> {
        const value = this.extract(field, token);
        if (!value) {
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(value);
    }

    private getJwtHelper(): JwtHelperService {
        return new JwtHelperService();
    }
}
