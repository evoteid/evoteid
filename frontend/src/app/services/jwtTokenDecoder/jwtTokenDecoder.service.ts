import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class JwtTokenDecoderService {

    abstract extractSingleField(field: string, token: string): Maybe<string>;

    abstract extractFieldArray(field: string, token: string): Maybe<string[]>;
}
