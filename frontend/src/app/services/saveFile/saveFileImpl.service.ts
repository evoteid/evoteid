import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SaveFileServiceImpl {

  constructor() { }

  saveFile(text: string, fileName: string) {
    const FileSaver = require('file-saver');

    new Blob();
    const blob = new Blob([text], {type: 'text/plain;charset=utf-8'});
    FileSaver.saveAs(blob, fileName);
  }
}
