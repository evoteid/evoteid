import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CurrentUserService } from '../services/currentUser/currentUser.service';

@Injectable()
export class IsTrusteeGuard implements CanActivate {

    private navigateOnError = '/home';

    constructor(private currentUserService: CurrentUserService,
                private router: Router) {
    }

    canActivate() {
        if (this.currentUserService.isCurrentUserTrusteeAtTheMoment()) {
            return true;
        }
        else {
            this.router.navigate([this.navigateOnError]);
            return false;
        }
    }
}
