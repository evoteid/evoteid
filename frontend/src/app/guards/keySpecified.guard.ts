import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CurrentUserService } from '../services/currentUser/currentUser.service';

@Injectable()
export class KeySpecifiedGuard implements CanActivate {

    private navigateOnError = '/voters';

    constructor(private currentUserService: CurrentUserService,
                private router: Router) {
    }

    canActivate() {
        const maybePublicKey = this.currentUserService.getPublicKey();
        if (maybePublicKey.isEmpty()) {
            this.router.navigate([this.navigateOnError]);
            return false;
        }
        return true;
    }
}
