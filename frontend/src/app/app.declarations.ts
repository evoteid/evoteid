import { KeysComponent } from 'src/app/components/keys/keys.component';
import { AddVotingComponent } from './components/addVoting/addVoting.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { VotingsComponent } from './components/votings/votings.component';
import { RolesComponent} from './components/roles/roles.component';
import {HeaderComponent} from './components/header/header.component';
import {EasyVoteComponent} from './components/easyVote/easyVote.component';
import {AddVoterComponent} from './components/addVoter/addVoter.component';
import {AddVoterFailedComponent} from './components/dialogs/addVoterFailed/addVoterFailed.component';
import {ManageVotingsComponent} from './components/manageVotings/manageVotings.component';
import {StartVotingComponent} from './components/startVoting/startVoting.component';
import {KeyDerivationComponent} from './components/keyDerivation/keyDerivation.component';
import {UploadPrivateKeyComponent} from './components/dialogs/uploadPrivateKey/uploadPrivateKey.component';
import {CountdownComponent} from './components/countdown/countdown.component';
import {DecryptionComponent} from './components/decryption/decryption.component';
import {ShowResultsComponent} from './components/dialogs/showResults/showResults.component';
import {SpecifyVotersComponent} from './components/dialogs/specifyVoters/specifyVoters.component';
import {VotingInformationComponent} from './components/dialogs/votingInformation/votingInformation.component';
import {PublishComponent} from './components/publish/publish.component';
import {ViewSummaryComponent} from './components/dialogs/viewSummary/viewSummary.component';
import {AdministratorComponent} from './components/administrator/administrator.component';
import {PkiComponent} from './components/pki/pki.component';
import {TrusteeComponent} from './components/trustee/trustee.component';

export const declarations =
[
    AppComponent,
    HeaderComponent,
    LoginComponent,
    VotingsComponent,
    AddVotingComponent,
    KeysComponent,
    RolesComponent,
    EasyVoteComponent,
    AddVoterComponent,
    AddVoterFailedComponent,
    ManageVotingsComponent,
    StartVotingComponent,
    KeyDerivationComponent,
    UploadPrivateKeyComponent,
    CountdownComponent,
    DecryptionComponent,
    ShowResultsComponent,
    SpecifyVotersComponent,
    VotingInformationComponent,
    PublishComponent,
    ViewSummaryComponent,
    AdministratorComponent,
    PkiComponent,
    TrusteeComponent,
];
