export class Helper {

    public static splitString(stringToSpilt: string, lengthOfChunks: number): string[] {
        const ret = new Array();
        const lengthOfString = stringToSpilt.length;
        let chunk = 0;
        while (true) {
            const from = chunk * lengthOfChunks;
            const to = ((chunk + 1) * lengthOfChunks);

            if (to < lengthOfString) {
                ret.push(stringToSpilt.substring(from, to));
                chunk++;
            }
            else {
                ret.push(stringToSpilt.substring(from, lengthOfString));
                return ret;
            }
        }
    }

    public static concatString(values: string[]): string {
        let ret = '';
        for (const value of values) {
            ret = ret.concat(value);
        }
        return ret;
    }

}
