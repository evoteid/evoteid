export class Pair<S, T> {
    private readonly first: S;
    private readonly second: T;

    constructor(first: S, second: T) {
        this.first = first;
        this.second = second;
    }

    public getFirst(): S {
        return this.first;
    }

    public getSecond(): T {
        return this.second;
    }
}
