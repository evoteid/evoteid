import { LoggedInGuard } from './../app/guards/loggedIn.guard';
import { TestBed } from '@angular/core/testing';

import { providers } from './../app/app.providers';
import { imports } from './../app/app.imports';
import { declarations } from './../app/app.declarations';

import { LocalStorageService } from './../app/services/localStorage/localStorage.service';
import { AuthenticationService } from '../app/services/communication/authentication/authentication.service';
import { APP_BASE_HREF } from '@angular/common';
import { JwtTokenDecoderService } from '../app/services/jwtTokenDecoder/jwtTokenDecoder.service';
import { CurrentUserService } from '../app/services/currentUser/currentUser.service';
import { AuthorizationInterceptor } from '../app/services/communication/authorizationInterceptor/authorizationInterceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { VotingsDataService } from '../app/services/communication/votingsData/votingsData.service';
import { UrlRewriteInterceptor } from '../app/services/communication/urlRewriteInterceptor/urlRewriteInterceptor.service';
import { VoteService } from '../app/services/communication/vote/vote.service';
import { SecretsService } from 'src/app/services/secrets/secrets.service';
import { CryptoService } from 'src/app/services/crypto/crypto.service';
import { SaveFileService } from 'src/app/services/saveFile/saveFile.service';
import { UserPublicKeyJsonParser } from 'src/app/services/jsonParser/userPublicKeyJsonParser.service';
import {DataForKeyCalculationJsonParser} from '../app/services/jsonParser/dataForKeyCalculationJsonParser.service';
import {MathService} from '../app/services/math/math.service';
import {KeysService} from '../app/services/communication/keys/keys.service';
import {VotingsJsonParser} from '../app/services/jsonParser/votingsJsonParser.service';

describe('ServiceProviderTests', () => {

    beforeEach(() => {TestBed.configureTestingModule({
      imports: imports,
      providers: providers,
      declarations: declarations});
    });

    beforeEach(() => {TestBed.configureTestingModule({
        providers: [
            { provide: APP_BASE_HREF, useValue : '/' }
        ]});
      });

    it('authentication service should be created', () => {
        let service: AuthenticationService;
        service = TestBed.get(AuthenticationService);
        expect(service).toBeTruthy();
    });

    it('jwtTokenDecoder service should be created', () => {
        let service: JwtTokenDecoderService;
        service = TestBed.get(JwtTokenDecoderService);
        expect(service).toBeTruthy();
    });

    it('currentUser service should be created', () => {
        let service: CurrentUserService;
        service = TestBed.get(CurrentUserService);
        expect(service).toBeTruthy();
    });

    it('localStorage service should be created', () => {
        let service: LocalStorageService;
        service = TestBed.get(LocalStorageService);
        expect(service).toBeTruthy();
    });

    it('votings parser should be created', () => {
        let service: VotingsJsonParser;
        service = TestBed.get(VotingsJsonParser);
        expect(service).toBeTruthy();
    });

    it('userPublicKeys parser should be created', () => {
        let service: UserPublicKeyJsonParser;
        service = TestBed.get(UserPublicKeyJsonParser);
        expect(service).toBeTruthy();
    });

    it('crypto service should be created', () => {
        let service: CryptoService;
        service = TestBed.get(CryptoService);
        expect(service).toBeTruthy();
    });

    it('saveFile service should be created', () => {
        let service: SaveFileService;
        service = TestBed.get(SaveFileService);
        expect(service).toBeTruthy();
    });

    it('authorization interceptor should be created', () => {
        const interceptors = TestBed.get(HTTP_INTERCEPTORS);
        const wantedInterceptorName = AuthorizationInterceptor.name;
        let found = false;
        for (let i = 0; i < interceptors.length; i++) {
            if (interceptors[i].constructor.name === wantedInterceptorName) {
                found = true;
            }
        }
        expect(found).toBeTruthy();
    });

    it('urlRewrite interceptor should be created', () => {
        const interceptors = TestBed.get(HTTP_INTERCEPTORS);
        const wantedInterceptorName = UrlRewriteInterceptor.name;
        let found = false;
        for (let i = 0; i < interceptors.length; i++) {
            if (interceptors[i].constructor.name === wantedInterceptorName) {
                found = true;
            }
        }
        expect(found).toBeTruthy();
    });

    it('loggedIn guard should be created', () => {
        let guard: LocalStorageService;
        guard = TestBed.get(LoggedInGuard);
        expect(guard).toBeTruthy();
    });

    it('votings data service should be created', () => {
        let service: VotingsDataService;
        service = TestBed.get(VotingsDataService);
        expect(service).toBeTruthy();
    });

    it('vote service should be created', () => {
        let service: VoteService;
        service = TestBed.get(VoteService);
        expect(service).toBeTruthy();
    });

    it('secrets service should be created', () => {
        let service: SecretsService;
        service = TestBed.get(SecretsService);
        expect(service).toBeTruthy();
    });

    it('secrets service should be created', () => {
        let service: KeysService;
        service = TestBed.get(KeysService);
        expect(service).toBeTruthy();
    });

    it('dataForKeyCalculationParserShouldBeCreated', () => {
        let service: DataForKeyCalculationJsonParser;
        service = TestBed.get(DataForKeyCalculationJsonParser);
        expect(service).toBeTruthy();
    });

    it('math service should be created', () => {
        let service: MathService;
        service = TestBed.get(MathService);
        expect(service).toBeTruthy();
    });

});
