import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { Router } from '@angular/router';
import { IsAdminOrRegistrarGuard} from '../../app/guards/isAdminOrRegistrar.guard';

describe('IsAdminOrRegistrarGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let isAdminOrRegistrarGuard: IsAdminOrRegistrarGuard;

    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        isAdminOrRegistrarGuard = new IsAdminOrRegistrarGuard(noopCurrentUserService, router);
    });

    it('should return true if admin', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserAdmin').and.returnValue(true);
        spyOn(noopCurrentUserService, 'isCurrentUserRegistrar').and.returnValue(false);
        expect(isAdminOrRegistrarGuard.canActivate()).toBeTruthy();
    });

    it('should return true if registrar', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserAdmin').and.returnValue(false);
        spyOn(noopCurrentUserService, 'isCurrentUserRegistrar').and.returnValue(true);
        expect(isAdminOrRegistrarGuard.canActivate()).toBeTruthy();
    });

    it('should return false if neither admin not registrar', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserAdmin').and.returnValue(false);
        spyOn(noopCurrentUserService, 'isCurrentUserRegistrar').and.returnValue(false);
        expect(isAdminOrRegistrarGuard.canActivate()).toBeFalsy();
    });

});
