import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { Router } from '@angular/router';
import { IsTrusteeGuard} from '../../app/guards/isTrustee.guard';

describe('IsTrusteeGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let isTrusteeGuard: IsTrusteeGuard;

    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        isTrusteeGuard = new IsTrusteeGuard(noopCurrentUserService, router);
    });

    it('should return false if not trustee', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserTrusteeAtTheMoment').and.returnValue(false);
        expect(isTrusteeGuard.canActivate()).toBeFalsy();
    });

    it('should return true if trustee', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserTrusteeAtTheMoment').and.returnValue(true);
        expect(isTrusteeGuard.canActivate()).toBeTruthy();
    });

});
