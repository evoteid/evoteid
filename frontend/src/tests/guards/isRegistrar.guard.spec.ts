import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { Router } from '@angular/router';
import {IsRegistrarGuard} from '../../app/guards/isRegistrar.guard';

describe('IsRegistrarGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let isPowerVoterGuard: IsRegistrarGuard;

    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        isPowerVoterGuard = new IsRegistrarGuard(noopCurrentUserService, router);
    });

    it('should return false if not power voter', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserRegistrar').and.returnValue(false);
        expect(isPowerVoterGuard.canActivate()).toBeFalsy();
    });

    it('should return true if power voter', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserRegistrar').and.returnValue(true);
        expect(isPowerVoterGuard.canActivate()).toBeTruthy();
    });

});
