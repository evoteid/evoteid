import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { Router } from '@angular/router';
import { IsVotingAdminGuard} from '../../app/guards/isVotingAdmin.guard';

describe('IsVotingAdminGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let isVotingAdminGuard: IsVotingAdminGuard;

    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        isVotingAdminGuard = new IsVotingAdminGuard(noopCurrentUserService, router);
    });

    it('should return false if not voting admin', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserVotingAdministrator').and.returnValue(false);
        expect(isVotingAdminGuard.canActivate()).toBeFalsy();
    });

    it('should return true if voting admin', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserVotingAdministrator').and.returnValue(true);
        expect(isVotingAdminGuard.canActivate()).toBeTruthy();
    });

});
