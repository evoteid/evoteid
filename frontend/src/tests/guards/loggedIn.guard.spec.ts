import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { LoggedInGuard } from '../../app/guards/loggedIn.guard';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import any = jasmine.any;

describe('LoggedInGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let loggedInGuard: LoggedInGuard;

    let router: Router;

    const mockSnapshot: any = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const mockActivatedRoute: any = jasmine.createSpyObj('ActivatedRouteSnapshot', ['toString']);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            providers: [
                {provide: RouterStateSnapshot, useValue: mockSnapshot},
                {provide: ActivatedRouteSnapshot, useValue: mockActivatedRoute}
            ]
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        loggedInGuard = new LoggedInGuard(noopCurrentUserService, router);
    });

    it('should return false if not logged in', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserLoggedIn').and.returnValue(false);
        expect(loggedInGuard.canActivate(TestBed.get(ActivatedRouteSnapshot), TestBed.get(RouterStateSnapshot))).toBeFalsy();
    });

    it('should redirect to login if not logged in', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserLoggedIn').and.returnValue(false);
        spyOn(router, 'navigate');
        expect(loggedInGuard.canActivate(TestBed.get(ActivatedRouteSnapshot), TestBed.get(RouterStateSnapshot))).toBeFalsy();
        expect(router.navigate).toHaveBeenCalledWith(['/login'], Object({ queryParams: Object({ returnUrl: undefined }) }));
    });

    it('should return true if not logged in', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserLoggedIn').and.returnValue(true);
        expect(loggedInGuard.canActivate(TestBed.get(ActivatedRouteSnapshot), TestBed.get(RouterStateSnapshot))).toBeTruthy();
    });


});
