import { Maybe } from 'src/app/utils/maybe.utils';

describe('MaybeObject', () => {
    it('If created from empty isEmpty should be true', () => {
        const empty = Maybe.fromEmpty();
        expect(empty.isEmpty()).toBeTruthy();
    });

    it('If created with data the data should be received back', () => {
        const five = Maybe.fromData(5);
        expect(five.isEmpty()).toBeFalsy();
        expect(five.getData()).toBe(5);
    });
});
