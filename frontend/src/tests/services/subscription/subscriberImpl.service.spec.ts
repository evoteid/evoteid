import { TestBed } from '@angular/core/testing';

import {SubscriberService} from '../../../app/services/subscription/subscriber.service';
import {SubscriberServiceImpl} from '../../../app/services/subscription/subscriberImpl.service';
import {DummyObserver} from './dummyObserver';

describe('SubscriberService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const subscriberService: SubscriberService = new SubscriberServiceImpl();

  let spy: any;

  it('should notify observers' , () => {
    const dummyObserver = new DummyObserver();
    subscriberService.subscribeOnTokenChange(dummyObserver);
    spy = spyOn(dummyObserver, 'notify');

    expect(dummyObserver.notify).toHaveBeenCalledTimes(0);
    subscriberService.notifyTokenChanged();
    expect(dummyObserver.notify).toHaveBeenCalledTimes(1);
  });
});
