import {CustomObserver} from '../../../app/services/subscription/customObserver';

export class DummyObserver implements CustomObserver {

    notify() {
    }
}
