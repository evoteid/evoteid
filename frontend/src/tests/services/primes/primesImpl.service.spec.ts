import {PrimesService} from '../../../app/services/primes/primes.service';
import {PrimesServiceImpl} from '../../../app/services/primes/primesImpl.service';

describe('PrimesServiceImpl', () => {

    let service: PrimesService;

    beforeEach(() => {
        service = new PrimesServiceImpl();
    });

    it('should be able to return prime', () => {
        expect(service.getNthPrime(3)).toBe(7);
    });

});
