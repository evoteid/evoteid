import { VotingsArray } from './../../../app/models/votings/votingsArray.json.model';
import { VotingsJsonParser } from '../../../app/services/jsonParser/votingsJsonParser.service';
import { VotingsJsonParserImpl } from '../../../app/services/jsonParser/votingsJsonParserImpl.service';
import { Maybe } from '../../../app/utils/maybe.utils';
import {VotingOverview} from '../../../app/models/votings/votingOverview.json.model';


describe('VotingsJsonParserImpl', () => {

    let service: VotingsJsonParser;
    let validJsonObject;

    beforeEach(() => {
        service = new VotingsJsonParserImpl();

        validJsonObject = {
            'votings':
            [{
                title: 'title',
                description: 'description',
                options: [
                    {optionName: 'Voting 1', optionPrimeNumber: 2},
                    {optionName: 'Voting 2', optionPrimeNumber: 3}
                ],
                id: 27,
                phase: 4,
                timeVotingIsFinished: 42,
                transactionId: 'transId'
            }]
        };
      });

    it('should be able to parser valid json', () => {
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        const voting = parsedMaybeObject.getData().getVotings()[0];

        expect(voting.getTitle()).toBe('title');
        expect(voting.getDescription()).toBe('description');
        expect(voting.getOptions().length).toBe(2);
        expect(voting.getOptions()[0].getOptionName()).toBe('Voting 1');
        expect(voting.getOptions()[1].getOptionName()).toBe('Voting 2');
        expect(voting.getOptions()[0].getOptionPrimeNumber()).toBe(2);
        expect(voting.getOptions()[1].getOptionPrimeNumber()).toBe(3);
        expect(voting.getId()).toBe(27);
        expect(voting.getTimeVotingIsFinished()).toBe(42);
    });

    it('throws parsing error if title is missing', () => {
        delete validJsonObject.votings[0].title;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if description is missing', () => {
        delete validJsonObject.votings[0].description;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if option is missing', () => {
        delete validJsonObject.votings[0].options;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if one option does not have a name ', () => {
        delete validJsonObject.votings[0].options[0].optionName;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if one option does not have an optionPrimeNumber ', () => {
        delete validJsonObject.votings[0].options[0].optionPrimeNumber;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if id is missing', () => {
        delete validJsonObject.votings[0].id;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if transaction id is missing', () => {
        delete validJsonObject.votings[0].transactionId;
        const parsedMaybeObject: Maybe<VotingsArray> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('voting parser can parse single voting', () => {
        const parsedMaybeObject: Maybe<VotingOverview> = service.parseSingleVoting(JSON.stringify(validJsonObject.votings[0]));
        const voting = parsedMaybeObject.getData();

        expect(voting.getTitle()).toBe('title');
        expect(voting.getDescription()).toBe('description');
        expect(voting.getOptions().length).toBe(2);
        expect(voting.getOptions()[0].getOptionName()).toBe('Voting 1');
        expect(voting.getOptions()[1].getOptionName()).toBe('Voting 2');
        expect(voting.getOptions()[0].getOptionPrimeNumber()).toBe(2);
        expect(voting.getOptions()[1].getOptionPrimeNumber()).toBe(3);
        expect(voting.getId()).toBe(27);
        expect(voting.getTimeVotingIsFinished()).toBe(42);
        expect(voting.getTransactionId()).toBe('transId');
    });

});
