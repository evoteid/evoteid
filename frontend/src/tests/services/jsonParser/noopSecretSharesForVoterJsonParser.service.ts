import { Maybe } from './../../../app/utils/maybe.utils';
import {SecretSharesForVoterJsonParser} from '../../../app/services/jsonParser/secretSharesForVoterJsonParser.service';
import {SecretSharesForVoter} from '../../../app/models/secretShares/secretSharesForVoter.model';

export class NoopSecretSharesForVoterJsonParser implements SecretSharesForVoterJsonParser {
    parseJson(jsonString: string): Maybe<SecretSharesForVoter> {
        return Maybe.fromEmpty();
    }
}
