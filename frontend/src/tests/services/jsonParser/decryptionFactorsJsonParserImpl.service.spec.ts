import {Maybe} from '../../../app/utils/maybe.utils';
import {DecryptionFactorsJsonParser} from '../../../app/services/jsonParser/decryptionFactorsJsonParser.service';
import {DecryptionFactorsJsonParserImpl} from '../../../app/services/jsonParser/decryptionFactorsJsonParserImpl.service';
import {DecryptionFactors} from '../../../app/models/decryptionFactors/decryptionFactors.model';

describe('DecryptionFactorsJsonParser', () => {

    let service: DecryptionFactorsJsonParser;
    const validObject = {
        decryptionFactors:
            [
                {
                    fromVoterId: 27,
                    fromVoterName: 'Yoda',
                    fromVoterPosition: 0,
                    decryptionFactor: '72',
                    signature: 'sign0',
                },
                {
                    fromVoterId: 29,
                    fromVoterName: 'Luke',
                    fromVoterPosition: 1,
                    decryptionFactor: '78',
                    signature: 'sign1',
                },
                {
                    fromVoterId: 42,
                    fromVoterName: 'Lea',
                    fromVoterPosition: 2,
                    decryptionFactor: '79',
                    signature: 'sign2',
                },
            ]
    };

    beforeEach(() => {
        service = new DecryptionFactorsJsonParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<DecryptionFactors> = service.parseJson(jsonString);

        expect(parsedMaybeObject.getData().getDecryptionFactors().length).toBe(3);

        expect(parsedMaybeObject.getData().getDecryptionFactors()[0].getFromVoterId()).toBe(27);
        expect(parsedMaybeObject.getData().getDecryptionFactors()[0].getFromVoterName()).toBe('Yoda');
        expect(parsedMaybeObject.getData().getDecryptionFactors()[0].getFromVoterPosition()).toBe(0);
        expect(parsedMaybeObject.getData().getDecryptionFactors()[0].getDecryptionFactor()).toBe('72');
        expect(parsedMaybeObject.getData().getDecryptionFactors()[0].getSignature()).toBe('sign0');

        expect(parsedMaybeObject.getData().getDecryptionFactors()[1].getFromVoterId()).toBe(29);
        expect(parsedMaybeObject.getData().getDecryptionFactors()[1].getFromVoterName()).toBe('Luke');
        expect(parsedMaybeObject.getData().getDecryptionFactors()[1].getFromVoterPosition()).toBe(1);
        expect(parsedMaybeObject.getData().getDecryptionFactors()[1].getDecryptionFactor()).toBe('78');
        expect(parsedMaybeObject.getData().getDecryptionFactors()[1].getSignature()).toBe('sign1');

        expect(parsedMaybeObject.getData().getDecryptionFactors()[2].getFromVoterId()).toBe(42);
        expect(parsedMaybeObject.getData().getDecryptionFactors()[2].getFromVoterName()).toBe('Lea');
        expect(parsedMaybeObject.getData().getDecryptionFactors()[2].getFromVoterPosition()).toBe(2);
        expect(parsedMaybeObject.getData().getDecryptionFactors()[2].getDecryptionFactor()).toBe('79');
        expect(parsedMaybeObject.getData().getDecryptionFactors()[2].getSignature()).toBe('sign2');
    });
});
