import {Maybe} from '../../../app/utils/maybe.utils';
import {VotingVoterProgressJsonParser} from '../../../app/services/jsonParser/votingVoterProgressJsonParser.service';
import {VotingVoterProgressJsonParserImpl} from '../../../app/services/jsonParser/votingVoterProgressJsonParserImpl.service';
import {VotingVoterProgress} from '../../../app/models/votings/votingVoterProgress';

describe('VotingVoterProgressJsonParserImpl', () => {

    let service: VotingVoterProgressJsonParser;
    const validObject = {
        votingId: 42,
        phaseNumber: 27,
        createdSecretShares: true,
        voted: false,
        participatedInDecryption: true,
        usesCustomKey: true
    };


    beforeEach(() => {
        service = new VotingVoterProgressJsonParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<VotingVoterProgress> = service.parseJson(jsonString);
        expect(parsedMaybeObject.getData().getVotingId()).toBe(42);
        expect(parsedMaybeObject.getData().getPhaseNumber()).toBe(27);
        expect(parsedMaybeObject.getData().getCreatedSecretShares()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVoted()).toBeFalsy();
        expect(parsedMaybeObject.getData().getParticipatedInDecryption()).toBeTruthy();
        expect(parsedMaybeObject.getData().getUsesCustomKey()).toBeTruthy();
    });

});
