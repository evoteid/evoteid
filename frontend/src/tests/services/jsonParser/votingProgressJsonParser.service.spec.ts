import {VotingProgressJsonParser} from '../../../app/services/jsonParser/votingProgressJsonParser.service';
import {Maybe} from '../../../app/utils/maybe.utils';
import {VotingProgress} from '../../../app/models/votings/votingProgress.json.model';
import {VotingProgressJsonParserImpl} from '../../../app/services/jsonParser/votingProgressJsonParserImpl.service';


describe('VotingProgressJsonParserService', () => {

    let service: VotingProgressJsonParser;

    beforeEach(() => {
        service = new VotingProgressJsonParserImpl();
    });

    it('should be able to parse data', () => {

        const jsonObject = {
            phaseNumber: 73,
            securityThreshold: 5,
            transactionId: 'dummyId',
            votersProgress:
                [
                    {
                        participatedInDecryption: false,
                        participatedInVoting: true,
                        voterName: 'foo',
                        position: 0,
                        participatedInKeyGeneration: true,
                        isTrustee: false,
                        canVote: true,
                        usesCustomKey: false,
                    },
                    {
                        participatedInDecryption: false,
                        participatedInVoting: false,
                        voterName: 'bar',
                        position: 1,
                        participatedInKeyGeneration: false,
                        isTrustee: false,
                        canVote: false,
                        usesCustomKey: true,
                    },
                    {
                        participatedInDecryption: true,
                        participatedInVoting: true,
                        voterName: 'baz',
                        position: 2,
                        participatedInKeyGeneration: true,
                        isTrustee: true,
                        canVote: true,
                        usesCustomKey: false,
                    },
                ]
        };

        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<VotingProgress> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();
        expect(parsedMaybeObject.getData().getPhaseNumber()).toBe(73);
        expect(parsedMaybeObject.getData().getSecurityThreshold()).toBe(5);
        expect(parsedMaybeObject.getData().getTransactionId()).toBe('dummyId');
        expect(parsedMaybeObject.getData().getVotersProgress().length).toBe(3);

        expect(parsedMaybeObject.getData().getVotersProgress()[0].getPosition()).toBe(0);
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getVoterName()).toBe('foo');
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getParticipatedInKeyGeneration()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getParticipatedInVoting()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getParticipatedInDecryption()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getIsTrustee()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getCanVote()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[0].getUsesCustomKey()).toBeFalsy();

        expect(parsedMaybeObject.getData().getVotersProgress()[1].getPosition()).toBe(1);
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getVoterName()).toBe('bar');
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getParticipatedInKeyGeneration()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getParticipatedInVoting()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getParticipatedInDecryption()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getIsTrustee()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getCanVote()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVotersProgress()[1].getUsesCustomKey()).toBeTruthy();

        expect(parsedMaybeObject.getData().getVotersProgress()[2].getPosition()).toBe(2);
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getVoterName()).toBe('baz');
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getParticipatedInKeyGeneration()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getParticipatedInVoting()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getParticipatedInDecryption()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getIsTrustee()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getCanVote()).toBeTruthy();
        expect(parsedMaybeObject.getData().getVotersProgress()[2].getUsesCustomKey()).toBeFalsy();
    });
});
