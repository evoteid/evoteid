import {DataForKeyCalculationJsonParser} from '../../../app/services/jsonParser/dataForKeyCalculationJsonParser.service';
import {DataForKeyCalculationJsonParserImpl} from '../../../app/services/jsonParser/dataForKeyCalculationJsonParserImpl.service';
import {DataForKeyCalculation} from '../../../app/models/dataForKeyCalculation/dataForKeyCalculation.model';
import {Maybe} from '../../../app/utils/maybe.utils';

describe('DataForKeyCalculationJsonParserImpl', () => {

    let service: DataForKeyCalculationJsonParser;
    const validObject = {
        eligibleVoters:
            [
                {
                    name: 'name1',
                    id: 1,
                    position: 0 ,
                    publicKey: 'key1',
                    isTrustee: true,
                    canVote: false,
                },
                {
                    name: 'name2',
                    id: 2,
                    position: 1,
                    publicKey: 'key2',
                    isTrustee: false,
                    canVote: false,
                },
                {
                    name: 'name3',
                    id: 3,
                    position: 2,
                    publicKey: 'key3',
                    isTrustee: false,
                    canVote: true,
                }
            ],
        calculationParameters:
            {
                primeQ: '10',
                primeP: '11',
                generatorG: '12'
            },
        securityThreshold: 27,
        numberOfEligibleVoters: 3
    };

    beforeEach(() => {
        service = new DataForKeyCalculationJsonParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<DataForKeyCalculation> = service.parseJson(jsonString);
        expect(parsedMaybeObject.getData().getNumberOfEligibleVoters()).toBe(3);
        expect(parsedMaybeObject.getData().getSecurityThreshold()).toBe(27);
        expect(parsedMaybeObject.getData().getEligibleVoters().length).toBe(3);
        expect(parsedMaybeObject.getData().getEligibleVoters()[0].getName()).toBe('name1');
        expect(parsedMaybeObject.getData().getEligibleVoters()[0].getPublicKey()).toBe('key1');
        expect(parsedMaybeObject.getData().getEligibleVoters()[0].getPosition()).toBe( 0);
        expect(parsedMaybeObject.getData().getEligibleVoters()[0].getId()).toBe(1);
        expect(parsedMaybeObject.getData().getEligibleVoters()[0].getIsTrustee()).toBeTruthy();
        expect(parsedMaybeObject.getData().getEligibleVoters()[0].getCanVote()).toBeFalsy();
        expect(parsedMaybeObject.getData().getCalculationParameters().getPrimeQ()).toBe('10');
        expect(parsedMaybeObject.getData().getCalculationParameters().getPrimeP()).toBe('11');
        expect(parsedMaybeObject.getData().getCalculationParameters().getGeneratorG()).toBe('12');
    });
});
