import { Maybe } from '../../../app/utils/maybe.utils';
import {VotersJsonParser} from '../../../app/services/jsonParser/votersJsonParser.service';
import {VotersJsonParserImpl} from '../../../app/services/jsonParser/votersJsonParserImpl.service';
import {VoterArray} from '../../../app/models/voters/voterArray.model';


describe('VotersJsonParserImpl', () => {

    let service: VotersJsonParser;

    beforeEach(() => {
        service = new VotersJsonParserImpl();
      });

    it('throws parsing error if votings array is missing', () => {
        const jsonObject: object = {};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<VoterArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('should be able to parse empty object', () => {
        const jsonObject: object = {voters: []};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<VoterArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const parsedObject: VoterArray = parsedMaybeObject.getData();
        expect(JSON.stringify(jsonObject)).toBe(JSON.stringify(parsedObject));
        expect(parsedObject.getVoters().length).toBe(0);
    });

    it('should return empty if not valid json', () => {
        const parsedMaybeObject: Maybe<VoterArray> = service.parseJson('notjson');
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if username is missing', () => {
        const jsonObject: object = {voters: [{ id: 42}]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<VoterArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if id is missing', () => {
        const jsonObject: object = {voters: [{ username: 'username'}]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<VoterArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('can parse valid object', () => {
        const jsonObject: object = {voters: [
            {name: 'u1', id: 27},
            {name: 'u2', id: 42},
            {name: 'u3', id: 23},
        ]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<VoterArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();
        expect(parsedMaybeObject.getData().getVoters().length).toBe(3);
        expect(parsedMaybeObject.getData().getVoters()[0].getName()).toBe('u1');
        expect(parsedMaybeObject.getData().getVoters()[0].getId()).toBe(27);
        expect(parsedMaybeObject.getData().getVoters()[1].getName()).toBe('u2');
        expect(parsedMaybeObject.getData().getVoters()[1].getId()).toBe(42);
        expect(parsedMaybeObject.getData().getVoters()[2].getName()).toBe('u3');
        expect(parsedMaybeObject.getData().getVoters()[2].getId()).toBe(23);
    });

});
