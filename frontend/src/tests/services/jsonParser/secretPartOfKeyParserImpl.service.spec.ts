import {Maybe} from '../../../app/utils/maybe.utils';
import {SecretPartOfKeyParser} from '../../../app/services/jsonParser/secretPartOfKeyParser.service';
import {SecretPartOfKeyParserImpl} from '../../../app/services/jsonParser/secretPartOfKeyParserImpl.service';
import {SecretPartOfKey} from '../../../app/models/secretPartOfKey/secretPartOfKey.json.model';

describe('SecretPartOfKeyParser', () => {

    let service: SecretPartOfKeyParser;
    const validObject = {
        fromVoterId: 27,
        fromVoterName: 'Yoda',
        secretPartOfKey:
            [
                's1',
                's2',
                's3',
            ]
    };

    beforeEach(() => {
        service = new SecretPartOfKeyParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<SecretPartOfKey> = service.parseJson(jsonString);
        expect(parsedMaybeObject.getData().getFromVoterId()).toBe(27);
        expect(parsedMaybeObject.getData().getFromVoterName()).toBe('Yoda');

        expect(parsedMaybeObject.getData().getSecretPartOfKey()[0]).toBe('s1');
        expect(parsedMaybeObject.getData().getSecretPartOfKey()[1]).toBe('s2');
        expect(parsedMaybeObject.getData().getSecretPartOfKey()[2]).toBe('s3');

    });

});
