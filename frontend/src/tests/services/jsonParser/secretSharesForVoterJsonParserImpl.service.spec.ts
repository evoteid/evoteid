import {Maybe} from '../../../app/utils/maybe.utils';
import {SecretSharesForVoterJsonParser} from '../../../app/services/jsonParser/secretSharesForVoterJsonParser.service';
import {SecretSharesForVoterJsonParserImpl} from '../../../app/services/jsonParser/secretSharesForVoterJsonParserImpl.service';
import {SecretSharesForVoter} from '../../../app/models/secretShares/secretSharesForVoter.model';

describe('SecretSharesForVoterJsonParserImpl', () => {

    let service: SecretSharesForVoterJsonParser;
    const validObject = {
            forVoterId: 4,
            forVoterName: 'John',
            forVoterPosition: 3,
            secretShares:
                [
                    {
                        secretShare:
                            [
                                's0',
                                's1',
                                's2'
                            ],
                        fromVoterId: 0,
                        fromVoterName: 'u0',
                        signature: 'sign0'
                    },
                    {
                        secretShare:
                            [
                                's3',
                                's4',
                                's5'
                            ],
                        fromVoterId: 1,
                        fromVoterName: 'u1',
                        signature: 'sign1'
                    },
                    {
                        secretShare:
                            [
                                's6',
                                's7',
                                's8'
                            ],
                        fromVoterId: 2,
                        fromVoterName: 'u2',
                        signature: 'sign2'
                    }
                ]
        };

    beforeEach(() => {
        service = new SecretSharesForVoterJsonParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<SecretSharesForVoter> = service.parseJson(jsonString);
        expect(parsedMaybeObject.getData().getForVoterId()).toBe(4);
        expect(parsedMaybeObject.getData().getForVoterName()).toBe('John');
        expect(parsedMaybeObject.getData().getForVoterPosition()).toBe(3);
        expect(parsedMaybeObject.getData().getSecretShares().length).toBe(3);
        expect(parsedMaybeObject.getData().getSecretShares()[0].getSecretShare()[0]).toBe('s0');
        expect(parsedMaybeObject.getData().getSecretShares()[0].getSecretShare()[1]).toBe('s1');
        expect(parsedMaybeObject.getData().getSecretShares()[0].getSecretShare()[2]).toBe('s2');
        expect(parsedMaybeObject.getData().getSecretShares()[1].getSecretShare()[0]).toBe('s3');
        expect(parsedMaybeObject.getData().getSecretShares()[1].getSecretShare()[1]).toBe('s4');
        expect(parsedMaybeObject.getData().getSecretShares()[1].getSecretShare()[2]).toBe('s5');
        expect(parsedMaybeObject.getData().getSecretShares()[2].getSecretShare()[0]).toBe('s6');
        expect(parsedMaybeObject.getData().getSecretShares()[2].getSecretShare()[1]).toBe('s7');
        expect(parsedMaybeObject.getData().getSecretShares()[2].getSecretShare()[2]).toBe('s8');
        expect(parsedMaybeObject.getData().getSecretShares()[0].getFromVoterId()).toBe(0);
        expect(parsedMaybeObject.getData().getSecretShares()[1].getFromVoterId()).toBe(1);
        expect(parsedMaybeObject.getData().getSecretShares()[2].getFromVoterId()).toBe(2);
        expect(parsedMaybeObject.getData().getSecretShares()[0].getSignature()).toBe('sign0');
        expect(parsedMaybeObject.getData().getSecretShares()[1].getSignature()).toBe('sign1');
        expect(parsedMaybeObject.getData().getSecretShares()[2].getSignature()).toBe('sign2');
        expect(parsedMaybeObject.getData().getSecretShares()[0].getFromVoterName()).toBe('u0');
        expect(parsedMaybeObject.getData().getSecretShares()[1].getFromVoterName()).toBe('u1');
        expect(parsedMaybeObject.getData().getSecretShares()[2].getFromVoterName()).toBe('u2');
    });
});
