import { Checker } from './../../../checker.service';
import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthenticationService } from './../../../../app/services/communication/authentication/authentication.service';
import { AuthenticationServiceImpl } from './../../../../app/services/communication/authentication/authenticationImpl.service';

const validTokenForJohnDoe = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.\
                              eyJzdWIiOiJKb2huIERvZSJ9.\
                              6neBz0ic4Ne9dDyyT8-MLXQOLNQnBHY8BIZIVdjqdRvJreqTbRYyvyX6xEgVK44gSItqiCAnstZY-VOUxK2MGA`;

describe('AuthentificationServiceImpl', () => {
  let service: AuthenticationService;
  let mockHttp: HttpTestingController;

  beforeEach(() => {TestBed.configureTestingModule({
    imports: [
        HttpClientTestingModule
    ],
    providers: [
        AuthenticationServiceImpl,
      ],
  });

  service = TestBed.get(AuthenticationServiceImpl);
  mockHttp = TestBed.get(HttpTestingController);
});



  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Wrong password sends an empty user object back', async(() => {
    const username = 'user1';
    const password = 'pass1';
    service.authenticate(username, password).subscribe(maybeUser => expect(maybeUser.isEmpty()).toBeTruthy());
    const req = mockHttp.expectOne(r => r.method === 'POST' && r.url === '/api/authenticate');
    expect(Checker.isEqual(req.request.body, {username: username, password: password})).toBeTruthy();
    req.flush({}, { status: 422, statusText: 'Invalid request' } );
  }));

  it('200 but no token sends empty user object back', async(() => {
    const username = 'user2';
    const password = 'pass2';
    service.authenticate(username, password).subscribe(maybeUser => expect(maybeUser.isEmpty()).toBeTruthy());
    const req = mockHttp.expectOne(r => r.method === 'POST' && r.url === '/api/authenticate');
    expect(Checker.isEqual(req.request.body, {username: username, password: password})).toBeTruthy();
    req.flush({token: ''}, { status: 200, statusText: 'Ok' } );
  }));

  it('200 but empty token sends empty user object back', async(() => {
    const username = 'user3';
    const password = 'pass3';
    service.authenticate(username, password).subscribe(maybeUser => expect(maybeUser.isEmpty()).toBeTruthy());
    const req = mockHttp.expectOne(r => r.method === 'POST' && r.url === '/api/authenticate');
    expect(Checker.isEqual(req.request.body, {username: username, password: password})).toBeTruthy();
    req.flush( {}, { status: 200, statusText: 'Ok' } );
  }));

  it('can handle null response', async(() => {
    const username = 'user4';
    const password = 'pass4';
    service.authenticate(username, password).subscribe(maybeUser => expect(maybeUser.isEmpty()).toBeTruthy());
    const req = mockHttp.expectOne(r => r.method === 'POST' && r.url === '/api/authenticate');
    expect(Checker.isEqual(req.request.body, {username: username, password: password})).toBeTruthy();
    req.flush( null );
  }));

  it('Right password sends the right object back', async(() => {
    const username = 'user5';
    const password = 'pass5';
    service.authenticate(username, password).subscribe(maybeUser => expect(maybeUser.isEmpty()).toBeFalsy());
    const req = mockHttp.expectOne(r => r.method === 'POST' && r.url === '/api/authenticate');
    expect(Checker.isEqual(req.request.body, {username: username, password: password})).toBeTruthy();
    req.flush({token: validTokenForJohnDoe}, { status: 200, statusText: 'Ok' } );
  }));


});
