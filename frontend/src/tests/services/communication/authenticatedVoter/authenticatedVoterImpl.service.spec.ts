import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {async, TestBed} from '@angular/core/testing';
import {AuthenticatedVoterService} from '../../../../app/services/communication/authenticatedVoter/authenticatedVoter.service';
import {AuthenticatedVoterServiceImpl} from '../../../../app/services/communication/authenticatedVoter/authenticatedVoterImpl.service';

describe('AuthenticatedVoterService', () => {
    let service: AuthenticatedVoterService;
    let mockHttp: HttpTestingController;

    beforeEach(() => {TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ],
        providers: [
            { provide: AuthenticatedVoterService, useClass: AuthenticatedVoterServiceImpl },
        ],
    });

        service = TestBed.get(AuthenticatedVoterService);
        mockHttp = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('error returns empty', async(() => {
        service.getAuthenticatedVoter().subscribe(authVoter => {
                expect(authVoter.isEmpty()).toBeTruthy();
            }
        );
        const req = mockHttp.expectOne(r => r.method === 'GET' && r.url === '/api/currentVoterToken');
        req.flush({voterName: 'James Bond', registered: 'Yes'}, {status: 401, statusText: 'Failure'});
    }));

    it('should return empty if name is missing', async(() => {
        service.getAuthenticatedVoter().subscribe(authVoter => {
                expect(authVoter.isEmpty()).toBeTruthy();
            }
        );
        const req = mockHttp.expectOne(r => r.method === 'GET' && r.url === '/api/currentVoterToken');
        req.flush({ registered: 'Yes'}, {status: 200, statusText: 'Ok'});
    }));

    it('should return not registered voter if voter is not registered', async(() => {
        service.getAuthenticatedVoter().subscribe(authVoter => {
            expect(authVoter.isEmpty()).toBeFalsy();
            expect(authVoter.getData().getVoterName()).toBe('James Bond');
            expect(authVoter.getData().getIsRegistered()).toBeFalsy();
            }
        );
        const req = mockHttp.expectOne(r => r.method === 'GET' && r.url === '/api/currentVoterToken');
        req.flush({voterName: 'James Bond', registered: 'No'}, {status: 200, statusText: 'Ok'});
    }));

    it('should return registered voter if voter is registered', async(() => {
        service.getAuthenticatedVoter().subscribe(authVoter => {
            expect(authVoter.isEmpty()).toBeFalsy();
            expect(authVoter.getData().getVoterName()).toBe('James Bond');
            expect(authVoter.getData().getIsRegistered()).toBeTruthy();
            }
        );
        const req = mockHttp.expectOne(r => r.method === 'GET' && r.url === '/api/currentVoterToken');
        req.flush({voterName: 'James Bond', registered: 'Yes'}, {status: 200, statusText: 'Ok'});
    }));
});
