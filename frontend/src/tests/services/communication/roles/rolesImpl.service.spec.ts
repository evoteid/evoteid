import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {async, TestBed} from '@angular/core/testing';
import {RolesService} from '../../../../app/services/communication/roles/roles.service';
import {RolesServiceImpl} from '../../../../app/services/communication/roles/rolesImpl.service';
import {RolesJsonParser} from '../../../../app/services/jsonParser/rolesJsonParser.service';
import {RolesJsonParserImpl} from '../../../../app/services/jsonParser/rolesJsonParserImpl.service';

describe('RolesService', () => {

    let rolesService: RolesService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: RolesService , useClass: RolesServiceImpl },
                { provide: RolesJsonParser, useClass: RolesJsonParserImpl }
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        rolesService = TestBed.get(RolesService);
    });

    it('should be created', () => {
        expect(rolesService).toBeTruthy();
    });

    it('should be able to fetch all available roles', async(() => {
        rolesService.getAllAvailableRoles().subscribe(roles => expect(roles.getData().getRoles()).toEqual(['role1', 'role2']));
        const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/roles');
        req.flush({'roles': ['role1', 'role2']}, { status: 200, statusText: 'Ok' });
    }));

    it('should be able to fetch all voters with their roles', async(() => {
        rolesService.getAllVotersWithRoles().subscribe(roles => expect(roles.getData().getVoters().length).toBe(1));
        const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters/roles');
        req.flush({voters: [{roles: [], name: 'u1', id: 1}]}, { status: 200, statusText: 'Ok' });
    }));

    it('should be able to add a role to a user and return ok', async(() => {
        const voterId = 42;
        const roleName = 'testRole';
        rolesService.addRole(voterId, roleName).subscribe(b => expect(b).toBeTruthy());
        const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/voters/' + voterId + '/roles/' + roleName);
        req.flush({}, { status: 200, statusText: 'Ok' });
    }));

    it('should return false if something went wrong', async(() => {
        const voterId = 42;
        const roleName = 'testRole';
        rolesService.addRole(voterId, roleName).subscribe(b => expect(b).toBeFalsy());
        const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/voters/' + voterId + '/roles/' + roleName);
        req.flush({}, { status: 400, statusText: 'Error' });
    }));

    it('should be able to delete a role from a user and return ok', async(() => {
        const voterId = 42;
        const roleName = 'testRole';
        rolesService.removeRole(voterId, roleName).subscribe(b => expect(b).toBeTruthy());
        const req = httpMock.expectOne(r => r.method === 'DELETE' && r.url === '/api/voters/' + voterId + '/roles/' + roleName);
        req.flush({}, { status: 200, statusText: 'Ok' });
    }));

    it('should return false if something went wrong', async(() => {
        const voterId = 42;
        const roleName = 'testRole';
        rolesService.removeRole(voterId, roleName).subscribe(b => expect(b).toBeFalsy());
        const req = httpMock.expectOne(r => r.method === 'DELETE' && r.url === '/api/voters/' + voterId + '/roles/' + roleName);
        req.flush({}, { status: 400, statusText: 'Error' });
    }));

});
