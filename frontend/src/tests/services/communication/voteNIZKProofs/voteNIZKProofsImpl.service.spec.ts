import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import {VoteNIZKProofService} from '../../../../app/services/communication/voteNIZKProofs/voteNIZKProofs.service';
import {VoteNIZKProofServiceImpl} from '../../../../app/services/communication/voteNIZKProofs/voteNIZKProofsImpl.service';
import {VoteNIZKProof} from '../../../../app/models/voteNIZKProof/voteNIZKProofs';
import {VotesNIZKProofJsonParser} from '../../../../app/services/jsonParser/votesNIZKProofJsonParser.service';
import {VotesNIZKProofJsonParserImpl} from '../../../../app/services/jsonParser/votesNIZKProofJsonParserImpl.service';

describe('VoteNIZKProofsService', () => {

    let voteNIZKProofsService: VoteNIZKProofService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: VoteNIZKProofService , useClass: VoteNIZKProofServiceImpl},
                { provide: VotesNIZKProofJsonParser, useClass: VotesNIZKProofJsonParserImpl}
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        voteNIZKProofsService = TestBed.get(VoteNIZKProofService);

    });

    it('should be created', async() => {
        expect(voteNIZKProofsService).toBeTruthy();
    });

    it('should return false if server returned an error', async() => {
        const voteId = 27;
        const voterId = 42;
        const proof = new VoteNIZKProof();

        voteNIZKProofsService.sendNIZKProofs(voteId, voterId, proof).subscribe(
            successful => {
                expect(successful.getFirst()).toBeFalsy();
                expect(successful.getSecond()).toBe('Dummy error');
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/voteZeroKnowledgeProofs/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toBe(JSON.stringify(proof));
        req.flush('Dummy error', { status: 400, statusText: 'Server error' } );
    });

    it('should return true if everything is ok', async() => {
        const voteId = 27;
        const voterId = 42;
        const proof = new VoteNIZKProof();

        voteNIZKProofsService.sendNIZKProofs(voteId, voterId, proof).subscribe(
            successful => {
                expect(successful.getFirst()).toBeTruthy();
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/voteZeroKnowledgeProofs/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toBe(JSON.stringify(proof));
        req.flush({}, { status: 201, statusText: 'Created' } );
    });

    it('should return empty if not found', async() => {
        const voteId = 27;

        voteNIZKProofsService.getNITKProofs(voteId).subscribe(
            data => {
                expect(data.isEmpty()).toBeTruthy();
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/voteZeroKnowledgeProofs');
        expect(req.request.method).toEqual('GET');
        req.flush({}, { status: 404, statusText: 'Not found' } );
    });

    it('should return empty if invalid data', async() => {
        const voteId = 27;

        voteNIZKProofsService.getNITKProofs(voteId).subscribe(
            data => {
                expect(data.isEmpty()).toBeTruthy();
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/voteZeroKnowledgeProofs');
        expect(req.request.method).toEqual('GET');
        req.flush({dummy: 'd'}, { status: 200, statusText: 'OK' } );
    });

    it('should return data if received correct data', async() => {
        const voteId = 27;

        voteNIZKProofsService.getNITKProofs(voteId).subscribe(
            data => {
                expect(data.isEmpty()).toBeFalsy();
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/voteZeroKnowledgeProofs');
        expect(req.request.method).toEqual('GET');
        req.flush({allProofs: []}, { status: 200, statusText: 'OK' } );
    });

});
