import { TestBed } from '@angular/core/testing';
import { VotingProgressService } from '../../../../app/services/communication/votingProgress/votingProgress.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {VotingsJsonParser} from '../../../../app/services/jsonParser/votingsJsonParser.service';
import { VotingProgressJsonParser} from '../../../../app/services/jsonParser/votingProgressJsonParser.service';
import {VotingProgressServiceImpl} from '../../../../app/services/communication/votingProgress/votingProgressImpl.service';
import {NoopVotingProgressJsonParser} from './noopVotingProgressJsonParser';
import {VotingProgress} from '../../../../app/models/votings/votingProgress.json.model';
import {Maybe} from '../../../../app/utils/maybe.utils';

describe('VotingProgressService', () => {

  let votingProgressService: VotingProgressService;
  let httpMock: HttpTestingController;
  let noopParser: VotingProgressJsonParser;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: VotingProgressService , useClass: VotingProgressServiceImpl },
        { provide: VotingProgressJsonParser, useClass: NoopVotingProgressJsonParser },
      ],
    });

    httpMock = TestBed.get(HttpTestingController);
    votingProgressService = TestBed.get(VotingProgressService);
    noopParser = TestBed.get(VotingProgressJsonParser);

  });

  it('should be created', () => {
    const service: VotingProgressService = TestBed.get(VotingProgressService);
    expect(service).toBeTruthy();
  });

  it('should return empty object if response status is not ok', async() => {
    votingProgressService.getVotingProgress(42).subscribe(maybeVotings => expect(maybeVotings.isEmpty()).toBeTruthy());
    const req = httpMock.expectOne('/api/votings/42/votingsOverview');
    expect(req.request.method).toEqual('GET');
    req.flush({}, { status: 422, statusText: 'Invalid request' });
  });

  it('should forward data that parser provided', async() => {
    const votingProgress = new VotingProgress();
    spyOn(votingProgress, 'getPhaseNumber').and.returnValue(7);
    spyOn(noopParser, 'parseJson').and.returnValue(Maybe.fromData(votingProgress));

    votingProgressService.getVotingProgress(27)
        .subscribe(maybeVotings => expect(maybeVotings.getData().getPhaseNumber()).toBe(7));
    const req = httpMock.expectOne('/api/votings/27/votingsOverview');
    expect(req.request.method).toEqual('GET');
    req.flush({}, { status: 200, statusText: 'Ok' } );
  });

});
