import {HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';

import {CryptoService } from 'src/app/services/crypto/crypto.service';
import {NoopCryptoService } from '../../crypto/noopCrypto.service';
import {VotersServiceImpl} from '../../../../app/services/communication/voters/votersImpl.service';
import {VotersService} from '../../../../app/services/communication/voters/voters.service';
import {VotersJsonParser} from '../../../../app/services/jsonParser/votersJsonParser.service';
import {VotersJsonParserImpl} from '../../../../app/services/jsonParser/votersJsonParserImpl.service';
import {Voter} from '../../../../app/models/voters/voter.model';
import {VoterArray} from '../../../../app/models/voters/voterArray.model';

describe('VotersService', () => {

  let votersService: VotersService;
  let httpMock: HttpTestingController;
  let cryptoService: CryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [
        HttpClientTestingModule
    ],
    providers: [
        { provide: VotersService , useClass: VotersServiceImpl },
        { provide: VotersJsonParser , useClass: VotersJsonParserImpl },
        { provide: CryptoService, useClass: NoopCryptoService },
      ],
  });

  httpMock = TestBed.get(HttpTestingController);
  votersService = TestBed.get(VotersService);
  cryptoService = TestBed.get(CryptoService);
  });

  it('should be created', () => {
    expect(votersService).toBeTruthy();
  });

  it('should return the right data if asked for all voters', async() => {

    votersService.getAllVoters().subscribe(returnedVoters => {
      expect(returnedVoters.getStatusCode()).toBe(200);
      expect(returnedVoters.getResponse().isEmpty()).toBeFalsy();
      expect(returnedVoters.getResponse().getData().getVoters().length).toBe(3);
      expect(returnedVoters.getResponse().getData().getVoters()[0].getName()).toBe('u1');
      expect(returnedVoters.getResponse().getData().getVoters()[0].getId()).toBe(1);
      expect(returnedVoters.getResponse().getData().getVoters()[1].getName()).toBe('u2');
      expect(returnedVoters.getResponse().getData().getVoters()[1].getId()).toBe(2);
      expect(returnedVoters.getResponse().getData().getVoters()[2].getName()).toBe('u3');
      expect(returnedVoters.getResponse().getData().getVoters()[2].getId()).toBe(3);
    });

    const mockAnswer = '{"voters":[{"name":"u1","id":1},{"name":"u2","id":2},{"name":"u3","id":3}]}';
    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters');
    req.flush(JSON.parse(mockAnswer), { status: 200, statusText: 'Ok' });
  });

  it('save voter works with keys created', () => {
    const voterArray = new VoterArray();
    const voters = [];
    voters.push(new Voter().setName('foo'));
    voters.push(new Voter().setName('bar'));
    voterArray.setVoters(voters);
    votersService.saveVoters(voterArray, true).subscribe(returnedVoters => {
      expect(returnedVoters.getStatusCode()).toBe(201);
      expect(returnedVoters.getResponse().isEmpty()).toBeFalsy();
      expect(returnedVoters.getResponse().getData().getVoters().length).toBe(1);
      expect(returnedVoters.getResponse().getData().getVoters()[0].getId()).toBe(27);
      expect(returnedVoters.getResponse().getData().getVoters()[0].getName()).toBe('foo');
    });

    const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/voters');
    expect(req.request.body).toEqual('{"voters":[{"name":"foo"},{"name":"bar"}],"createKeys":true}');

    const mockAnswer = '{"voters":[{"name":"foo","id":27}]}';
    req.flush(JSON.parse(mockAnswer), { status: 201, statusText: 'Ok' });
  });

  it('save voter works', () => {
    const voterArray = new VoterArray();
    const voters = [];
    voters.push(new Voter().setName('foo'));
    voters.push(new Voter().setName('bar'));
    voterArray.setVoters(voters);
    votersService.saveVoters(voterArray, false).subscribe(returnedVoters => {
      expect(returnedVoters.getStatusCode()).toBe(201);
      expect(returnedVoters.getResponse().isEmpty()).toBeFalsy();
      expect(returnedVoters.getResponse().getData().getVoters().length).toBe(1);
      expect(returnedVoters.getResponse().getData().getVoters()[0].getId()).toBe(27);
      expect(returnedVoters.getResponse().getData().getVoters()[0].getName()).toBe('foo');
     });

    const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/voters');
    expect(req.request.body).toEqual('{"voters":[{"name":"foo"},{"name":"bar"}],"createKeys":false}');

    const mockAnswer = '{"voters":[{"name":"foo","id":27}]}';
    req.flush(JSON.parse(mockAnswer), { status: 201, statusText: 'Ok' });
  });

  it('save voter error is propagated', () => {
    const voterArray = new VoterArray();
    const voters = [];
    voters.push(new Voter().setName('foo'));
    voters.push(new Voter().setName('bar'));
    voterArray.setVoters(voters);
    votersService.saveVoters(voterArray, false).subscribe(returnedVoters => {
      expect(returnedVoters.getStatusCode()).toBe(409);
      expect(returnedVoters.getResponse().isEmpty()).toBeTruthy();
    });

    const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/voters');
    expect(req.request.body).toEqual('{"voters":[{"name":"foo"},{"name":"bar"}],"createKeys":false}');

    const mockAnswer = '{"voters":[{"name":"foo","id":27}]}';
    req.flush(JSON.parse(mockAnswer), { status: 409, statusText: 'Conflict' });
  });

  it('delete voter propagates error', () => {
    const voterId = 27;
    votersService.deleteVoter(voterId).subscribe(data => {
        expect(data.getFirst()).toBeFalsy();
        expect(data.getSecond()).toEqual('Dummy text');
    });

    const req = httpMock.expectOne(r => r.method === 'DELETE' && r.url === '/api/voters/' + voterId);
    req.flush('Dummy text', { status: 404, statusText: 'Dummy status' });
  });

  it('delete voter returns ok if worked', () => {
    const voterId = 27;
    votersService.deleteVoter(voterId).subscribe(data => {
      expect(data.getFirst()).toBeTruthy();
      expect(data.getSecond()).toEqual('Successful');
    });

    const req = httpMock.expectOne(r => r.method === 'DELETE' && r.url === '/api/voters/' + voterId);
    req.flush('Successful', { status: 200, statusText: 'Dummy status' });
  });

  it('register current voter propagates error', () => {
    votersService.registerCurrentVoter().subscribe(data => {
      expect(data.getFirst()).toBeFalsy();
      expect(data.getSecond()).toEqual('Dummy text');
    });

    const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/registerCurrentVoter');
    req.flush('Dummy text', { status: 404, statusText: 'Dummy status' });
  });

  it('register current voter returns ok if worked', () => {
    votersService.registerCurrentVoter().subscribe(data => {
      expect(data.getFirst()).toBeTruthy();
      expect(data.getSecond()).toEqual('Successful');
    });

    const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/registerCurrentVoter');
    req.flush('Successful', { status: 201, statusText: 'Dummy status' });
  });

});
