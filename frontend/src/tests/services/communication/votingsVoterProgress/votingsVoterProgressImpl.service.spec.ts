import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {VotingVoterProgressService} from '../../../../app/services/communication/votingVoterProgress/votingVoterProgress.service';
import {VotingVoterProgressServiceImpl} from '../../../../app/services/communication/votingVoterProgress/votingVoterProgressImpl.service';
import {VotingVoterProgressJsonParser} from '../../../../app/services/jsonParser/votingVoterProgressJsonParser.service';
import {VotingVoterProgressJsonParserImpl} from '../../../../app/services/jsonParser/votingVoterProgressJsonParserImpl.service';

describe('VotingVoterProgressService', () => {

    let service: VotingVoterProgressService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: VotingVoterProgressService , useClass: VotingVoterProgressServiceImpl },
                { provide: VotingVoterProgressJsonParser, useClass: VotingVoterProgressJsonParserImpl }
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        service = TestBed.get(VotingVoterProgressService);

    });

    it('should return false if something went wrong', async() => {
        const votingId = 42;
        const voterId = 27;

        service.getVotingVoterProgress(votingId, voterId).subscribe(s => expect(s.getData()).toBeFalsy());

        const req = httpMock.expectOne('/api/votings/' + votingId + '/progress/voters/' + voterId);
        expect(req.request.method).toEqual('GET');

        req.flush({}, { status: 500, statusText: 'Internal server error' } );

    });

    it('should return valid data', async() => {
        const votingId = 42;
        const voterId = 27;

        service.getVotingVoterProgress(votingId, voterId).subscribe(s => {
                expect(s.getData()).toBeTruthy();
                expect(s.getData().getVotingId()).toBe(27);
                expect(s.getData().getPhaseNumber()).toBe(3);
                expect(s.getData().getCreatedSecretShares()).toBeTruthy();
                expect(s.getData().getVoted()).toBeFalsy();
                expect(s.getData().getParticipatedInDecryption()).toBeFalsy();
                expect(s.getData().getUsesCustomKey()).toBeTruthy();
            }
        );

        const req = httpMock.expectOne('/api/votings/' + votingId + '/progress/voters/' + voterId);
        expect(req.request.method).toEqual('GET');

        req.flush({
            phaseNumber: 3,
            createdSecretShares: true,
            voted: false,
            participatedInDecryption: false,
            usesCustomKey: true,
            votingId: 27,
        }, { status: 200, statusText: 'OK' } );

    });

});
