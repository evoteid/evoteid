import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { VoteService } from '../../../../app/services/communication/vote/vote.service';
import { VoteServiceImpl } from '../../../../app/services/communication/vote/voteImpl.service';
import { DataForKeyCalculationJsonParser } from '../../../../app/services/jsonParser/dataForKeyCalculationJsonParser.service';
import { DataForKeyCalculationJsonParserImpl } from '../../../../app/services/jsonParser/dataForKeyCalculationJsonParserImpl.service';
import {DataForKeyCalculation} from '../../../../app/models/dataForKeyCalculation/dataForKeyCalculation.model';
import {Maybe} from '../../../../app/utils/maybe.utils';
import {SingleVote} from '../../../../app/models/vote/singleVote.json.model';
import {Votes} from '../../../../app/models/vote/votes.json.model';
import {VotesJsonParser} from '../../../../app/services/jsonParser/votesJsonParser.service';
import {VotesJsonParserImpl} from '../../../../app/services/jsonParser/votesJsonParserImpl.service';

describe('VoteServiceImpl', () => {

    let voteService: VoteService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ],
        providers: [
            { provide: VoteService , useClass: VoteServiceImpl },
            { provide: DataForKeyCalculationJsonParser, useClass: DataForKeyCalculationJsonParserImpl },
            { provide: VotesJsonParser, useClass: VotesJsonParserImpl}
          ],
      });

      httpMock = TestBed.get(HttpTestingController);
      voteService = TestBed.get(VoteService);

    });

    it('should be created', async() => {
        expect(voteService).toBeTruthy();
    });

    it('should return false if server returned an error', async() => {
        const voteId = 27;
        const voterId = 42;
        const voter = 'Homer';
        const alpha = '123';
        const beta = '456';
        const sign = 'sign';
        const vote = new SingleVote(voteId, voter, alpha, beta, sign);
        voteService.vote(voteId, voterId, vote).subscribe(successful => expect(successful.getFirst()).toBeFalsy());
        const req = httpMock.expectOne('/api/votings/' + voteId + '/votes/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toBe(vote);
        req.flush({}, { status: 400, statusText: 'Dummy error' } );
    });

    it('should return true if server returned ok', async() => {
        const voteId = 27;
        const voterId = 42;
        const voter = 'Homer';
        const alpha = '123';
        const beta = '456';
        const sign = 'sign';
        const vote = new SingleVote(voterId, voter, alpha, beta, sign);
        voteService.vote(voteId, voterId, vote).subscribe(successful => expect(successful.getFirst()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/' + voteId + '/votes/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toBe(vote);
        req.flush({}, { status: 201, statusText: 'Created' } );
    });

    it('getting votes works', async() => {
        const voteId = 27;
        const validObject = {votes:
                [
                    {
                        fromVoterId: 1,
                        fromVoterName: 'v1',
                        alpha: 'a1',
                        beta: 'b1',
                        signature: 'sign1'
                    },
                    {
                        fromVoterId: 2,
                        fromVoterName: 'v2',
                        alpha: 'a2' ,
                        beta: 'b2',
                        signature: 'sign2'
                    }
                    ]
        };

        let maybeVotes: Maybe<Votes>;

        voteService.getVotes(voteId).subscribe(mVotes => maybeVotes = mVotes);
        const req = httpMock.expectOne('/api/votings/' + voteId + '/votes');
        expect(req.request.method).toEqual('GET');

        req.flush(validObject, { status: 200, statusText: 'Ok' });
        expect(maybeVotes.getData().getVotes().length).toBe(2);
    });

    it('returns empty if invalid', async() => {
        const voteId = 27;
        const inValidObject = {nope: 'notValid'};

        let maybeVotes: Maybe<Votes>;

        voteService.getVotes(voteId).subscribe(mVotes => maybeVotes = mVotes);
        const req = httpMock.expectOne('/api/votings/' + voteId + '/votes');
        expect(req.request.method).toEqual('GET');

        req.flush(inValidObject, { status: 200, statusText: 'Ok' });
        expect(maybeVotes.isEmpty()).toBeTruthy();
    });

    it('returns empty if invalid status code', async() => {
        const voteId = 27;
        const validObject = {votes: [{fromVoterId: 1, fromVoterName: 'v1', alpha: 'a1' , beta: 'b1'}, {fromVoterId: 2, fromVoterName: 'v2', alpha: 'a2' , beta: 'b2'}]};

        let maybeVotes: Maybe<Votes>;

        voteService.getVotes(voteId).subscribe(mVotes => maybeVotes = mVotes);
        const req = httpMock.expectOne('/api/votings/' + voteId + '/votes');
        expect(req.request.method).toEqual('GET');

        req.flush(validObject, { status: 500, statusText: 'Internal server error' });
        expect(maybeVotes.isEmpty()).toBeTruthy();
    });

    it('getting data for key calculation works', async() => {
        const validObject = {
            eligibleVoters:
                [
                    {
                        name: 'name1',
                        id: 1,
                        position: 0,
                        publicKey: 'key1',
                        isTrustee: true,
                        canVote: true,
                    },
                    {
                        name: 'name2',
                        id: 2,
                        position: 1,
                        publicKey: 'key2',
                        isTrustee: true,
                        canVote: false,
                    },
                    {
                        name: 'name3',
                        id: 3,
                        position: 2,
                        publicKey: 'key3',
                        isTrustee: false,
                        canVote: false,
                    }
                ],
            calculationParameters:
                {
                    primeQ: '10',
                    primeP: '11',
                    generatorG: '12'
                },
            securityThreshold: 27,
            numberOfEligibleVoters: 3
        };
        const voteId = 27;

        let maybeDataForKeyCalculation: Maybe<DataForKeyCalculation>;

        voteService.getDataForKeyCalculation(voteId).subscribe(s => maybeDataForKeyCalculation = s);
        const req = httpMock.expectOne('/api/votings/' + voteId + '/keyCalculationData');
        expect(req.request.method).toEqual('GET');

        req.flush(validObject, { status: 200, statusText: 'Ok' });
        expect(maybeDataForKeyCalculation.getData().getCalculationParameters().getPrimeQ()).toBe('10');
        expect(maybeDataForKeyCalculation.getData().getCalculationParameters().getPrimeP()).toBe('11');
        expect(maybeDataForKeyCalculation.getData().getCalculationParameters().getGeneratorG()).toBe('12');
        expect(maybeDataForKeyCalculation.getData().getSecurityThreshold()).toBe(27);
        expect(maybeDataForKeyCalculation.getData().getNumberOfEligibleVoters()).toBe(3);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters().length).toBe(3);

        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[0].getPublicKey()).toBe('key1');
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[0].getId()).toBe(1);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[0].getPosition()).toBe(0);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[0].getName()).toBe('name1');
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[0].getIsTrustee()).toBeTruthy();
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[0].getCanVote()).toBeTruthy();

        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[1].getPublicKey()).toBe('key2');
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[1].getId()).toBe(2);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[1].getPosition()).toBe(1);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[1].getName()).toBe('name2');
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[1].getIsTrustee()).toBeTruthy();
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[1].getCanVote()).toBeFalsy();

        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[2].getPublicKey()).toBe('key3');
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[2].getId()).toBe(3);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[2].getPosition()).toBe(2);
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[2].getName()).toBe('name3');
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[2].getIsTrustee()).toBeFalsy();
        expect(maybeDataForKeyCalculation.getData().getEligibleVoters()[2].getCanVote()).toBeFalsy();
    });
});
