import { AsymmetricKey } from './../../../app/data/asymmetricKey';
import { async } from '@angular/core/testing';
import { CryptoServiceImpl } from './../../../app/services/crypto/cyptoImpl.service';
import { CryptoService } from 'src/app/services/crypto/crypto.service';

describe('CryptoImplService', () => {

    let cryptoService: CryptoService;
    cryptoService = new CryptoServiceImpl();

    const publicKey =
    '-----BEGIN PUBLIC KEY-----\
    MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHoX//s4fQlC0otOI3R5G8Li2Q9T\
    5WcnYpx1YJo7WyuIIsSDWW7PcATtMC+FZHgQ8abrLEZ66PfnBANlGRea8489m93L\
    xj1cBU1VL2np1yDeCD0rrzBiStGRlC+HucjSZF1QegM9zNQ8LVMkIgerY6xwrhBg\
    uE8LUY3I9d091wrXAgMBAAE=\
    -----END PUBLIC KEY-----';

    const privateKey =
    '-----BEGIN RSA PRIVATE KEY-----\
    MIICWgIBAAKBgHoX//s4fQlC0otOI3R5G8Li2Q9T5WcnYpx1YJo7WyuIIsSDWW7P\
    cATtMC+FZHgQ8abrLEZ66PfnBANlGRea8489m93Lxj1cBU1VL2np1yDeCD0rrzBi\
    StGRlC+HucjSZF1QegM9zNQ8LVMkIgerY6xwrhBguE8LUY3I9d091wrXAgMBAAEC\
    gYATQAd3N5/XPvbtSeOjMJkk8BljJkosKnSM0KDrkGI+iddZfpAyPXie/jqzs2d0\
    2s8PD1NKxYRH2d+zHVSIeUDfZAmPw+Qw3cDOIiv8JzqcIS/t2UXJTM3uGAp4LhBS\
    z2+dYtS/wBZLAXsoCMEOvy7dtytFywB5CEKV/0siun2AoQJBALK4Yg64cp4epxFW\
    tnScAZGW7caUTkZF3Xhu6+WdoQnGdJYOTBsYnmyZRP7wlw2fybH5gNub/jxrXWDV\
    tkrZCDECQQCu40pAaXcnAS+xRlKCEfyktub2nhopHp93sp/02JKFMYw1FBJiy7go\
    Xi6A7sILs4+/cuhn5PFU/ateNlmZOgmHAkBVZXsdU48Y9MaZp1hpYb3yJi8ZivXX\
    6DxJ9p5rSiWPuS8uIEjHTb0tYRMxyh4zWV2T2Ad1aBVUI20r/xIqs4eRAkABOOFp\
    9dK5WQ7GRGGLOyjy1bxjeNNh+i7PHVZ01Zagi6oZLNy8CARvc8kLaZ+9iTG1+s2Y\
    /Eabe3JZJDQYGAm9AkAEzosvf0yEXUsS7DLyzhB6WPFaOi8VMPvFt3az7Wli78+m\
    Vzyqow2PpOG/hVB8MnHOPMJ/B+0PRyj1Xq7kDIfk\
    -----END RSA PRIVATE KEY-----';

    it('should generate', async(() => {
        expect(cryptoService).toBeTruthy();
    }));

    it('should be able to encrypt in a correct way', () => {
        const message = 'dummyMessage';
        const cipherText = cryptoService.encrypt(publicKey, message);
        const clearText = cryptoService.decrypt(privateKey, cipherText);
        expect(clearText.isEmpty()).toBeFalsy();
        expect(clearText.getData()).toBe(message);
    });

    it('encrypt with invalid key returns empty', () => {
        const message = 'dummyMessage';
        const cipherText = cryptoService.encrypt(publicKey, message);
        const clearText = cryptoService.decrypt('invalidKey', cipherText);
        expect(clearText.isEmpty()).toBeTruthy();
    });

    it('should be able to decrypt', () => {
        const cipherText = 'bWZ+Rl3erWD95K5C6cg0TV7qwXlOB8yWxpAY6cCgajfitze2pdypGter3Hxpewt6D9FXtqdiXVsv3Qa8yVTNF7ZkHwZd8gWGXlX98C13vX/mc8mmeOWsxWwfwkbcdGnOoYhFmNUmA/wuZC/HoQ9OuS9pJq5p7509UH2yY31PiUg=';
        const clearText = cryptoService.decrypt(privateKey, cipherText);
        const expectedClearText = 'dummyMessage';
        expect(clearText.isEmpty()).toBeFalsy();
        expect(clearText.getData()).toBe(expectedClearText);
    });

    it('invalid key format decrypt returns empty maybe', () => {
        const cipherText = 'dummy';
        const invalidKey = 'invalidKey';
        const clearText = cryptoService.decrypt(invalidKey, cipherText);
        expect(clearText.isEmpty()).toBeTruthy();
    });

    it('should calculate right signature', () => {
        const expectedSignatureValue =
        `0eccb502da3425b6b770f1b0bffdd9ffa9e15ad0f36f6de98e1dcde5982dd4c8\
        c44b59fbd1e7fc398d032ac29bda8020ea2aaf5aab2af628612541a2c7858d0f\
        2e15eefd6e6d8b8679177a3407228f6103330f4baa1af223a819c9c9a785e467\
        9155b42796debfb5e4f5af8a69e64ceddac3b28acd353e6935b033f350b22050`.replace(/ /g, '');

        const signature = cryptoService.signSHA256(privateKey, 'someMessage').getData();
        const hexEncoding = Buffer.from(signature, 'base64').toString('hex');

        expect(hexEncoding).toBe(expectedSignatureValue);
    });

    it('should return true if signature is valid', () => {
        const message = 'otherMessage';
        const validHexSignature =
        `784e1f175cc4c7ed429375c0ff1e82d3600acf9061afc082a619955b2a96ba31\
        22dfcaf18b3fc47ba355d03be737a6dc5638684aa0b1e536de20da1a5113b9c7\
        d7ed4c8d7d22c1478491b62401e4918da435a25aaf6359c266fcc3ec7ca56cf4\
        b1394a8df875a30ed62f100275099b1d8c312b0b93ebe3d367344d1988efa301`.replace(/ /g, '');

        const validBase64Signature = Buffer.from(validHexSignature.toString(), 'hex').toString('base64');

        expect(cryptoService.verifySHA256Signature(publicKey, message, validBase64Signature)).toBeTruthy();
    });

    it('should return true if signature is valid', () => {
        const message = 'otherMessage';
        const validHexSignature =
        `2de355fa10e5feeddda1cdffc77acd46055222e6b7e2f30b813c7a673b9a544e\
        941504c794045925a71065c87b0740969b0a279d47532342bb5c55863cef0c9e\
        e9c1e2c20a1cb2646e05c09ba7852f536542db6e3cf4e76ac33c083869bd7689\
        c58a8bcb8240d48760be817073d33a12c42480bbfd58976083d61005acf4bd7f`.replace(/ /g, '');

        const validBase64Signature = Buffer.from(validHexSignature.toString(), 'hex').toString('base64');

        expect(cryptoService.verifySHA256Signature(publicKey, message, validBase64Signature)).toBeFalsy();
    });

    it('should generate valid key',  function (done) {
        cryptoService.generateKeyPair()
        .then(keyPair => expect(keyPair.isEmpty()).toBeFalsy())
        .then(done);
    });

    it('should verify valid key pair', () => {
        const validKeyPair = new AsymmetricKey(publicKey, privateKey);
        expect(cryptoService.validateKeyPair(validKeyPair)).toBeTruthy();
    });

    it('should negate invalid key pair', () => {
        const validKeyPair = new AsymmetricKey(publicKey, 'invalidKey');
        expect(cryptoService.validateKeyPair(validKeyPair)).toBeFalsy();
    });

    it('should not verify invalid key pair', () => {
        const notMatchingPrivateKey =
        '-----BEGIN RSA PRIVATE KEY-----\
        MIICXgIBAAKBgQC6VSOQsUDLIUJPWK6cYqApN4kjbCtLrvteuviFMhr5CCU8lDc3\
        zqkO4ULB7FsvWQBX7nsI1dzaUXzk6goQtoC55MvNw0fHGNg+SHeSAL474o9QINcx\
        18rIaxjPmw+KJ7x+yqUaQ0EqIAJ9fqWnzk2hZQ8XRj1SsjZDowiSsUY9ZwIDAQAB\
        AoGAeEhxlvv1eatUpOf4f9di0HibPJSdNlQUzMghmcoIg23wq7R6GLp9g6+CarDr\
        BBuODzVRkxiLYxtrZlJ2gOdvHHRylmIUg0pCdh362FPid0llFj/2Lpc74cxgXeCf\
        5+TpGW645Wl+78fgAjZoYX/5cjzppYqzaNeq9aWg+PuyvpECQQD7xZg58RleRhQY\
        0kP2xj434M4DtMQg3Rc7g/JuJrSXxiPcDB+yRB2pYWgtCrDgcuIW4Yw0oUtwTAp8\
        JYIiBzbLAkEAvXY1/gkhgcCM9mJHyiw0ihaBZ9CvsBBJT2vxzclmwsYdBIl8rQPq\
        QeOAC+R5eXkXr7Lum63MG2T+PxZ70XWkVQJBAL+NuKHs48YsP0IbSAZLEi9lZJjB\
        Qa9XR0PKwfo4jJIk+EeFGeCBnqeuKpqKVcSO46FQ/WFVjpXj5N5Lk1TcdD0CQQC5\
        FP/NuPHIpWePUZc8A08YDsA/wHfAkUhsk9DV7Ye7gnnacs0e35I655mxBHM+nXLs\
        Kuo5nuCjaleNHdO7XSc5AkEA36fMaGjCy1q9Bs6thDEdMKidS5GZHTT24yhC6sCP\
        2XwDUvswMSwIdCDfU6YnH0rmhxLpSwqBnJLUeE0/HP77/g==\
        -----END RSA PRIVATE KEY-----';

        const key = new AsymmetricKey(publicKey, notMatchingPrivateKey);
        expect(cryptoService.validateKeyPair(key)).toBeFalsy();
    });

    it('should generate keypair that can be validated', function (done) {
        cryptoService.generateKeyPair()
        .then(keyPair => expect(cryptoService.validateKeyPair(keyPair.getData())).toBeTruthy())
        .then(done);
    });

    it('should be able to generate sha256 hash from pem', () => {
        const test = 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC9FDf42ytnWM79xfc6aq9neiWfwO29ZSxC5A8dEiykDPsRIdq2i7Q0eS5UTFBmAofb8uy7SGqB+IiKCIss5ij2HW5mUlqtz7IYmNNWyeKYdUDw0OnXFCJ4eio2J5O+TRB3DcqKVgDKpZTiV4dVuJjh6q6YJP/bssdPYrGfsiY+IhvdnmHa5GbAxiPJKWcun/6/8Y6yGC3u21HTngXqveeM88qmmsjIzkeDdtA5nibtTTwkeyV2x/4lthWJDHngxD2TPUbqMF9nEPxXohOSjkxLMuD2xoRT9ETrEMOrQMM1hkP5GmGpeZy8MSw8JqytRQ2DDNxG1011WM2C/Kv0Yh1D';
        const expectedResult = 'ttPIdAOyaysKUURlG5gOMN60JPw2fg2qRZyO2FT0yRI';
        expect(cryptoService.pemToSHA256Fingerprint(test)).toBe(expectedResult);
    });

    it('should be able to generate sha256 hash from pem', () => {
        const test = 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDE82adQq7g9ure1ehIcLNwIcB7Y6OR7OkLUrB5odNdiFOZjbNID629CIwfkWUerI5Ytq0tPLlybmavnuOLvmTwSb/GZu2neEQ8f0mdG6EPf+/VSe5wdKU7tDnoxTfSfR4YSsG5qEfdM0YTfEml9HBcZ4DD4AMh/8AVd2t+R1BwMqSUKW95UAygbnkDslO/yaWsh3S3YCVF+0w6tso4oQjy4cDoQDG591BrsTfy6culHYP8H+Dtzv3CUZLCmQguzWoXDdlkTJcIisINwicT64D/D4bou4skYMQCHDnaiT7K1HriD437J2jyf8x1+rJs3spIn2Hdub+hKOIpkFwz0wVZ';
        const expectedResult = 'C2lGCEKiXJeNxOX6P8CaKr2KIPpY/FYX/SzfHoeEc14';
        expect(cryptoService.pemToSHA256Fingerprint(test)).toBe(expectedResult);
    });

    it('should be able to generate sha256 hash from pem with header', () => {
        const test = '-----BEGIN PUBLIC KEY-----\r\nAAAAB3NzaC1yc2EAAAADAQABAAABAQDE82adQq7g9ure1ehIcLNwIcB7Y6OR7OkLUrB5odNdiFOZjbNID629CIwfkWUerI5Ytq0tPLlybmavnuOLvmTwSb/GZu2neEQ8f0mdG6EPf+/VSe5wdKU7tDnoxTfSfR4YSsG5qEfdM0YTfEml9HBcZ4DD4AMh/8AVd2t+R1BwMqSUKW95UAygbnkDslO/yaWsh3S3YCVF+0w6tso4oQjy4cDoQDG591BrsTfy6culHYP8H+Dtzv3CUZLCmQguzWoXDdlkTJcIisINwicT64D/D4bou4skYMQCHDnaiT7K1HriD437J2jyf8x1+rJs3spIn2Hdub+hKOIpkFwz0wVZ\r\n-----END PUBLIC KEY-----';
        const expectedResult = 'C2lGCEKiXJeNxOX6P8CaKr2KIPpY/FYX/SzfHoeEc14';
        expect(cryptoService.pemToSHA256Fingerprint(test)).toBe(expectedResult);
    });

    it('should be able to generate primes p and q', function (done) {
        const BigInteger = require('jsbn').BigInteger;

        cryptoService.generateCalculationParams().then(calcParams => {
            expect(new BigInteger(calcParams.getData().getPrimeP()).isProbablePrime()).toBeTruthy();
            expect(new BigInteger(calcParams.getData().getPrimeQ()).isProbablePrime()).toBeTruthy();
            expect(calcParams.getData().getGeneratorG()).toBe('65537');
        })
        .then(done);
    });

    it('hash sha256 works', () => {
        const message = 'dummyMessage';
        const expectedHash = '50b3df0a8670d2469f5003ae039e66ce3abb39aa70317d2e401984cb78d2e38c';
        const calculatedHash = cryptoService.hashSHA256(message);
        expect(calculatedHash).toBe(expectedHash);
    });

});
