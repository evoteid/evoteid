import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../../app/services/localStorage/localStorage.service';
import { Maybe } from '../../../app/utils/maybe.utils';

@Injectable()
export class NoopLocalStorageService implements LocalStorageService {

    get(key: string): Maybe<string> {
        return Maybe.fromEmpty();
    }

    upsert(key: string, value: string) {

    }

    delete(key: string): boolean {
        return false;
    }
}
