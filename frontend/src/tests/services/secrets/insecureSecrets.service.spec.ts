import { SecretsService } from 'src/app/services/secrets/secrets.service';
import { InsecureSecretsService } from 'src/app/services/secrets/insecureSecrets.service';

describe('InsecureSecretsService', () => {

    let secretsService: SecretsService;

    beforeEach(() => {
        secretsService = new InsecureSecretsService();
    });

    it('should create', () => {
        expect(secretsService).toBeTruthy();
    });

    it('not stored value should return empty', () => {
        expect(secretsService.getSecret('dummy').isEmpty()).toBeTruthy();
    });

    it('store works', () => {
        const key = 'dummy';
        const secret = 'verySecret';
        secretsService.saveSecret(key, secret);
        expect(secretsService.getSecret(key).getData()).toBe(secret);
    });

    it('update works', () => {
        const key = 'dummy';
        const secret = 'verySecret';
        secretsService.saveSecret(key, 'other');
        secretsService.saveSecret(key, secret);
        expect(secretsService.getSecret(key).getData()).toBe(secret);
    });

});
