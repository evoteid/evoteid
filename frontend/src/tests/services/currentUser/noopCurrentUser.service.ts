import { CurrentUserService } from '../../../app/services/currentUser/currentUser.service';
import { Maybe } from 'src/app/utils/maybe.utils';

export class NoopCurrentUserService implements CurrentUserService {

    isCurrentUserLoggedIn(): boolean {
        return false;
    }

    getUserId(): Maybe<number> {
        return Maybe.fromEmpty();
    }

    getPublicKey(): Maybe<string> {
        return Maybe.fromEmpty();
    }

    getVoterName(): Maybe<string> {
        return Maybe.fromEmpty();
    }

    getRoles(): Maybe<string[]> {
        return Maybe.fromEmpty();
    }

    isCurrentUserAdmin(): boolean {
        return false;
    }

    isCurrentUserRegistrar(): boolean {
        return false;
    }

    isCurrentUserVotingAdministrator(): boolean {
        return false;
    }

    isCurrentUserRegistered(): boolean {
        return false;
    }

    setIsCurrentUserRegistered(isCurrentUserRegistered: boolean) {
    }

    isCurrentUserTrusteeAtTheMoment(): boolean {
        return false;
    }
}
