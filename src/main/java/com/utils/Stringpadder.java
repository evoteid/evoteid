package com.utils;

import java.util.Arrays;

public class Stringpadder {

    private Stringpadder() {
    }

    public static String pad(String s,int length) {
        String pad = repeatN(' ',length);

        String[] splitted = s.split(System.lineSeparator());
        StringBuilder stringBuilder = new StringBuilder();
        for(String line : splitted) {
            stringBuilder.append(pad+line+System.lineSeparator());
        }
        String ret = stringBuilder.toString();
        return ret.substring(0,ret.length()-1);
    }

    private static final String repeatN(char c, int length) {
        char[] data = new char[length];
        Arrays.fill(data, c);
        return new String(data);
    }
}
