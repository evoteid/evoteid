package com.init;

import com.data.voter.SingleVoter;
import com.enums.Role;
import com.services.data.roles.RoleService;
import com.services.data.voters.VotersService;
import com.services.environment.EnvironmentService;
import com.services.secrets.SecretsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Optional;

@Component
public class Initializer {

    private final Environment env;

    private final VotersService votersService;

    private final RoleService roleService;

    private final SecretsService secretsService;

    private final EnvironmentService environmentService;

    @Autowired
    public Initializer(Environment env, VotersService votersService, RoleService roleService, SecretsService secretsService, EnvironmentService environmentService) {
        this.env = env;
        this.votersService = votersService;
        this.roleService = roleService;
        this.secretsService = secretsService;
        this.environmentService = environmentService;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void setup() throws NoSuchAlgorithmException {
        if(env.getProperty("mode") != null && env.getProperty("mode").equals("demo")) {
            this.environmentService.setIsDemoEnvironment(true);
        }

        if(!env.getProperty("isRunningTest").equals("true")) {
            this.votersService.createVoter("admin");

            Optional<SingleVoter> optAdminVoter = this.votersService.getVoter("admin");
            if(optAdminVoter.isPresent()) {
                Long adminId = optAdminVoter.get().getId();
                this.roleService.addRoleToVoter(adminId,Role.ADMINISTRATOR);
            }

            byte[] randomBytes = new byte[64];
            SecureRandom.getInstanceStrong().nextBytes(randomBytes);
            this.secretsService.storeJwtSigningKey(randomBytes);
        }
    }
}
