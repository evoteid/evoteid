package com.interceptors;

import com.data.voter.SingleVoter;
import com.services.context.RequestContextHolder;
import com.services.data.voters.VotersService;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCreatorService;
import com.strings.Strings;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
@Qualifier("AuthenticationInterceptor")
public class AuthenticationInterceptor implements HandlerInterceptor {

    private static final String UNPROTECTED_URL = "/api/authenticate";

    private static final String CURRENT_VOTER_URL = "/api/currentVoterToken";

    private static final String REGISTER_CURRENT_VOTER_URL = "/api/registerCurrentVoter";

    private static final String TOKEN_PREFIX = "Bearer ";

    private final JwtTokenCoderService jwtTokenCoderService;

    private final JwtTokenCreatorService jwtTokenCreatorService;

    private final VotersService votersService;

    private final RequestContextHolder requestContextHolder;

    @Autowired
    public AuthenticationInterceptor(
            JwtTokenCoderService jwtTokenCoderService,
            JwtTokenCreatorService jwtTokenCreatorService,
            VotersService votersService,
            RequestContextHolder requestContextHolder) {
        this.jwtTokenCoderService = jwtTokenCoderService;
        this.jwtTokenCreatorService = jwtTokenCreatorService;
        this.votersService = votersService;
        this.requestContextHolder = requestContextHolder;
    }

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        Object homeOrganization = request.getAttribute("homeOrganization");
        Object emailAddress = request.getAttribute("mail");
        Object givenName = request.getAttribute("givenName");
        Object surname = request.getAttribute("surname");
        Object principalName = request.getAttribute("principalName");
        Object uniqueID = request.getAttribute("uniqueID");

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        System.out.println("Access:");
        System.out.println(formatter.format(date));
        System.out.println(homeOrganization.toString());
        System.out.println(emailAddress.toString());
        System.out.println(givenName.toString());
        System.out.println(surname.toString());
        System.out.println(principalName.toString());
        System.out.println(uniqueID.toString());

        if(homeOrganization == null) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write("Can't determine which organization the voter belongs to.");
            return false;
        }

        String homeOrganizationName = homeOrganization.toString();

        if(!Strings.EXAMPLE_COM_NAME.toString().equals(homeOrganizationName)) {
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.getWriter().write("At the moment only members from ETH Zurich can access the tool.");
            return false;
        }

        if(emailAddress == null) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write("Can't determine the e-mail address of the voter.");
            return false;
        }

        String email = emailAddress.toString();
        int endOfId = email.indexOf('@');
        if(endOfId == -1) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write("The mail address of voter is invalid.");
            return false;
        }

        if(givenName == null || surname == null) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write("Can't determine the name voter.");
            return false;
        }

        if(principalName == null || uniqueID == null) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write("Can't determine voter id.");
            return false;
        }

        if(hasAdminToken(request)) {
            Optional<String> optNewToken = jwtTokenCreatorService.createTokenForUser("admin");
            if(optNewToken.isPresent()) {
                String newToken = optNewToken.get();
                Optional<SingleVoter> optionalSingleVoter = jwtTokenCoderService.getVoterFromToken(newToken);
                if(!optionalSingleVoter.isPresent()) {
                    return false;
                }
                SingleVoter singleVoter = optionalSingleVoter.get();
                requestContextHolder.setCurrentVoter(singleVoter);

                response.addHeader("RefreshToken",newToken);
                return true;
            }
        }

        if(request.getRequestURI().equals(UNPROTECTED_URL)) {
            return true;
        }

        String voterName = givenName + " " + surname;
        Optional<SingleVoter> optSingleVoter = votersService.upsertVotersData(uniqueID.toString(),voterName,email);
        if(!optSingleVoter.isPresent()) {
            if(request.getRequestURI().equals(CURRENT_VOTER_URL) || request.getRequestURI().equals(REGISTER_CURRENT_VOTER_URL)) {
                this.requestContextHolder.setCurrentVoter(new SingleVoter()
                        .setName(voterName)
                        .setEmail(email)
                        .setUniqueId(uniqueID.toString()));
                return true;
            }
            else {
                response.setStatus(HttpStatus.FORBIDDEN.value());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("voterName", voterName);
                response.getWriter().write(jsonObject.toString());
                response.setHeader("ErrorMessage", "The voter is not registered yet.");
                return false;
            }
        }

        SingleVoter singleVoter = optSingleVoter.get();

        Optional<String> optToken = jwtTokenCreatorService.createTokenForUser(singleVoter.getId());
        if(!optToken.isPresent()) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setHeader("ErrorMessage", "The voter is not registered yet.");
            return false;
        }

        String newToken = optToken.get();
        requestContextHolder.setCurrentVoter(singleVoter);
        response.addHeader("RefreshToken",newToken);
        return true;
    }

    private boolean hasAdminToken(HttpServletRequest request) {
        String token = request.getHeader("authorization");
        if(token == null) {
            return false;
        }

        if(token.startsWith(TOKEN_PREFIX)) {
            token = token.substring(TOKEN_PREFIX.length());
        }

        Optional<SingleVoter> voterOptional = jwtTokenCoderService.getVoterFromToken(token);
        if(!voterOptional.isPresent()) {
            return false;
        }

        SingleVoter voter = voterOptional.get();

        return "admin".equals(voter.getName());
    }

}
