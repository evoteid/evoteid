package com.exceptions;

public class DuplicateException extends IllegalVoteException{

    public DuplicateException(String message) {
        super(message);
    }
}
