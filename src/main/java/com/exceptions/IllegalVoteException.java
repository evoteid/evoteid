package com.exceptions;

public abstract class IllegalVoteException extends Exception {

    private final String message;

    public IllegalVoteException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
