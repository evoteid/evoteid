package com.exceptions.parser;

public class InvalidFieldFormatException extends ParserException {

    public InvalidFieldFormatException(String message) {
        super(message);
    }

    public InvalidFieldFormatException(String message, Exception e) {
        super(message,e);
    }
}
