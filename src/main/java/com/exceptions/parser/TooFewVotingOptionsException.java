package com.exceptions.parser;

public class TooFewVotingOptionsException extends ParserException {
    public TooFewVotingOptionsException(String message) {
        super(message);
    }
}
