package com.exceptions.parser;

public abstract class ParserException extends Exception {

    private final String message;
    private final Exception exception;

    public ParserException(String message) {
        super(message);
        this.message = message;
        this.exception = null;
    }

    public ParserException(String message, Exception exception) {
        super(message,exception);
        this.message = message;
        this.exception = exception;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Exception getException() {
        return exception;
    }
}
