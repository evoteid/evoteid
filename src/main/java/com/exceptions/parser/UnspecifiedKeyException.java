package com.exceptions.parser;

public class UnspecifiedKeyException extends ParserException {
    public UnspecifiedKeyException(String message) {
        super(message);
    }
}