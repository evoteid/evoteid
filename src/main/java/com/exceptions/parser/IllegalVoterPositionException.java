package com.exceptions.parser;

public class IllegalVoterPositionException extends ParserException{
    public IllegalVoterPositionException(String message) {
        super(message);
    }
}
