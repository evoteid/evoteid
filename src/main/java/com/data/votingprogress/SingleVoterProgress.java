package com.data.votingprogress;

import com.data.JsonMappable;
import com.data.voter.SingleVoter;
import org.json.JSONObject;

public class SingleVoterProgress implements JsonMappable {

    private long position;

    private SingleVoter voter;

    private boolean participatedInKeyGeneration;

    private boolean participatedInVoting;

    private boolean participatedInDecryption;

    private boolean isTrustee;

    private boolean canVote;

    private boolean createdCustomKey;

    public SingleVoterProgress setPosition(long position) {
        this.position = position;
        return this;
    }

    public SingleVoterProgress setVoter(SingleVoter voter) {
        this.voter = voter;
        return this;
    }

    public SingleVoterProgress setParticipatedInKeyGeneration(boolean participatedInKeyGeneration) {
        this.participatedInKeyGeneration = participatedInKeyGeneration;
        return this;
    }

    public SingleVoterProgress setParticipatedInVoting(boolean participatedInVoting) {
        this.participatedInVoting = participatedInVoting;
        return this;
    }

    public SingleVoterProgress setParticipatedInDecryption(boolean participatedInDecryption) {
        this.participatedInDecryption = participatedInDecryption;
        return this;
    }

    public long getPosition() {
        return position;
    }

    public SingleVoter getVoter() {
        return voter;
    }

    public boolean getParticipatedInKeyGeneration() {
        return participatedInKeyGeneration;
    }

    public boolean getParticipatedInVoting() {
        return participatedInVoting;
    }

    public boolean getParticipatedInDecryption() {
        return participatedInDecryption;
    }

    public boolean getIsTrustee() {
        return isTrustee;
    }

    public SingleVoterProgress setIsTrustee(boolean isTrustee) {
        this.isTrustee = isTrustee;
        return this;
    }

    public boolean getCanVote() {
        return canVote;
    }

    public SingleVoterProgress setCanVote(boolean canVote) {
        this.canVote = canVote;
        return this;
    }

    public boolean getCreatedCustomKey() {
        return createdCustomKey;
    }

    public SingleVoterProgress setCreatedCustomKey(boolean createdCustomKey) {
        this.createdCustomKey = createdCustomKey;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("position", position)
                .put("voterName", voter.getName())
                .put("participatedInKeyGeneration", participatedInKeyGeneration)
                .put("participatedInVoting", participatedInVoting)
                .put("participatedInDecryption", participatedInDecryption)
                .put("isTrustee",isTrustee)
                .put("canVote", canVote)
                .put("usesCustomKey", createdCustomKey);
    }
}
