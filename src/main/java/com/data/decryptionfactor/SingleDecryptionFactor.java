package com.data.decryptionfactor;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;


public class SingleDecryptionFactor implements JsonMappable {

    @JsonProperty("decryptionFactor")
    private String decryptionFactor;

    @JsonProperty("signature")
    private String signature;

    private Long fromVoterId = null;

    private String fromVoterName = null;

    private long fromVoterPosition;

    public Long getFromVoterId() {
        return fromVoterId;
    }

    public SingleDecryptionFactor setFromVoterId(Long fromVoterId) {
        this.fromVoterId = fromVoterId;
        return this;
    }

    public String getFromVoterName() {
        return fromVoterName;
    }

    public SingleDecryptionFactor setFromVoterName(String fromVoterName) {
        this.fromVoterName = fromVoterName;
        return this;
    }

    public String getDecryptionFactor() {
        return decryptionFactor;
    }

    public SingleDecryptionFactor setDecryptionFactor(String decryptionFactor) {
        this.decryptionFactor = decryptionFactor;
        return this;
    }

    public Long getFromVoterPosition() {
        return fromVoterPosition;
    }

    public SingleDecryptionFactor setFromVoterPosition(long fromVoterPosition) {
        this.fromVoterPosition = fromVoterPosition;
        return this;
    }

    public String getSignature() {
        return signature;
    }

    public SingleDecryptionFactor setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("fromVoterId",fromVoterId)
                .put("fromVoterName",fromVoterName)
                .put("decryptionFactor",decryptionFactor)
                .put("fromVoterPosition",fromVoterPosition)
                .put("signature",signature);
    }
}
