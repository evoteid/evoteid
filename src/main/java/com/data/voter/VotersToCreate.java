package com.data.voter;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class VotersToCreate {

    @JsonProperty("voters")
    List<SingleVoter> voters = new ArrayList<>();

    @JsonProperty("createKeys")
    Boolean createKeys;

    public List<SingleVoter> getVoters() {
        return voters;
    }

    public Boolean doCreateKeys() {
        return createKeys;
    }
}
