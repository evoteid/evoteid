package com.data.secretshares;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SecretSharesFromVoter {

    @JsonProperty("fromVoterId")
    private Long fromVoterId;

    @JsonProperty("fromVoterName")
    private String fromVoterName;

    @JsonProperty("secretShares")
    private List<SingleSecretShareFromVoter> allShares;

    public SecretSharesFromVoter() {
        allShares = new ArrayList<>();
    }

    public SecretSharesFromVoter setFromVoterId(long fromVoterId) {
        this.fromVoterId = fromVoterId;
        return this;
    }

    public SecretSharesFromVoter setFromVoterName(String fromVoterName) {
        this.fromVoterName = fromVoterName;
        return this;
    }

    public Long getFromVoterId() {
        return fromVoterId;
    }

    public String getFromVoterName() {
        return fromVoterName;
    }

    public SecretSharesFromVoter addSingleSecretShare(SingleSecretShareFromVoter share) {
        allShares.add(share);
        return this;
    }

    public List<SingleSecretShareFromVoter> getAllShares() {
        return allShares;
    }
}
