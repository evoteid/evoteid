package com.data.votingvoterprogress;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VotingsVoterProgress implements JsonMappable {

    private List<VotingVoterProgress> votingsVoterProgressList;

    public VotingsVoterProgress() {
        votingsVoterProgressList = new ArrayList<>();
    }

    public VotingsVoterProgress addVotingProgress(VotingVoterProgress progress) {
        this.votingsVoterProgressList.add(progress);
        return this;
    }

    public List<VotingVoterProgress> getVotingsVoterProgress() {
        return this.votingsVoterProgressList;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject().put("votingsVoterProgress",new JSONArray(votingsVoterProgressList
                .stream()
                .map(VotingVoterProgress::toJson)
                .collect(Collectors.toList())));
    }
}
