package com.data.votings;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class VotingOption implements JsonMappable {

    @JsonProperty("optionName")
    private String optionName;

    @JsonProperty("optionPrimeNumber")
    private Long optionPrimeNumber;

    public VotingOption setOptionName(String optionName) {
        this.optionName = optionName;
        return this;
    }

    public String getOptionName() {
        return this.optionName;
    }

    public Long getOptionPrimeNumber() {
        return optionPrimeNumber;
    }

    public VotingOption setOptionPrimeNumber(long optionPrimeNumber) {
        this.optionPrimeNumber = optionPrimeNumber;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("optionName",this.optionName)
                .put("optionPrimeNumber",this.optionPrimeNumber);
    }
}
