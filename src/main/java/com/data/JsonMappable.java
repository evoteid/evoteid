package com.data;

import org.json.JSONObject;

public interface JsonMappable {

    JSONObject toJson();
}
