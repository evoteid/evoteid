package com.data.proofs;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VoteNIZKProofs implements JsonMappable {

    private long fromVoterId;

    private String fromVoterName;

    @JsonProperty("proofs")
    List<SingleVoteNIZKProof> singleVoteNIZKProofs = new ArrayList<>();

    public VoteNIZKProofs addProof(SingleVoteNIZKProof proof) {
        singleVoteNIZKProofs.add(proof);
        return this;
    }

    public List<SingleVoteNIZKProof> getSingleVoteNIZKProofs() {
        return singleVoteNIZKProofs;
    }

    public VoteNIZKProofs setFromVoterId(long fromVoterId) {
        this.fromVoterId = fromVoterId;
        return this;
    }

    public long getFromVoterId() {
        return fromVoterId;
    }

    public VoteNIZKProofs setFromVoterName(String fromVoterName) {
        this.fromVoterName = fromVoterName;
        return this;
    }

    public String getFromVoterName() {
        return fromVoterName;
    }

    @Override
    public JSONObject toJson() {
        JSONArray jsonArray = new JSONArray();
        for(SingleVoteNIZKProof proof : singleVoteNIZKProofs) {
            jsonArray.put(proof.toJson());
        }
        return new JSONObject()
                .put("proofs",jsonArray)
                .put("fromVoterId",fromVoterId)
                .put("fromVoterName",fromVoterName);
    }
}
