package com.models.user;

import java.util.Optional;

public interface UserRepositoryCustom {

    Optional<UserEntity> findOneByUsernameWithVotingsFetched(String username);

    Optional<UserEntity> findOneByIdWithVotingsFetched(Long id);

    Optional<UserEntity> findOneByIdWithCreatedVotingsFetched(Long id);

    Optional<UserEntity> findOneByIdWithInvolvedVotingsFetched(Long id);

    Optional<UserEntity> findOneByExternalIdOrUsernameAndEmail(String externalId, String username, String email);

    Optional<UserEntity> findOneByUsernameAndEmail(String username, String email);
}
