package com.models.createdvotings;

import com.models.user.UserEntity;
import com.models.voting.VotingEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="CreatedVotingsEntity")
@Table
public class CreatedVotingsEntity {

    @EmbeddedId
    private CreatedVotingsEntityId id;

    @ManyToOne
    @MapsId("userId")
    private UserEntity userEntity;

    @ManyToOne
    @MapsId("votingId")
    private VotingEntity votingEntity;

    private CreatedVotingsEntity() {
    }

    public CreatedVotingsEntity(UserEntity userEntity, VotingEntity votingEntity) {
        this.userEntity = userEntity;
        this.votingEntity = votingEntity;
        this.id = new CreatedVotingsEntityId(userEntity.getId(),votingEntity.getId());
    }

    public CreatedVotingsEntityId getId() {
        return id;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public VotingEntity getVotingEntity() {
        return votingEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreatedVotingsEntity that = (CreatedVotingsEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
