package com.models.singlevotingvoter;

import org.springframework.data.jpa.repository.JpaRepository;


public interface SingleVotingVoterRepository extends JpaRepository<SingleVotingVoterEntity, SingleVotingVoterEntityId>, SingleVotingVoterRepositoryCustom {

}
