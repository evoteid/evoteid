package com.models.singlevotingvoter;

import java.util.Optional;

public interface SingleVotingVoterRepositoryCustom {

    Optional<SingleVotingVoterEntity> findOneByUserAndVotingWithCommitmentsFetched(long voterId, long votingId);

    Optional<SingleVotingVoterEntity> findOneByUserAndVotingWithSecretSharesFetched(long voterId, long votingId);

    Optional<SingleVotingVoterEntity> findOneByUserAndVotingWithNIZKPFetched(long voterId, long votingId);

    Optional<SingleVotingVoterEntity> findOne(long voterId, long votingId);
}
