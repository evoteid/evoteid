package com.models.singlevotingvoter;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class VoteNIZKPEntity {

    private Integer messageIndex;

    @Lob
    private String a;

    @Lob
    private String b;

    @Lob
    private String c;

    @Lob
    private String r;

    @Lob
    private String signature;

    public Integer getMessageIndex() {
        return messageIndex;
    }

    public VoteNIZKPEntity setMessageIndex(Integer messageIndex) {
        this.messageIndex = messageIndex;
        return this;
    }

    public String getA() {
        return a;
    }

    public VoteNIZKPEntity setA(String a) {
        this.a = a;
        return this;
    }

    public String getB() {
        return b;
    }

    public VoteNIZKPEntity setB(String b) {
        this.b = b;
        return this;
    }

    public String getC() {
        return c;
    }

    public VoteNIZKPEntity setC(String c) {
        this.c = c;
        return this;
    }

    public String getR() {
        return r;
    }

    public VoteNIZKPEntity setR(String r) {
        this.r = r;
        return this;
    }

    public String getSignature() {
        return signature;
    }

    public VoteNIZKPEntity setSignature(String signature) {
        this.signature = signature;
        return this;
    }
}
