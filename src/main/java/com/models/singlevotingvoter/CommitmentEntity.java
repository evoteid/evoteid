package com.models.singlevotingvoter;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class CommitmentEntity {

    @Lob
    private String commitment;

    private Long forCoefficientNumber;

    @Lob
    private String signature;

    public CommitmentEntity setCommitment(String commitment) {
        this.commitment = commitment;
        return this;
    }

    public String getCommitment() {
        return commitment;
    }

    public CommitmentEntity setForCoefficientNumber(long coefficientNumber) {
        this.forCoefficientNumber = coefficientNumber;
        return this;
    }

    public long getForCoefficientNumber() {
        return forCoefficientNumber;
    }

    public String getSignature() {
        return signature;
    }

    public CommitmentEntity setSignature(String signature) {
        this.signature = signature;
        return this;
    }
}
