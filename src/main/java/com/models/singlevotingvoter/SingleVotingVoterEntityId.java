package com.models.singlevotingvoter;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SingleVotingVoterEntityId implements Serializable {

    private Long userId;

    private Long votingId;

    private SingleVotingVoterEntityId() {
    }

    public SingleVotingVoterEntityId(Long userId, Long votingId) {
        this.userId = userId;
        this.votingId = votingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SingleVotingVoterEntityId that = (SingleVotingVoterEntityId) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(votingId, that.votingId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, votingId);
    }
}
