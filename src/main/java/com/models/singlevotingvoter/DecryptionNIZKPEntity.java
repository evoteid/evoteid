package com.models.singlevotingvoter;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class DecryptionNIZKPEntity {

    private boolean proofAdded = false;

    @Lob
    private String a;

    @Lob
    private String b;

    @Lob
    private String r;

    @Lob
    private String signature;

    public String getA() {
        return a;
    }

    public DecryptionNIZKPEntity setA(String a) {
        this.a = a;
        return this;
    }

    public String getB() {
        return b;
    }

    public DecryptionNIZKPEntity setB(String b) {
        this.b = b;
        return this;
    }

    public String getR() {
        return r;
    }

    public DecryptionNIZKPEntity setR(String r) {
        this.r = r;
        return this;
    }

    public String getSignature() {
        return signature;
    }

    public DecryptionNIZKPEntity setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public boolean isProofAdded() {
        return proofAdded;
    }

    public DecryptionNIZKPEntity setProofAdded(boolean proofAdded) {
        this.proofAdded = proofAdded;
        return this;
    }
}
