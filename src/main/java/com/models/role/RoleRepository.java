package com.models.role;

import com.models.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface RoleRepository extends JpaRepository<UserEntity,Long> {

    @Transactional
    @Modifying
    @Query("update UserEntity u set u.roles.administratorRole = ?2 where u.id = ?1")
    int updateAdminRole(long id, boolean newValue);

    @Transactional
    @Modifying
    @Query("update UserEntity u set u.roles.votingAdministratorRole = ?2 where u.id = ?1")
    int updateVotingAdminRole(long id, boolean newValue);

    @Transactional
    @Modifying
    @Query("update UserEntity u set u.roles.registrarRole = ?2 where u.id = ?1")
    int updateRegistrarRoleRole(long id, boolean newValue);
}
