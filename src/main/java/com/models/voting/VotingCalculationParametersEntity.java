package com.models.voting;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class VotingCalculationParametersEntity {

    @Lob
    private String primeP;

    @Lob
    private String primeQ;

    @Lob
    private String generatorG;

    public String getPrimeP() {
        return primeP;
    }

    public VotingCalculationParametersEntity setPrimeP(String primeP) {
        this.primeP = primeP;
        return this;
    }

    public String getPrimeQ() {
        return primeQ;
    }

    public VotingCalculationParametersEntity setPrimeQ(String primeQ) {
        this.primeQ = primeQ;
        return this;
    }

    public String getGeneratorG() {
        return generatorG;
    }

    public VotingCalculationParametersEntity setGeneratorG(String generatorG) {
        this.generatorG = generatorG;
        return this;
    }

    @Override
    public String toString() {
        return  "Prime p:     "+primeP+System.lineSeparator()+
                "Prime q:     "+primeQ+System.lineSeparator()+
                "Generator g: "+generatorG+System.lineSeparator();
    }
}
