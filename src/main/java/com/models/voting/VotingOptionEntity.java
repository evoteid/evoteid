package com.models.voting;

import javax.persistence.*;

@Embeddable
public class VotingOptionEntity {

    private String optionName;

    private Long optionPrimeNumber;

    public VotingOptionEntity setOptionName(String optionName) {
        this.optionName = optionName;
        return this;
    }

    public String getOptionName() {
        return this.optionName;
    }

    public long getOptionPrimeNumber() {
        return optionPrimeNumber;
    }

    public VotingOptionEntity setOptionPrimeNumber(Long optionPrimeNumber) {
        this.optionPrimeNumber = optionPrimeNumber;
        return this;
    }

    @Override
    public String toString() {
        return "Optionname: "+optionName+System.lineSeparator() +
               "OptionPrimeNumber: "+optionPrimeNumber+System.lineSeparator();
    }
}
