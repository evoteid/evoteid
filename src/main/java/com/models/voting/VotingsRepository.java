package com.models.voting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface VotingsRepository extends JpaRepository<VotingEntity,Long>, VotingsRepositoryCustom {

    Iterable<VotingEntity> findAllByOrderByIdAsc();

    @Transactional
    @Modifying
    @Query("update VotingEntity v set v.phaseNumber = ?2 where v.id = ?1")
    int updatePhaseNumber(long id, int phaseNumber);

}
