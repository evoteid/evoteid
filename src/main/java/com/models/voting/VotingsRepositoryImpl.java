package com.models.voting;

import com.strings.Strings;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Repository
@Transactional
public class VotingsRepositoryImpl implements VotingsRepositoryCustom{

    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<VotingEntity> findOneByIdWithEligibleVotersFetched(Long id) {
        VotingEntity votingEntity;

        try {
            votingEntity = em.createQuery("SELECT v FROM VotingEntity AS v WHERE v.id = :id",VotingEntity.class)
                    .setParameter("id",id)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(),em.getEntityGraph("VotingEntity.eligibleVoters"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }
        return Optional.of(votingEntity);
    }

    @Override
    public Optional<VotingEntity> findOneByIdWithEligibleVotersWithCommitmentsFetched(Long id) {
        VotingEntity votingEntity;

        try {
            votingEntity = em.createQuery("SELECT v FROM VotingEntity AS v WHERE v.id = :id",VotingEntity.class)
                    .setParameter("id",id)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(),em.getEntityGraph("VotingEntity.eligibleVotersWithCommitments"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }
        return Optional.of(votingEntity);
    }
}
