package com.models.voting;

import java.util.Optional;

public interface VotingsRepositoryCustom {

    Optional<VotingEntity> findOneByIdWithEligibleVotersFetched(Long id);

    Optional<VotingEntity> findOneByIdWithEligibleVotersWithCommitmentsFetched(Long id);
}
