package com.models.voting;

import com.models.createdvotings.CreatedVotingsEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.utils.Stringpadder;

import javax.persistence.*;
import java.util.*;

@Entity(name="VotingEntity")
@Table
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "VotingEntity.eligibleVoters",
                attributeNodes = @NamedAttributeNode("eligibleVoters")),
        @NamedEntityGraph(
                name = "VotingEntity.eligibleVotersWithCommitments",
                attributeNodes = @NamedAttributeNode(value = "eligibleVoters", subgraph = "eligibleVoters"),
                subgraphs = @NamedSubgraph(name = "eligibleVoters", attributeNodes = @NamedAttributeNode("commitments")))
})
public class VotingEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Lob
    private String description;

    private Long securityThreshold;

    private Long numberOfEligibleVoters;

    private Integer phaseNumber;

    private Long timeVotingIsFinished = -1L;

    @Embedded
    private VotingCalculationParametersEntity calculationParametersEntity;

    @OneToOne
    private CreatedVotingsEntity votingAdministrator;

    @ElementCollection(fetch = FetchType.EAGER)
    @OrderColumn
    private List<VotingOptionEntity> votingOptions = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @OrderColumn
    private List<SingleVotingVoterEntity> eligibleVoters = new ArrayList<>();

    @Lob
    private String votingsSummary = "";

    @Lob
    private String transactionId = "";

    public Long getId() {
        return id;
    }

    public VotingEntity setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public VotingEntity setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getDescription() {
        return this.description;
    }

    public VotingEntity addVotingOption(VotingOptionEntity option) {
        this.votingOptions.add(option);
        return this;
    }

    public List<VotingOptionEntity> getVotingOptions() {
        return this.votingOptions;
    }

    public Long getSecurityThreshold() {
        return securityThreshold;
    }

    public VotingEntity setSecurityThreshold(long securityThreshold) {
        this.securityThreshold = securityThreshold;
        return this;
    }

    public Long getNumberOfEligibleVoters() {
        return numberOfEligibleVoters;
    }

    public VotingEntity setNumberOfEligibleVoters(long numberOfEligibleVoters) {
        this.numberOfEligibleVoters = numberOfEligibleVoters;
        return this;
    }

    public List<SingleVotingVoterEntity> getAllEligibleVoters() {
        return eligibleVoters;
    }

    public VotingEntity addEligibleVoter(SingleVotingVoterEntity voter) {
        this.eligibleVoters.add(voter);
        return this;
    }

    public VotingCalculationParametersEntity getCalculationParametersEntity() {
        return calculationParametersEntity;
    }

    public VotingEntity setCalculationParametersEntity(VotingCalculationParametersEntity calculationParametersEntity) {
        this.calculationParametersEntity = calculationParametersEntity;
        return this;
    }

    public int getPhaseNumber() {
        return phaseNumber;
    }

    public VotingEntity setPhaseNumber(int phaseNumber) {
        this.phaseNumber = phaseNumber;
        return this;
    }

    public CreatedVotingsEntity getVotingAdministrator() {
        return votingAdministrator;
    }

    public VotingEntity setVotingAdministrator(CreatedVotingsEntity votingAdministrator) {
        this.votingAdministrator = votingAdministrator;
        return this;
    }

    public Long getTimeVotingIsFinished() {
        return timeVotingIsFinished;
    }

    public VotingEntity setTimeVotingIsFinished(Long timeVotingIsFinished) {
        this.timeVotingIsFinished = timeVotingIsFinished;
        return this;
    }

    public String getVotingSummary() {
        return votingsSummary;
    }

    public VotingEntity setVotingSummary(String votingsSummary) {
        this.votingsSummary = votingsSummary;
        return this;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public VotingEntity setTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    @Override
    public String toString() {
        String ret = "VotingEntitiy:"+System.lineSeparator();
        ret+="   Id:          "+id+System.lineSeparator();
        ret+="   Title:       "+title+System.lineSeparator();
        ret+="   Description: "+description+System.lineSeparator();
        ret+="   Threshold:   "+securityThreshold+System.lineSeparator();
        ret+="   Elig Voters: "+numberOfEligibleVoters+System.lineSeparator();
        ret+="   Phase:       "+phaseNumber+System.lineSeparator();
        ret+="   Calc Params: "+System.lineSeparator();
        String calcParameters = calculationParametersEntity.toString();
        ret+= Stringpadder.pad(calcParameters,6)+System.lineSeparator();
        ret+="   Options:"+System.lineSeparator();

        StringBuilder optionalStringBuilder = new StringBuilder();
        for(VotingOptionEntity o : votingOptions) {
            optionalStringBuilder.append(o.toString());
        }
        String optString = optionalStringBuilder.toString();

        optString = Stringpadder.pad(optString,6);
        ret+=optString+System.lineSeparator();

        ret+="   Eligible eligibleVoters:"+System.lineSeparator();

        StringBuilder keyStringBuilder = new StringBuilder();
        for(SingleVotingVoterEntity e : eligibleVoters) {
            keyStringBuilder.append(e.getUserEntity().getUsername());
        }
        String keysString = keyStringBuilder.toString();
        keysString = Stringpadder.pad(keysString,6);
        ret+=keysString+System.lineSeparator();

        return ret;
    }

}
