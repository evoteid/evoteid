package com.strings;

public enum Strings {

    USERNAME("username"),
    EXTERNAL_ID("externalId"),
    FROM_VOTER_ID("fromVoterId"),
    COMMITMENT("commitment"),
    COMMITMENTS("commitments"),
    SECRET_SHARES("secretShares"),
    PUBLIC_KEY("publicKey"),
    IS_TRUSTEE("isTrustee"),
    CAN_VOTE("canVote"),
    POSITION("position"),
    ID("id"),
    NAME("name"),
    EMAIL("email"),
    META("meta"),
    ELIGIBLE_VOTERS("eligibleVoters"),
    KEY_GENERATION_PHASE("keyGenerationPhase"),
    VOTING_PHASE("votingPhase"),
    DECRYPTION_PHASE("decryptionPhase"),
    SECURITY_THRESHOLD("securityThreshold"),
    TITLE("title"),
    DESCRIPTION("description"),
    INFO("info"),
    CALCULATION_PARAMETERS("calculationParameters"),
    OPTIONS("options"),
    OPTION_NAME("optionName"),
    OPTION_PRIME_NUMBER("optionPrimeNumber"),
    FROM_VOTER_NAME("fromVoterName"),
    ALPHA("alpha"),
    BETA("beta"),
    SIGNATURE("signature"),
    DATA("data"),
    PROOFS("proofs"),
    FROM_VOTER_POSITION("fromVoterPosition"),
    DECRYPTION_FACTOR("decryptionFactor"),
    PROOF("proof"),
    PRIME_Q("primeQ"),
    PRIME_P("primeP"),
    GENERATOR_G("generatorG"),
    FETCH_GRAPH_HINT("javax.persistence.fetchgraph"),
    SERVER_SIDE_FAILURE("Server side failure"),
    EXAMPLE_COM_NAME("example.com"),
    VOTER_NOT_KNOWN("Current voter is not known.");

    private final String text;

    Strings(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
