package com.controller;

import com.controller.data.request.PublicKeyBody;
import com.data.voter.SingleVoter;
import com.exceptions.IllegalVoteException;
import com.exceptions.PKIClosedException;
import com.services.context.RequestContextHolder;
import com.services.data.keys.KeyService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class KeysController {

    private KeyService keyService;

    private RequestContextHolder requestContextHolder;

    @Autowired
    public KeysController(KeyService keyService,
                          RequestContextHolder requestContextHolder) {
        this.keyService = keyService;
        this.requestContextHolder = requestContextHolder;
    }

    @PutMapping("/api/votings/{id}/publicKeys/voters/{voterId}")
    public ResponseEntity setPublicKeyOfVoterForVoting(
            @PathVariable Long id,
            @PathVariable Long voterId,
            @RequestBody PublicKeyBody keyBody) {
        Optional<SingleVoter> currentUser = requestContextHolder.getCurrentVoter();
        if(!currentUser.isPresent() || !currentUser.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only specify his own public key");
        }

        try {
            keyService.setPublicKeyOfVoterInVoting(voterId,id,keyBody.getPublicKey());
        } catch (PKIClosedException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body("The key was successfully added");
    }

    @GetMapping("/api/votings/{id}/publicKeys/voters/{voterId}")
    public ResponseEntity getPublicKeyOfVoterForVoting(
            @PathVariable Long id,
            @PathVariable Long voterId) {
        Optional<SingleVoter> currentUser = requestContextHolder.getCurrentVoter();
        if(!currentUser.isPresent() || !currentUser.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only view his own public key");
        }

        Optional<String> optPublicKey = keyService.getPublicKeyOfVoterInVoting(voterId,id);
        if(!optPublicKey.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("The public key for this voting was not found");
        }

        JSONObject keyObject = new JSONObject()
                .put("publicKey",optPublicKey.get());

        return ResponseEntity.status(HttpStatus.OK)
                .body(keyObject.toString());
    }
}
