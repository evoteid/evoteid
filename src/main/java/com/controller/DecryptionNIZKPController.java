package com.controller;

import com.data.proofs.DecryptionNIZKProof;
import com.data.voter.SingleVoter;
import com.exceptions.IllegalVoteException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.proofs.DecryptionNIZKProofService;
import com.services.parser.DecryptionNIZKProofParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class DecryptionNIZKPController {

    private final DecryptionNIZKProofService decryptionNIZKProofService;
    private final RequestContextHolder requestContextHolder;

    @Autowired
    public DecryptionNIZKPController(DecryptionNIZKProofService decryptionNIZKProofService,
                                     RequestContextHolder requestContextHolder) {
        this.decryptionNIZKProofService = decryptionNIZKProofService;
        this.requestContextHolder = requestContextHolder;
    }

    @PostMapping("/api/votings/{id}/decryptionZeroKnowledgeProofs/voters/{voterId}")
    public ResponseEntity setDecryptionZeroKnowledgeProof(
            @PathVariable Long id,
            @PathVariable Long voterId,
            @RequestBody String jsonData) {
        long votingId = id;
        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent() || !currentVoter.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only provide zero knowledge proofs for himself.");
        }

        DecryptionNIZKProof decryptionNIZKProofs;
        try {
            decryptionNIZKProofs = DecryptionNIZKProofParser.parse(jsonData);
            decryptionNIZKProofService.setNIZKProofs(votingId,voterId,decryptionNIZKProofs);
        }
        catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("\"Zero knowledge proof for decryption was added.\"");
    }
}
