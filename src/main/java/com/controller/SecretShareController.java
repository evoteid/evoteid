package com.controller;

import com.data.secretshares.SecretSharesForVoter;
import com.data.secretshares.SecretSharesFromVoter;
import com.data.voter.SingleVoter;
import com.exceptions.IllegalVoteException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.secretshares.SecretShareService;
import com.services.parser.SecretShareParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class SecretShareController {

    private final SecretShareService secretShareService;
    private final RequestContextHolder requestContextHolder;

    @Autowired
    public SecretShareController(SecretShareService secretShareService,
                                RequestContextHolder requestContextHolder) {
        this.secretShareService = secretShareService;
        this.requestContextHolder = requestContextHolder;
    }

    @PostMapping("/api/votings/{id}/secretShares")
    public ResponseEntity addSecretShares(
            @PathVariable Long id,
            @RequestBody String jsonData)  {
        long votingId = id;
        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body("Unspecified user");
        }

        SecretSharesFromVoter secretSharesFromVoter;
        try {
            secretSharesFromVoter = SecretShareParser.parse(jsonData);
        } catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        SingleVoter currentVoter = optCurrentVoter.get();
        if(!secretSharesFromVoter.getFromVoterName().equals(currentVoter.getName()) ||
           !secretSharesFromVoter.getFromVoterId().equals(currentVoter.getId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only specify the secret shares that come from him.");
        }

        try {
            secretShareService.addSecretShares(votingId, secretSharesFromVoter);
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Secret shares were added");
    }

    @GetMapping("/api/votings/{id}/secretShares/forVoters/{voterId}")
    public ResponseEntity getSecretShares(
            @PathVariable Long id,
            @PathVariable Long voterId)  {

        long votingId = id;
        SecretSharesForVoter secretSharesForUser = secretShareService.getSharesForUser(votingId,voterId,"");
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(secretSharesForUser.toJson().toString());
    }
}
