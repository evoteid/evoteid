package com.controller;

import com.data.voter.SingleVoter;
import com.data.votingprogress.SingleVoterProgress;
import com.data.votingprogress.VotingProgress;
import com.data.votings.NewlyCreatedVoting;
import com.data.votings.Voting;
import com.data.votings.Votings;
import com.data.votingsforvoterwithprogress.VotingsForVoterWithProgress;
import com.enums.Phases;
import com.enums.Role;
import com.exceptions.NoSuchVotingException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.roles.RoleService;
import com.services.datamodelchecker.DataModelCheckerService;
import com.services.ethereum.EthereumService;
import com.services.hash.HashService;
import com.services.mail.MailService;
import com.services.mail.MailTextCreatorService;
import com.services.parser.VotingParser;
import com.services.data.votings.VotingsDataService;
import com.services.task.TaskScheduler;
import com.services.votingssummary.VotingsSummaryService;
import com.strings.Strings;
import com.utils.data.Triplet;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class PhaseNumberData {
    public Integer phaseNumber;
    public Boolean sendNotificationMail;
}

class VotingEndTimeData {
    public Long votingEndTime;
}

@RestController
public class VotingsDataController {

    private final VotingsDataService votingsDataService;
    private final RoleService roleService;
    private final MailService mailService;
    private final VotingsSummaryService votingsSummaryService;
    private final HashService hashService;
    private final EthereumService ethereumService;
    private final MailTextCreatorService mailTextCreatorService;

    private final RequestContextHolder requestContextHolder;

    private final TaskScheduler taskScheduler;

    private final DataModelCheckerService dataModelCheckerService;

    private static final String NOT_AUTHENTICATED_MESSAGE = "The request won't be fulfilled as the the user is not authenticated.";

    private static final String INTERNAL_SERVER_ERROR = "Internal server error";
    @Autowired
    public VotingsDataController(
            final VotingsDataService votingsDataService,
            final RoleService roleService,
            final MailService mailService,
            final MailTextCreatorService mailTextCreatorService,
            final VotingsSummaryService votingsSummaryService,
            final HashService hashService,
            final EthereumService ethereumService,
            final RequestContextHolder requestContextHolder,
            final DataModelCheckerService dataModelCheckerService,
            final TaskScheduler taskScheduler) {
        this.votingsDataService = votingsDataService;
        this.roleService = roleService;
        this.mailService = mailService;
        this.mailTextCreatorService = mailTextCreatorService;
        this.votingsSummaryService = votingsSummaryService;
        this.hashService = hashService;
        this.ethereumService = ethereumService;
        this.requestContextHolder = requestContextHolder;
        this.dataModelCheckerService = dataModelCheckerService;
        this.taskScheduler = taskScheduler;
    }

    @GetMapping("/api/votings/phaseNumber/{phaseNumber}")
    @ResponseBody
    public ResponseEntity getVotings(@RequestHeader HttpHeaders headers,
                                     @PathVariable Integer phaseNumber) {
        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(NOT_AUTHENTICATED_MESSAGE);
        }

        Long voterId = currentVoter.get().getId();

        VotingsForVoterWithProgress votingsForVoterWithProgress = votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(voterId,phaseNumber);

        return ResponseEntity.status(HttpStatus.OK)
                .body(votingsForVoterWithProgress.toJson().toString());
    }

    @GetMapping("/api/createdVotings")
    @ResponseBody
    public ResponseEntity getCreatedVotings(@RequestHeader HttpHeaders headers) {
        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(NOT_AUTHENTICATED_MESSAGE);
        }

        Long voterId = currentVoter.get().getId();

        if(!roleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The current user is not an voting administrator, therefore permission is denied");
        }

        Votings votings = votingsDataService.getAllVotingsByVotingAdministratorId(voterId);

        return ResponseEntity.status(HttpStatus.OK)
                .body(votings.toJson().toString());
    }

    @GetMapping("/api/votings/{id}/votingsOverview")
    @ResponseBody
    public ResponseEntity getVotingsOverview(@PathVariable Long id) {
        Triplet<HttpStatus,String, VotingProgress> triplet = checkIfCurrentVoterIsVotingAdministratorForVoting(id);
        if(!triplet.getFirst().is2xxSuccessful()) {
            return ResponseEntity.status(triplet.getFirst())
                    .body(triplet.getSecond());
        }

        VotingProgress votingProgress = triplet.getThird();

        return ResponseEntity.status(HttpStatus.OK)
                .body(votingProgress.toJson().toString());
    }

    @GetMapping("/api/votings/{id}/votingSummary")
    @ResponseBody
    public ResponseEntity getVotingsSummary(@PathVariable Long id) {
        Optional<JSONObject> jsonObjectOptional = votingsDataService.getVotingSummary(id);
        if(!jsonObjectOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("No voting summary was found");
        }

        JSONObject jsonObject = jsonObjectOptional.get();
        return ResponseEntity.status(HttpStatus.OK)
                .body(jsonObject.toString());
    }

    @PostMapping("/api/votings")
    @ResponseBody
    public ResponseEntity addVoting(@RequestBody String jsonData) {
        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(NOT_AUTHENTICATED_MESSAGE);
        }

        SingleVoter currentVoter = optCurrentVoter.get();

        if(!roleService.hasRole(currentVoter.getId(), Role.VOTING_ADMINISTRATOR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Only voting administrators can add votings.");
        }

        NewlyCreatedVoting voting = null;
        try {
            voting = VotingParser.parse(jsonData);
        } catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        voting.setVotingAdministrator(currentVoter);

        Optional<Voting> createdVoting = votingsDataService.addVoting(voting);

        if(createdVoting.isPresent()) {
            boolean changed = dataModelCheckerService.checkDataModel();
            while(changed) {
                changed = dataModelCheckerService.checkDataModel();
            }

            Votings createdVotings = new Votings()
                    .addVoting(createdVoting.get());
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(createdVotings.toJson().toString());
        }
        else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/api/votings/{id}/phaseNumber")
    public ResponseEntity putPhaseNumber(
            @PathVariable Long id,
            @RequestBody PhaseNumberData phaseNumber) {

        Triplet<HttpStatus, String, VotingProgress> triplet = checkIfCurrentVoterIsVotingAdministratorForVoting(id);
        if (!triplet.getFirst().is2xxSuccessful()) {
            return ResponseEntity.status(triplet.getFirst())
                    .body(triplet.getSecond());
        }

        VotingProgress votingProgress = triplet.getThird();
        int currentPhaseNumber = votingProgress.getPhaseNumber();

        int phaseNumberToSet = phaseNumber.phaseNumber;

        if(currentPhaseNumber != phaseNumberToSet-1) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Phase number can only be increased by 1.");
        }

        if(currentPhaseNumber == Phases.VOTING_PHASE.getNumber()) {
            long votingEndTime = votingProgress.getTimeVotingIsFinished().longValue();
            long millis = System.currentTimeMillis();

            if(votingEndTime != -1 && votingEndTime > millis) {
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body("Cannot increase phase number since the voting is still in progress.");
            }
        }

        if(phaseNumber.phaseNumber == Phases.PUBLISH_PHASE.getNumber()) {
            Optional<JSONObject> votingsSummaryOptional = votingsSummaryService.createVotingsSummary(id);
            if(!votingsSummaryOptional.isPresent()) {
                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Creating vote summary failed");
            }

            JSONObject votingsSummary = votingsSummaryOptional.get();
            String hash = hashService.createSHA256Hash(votingsSummary.toString());
            Optional<String> optTransactionId = this.ethereumService.sendMessageToTestNetwork(hash);

            if(!optTransactionId.isPresent()) {
                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Sending content to blockchain failed");
            }

            Optional<Voting> returnedOptVoting = votingsDataService.setVotingSummary(id,votingsSummary,optTransactionId.get());

            if(!returnedOptVoting.isPresent()) {
                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(INTERNAL_SERVER_ERROR);
            }
        }

        Optional<Voting> optVoting = votingsDataService.setPhaseNumber(id, phaseNumber.phaseNumber);

        if (!optVoting.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(INTERNAL_SERVER_ERROR);
        }

        if(phaseNumber.sendNotificationMail) {
            sendNotificationMails(id, votingProgress, phaseNumberToSet);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(optVoting.get().toJson().toString());

    }

    @PutMapping("/api/votings/{id}/timeVotingFinishes")
    public ResponseEntity responseEntity(
            @PathVariable Long id,
            @RequestBody VotingEndTimeData votingEndTime) {
        Triplet<HttpStatus, String, VotingProgress> triplet = checkIfCurrentVoterIsVotingAdministratorForVoting(id);
        if (!triplet.getFirst().is2xxSuccessful()) {
            return ResponseEntity.status(triplet.getFirst())
                    .body(triplet.getSecond());
        }

        if(triplet.getThird().getPhaseNumber() != Phases.KEY_DERIVATION_PHASE.getNumber()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Specifying the voting end data is only allowed before the voting phase.");
        }

        Long timeVotingFinishes = votingEndTime.votingEndTime;

        Optional<Voting> savedVoting = votingsDataService.setTimeVotingIsFinished(id,timeVotingFinishes);
        if(!savedVoting.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(INTERNAL_SERVER_ERROR);
        }
        else {

            this.taskScheduler.schedulePhaseUpdateTask(id,timeVotingFinishes);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(savedVoting.get().toJson().toString());
        }
    }

    private Triplet<HttpStatus,String,VotingProgress> checkIfCurrentVoterIsVotingAdministratorForVoting(Long id) {

        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent()) {
            return new Triplet(HttpStatus.FORBIDDEN, NOT_AUTHENTICATED_MESSAGE, null);
        }

        Long voterId = currentVoter.get().getId();

        if(!roleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)) {
            return new Triplet(HttpStatus.FORBIDDEN, "The current user is not an voting administrator, therefore permission is denied",null);
        }

        Optional<VotingProgress> optVotingProgress = votingsDataService.getVotingProgress(id);
        if(!optVotingProgress.isPresent()) {
            return new Triplet(HttpStatus.NOT_FOUND,
                    String.format("The voting with id '%d' was not found",id),
                    null);
        }

        VotingProgress votingProgress = optVotingProgress.get();
        if(!votingProgress.getVotingAdministrator().getId().equals(voterId)) {
            return new Triplet(HttpStatus.FORBIDDEN,
                    String.format("The voting was not created by the voter with id '%d', therefore permission is denied",voterId),
                    null);
        }

        return new Triplet(HttpStatus.OK,null,votingProgress);
    }

    @DeleteMapping("/api/votings/{id}")
    public ResponseEntity deleteVoter(
            @PathVariable Long id,
            @RequestHeader HttpHeaders headers) {

        Triplet<HttpStatus, String, VotingProgress> triplet = checkIfCurrentVoterIsVotingAdministratorForVoting(id);
        if (!triplet.getFirst().is2xxSuccessful()) {
            return ResponseEntity.status(triplet.getFirst())
                    .body(triplet.getSecond());
        }

        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        try {
            votingsDataService.deleteVoting(id);
        } catch (NoSuchVotingException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body("The voting was deleted");

    }

    private void sendNotificationMails(long votingId, VotingProgress votingProgress, int phasenumber) {
        List<SingleVoter> votersToSendEmailTo = new ArrayList<>();

        if(     phasenumber == Phases.PKI_PHASE.getNumber() ||
                phasenumber == Phases.KEY_DERIVATION_PHASE.getNumber() ||
                phasenumber == Phases.DECRYPTION_PHASE.getNumber()) {
            votersToSendEmailTo = votingProgress.getVotersProgressList()
                    .stream()
                    .filter(SingleVoterProgress::getIsTrustee)
                    .map(SingleVoterProgress::getVoter)
                    .collect(Collectors.toList());
        }
        else if(phasenumber == Phases.VOTING_PHASE.getNumber()){
            votersToSendEmailTo = votingProgress.getVotersProgressList()
                    .stream()
                    .filter(SingleVoterProgress::getCanVote)
                    .map(SingleVoterProgress::getVoter)
                    .collect(Collectors.toList());
        }

        if(phasenumber == Phases.PKI_PHASE.getNumber()) {
            sendCallForPkiMail(votersToSendEmailTo, votingProgress.getVotingAdministrator());
        }
        else if(phasenumber == Phases.KEY_DERIVATION_PHASE.getNumber()) {
            sendCallForKeyGenerationMail(votersToSendEmailTo, votingProgress.getVotingAdministrator());
        }
        else if(phasenumber == Phases.VOTING_PHASE.getNumber()) {
            sendCallForVotingMail(votingId, votersToSendEmailTo, votingProgress.getVotingAdministrator());
        }
        else if(phasenumber == Phases.DECRYPTION_PHASE.getNumber()) {
            sendCallForDecryption(votersToSendEmailTo, votingProgress.getVotingAdministrator());
        }
    }

    private void sendCallForPkiMail(List<SingleVoter> voters, SingleVoter votingAdmin) {
        for (SingleVoter singleVoter : voters) {
            String mailAddress = singleVoter.getEmail();
            String subject = "Call for adding custom RSA key pair";
            String url = "https://"example.com"/pki";
            String message = mailTextCreatorService.createCallForPKI(singleVoter.getName(),votingAdmin.getName(),url);
            mailService.sendMail(mailAddress, subject, message);
        }
    }

    private void sendCallForKeyGenerationMail(List<SingleVoter> voters, SingleVoter votingAdmin) {
        for (SingleVoter singleVoter : voters) {
            String mailAddress = singleVoter.getEmail();
            String subject = "Call for key generation";
            String url = "https://"example.com"/keyDerivation";
            String message = mailTextCreatorService.createCallForKeyGeneration(singleVoter.getName(),votingAdmin.getName(),url);
            mailService.sendMail(mailAddress, subject, message);
        }
    }

    private void sendCallForVotingMail(long votingId, List<SingleVoter> voters, SingleVoter votingAdmin) {
        for (SingleVoter singleVoter : voters) {
            String mailAddress = singleVoter.getEmail();
            String subject = "Call for voting";
            String url = "https://"example.com"/vote/votingId/" + votingId + "/voterId/" + singleVoter.getId();
            String message = mailTextCreatorService.createVoteMailText(singleVoter.getName(),votingAdmin.getName(),url);
            mailService.sendMail(mailAddress, subject, message);
        }
    }

    private void sendCallForDecryption(List<SingleVoter> voters, SingleVoter votingAdmin) {
        for (SingleVoter singleVoter : voters) {
            String mailAddress = singleVoter.getEmail();
            String subject = "Call for decryption";
            String url = "https://"example.com"/decryption";
            String message = mailTextCreatorService.createDecryptionMailText(singleVoter.getName(),votingAdmin.getName(),url);
            mailService.sendMail(mailAddress, subject, message);
        }
    }

}
