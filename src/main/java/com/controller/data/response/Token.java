package com.controller.data.response;

public class Token {

    private String tokenString;

    public Token(String token) {
        this.tokenString = token;
    }

    public String getToken() {
        return tokenString;
    }
}
