package com.controller;

import com.data.dataforkeyderivation.DataForKeyDerivation;
import com.data.voter.SingleVoter;
import com.data.votes.SingleVote;
import com.data.votes.Votes;
import com.exceptions.IllegalVoteException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.vote.VoteService;
import com.services.parser.VoteParser;
import com.strings.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class VoteController {

    private final VoteService voteService;

    private final RequestContextHolder requestContextHolder;

    @Autowired
    public VoteController(VoteService voteService,
                          RequestContextHolder requestContextHolder) {
        this.voteService = voteService;
        this.requestContextHolder = requestContextHolder;
    }

    @GetMapping("/api/votings/{id}/keyCalculationData")
    public ResponseEntity getKeyCalculationData(@PathVariable Long id) {
        Optional<SingleVoter> optVoter = requestContextHolder.getCurrentVoter();
        if(!optVoter.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        SingleVoter voter = optVoter.get();
        Long voterId = voter.getId();

        Optional<DataForKeyDerivation> optDataForKeyDerivation = voteService.getDataForKeyDerivation(id);
        if(!optDataForKeyDerivation.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(String.format("The voting with id '%d' cannot be found",id));
        }

        DataForKeyDerivation dataForKeyDerivation = optDataForKeyDerivation.get();
        boolean currentVoterIsPartOfVoting = dataForKeyDerivation.containsVoterId(voterId);
        boolean isVotingAdministratorForVoting = (dataForKeyDerivation.getVotingAdministratorId() == voterId);

        if(!currentVoterIsPartOfVoting && !isVotingAdministratorForVoting) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(String.format("Voter with id '%d' is not eligible to vote in voting with id '%d' and cannot access calculation parameters",voterId,id));
        }

        return ResponseEntity
                .ok(dataForKeyDerivation.toJson().toString());
    }

    @GetMapping("/api/votings/{id}/votes")
    public ResponseEntity getAllVotes(@PathVariable Long id) {
        Votes votes = voteService.getAllVotesForVoting(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(votes.toJson().toString());
    }


    @PostMapping("/api/votings/{id}/votes/voters/{voterId}")
    public ResponseEntity vote(
                    @PathVariable Long id,
                    @PathVariable Long voterId,
                    @RequestBody String jsonData) {
        long votingId = id;
        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent() || !currentVoter.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only vote for himself.");
        }

        SingleVote singleVote;
        try {
            singleVote = VoteParser.parse(jsonData);
            voteService.setVoteForVoter(voterId,votingId, singleVote);
        } catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Vote was added.");

    }

}
