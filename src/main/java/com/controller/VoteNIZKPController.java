package com.controller;

import com.data.proofs.VoteNIZKProofs;
import com.data.voter.SingleVoter;
import com.exceptions.IllegalVoteException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.proofs.VoteNIZKProofService;
import com.services.parser.VoteNIZKProofParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class VoteNIZKPController {

    private final VoteNIZKProofService voteNIZKProofService;
    private final RequestContextHolder requestContextHolder;

    @Autowired
    public VoteNIZKPController(VoteNIZKProofService voteNIZKProofService,
                               RequestContextHolder requestContextHolder) {
        this.voteNIZKProofService = voteNIZKProofService;
        this.requestContextHolder = requestContextHolder;
    }

    @PostMapping("/api/votings/{id}/voteZeroKnowledgeProofs/voters/{voterId}")
    public ResponseEntity setZeroKnowledgeProof(
            @PathVariable Long id,
            @PathVariable Long voterId,
            @RequestBody String jsonData) {
        long votingId = id;
        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent() || !currentVoter.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only provide zero knowledge proofs for himself.");
        }

        VoteNIZKProofs voteNIZKProofs;
        try {
            voteNIZKProofs = VoteNIZKProofParser.parse(jsonData);
            voteNIZKProofService.setNIZKProofs(votingId,voterId,voteNIZKProofs);
        }
        catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Zero knowledge proof for voting was added.");

    }

    @GetMapping("/api/votings/{id}/voteZeroKnowledgeProofs")
    public ResponseEntity getAllNIKZProofsForVotes(@PathVariable Long id) {
        Optional<List<VoteNIZKProofs>> optVoteNIZKProofs = voteNIZKProofService.getNIZKProofs(id);
        if(!optVoteNIZKProofs.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(String.format("Could not find voting with id '%d'",id));
        }

        List<VoteNIZKProofs> proofsList = optVoteNIZKProofs.get();

        JSONArray jsonArray = new JSONArray();
        for(VoteNIZKProofs proofs : proofsList) {
            jsonArray.put(proofs.toJson());
        }
        JSONObject ret = new JSONObject()
                .put("allProofs", jsonArray);

        return ResponseEntity.status(HttpStatus.OK)
                .body(ret.toString());
    }
}
