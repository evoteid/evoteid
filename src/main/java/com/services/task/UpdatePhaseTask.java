package com.services.task;

import com.enums.Phases;
import com.services.data.votings.VotingsDataService;

import java.util.TimerTask;

public class UpdatePhaseTask extends TimerTask {


    private VotingsDataService votingsDataService;

    private Long votingId;

    public UpdatePhaseTask setVotingsDataService(VotingsDataService votingsDataService) {
        this.votingsDataService = votingsDataService;
        return this;
    }

    public UpdatePhaseTask setVotingId(Long votingId) {
        this.votingId = votingId;
        return this;
    }

    @Override
    public void run() {
        votingsDataService.setPhaseNumber(votingId,Phases.DECRYPTION_PHASE.getNumber());
    }
}
