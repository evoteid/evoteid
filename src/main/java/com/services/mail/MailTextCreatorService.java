package com.services.mail;

public interface MailTextCreatorService {

    String createCallForPKI(String voterName, String votingAdminName, String url);

    String createCallForKeyGeneration(String voterName, String votingAdminName, String url);

    String createVoteMailText(String voterName, String votingAdminName, String url);

    String createDecryptionMailText(String voterName, String votingAdminName, String url);
}
