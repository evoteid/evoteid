package com.services.datamodelchecker;

import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Qualifier("DataModelCheckerService")
public class DataModelCheckerService {

    @Autowired
    SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    UserRepository userRepository;

    public boolean checkDataModel() {
        boolean didUpdate = false;

        List<SingleVotingVoterEntity> singleVotingVoterEntityList = singleVotingVoterRepository.findAll();
        for(SingleVotingVoterEntity entity : singleVotingVoterEntityList) {
            VotingEntity votingEntity = entity.getVotingEntity();
            UserEntity userEntity = entity.getUserEntity();

            Optional<UserEntity> optLoadedUserEntity = userRepository.findOneByIdWithInvolvedVotingsFetched(userEntity.getId());
            if(!optLoadedUserEntity.isPresent()) {
                continue;
            }

            UserEntity loadedUserEntity = optLoadedUserEntity.get();
            boolean success = loadedUserEntity.
                    getVotings()
                    .stream()
                    .map(SingleVotingVoterEntity::getVotingEntity)
                    .map(VotingEntity::getId)
                    .collect(Collectors.toList()).contains(votingEntity.getId());

            if(!success) {
                didUpdate = true;

                loadedUserEntity.addVoting(entity);
                userRepository.saveAndFlush(loadedUserEntity);
            }
        }

        return didUpdate;
    }

}
