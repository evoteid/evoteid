package com.services.token;

import com.data.roles.Roles;
import com.data.voter.SingleVoter;

import java.util.Optional;

public interface JwtTokenCoderService {

    String createToken(Long voterId, String voterName, Long expirationTime, Roles roles, Long numberOfVotingsWithTrusteeRole);

    String createToken(Long voterId, String voterName, String voterPublicKey, Long expirationTime, Roles roles, Long numberOfVotingsWithTrusteeRole);

    Optional<SingleVoter> getVoterFromToken(String token);
}
