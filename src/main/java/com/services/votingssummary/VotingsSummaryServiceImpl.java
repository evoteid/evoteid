package com.services.votingssummary;

import com.data.commitments.SingleCommitment;
import com.data.secretshares.SingleSecretShareForVoter;
import com.models.singlevotingvoter.*;
import com.models.voting.VotingEntity;
import com.models.voting.VotingOptionEntity;
import com.models.voting.VotingsRepository;
import com.services.conversion.*;
import com.strings.Strings;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("VotingsSummaryService")
public class VotingsSummaryServiceImpl implements VotingsSummaryService {

    private VotingsRepository votingsRepository;

    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    public VotingsSummaryServiceImpl(VotingsRepository votingsRepository,
                                     SingleVotingVoterRepository singleVotingVoterRepository) {
        this.votingsRepository = votingsRepository;
        this.singleVotingVoterRepository = singleVotingVoterRepository;
    }

    @Override
    @Transactional
    public Optional<JSONObject> createVotingsSummary(long votingId) {
        Optional<VotingEntity> votingEntityOptional = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);
        if(!votingEntityOptional.isPresent()) {
            return Optional.empty();
        }

        VotingEntity votingEntity = votingEntityOptional.get();

        JSONObject retObject = new JSONObject()
                .put(Strings.META.toString(),createMetaDataSummary(votingEntity))
                .put(Strings.ELIGIBLE_VOTERS.toString(),createEligibleVotersSummary(votingEntity))
                .put(Strings.KEY_GENERATION_PHASE.toString(), createKeyGenerationPhase(votingEntity))
                .put(Strings.VOTING_PHASE.toString(),createVotingPhase(votingEntity))
                .put(Strings.DECRYPTION_PHASE.toString(),createDecryptionPhase(votingEntity));

        return Optional.of(retObject);
    }

    private JSONObject createMetaDataSummary(VotingEntity votingEntity) {
        List<VotingOptionEntity> votingOptionEntityList = votingEntity.getVotingOptions();
        JSONArray optionsJsonArray = new JSONArray();

        for(VotingOptionEntity votingOptionEntity : votingOptionEntityList) {
            JSONObject jsonObject = VotingsConverter.toData(votingOptionEntity).toJson();
            optionsJsonArray.put(jsonObject);
        }

        VotingsConverter.toData(votingEntity.getCalculationParametersEntity()).toJson();

        JSONObject calculationParametersJsonObject = VotingsConverter.toData(votingEntity.getCalculationParametersEntity()).toJson();

        JSONObject infoJsonObject = new JSONObject()
                .put(Strings.SECURITY_THRESHOLD.toString(),votingEntity.getSecurityThreshold())
                .put(Strings.TITLE.toString(),votingEntity.getTitle())
                .put(Strings.DESCRIPTION.toString(),votingEntity.getDescription());

        return new JSONObject()
                .put(Strings.INFO.toString(),infoJsonObject)
                .put(Strings.CALCULATION_PARAMETERS.toString(),calculationParametersJsonObject)
                .put(Strings.OPTIONS.toString(),optionsJsonArray);
    }

    private JSONArray createEligibleVotersSummary(VotingEntity votingEntity) {
        JSONArray jsonArray = new JSONArray();

        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            JSONObject eligibleVoterObject = new JSONObject()
                    .put(Strings.PUBLIC_KEY.toString(),singleVotingVoterEntity.getPublicKey())
                    .put(Strings.IS_TRUSTEE.toString(),singleVotingVoterEntity.getIsTrustee())
                    .put(Strings.CAN_VOTE.toString(),singleVotingVoterEntity.getCanVote())
                    .put(Strings.POSITION.toString(),singleVotingVoterEntity.getVoterNumber())
                    .put(Strings.ID.toString(),singleVotingVoterEntity.getUserEntity().getId())
                    .put(Strings.NAME.toString(),"***");

            jsonArray.put(eligibleVoterObject);
        }

        return jsonArray;
    }

    private JSONArray createKeyGenerationPhase(VotingEntity votingEntity) {
        JSONArray jsonArray = new JSONArray();

        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            long voterId = singleVotingVoterEntity.getUserEntity().getId();
            long votingId = singleVotingVoterEntity.getVotingEntity().getId();

            if(!singleVotingVoterEntity.createdSecretShares()) {
                continue;
            }

            JSONArray commitmentArray = getCommitments(voterId,votingId);
            JSONArray secretSharesArray = getShares(voterId,votingId);

            JSONObject jsonObject = new JSONObject()
                    .put(Strings.FROM_VOTER_ID.toString(),voterId)
                    .put(Strings.COMMITMENTS.toString(),commitmentArray)
                    .put(Strings.SECRET_SHARES.toString(),secretSharesArray);

            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    private JSONArray getCommitments(long voterId, long votingId) {
        JSONArray commitmentArray = new JSONArray();
        Optional<SingleVotingVoterEntity> singleVotingVoterEntityOptional = singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(voterId,votingId);
        if(!singleVotingVoterEntityOptional.isPresent()) {
            return commitmentArray;
        }

        SingleVotingVoterEntity singleVotingVoterEntityWithCommitments = singleVotingVoterEntityOptional.get();

        for(CommitmentEntity commitmentEntity : singleVotingVoterEntityWithCommitments.getCommitments()) {
            SingleCommitment singleCommitment = CommitmentConverter.toData(commitmentEntity);
            JSONObject commitment = singleCommitment.toJson();

            commitmentArray.put(commitment);
        }

        return commitmentArray;
    }

    private JSONArray getShares(long voterId, long votingId) {
        JSONArray commitmentArray = new JSONArray();
        Optional<SingleVotingVoterEntity> singleVotingVoterEntityOptional = singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(voterId,votingId);
        if(!singleVotingVoterEntityOptional.isPresent()) {
            return commitmentArray;
        }

        SingleVotingVoterEntity singleVotingVoterEntity = singleVotingVoterEntityOptional.get();

        for(SecretShareEntity secretShareEntity : singleVotingVoterEntity.getSecretShares()) {
            secretShareEntity.setFromVotername("***");
            SingleSecretShareForVoter share = SecretShareForVoterConverter.toData(secretShareEntity);
            commitmentArray.put(share.toJson());
        }

        return commitmentArray;
    }

    private JSONArray createVotingPhase(VotingEntity votingEntity) {
        JSONArray votingPhaseArray = new JSONArray();

        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            if(!singleVotingVoterEntity.getVoted()) {
                continue;
            }

            JSONArray jsonProofArray = new JSONArray();

            for(VoteNIZKPEntity voteNIZKPEntity : singleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().values()) {
                JSONObject proof = VoteNIZKProofsConverter.toData(voteNIZKPEntity).toJson();
                jsonProofArray.put(proof);
            }

            JSONObject dataObject = new JSONObject()
                    .put(Strings.FROM_VOTER_ID.toString(),singleVotingVoterEntity.getUserEntity().getId())
                    .put(Strings.FROM_VOTER_NAME.toString(),"***")
                    .put(Strings.ALPHA.toString(),singleVotingVoterEntity.getAlpha())
                    .put(Strings.BETA.toString(),singleVotingVoterEntity.getBeta())
                    .put(Strings.SIGNATURE.toString(),singleVotingVoterEntity.getVoteSignature());

            JSONObject jsonObject = new JSONObject()
                    .put(Strings.DATA.toString(),dataObject)
                    .put(Strings.PROOFS.toString(),jsonProofArray);

            votingPhaseArray.put(jsonObject);
        }

        return votingPhaseArray;
    }

    private JSONArray createDecryptionPhase(VotingEntity votingEntity) {
        JSONArray decryptionPhaseArray = new JSONArray();

        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {

            if(!singleVotingVoterEntity.participatedInDecryption()) {
                continue;
            }

            JSONObject dataObject = new JSONObject()
                    .put(Strings.FROM_VOTER_ID.toString(),singleVotingVoterEntity.getUserEntity().getId())
                    .put(Strings.FROM_VOTER_POSITION.toString(),singleVotingVoterEntity.getVoterNumber())
                    .put(Strings.DECRYPTION_FACTOR.toString(),singleVotingVoterEntity.getDecryptionFactor())
                    .put(Strings.FROM_VOTER_NAME.toString(),"***")
                    .put(Strings.SIGNATURE.toString(),singleVotingVoterEntity.getDecryptionFactorSignature());

            JSONObject proofObject = DecryptionNIZKProofConverter.toData(singleVotingVoterEntity.getDecryptionNIZKProof()).toJson();

            JSONObject jsonObject = new JSONObject()
                    .put(Strings.DATA.toString(),dataObject)
                    .put(Strings.PROOF.toString(),proofObject);

            decryptionPhaseArray.put(jsonObject);
        }

        return decryptionPhaseArray;
    }

}
