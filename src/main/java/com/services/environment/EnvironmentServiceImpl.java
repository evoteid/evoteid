package com.services.environment;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("EnvironmentService")
public class EnvironmentServiceImpl implements EnvironmentService{

    private boolean isDemoEnvironment = false;

    @Override
    public boolean isDemoEnvironment() {
        return this.isDemoEnvironment;
    }

    @Override
    public void setIsDemoEnvironment(boolean value) {
        this.isDemoEnvironment = value;
    }
}
