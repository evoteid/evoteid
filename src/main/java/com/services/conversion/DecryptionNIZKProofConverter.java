package com.services.conversion;

import com.data.proofs.DecryptionNIZKProof;
import com.models.singlevotingvoter.DecryptionNIZKPEntity;

public class DecryptionNIZKProofConverter {

    private DecryptionNIZKProofConverter() {
    }

    public static DecryptionNIZKProof toData(DecryptionNIZKPEntity decryptionNIZKPEntity) {
        return new DecryptionNIZKProof()
                .setA(decryptionNIZKPEntity.getA())
                .setB(decryptionNIZKPEntity.getB())
                .setR(decryptionNIZKPEntity.getR())
                .setSignature(decryptionNIZKPEntity.getSignature());
    }

    public static DecryptionNIZKPEntity toEntity(DecryptionNIZKProof decryptionNIZKProof) {
        return new DecryptionNIZKPEntity()
                .setA(decryptionNIZKProof.getA())
                .setB(decryptionNIZKProof.getB())
                .setR(decryptionNIZKProof.getR())
                .setSignature(decryptionNIZKProof.getSignature());
    }
}
