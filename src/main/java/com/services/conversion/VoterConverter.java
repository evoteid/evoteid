package com.services.conversion;

import com.data.voter.SingleVoter;
import com.models.user.UserEntity;

public class VoterConverter {

    private VoterConverter(){
    }

    public static SingleVoter toData(UserEntity userEntity) {
        SingleVoter voter = new SingleVoter()
                .setName(userEntity.getUsername())
                .setEmail(userEntity.getEmail())
                .setNumberOfVotingsWithTrusteeRole(userEntity.getNumberOfVotingsWithTrusteeRole());
        if(userEntity.getId()!=null) {
            voter.setId(userEntity.getId());
        }
        if(userEntity.getPublicKey()!=null) {
            voter.setPublicKey(userEntity.getPublicKey());
        }
        if(userEntity.getExternalId()!=null) {
            voter.setUniqueId(userEntity.getExternalId());
        }
        return voter;
    }
}
