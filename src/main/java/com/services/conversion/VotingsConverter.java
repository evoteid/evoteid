package com.services.conversion;

import com.data.votings.*;
import com.models.user.UserEntity;
import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingOptionEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;

public class VotingsConverter {

    private VotingsConverter(){
    }

    public static Votings toData(Iterable<VotingEntity> entities) {
        Votings votings = new Votings();
        for(VotingEntity votingEntity : entities) {
            votings.addVoting(toData(votingEntity));
        }
        return votings;
    }

    public static Voting toData(VotingEntity entity) {
        Voting voting = new Voting();
        setVotingFieldsFromVotingEntity(voting,entity);
        return voting;
    }

    private static void setVotingFieldsFromVotingEntity(Voting voting, VotingEntity votingEntity) {
        voting
                .setId(votingEntity.getId())
                .setTitle(votingEntity.getTitle())
                .setDescription(votingEntity.getDescription())
                .setPhase(votingEntity.getPhaseNumber())
                .setTimeVotingIsFinished(votingEntity.getTimeVotingIsFinished())
                .setTransactionId(votingEntity.getTransactionId());

        for(VotingOptionEntity option : votingEntity.getVotingOptions()) {
            voting.addVotingOption(toData(option));
        }
    }

    public static VotingOfficials toVotingOfficials(VotingEntity v) {
        VotingOfficials votingOfficials = new VotingOfficials();
        votingOfficials
                .setVotingId(v.getId())
                .setVotingAdministrator(v.getVotingAdministrator().getUserEntity().getUsername());

        for(SingleVotingVoterEntity s : v.getAllEligibleVoters()) {
            if(s.getIsTrustee()) {
                votingOfficials.addTrustee(s.getUserEntity().getUsername());
            }
        }

        return votingOfficials;
    }

    public static VotingWithVoterStatus toDataWithVotingStatus(VotingEntity votingEntity, boolean specifiedSecretShares, boolean voted) {
        VotingWithVoterStatus votingWithVoterStatus = new VotingWithVoterStatus();
        setVotingFieldsFromVotingEntity(votingWithVoterStatus,votingEntity);
        votingWithVoterStatus
                .setCreatedSecretShares(specifiedSecretShares)
                .setVoted(voted);
        return votingWithVoterStatus;
    }

    public static VotingOption toData(VotingOptionEntity option) {
        return new VotingOption()
                .setOptionName(option.getOptionName())
                .setOptionPrimeNumber(option.getOptionPrimeNumber());
    }

    public static EligibleVoterWithKey toData(SingleVotingVoterEntity singleVotingVoterEntity) {
        UserEntity userEntity = singleVotingVoterEntity.getUserEntity();
        return new EligibleVoterWithKey()
                .setId(userEntity.getId())
                .setName(userEntity.getUsername())
                .setPublicKey(singleVotingVoterEntity.getPublicKey())
                .setPosition(singleVotingVoterEntity.getVoterNumber())
                .setCanVote(singleVotingVoterEntity.getCanVote())
                .setIsTrustee(singleVotingVoterEntity.getIsTrustee());
    }

    public static VotingCalculationParameters toData(VotingCalculationParametersEntity calculationParametersEntity) {
        return new VotingCalculationParameters()
                .setPrimeP(calculationParametersEntity.getPrimeP())
                .setPrimeQ(calculationParametersEntity.getPrimeQ())
                .setGeneratorG(calculationParametersEntity.getGeneratorG());
    }

    public static VotingEntity toEntity(NewlyCreatedVoting voting) {
        VotingEntity votingEntity = new VotingEntity()
                .setTitle(voting.getTitle())
                .setDescription(voting.getDescription())
                .setSecurityThreshold(voting.getSecurityThreshold())
                .setNumberOfEligibleVoters(voting.getNumberOfEligibleVoters())
                .setPhaseNumber(voting.getPhase());

        VotingCalculationParametersEntity votingCalculationParametersEntity = toEntity(voting.getVotingCalculationParameters());
        votingEntity.setCalculationParametersEntity(votingCalculationParametersEntity);

        for(VotingOption option : voting.getVotingOptions()) {
            votingEntity.addVotingOption(toEntity(option));
        }

        return votingEntity;
    }

    public static VotingCalculationParametersEntity toEntity(VotingCalculationParameters calculationParameters) {
        return new VotingCalculationParametersEntity()
                .setPrimeP(calculationParameters.getPrimeP())
                .setPrimeQ(calculationParameters.getPrimeQ())
                .setGeneratorG(calculationParameters.getGeneratorG());
    }

    public static VotingOptionEntity toEntity(VotingOption votingOption) {
        return new VotingOptionEntity()
                .setOptionName(votingOption.getOptionName())
                .setOptionPrimeNumber(votingOption.getOptionPrimeNumber());
    }

}
