package com.services.conversion;

import com.data.votingvoterprogress.VotingVoterProgress;
import com.models.singlevotingvoter.SingleVotingVoterEntity;

public class VotingVoterProgressConverter {

    private VotingVoterProgressConverter() {
    }

    public static VotingVoterProgress toData(SingleVotingVoterEntity singleVotingVoterEntity) {
        return new VotingVoterProgress()
                .setVotingId(singleVotingVoterEntity.getVotingEntity().getId())
                .setPhaseNumber(singleVotingVoterEntity.getVotingEntity().getPhaseNumber())
                .setCreatedSecretShares(singleVotingVoterEntity.createdSecretShares())
                .setVoted(singleVotingVoterEntity.getVoted())
                .setParticipatedInDecryption(singleVotingVoterEntity.participatedInDecryption())
                .setUsesCustomKey(singleVotingVoterEntity.getCreatedOwnKey());
    }
}
