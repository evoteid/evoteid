package com.services.conversion;

import com.data.secretshares.SingleSecretShareForVoter;
import com.models.singlevotingvoter.SecretShareEntity;
import com.utils.StringUtils;

import java.util.List;

public class SecretShareForVoterConverter {

    private SecretShareForVoterConverter() {
    }

    public static SingleSecretShareForVoter toData(SecretShareEntity secretShareEntity) {
        long fromVoterId = secretShareEntity.getFromVoterId();
        String fromVoterName = secretShareEntity.getFromVotername();
        String secretShare = secretShareEntity.getSecretShare();
        String signature = secretShareEntity.getSignature();
        List<String> splittedShare = StringUtils.stringToList(secretShare);
        return new SingleSecretShareForVoter(fromVoterId,fromVoterName,splittedShare,signature);
    }
}
