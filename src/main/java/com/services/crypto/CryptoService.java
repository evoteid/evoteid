package com.services.crypto;

import com.utils.data.RsaKeyPair;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class CryptoService {

    private CryptoService() {
    }

    public static RsaKeyPair generateKey() {
        KeyPairGenerator kpg = null;

        try {
            kpg = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        kpg.initialize(2048); KeyPair kp = kpg.generateKeyPair();

        String publicKey =
        "-----BEGIN PUBLIC KEY-----\n"+
        Base64.getMimeEncoder(65,"\n".getBytes()).encodeToString( kp.getPublic().getEncoded())+"\n"+
        "-----END PUBLIC KEY-----";

        String privateKey =
        "-----BEGIN PRIVATE KEY-----\n"+
        Base64.getMimeEncoder(65,"\n".getBytes()).encodeToString( kp.getPrivate().getEncoded())+"\n"+
        "-----END PRIVATE KEY-----";

        return new RsaKeyPair(publicKey,privateKey);
    }
}
