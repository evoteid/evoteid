package com.services.secrets;

public interface SecretsService {

    byte[] getJwtSigningKey();

    void storeJwtSigningKey(byte[] key);

    String getAdminPassword();

    String getEthereumPassword();
}
