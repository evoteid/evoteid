package com.services.secrets;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("SecretsService")
public class FakeSecretsService implements SecretsService {

    private byte[] signingKey;

    @Override
    public byte[] getJwtSigningKey() {
        return signingKey;
    }

    @Override
    public void storeJwtSigningKey(byte[] key) {
        this.signingKey = key;
    }

    @Override
    public String getAdminPassword() {
        return "karnutenwald35c";
    }

    @Override
    public String getEthereumPassword() {
        return "test";
    }
}
