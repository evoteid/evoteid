package com.services.parser;

import com.data.votes.SingleVote;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoContentException;
import com.exceptions.parser.ParserException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class VoteParser {

    private VoteParser() {
    }

    public static SingleVote parse(String jsonString) throws ParserException {
        SingleVote singleVote = parseWithException(jsonString);
        validate(singleVote,jsonString);
        return singleVote;
    }

    private static SingleVote parseWithException(String jsonString) throws NoContentException {
        SingleVote singleVote = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            singleVote = mapper.readValue(jsonString, SingleVote.class);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return singleVote;
    }

    private static void validate(SingleVote singleVote, String s) throws ParserException {
        if (singleVote.getFromVoterId() == null) {
            throw new MissingFieldException("No fromVoterId field in \"" + s + "\"");
        }
        if (singleVote.getFromVoterName() == null) {
            throw new MissingFieldException("No fromVoterName field in \"" + s + "\"");
        }
        if (singleVote.getAlpha() == null) {
            throw new MissingFieldException("No alpha field in \"" + s + "\"");
        }
        if (singleVote.getBeta() == null) {
            throw new MissingFieldException("No beta field in \"" + s + "\"");
        }
        if (singleVote.getSignature() == null) {
            throw new MissingFieldException("No signature field in \"" + s + "\"");
        }
    }
}
