package com.services.parser;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.SingleCommitment;
import com.exceptions.parser.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class CommitmentParser {

    private CommitmentParser() {
    }

    public static CommitmentFromVoter parse(String jsonString) throws ParserException {
        CommitmentFromVoter commitmentFromVoter = parseWithException(jsonString);
        validate(commitmentFromVoter,jsonString);
        return commitmentFromVoter;
    }

    private static CommitmentFromVoter parseWithException(String jsonString) throws NoContentException {
        CommitmentFromVoter commitmentFromVoter = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            commitmentFromVoter = mapper.readValue(jsonString, CommitmentFromVoter.class);
        } catch (IOException e) {
            throw new NoContentException(String.format("No content in string '%s'",jsonString),e);
        }
        return commitmentFromVoter;
    }

    private static void validate(CommitmentFromVoter commitmentFromVoter, String s) throws ParserException {
        if(commitmentFromVoter.getNumberOfCommitments()==null) {
            throw new MissingFieldException(String.format("Missing number of commitment fields in '%s'",s));
        }

        if(commitmentFromVoter.getNumberOfCommitments()> commitmentFromVoter.getSingleCommitmentList().size()) {
            throw new TooFewCommitmentsException(String.format("Too few commitmentFromVoter in '%s'",s));
        }

        if(commitmentFromVoter.getNumberOfCommitments()< commitmentFromVoter.getSingleCommitmentList().size()) {
            throw new TooManyCommitmentsException(String.format("Too many commitmentFromVoter in '%s'",s));
        }

        int value = commitmentFromVoter.getNumberOfCommitments().intValue();

        boolean[] commitmentsThere = new boolean[value];

        for(SingleCommitment singleCommitment : commitmentFromVoter.getSingleCommitmentList()) {
            if(singleCommitment.getCommitment()==null) {
                throw new MissingFieldException(String.format("A commitment is missing the value in '%s'",s));
            }
            if(singleCommitment.getCoefficientNumber()==null) {
                throw new MissingFieldException(String.format("A commitment is missing the coefficient number in '%s'",s));
            }
            if(singleCommitment.getSignature()==null) {
                throw new MissingFieldException(String.format("A commitment is missing the signature in '%s'",s));
            }

            int coefficientValue = singleCommitment.getCoefficientNumber().intValue();
            if(coefficientValue<0) {
                throw new IllegalCoefficientValueException(String.format("Illegal (too low) coefficient value '%d' in '%s'",coefficientValue,s));
            }

            if(coefficientValue>=value) {
                throw new IllegalCoefficientValueException(String.format("Illegal (too high) coefficient value '%d' in '%s'",coefficientValue,s));
            }

            if(commitmentsThere[coefficientValue]) {
                throw new IllegalCoefficientValueException(String.format("Illegal (duplicate) coefficient value '%d' in '%s'",coefficientValue,s));
            }

            commitmentsThere[coefficientValue] = true;
        }


    }
}
