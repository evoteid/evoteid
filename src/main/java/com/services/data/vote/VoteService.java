package com.services.data.vote;

import com.data.dataforkeyderivation.DataForKeyDerivation;
import com.data.decryptionfactor.DecryptionFactors;
import com.data.decryptionfactor.SingleDecryptionFactor;
import com.data.votes.SingleVote;
import com.data.votes.Votes;
import com.exceptions.IllegalVoteException;

import java.util.Optional;

public interface VoteService {

    Optional<DataForKeyDerivation> getDataForKeyDerivation(Long votingId);

    boolean setVoteForVoter(long voterId, long votingId, SingleVote singleVote) throws IllegalVoteException;

    Votes getAllVotesForVoting(long votingId);

    boolean setDecryptionFactor(long voterId, long votingId, SingleDecryptionFactor singleDecryptionFactor) throws IllegalVoteException;

    DecryptionFactors getAllDecryptionFactors(long votingId);

}
