package com.services.data.commitments;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.Commitments;
import com.data.commitments.SingleCommitment;
import com.enums.Phases;
import com.exceptions.DuplicateException;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.exceptions.VotingClosedException;
import com.models.singlevotingvoter.CommitmentEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.services.conversion.CommitmentConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("CommitmentService")
public class CommitmentServiceImpl implements CommitmentService {

    private final SingleVotingVoterRepository singleVotingVoterRepository;

    private final VotingsRepository votingsRepository;

    @Autowired
    public CommitmentServiceImpl(SingleVotingVoterRepository singleVotingVoterRepository,
                                 VotingsRepository votingsRepository) {
        this.singleVotingVoterRepository = singleVotingVoterRepository;
        this.votingsRepository = votingsRepository;
    }

    @Override
    public Commitments getAllCommitmentsForVoting(long votingId) {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findOneByIdWithEligibleVotersWithCommitmentsFetched(votingId);
        if(!optVotingEntity.isPresent()) {
            //TODO: 12.11.2018 handle error case
            throw new UnsupportedOperationException();
        }

        VotingEntity votingEntity = optVotingEntity.get();

        Commitments commitments = new Commitments();

        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            if(!singleVotingVoterEntity.createdSecretShares()) {
                continue;
            }

            CommitmentFromVoter commitmentFromVoter = toCommitmentFromVoter(singleVotingVoterEntity);
            commitments.addCommitmentsFromVoter(commitmentFromVoter);
        }

        return commitments;
    }

    @Override
    public CommitmentFromVoter getCommitmentsFromUser(long votingId, long userId) {
        Optional<SingleVotingVoterEntity> optEntity = singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(userId,votingId);
        if(!optEntity.isPresent()) {
            //TODO: 31.10.2018 handle error case
            throw new UnsupportedOperationException();
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optEntity.get();

        return toCommitmentFromVoter(singleVotingVoterEntity);
    }

    private CommitmentFromVoter toCommitmentFromVoter(SingleVotingVoterEntity singleVotingVoterEntity) {
        CommitmentFromVoter commitmentFromVoter = new CommitmentFromVoter();
        for(CommitmentEntity commitmentEntity : singleVotingVoterEntity.getCommitments()) {
            SingleCommitment singleCommitment = CommitmentConverter.toData(commitmentEntity);
            commitmentFromVoter.addCommitments(singleCommitment);
        }
        commitmentFromVoter
                .setFromVoterId(singleVotingVoterEntity.getUserEntity().getId())
                .setFromVoterName(singleVotingVoterEntity.getUserEntity().getUsername())
                .setFromVoterPosition(singleVotingVoterEntity.getVoterNumber())
                .setNumberOfCommitments(singleVotingVoterEntity.getCommitments().size());
        return commitmentFromVoter;
    }


    @Override
    public void setCommitmentsForUser(long votingId, long userId, CommitmentFromVoter commitmentFromVoter) throws IllegalVoteException {
        Optional<SingleVotingVoterEntity> optEntity = singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(userId,votingId);
        if(!optEntity.isPresent()) {
            throw new NoSuchVotingException("The voting was not found.");
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optEntity.get();

        if(singleVotingVoterEntity.getVotingEntity().getPhaseNumber() != Phases.KEY_DERIVATION_PHASE.getNumber()) {
            throw new VotingClosedException("The key derivation phase is over.");
        }
        if(!singleVotingVoterEntity.getCommitments().isEmpty()) {
            throw new DuplicateException("You already provided your commitments.");
        }

        for(SingleCommitment singleCommitment : commitmentFromVoter.getSingleCommitmentList()) {
            CommitmentEntity commitmentEntity = CommitmentConverter.toEntity(singleCommitment);
            singleVotingVoterEntity.addCommitments(commitmentEntity);
        }

        singleVotingVoterRepository.saveAndFlush(singleVotingVoterEntity);
    }
}
