package com.services.data.roles;

import com.data.roles.Roles;
import com.data.voter.SingleVoter;
import com.data.voter.Voters;
import com.enums.Role;
import com.models.role.RoleRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.services.conversion.RoleConverter;
import com.services.conversion.VoterConverter;
import jnr.ffi.annotations.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@Qualifier("RoleService")
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    private UserRepository userRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Optional<SingleVoter> getVoterWithRoles(long voterId) {
        Optional<UserEntity> userWithRole = userRepository.findOneById(voterId);

        if(!userWithRole.isPresent()) {
            return Optional.empty();
        }
        return toData(userWithRole);
    }

    @Override
    public Optional<SingleVoter> getVoterWithRoles(String voterName) {
        Optional<UserEntity> userWithRole = userRepository.findOneByUsername(voterName);

        if(!userWithRole.isPresent()) {
            return Optional.empty();
        }
        return toData(userWithRole);
    }

    @Override
    public boolean hasRole(long voterId, Role role) {
        Optional<UserEntity> optVoter = userRepository.findOneById(voterId);
        if(!optVoter.isPresent()) {
            return false;
        }

        UserEntity voter = optVoter.get();

        if(Role.ADMINISTRATOR.equals(role)) {
            return voter.getRoles().hasAdministratorRole();
        }
        else if(Role.VOTING_ADMINISTRATOR.equals(role)) {
            return voter.getRoles().hasVotingAdministratorRole();
        }
        else if(Role.REGISTRAR.equals(role)) {
            return voter.getRoles().hasRegistrarRole();
        }
        return false;
    }

    @Override
    @Synchronized
    public boolean addRoleToVoter(long voterId, Role role) {
        int modifiedRows = -1;
        if(Role.ADMINISTRATOR.equals(role)) {
            modifiedRows = roleRepository.updateAdminRole(voterId,true);
        }
        else if(Role.VOTING_ADMINISTRATOR.equals(role)) {
            modifiedRows = roleRepository.updateVotingAdminRole(voterId,true);
        }
        else if(Role.REGISTRAR.equals(role)) {
            modifiedRows = roleRepository.updateRegistrarRoleRole(voterId,true);
        }

        return modifiedRows==1;
    }

    @Override
    public boolean deleteRoleFromVoter(long voterId, Role role) {
        int modifiedRows = -1;
        if(Role.ADMINISTRATOR.equals(role)) {
            modifiedRows = roleRepository.updateAdminRole(voterId,false);
        }
        else if(Role.VOTING_ADMINISTRATOR.equals(role)) {
            modifiedRows = roleRepository.updateVotingAdminRole(voterId,false);
        }
        else if(Role.REGISTRAR.equals(role)) {
            modifiedRows = roleRepository.updateRegistrarRoleRole(voterId,false);
        }

        return modifiedRows==1;
    }

    @Override
    public Voters getAllVotersWithTheirRoles() {
        Voters voters = new Voters();
        userRepository.findAll().forEach(u -> voters.addVoter(toData(u)));
        return voters;
    }

    @Override
    public Voters getAllVotersByIdWithTheirRoles(List<Long> ids) {
        Voters voters = new Voters();
        userRepository.findAllById(ids).forEach(u -> voters.addVoter(toData(u)));
        return voters;
    }

    @Override
    public Roles getAllAvailableRoles() {
        Roles roles = new Roles();
        for(Role role : Role.values()) {
            roles.addRole(role);
        }
        return roles;
    }

    private Optional<SingleVoter> toData(Optional<UserEntity> userEntity) {

        if(!userEntity.isPresent()) {
            return Optional.empty();
        }

        return Optional.of(toData(userEntity.get()));
    }

    private SingleVoter toData(UserEntity userEntity) {
        SingleVoter voter = VoterConverter.toData(userEntity);
        Roles roles = RoleConverter.toData(userEntity.getRoles());
        voter.setRoles(roles);
        return voter;
    }
}


