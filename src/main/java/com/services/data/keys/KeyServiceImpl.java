package com.services.data.keys;

import com.enums.Phases;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.exceptions.PKIClosedException;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("KeyService")
public class KeyServiceImpl implements KeyService {

    private UserRepository userRepository;

    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    public KeyServiceImpl(UserRepository userRepository,
                          SingleVotingVoterRepository singleVotingVoterRepository) {
        this.userRepository = userRepository;
        this.singleVotingVoterRepository = singleVotingVoterRepository;
    }

    @Override
    public Optional<String> getPrivateKey(Long voterId) {
        Optional<UserEntity> optVoterEntity = userRepository.findOneById(voterId);
        if(!optVoterEntity.isPresent()) {
            return Optional.empty();
        }

        UserEntity userEntity = optVoterEntity.get();
        if(userEntity.getPrivateKey() == null) {
            return Optional.empty();
        }

        return Optional.of(userEntity.getPrivateKey());
    }

    @Override
    public boolean setPublicKeyOfVoter(Long voterId, String publicKey) {
        Optional<UserEntity> optionalUserEntity = userRepository.findOneById(voterId);
        if(!optionalUserEntity.isPresent()) {
            return false;
        }
        UserEntity userEntity = optionalUserEntity.get();
        userEntity.setPublicKey(publicKey);
        userEntity.setPrivateKey(null);
        userRepository.saveAndFlush(userEntity);
        return true;
    }

    @Override
    public Optional<String> getPublicKeyOfVoterInVoting(Long voterId, Long votingId) {
        Optional<SingleVotingVoterEntity> optSingleVotingVoterEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optSingleVotingVoterEntity.isPresent()) {
            return Optional.empty();
        }
        return Optional.ofNullable(optSingleVotingVoterEntity.get().getPublicKey());
    }

    @Override
    public boolean setPublicKeyOfVoterInVoting(Long voterId, Long votingId, String publicKey) throws IllegalVoteException {
        Optional<SingleVotingVoterEntity> optEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optEntity.isPresent()) {
            throw new NoSuchVotingException("The voting does not exist");
        }

        SingleVotingVoterEntity entity = optEntity.get();

        if(entity.getVotingEntity().getPhaseNumber() != Phases.PKI_PHASE.getNumber()) {
            throw new PKIClosedException("The phase for specifying a custom public key is already over");
        }

        entity.setPublicKey(publicKey).setCreatedOwnKeyToTrue();
        singleVotingVoterRepository.saveAndFlush(entity);
        return true;
    }
}
