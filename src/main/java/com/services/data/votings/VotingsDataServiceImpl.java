package com.services.data.votings;

import com.data.voter.SingleVoter;
import com.data.votingprogress.SingleVoterProgress;
import com.data.votingprogress.VotingProgress;
import com.data.votingvoterprogress.VotingVoterProgress;
import com.data.votings.*;
import com.data.votingsforvoterwithprogress.VotingsForVoterWithProgress;
import com.enums.Phases;
import com.exceptions.NoSuchVotingException;
import com.models.createdvotings.CreatedVotingsEntity;
import com.models.createdvotings.CreatedVotingsRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.services.conversion.VoterConverter;
import com.services.conversion.VotingVoterProgressConverter;
import com.services.conversion.VotingsConverter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Qualifier("VotingsDataService")
public class VotingsDataServiceImpl implements VotingsDataService {

    private final VotingsRepository votingsRepository;

    private final SingleVotingVoterRepository singleVotingVoterRepository;

    private final UserRepository userRepository;

    private final CreatedVotingsRepository createdVotingsRepository;

    @Autowired
    public VotingsDataServiceImpl(final VotingsRepository votingsRepository,
                                  final SingleVotingVoterRepository singleVotingVoterRepository,
                                  final UserRepository userRepository,
                                  final CreatedVotingsRepository createdVotingsRepository) {
        this.votingsRepository = votingsRepository;
        this.singleVotingVoterRepository = singleVotingVoterRepository;
        this.userRepository = userRepository;
        this.createdVotingsRepository = createdVotingsRepository;
    }

    @Override
    public Votings getAllVotings() {
        Iterable<VotingEntity> votingEntities = votingsRepository.findAll();
        return VotingsConverter.toData(votingEntities);
    }

    @Override
    public Votings getAllVotingsByVoterId(Long id) {
        Votings votings = new Votings();

        Optional<UserEntity> userEntity = userRepository.findOneByIdWithVotingsFetched(id);
        if(!userEntity.isPresent()){
            return votings;
        }

        List<SingleVotingVoterEntity> allVotings = userEntity.get().getVotings();

        for(SingleVotingVoterEntity singleVoting : allVotings) {
            Voting voting = VotingsConverter.toData(singleVoting.getVotingEntity());
            votings.addVoting(voting);
        }
        return votings;
    }

    @Override
    public VotingsForVoterWithProgress getAllVotingsByVoterIdAndPhaseNumber(Long id, Integer phaseNumber) {
        Optional<UserEntity> userEntity = userRepository.findOneByIdWithVotingsFetched(id);
        if(!userEntity.isPresent()){
            return new VotingsForVoterWithProgress();
        }

        List<SingleVotingVoterEntity> allVotings = userEntity.get().getVotings();

        List<Voting> votings = new ArrayList<>();
        List<VotingVoterProgress> votingProgresses = new ArrayList<>();
        List<VotingOfficials> votingOfficialsList = new ArrayList<>();

        for(SingleVotingVoterEntity singleVoting : allVotings) {
            if(!shouldBeIncluded(singleVoting, phaseNumber)) {
                continue;
            }

            Voting voting = VotingsConverter.toData(singleVoting.getVotingEntity());
            votings.add(voting);

            VotingVoterProgress votingVoterProgress = VotingVoterProgressConverter.toData(singleVoting);
            votingProgresses.add(votingVoterProgress);

            VotingOfficials votingOfficials = VotingsConverter.toVotingOfficials(singleVoting.getVotingEntity());
            votingOfficialsList.add(votingOfficials);
        }
        return new VotingsForVoterWithProgress(votings, votingProgresses, votingOfficialsList);
    }

    @Override
    public Votings getAllVotingsByVotingAdministratorId(Long id) {
        Votings votings = new Votings();

        Optional<UserEntity> userEntity = userRepository.findOneByIdWithCreatedVotingsFetched(id);
        if(!userEntity.isPresent()){
            return votings;
        }

        List<CreatedVotingsEntity> createdVotings = userEntity.get().getCreatedVotings();

        for(CreatedVotingsEntity createdVotingsEntity : createdVotings) {
            Voting voting = VotingsConverter.toData(createdVotingsEntity.getVotingEntity());
            votings.addVoting(voting);
        }
        return votings;
    }

    @Override
    public Optional<VotingProgress> getVotingProgress(Long votingId) {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);
        if(!optVotingEntity.isPresent()) {
            return Optional.empty();
        }

        VotingEntity votingEntity = optVotingEntity.get();

        List<SingleVotingVoterEntity> eligibleVoters = votingEntity.getAllEligibleVoters();
        List<SingleVoterProgress> voterProgress = new ArrayList<>();

        for(SingleVotingVoterEntity singleVotingVoterEntity : eligibleVoters) {
            SingleVoter voter = VoterConverter.toData(singleVotingVoterEntity.getUserEntity());

            SingleVoterProgress singleVoterProgress = new SingleVoterProgress()
                    .setVoter(voter)
                    .setPosition(singleVotingVoterEntity.getVoterNumber())
                    .setParticipatedInKeyGeneration(singleVotingVoterEntity.createdSecretShares())
                    .setParticipatedInVoting(singleVotingVoterEntity.getVoted())
                    .setParticipatedInDecryption(singleVotingVoterEntity.participatedInDecryption())
                    .setCanVote(singleVotingVoterEntity.getCanVote())
                    .setIsTrustee(singleVotingVoterEntity.getIsTrustee())
                    .setCreatedCustomKey(singleVotingVoterEntity.getCreatedOwnKey());

            voterProgress.add(singleVoterProgress);
        }

        VotingProgress votingProgress = new VotingProgress()
                .setPhaseNumber(votingEntity.getPhaseNumber())
                .setSecurityThreshold(votingEntity.getSecurityThreshold())
                .setTransactionId(votingEntity.getTransactionId())
                .setTimeVotingIsFinished(votingEntity.getTimeVotingIsFinished())
                .setVotersProgressList(voterProgress)
                .setVotingAdministrator(VoterConverter.toData(votingEntity.getVotingAdministrator().getUserEntity()));

        return Optional.of(votingProgress);
    }

    @Override
    public Optional<Voting> addVoting(NewlyCreatedVoting voting) {
        VotingEntity votingEntity = VotingsConverter.toEntity(voting);
        votingEntity.setPhaseNumber(-1);
        votingsRepository.saveAndFlush(votingEntity);

        for(EligibleVoter voter : voting.getEligibleVoters()) {
            Long voterId = voter.getId();
            Optional<UserEntity> optUserEntity = userRepository.findOneByIdWithVotingsFetched(voterId);
            if(!optUserEntity.isPresent()) {
                return Optional.empty();
            }
            UserEntity userEntity = optUserEntity.get();
            SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity)
                    .setVoterNumber(voter.getPosition())
                    .setPublicKey(userEntity.getPublicKey())
                    .setIsTrustee(voter.getIsTrustee())
                    .setCanVote(voter.getCanVote());

            singleVotingVoterRepository.saveAndFlush(singleVotingVoterEntity);

            votingEntity.addEligibleVoter(singleVotingVoterEntity);
            userEntity.addVoting(singleVotingVoterEntity);
            if(voter.getIsTrustee()) {
                long newTrusteeCount = userEntity.getNumberOfVotingsWithTrusteeRole() + 1;
                userEntity.setNumberOfVotingsWithTrusteeRole(newTrusteeCount);
            }
            userRepository.saveAndFlush(userEntity);
        }

        SingleVoter voter = voting.getVotingAdministrator();
        Long id = voter.getId();

        Optional<UserEntity> optVotingAdministrator = userRepository.findOneByIdWithCreatedVotingsFetched(id);
        if(!optVotingAdministrator.isPresent()) {
            return Optional.empty();
        }

        UserEntity votingAdministrator = optVotingAdministrator.get();

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(votingAdministrator,votingEntity);
        createdVotingsEntity = createdVotingsRepository.saveAndFlush(createdVotingsEntity);

        votingAdministrator.addCreatedVoting(createdVotingsEntity);
        votingEntity.setVotingAdministrator(createdVotingsEntity);

        VotingEntity savedEntity = votingsRepository.saveAndFlush(votingEntity);
        userRepository.saveAndFlush(votingAdministrator);

        return Optional.of(VotingsConverter.toData(savedEntity));
    }

    @Override
    public Optional<Voting> setPhaseNumber(long votingId, int phaseNumber) {
        int numberOfUpdatedRows = votingsRepository.updatePhaseNumber(votingId,phaseNumber);
        if(numberOfUpdatedRows != 1) {
            return Optional.empty();
        }

        Optional<VotingEntity> optVotingEntity = votingsRepository.findById(votingId);
        if(!optVotingEntity.isPresent()) {
            return Optional.empty();
        }

        VotingEntity votingEntity = optVotingEntity.get();
        return Optional.of(VotingsConverter.toData(votingEntity));
    }

    @Override
    public Optional<Voting> setVotingSummary(long votingId, JSONObject votingSummary, String transactionId) {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findById(votingId);
        if(!optVotingEntity.isPresent()) {
            return Optional.empty();
        }

        VotingEntity votingEntity = optVotingEntity
                .get()
                .setVotingSummary(votingSummary.toString())
                .setTransactionId(transactionId);

        VotingEntity savedVotingEntity = votingsRepository.saveAndFlush(votingEntity);
        return Optional.of(VotingsConverter.toData(savedVotingEntity));
    }

    @Override
    public Optional<JSONObject> getVotingSummary(long votingId) {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findById(votingId);
        if(!optVotingEntity.isPresent()) {
            return Optional.empty();
        }

        VotingEntity votingEntity = optVotingEntity.get();

        return Optional.of(new JSONObject(votingEntity.getVotingSummary()));
    }

    @Override
    public Optional<Voting> setTimeVotingIsFinished(long votingId, long endTime) {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findById(votingId);
        if(!optVotingEntity.isPresent()) {
            return Optional.empty();
        }
        VotingEntity votingEntity = optVotingEntity.get();
        if(votingEntity.getPhaseNumber() != Phases.KEY_DERIVATION_PHASE.getNumber()) {
            return Optional.empty();
        }

        if(votingEntity.getTimeVotingIsFinished() != -1) {
            return Optional.empty();
        }

        votingEntity.setTimeVotingIsFinished(endTime);
        VotingEntity savedVotingEntity = votingsRepository.saveAndFlush(votingEntity);
        return Optional.of(VotingsConverter.toData(savedVotingEntity));
    }

    @Override
    @Transactional
    public boolean deleteVoting(Long votingId) throws NoSuchVotingException {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);
        if(!optVotingEntity.isPresent()) {
            throw new NoSuchVotingException("The voting could not be found");
        }

        VotingEntity votingEntity = optVotingEntity.get();
        List<SingleVotingVoterEntity> singleVotingVoterEntities = deleteVotersReference(votingEntity);
        CreatedVotingsEntity createdVotingsEntity = deleteVotingAdministratorReference(votingEntity);

        singleVotingVoterRepository.deleteAll(singleVotingVoterEntities);
        createdVotingsRepository.delete(createdVotingsEntity);

        votingsRepository.deleteById(votingId);

        return true;
    }

    private List<SingleVotingVoterEntity> deleteVotersReference(VotingEntity votingEntity) throws NoSuchVotingException {
        List<SingleVotingVoterEntity> singleVotingVoterEntities = votingEntity.getAllEligibleVoters();

        for(SingleVotingVoterEntity singleVotingVoterEntity : singleVotingVoterEntities) {
            Optional<UserEntity> optUserEntityWithVotings = userRepository.findOneByIdWithVotingsFetched(singleVotingVoterEntity.getUserEntity().getId());
            if(!optUserEntityWithVotings.isPresent()) {
                throw new NoSuchVotingException("Voter in voting cannot be found");
            }

            UserEntity userEntityWithVoting = optUserEntityWithVotings.get();

            userEntityWithVoting.getVotings().remove(singleVotingVoterEntity);
            if(singleVotingVoterEntity.getIsTrustee()) {
                long newTrusteeCount = userEntityWithVoting.getNumberOfVotingsWithTrusteeRole() - 1;
                userEntityWithVoting.setNumberOfVotingsWithTrusteeRole(newTrusteeCount);
            }
            userRepository.saveAndFlush(userEntityWithVoting);
        }

        return singleVotingVoterEntities;
    }

    private CreatedVotingsEntity deleteVotingAdministratorReference(VotingEntity votingEntity) throws NoSuchVotingException {
        CreatedVotingsEntity createdVotingsEntity = votingEntity.getVotingAdministrator();
        long votingAdministratorId = createdVotingsEntity.getUserEntity().getId();
        Optional<UserEntity> optUserEntityWithCreatedVotings = userRepository.findOneByIdWithCreatedVotingsFetched(votingAdministratorId);
        if(!optUserEntityWithCreatedVotings.isPresent()) {
            throw new NoSuchVotingException("Voting Administrator in voting cannot be found");
        }

        UserEntity userEntityWithCreatedVotings = optUserEntityWithCreatedVotings.get();
        userEntityWithCreatedVotings.getCreatedVotings().remove(createdVotingsEntity);
        userRepository.saveAndFlush(userEntityWithCreatedVotings);

        votingEntity.setVotingAdministrator(null);
        votingsRepository.saveAndFlush(votingEntity);

        return createdVotingsEntity;
    }

    private boolean shouldBeIncluded(SingleVotingVoterEntity singleVoting, Integer phaseNumber) {
        if(singleVoting.getVotingEntity().getPhaseNumber() != phaseNumber) {
            return false;
        }
        else if(phaseNumber==Phases.KEY_DERIVATION_PHASE.getNumber() && !singleVoting.getIsTrustee()) {
            return false;
        }
        else if(phaseNumber==Phases.VOTING_PHASE.getNumber() && !singleVoting.getCanVote()) {
            return false;
        }
        else if(phaseNumber==Phases.DECRYPTION_PHASE.getNumber() && !singleVoting.getIsTrustee()) {
            return false;
        }
        else {
            return true;
        }
    }

}
