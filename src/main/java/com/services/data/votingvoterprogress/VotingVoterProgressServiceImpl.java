package com.services.data.votingvoterprogress;

import com.data.votingvoterprogress.VotingVoterProgress;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("VotingVoterProgressService")
public class VotingVoterProgressServiceImpl implements VotingVoterProgressService{

    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    public VotingVoterProgressServiceImpl(SingleVotingVoterRepository singleVotingVoterRepository) {
        this.singleVotingVoterRepository = singleVotingVoterRepository;
    }

    @Override
    public Optional<VotingVoterProgress> getVotingProgressForVoter(Long votingId, Long voterId) {
        Optional<SingleVotingVoterEntity> optionalVotingEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optionalVotingEntity.isPresent()) {
            return Optional.empty();
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optionalVotingEntity.get();

        VotingVoterProgress votingVoterProgress = new VotingVoterProgress()
                .setPhaseNumber(singleVotingVoterEntity.getVotingEntity().getPhaseNumber())
                .setCreatedSecretShares(singleVotingVoterEntity.createdSecretShares())
                .setVoted(singleVotingVoterEntity.getVoted())
                .setParticipatedInDecryption(singleVotingVoterEntity.participatedInDecryption())
                .setUsesCustomKey(singleVotingVoterEntity.getCreatedOwnKey());

        return Optional.of(votingVoterProgress);
    }
}
