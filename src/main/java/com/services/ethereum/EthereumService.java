package com.services.ethereum;

import java.util.Optional;

public interface EthereumService {

    EthereumNetwork getRopstenNetwork();

    Optional<String> sendMessageToTestNetwork(String message);
}
