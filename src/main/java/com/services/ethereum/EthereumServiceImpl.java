package com.services.ethereum;

import com.services.secrets.SecretsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Service
@Qualifier("EthereumService")
public class EthereumServiceImpl implements EthereumService {

    private String ropstenToken = "https://ropsten.infura.io/v3/4457a84d1e8f4fb49bdfd954f1830f1d";

    private SecretsService secretsService;

    private Web3j web3Ropsten;

    private EthereumNetwork ropstenNetwork;

    @Autowired
    public EthereumServiceImpl(SecretsService secretsService) {
        web3Ropsten = Web3j.build(new HttpService(ropstenToken));
        ropstenNetwork = new EthereumNetwork(web3Ropsten);
        this.secretsService = secretsService;
    }

    public EthereumNetwork getRopstenNetwork() {
        return ropstenNetwork;
    }


    @Override
    public Optional<String> sendMessageToTestNetwork(String message) {
        try {
            String password = this.secretsService.getEthereumPassword();
            Path path = Paths.get("ethereumCredentials.json").toAbsolutePath();
            Credentials credentials = WalletUtils.loadCredentials(password, path.toString());

            String address = credentials.getAddress();

            EthereumNetwork ethereumNetwork = getRopstenNetwork();
            Optional<String> optTransactionId = Optional.empty();
            int count = 0;
            while(!optTransactionId.isPresent() && count < 10) {
                optTransactionId = ethereumNetwork.sendMessage(message,address,credentials);
                count++;
            }
            return optTransactionId;
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public Optional<Credentials> getCredentialsFromFile(Path filePath, String password) {
        Credentials credentials;
        try {
            credentials = WalletUtils.loadCredentials(password, filePath.toString());
        } catch (Exception e) {
            return Optional.empty();
        }
        return Optional.of(credentials);
    }

    public Optional<String> generateWallet(String password) {
        String name = null;
        try {
            name = WalletUtils.generateNewWalletFile(password,new File("."));
        } catch (Exception e) {
            return Optional.empty();
        }
        return Optional.of(name);
    }

}

