package com.utils;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;

public class StringUtilsTest {

    private final String stringRep = "[foo, bar, baz]";
    private final List<String> listRep = Arrays.asList("foo","bar","baz");

    @Test
    public void ListToString() {
        Assert.assertEquals(stringRep,StringUtils.listToString(listRep));
    }

    @Test
    public void StringToList() {
        Assert.assertEquals(listRep,StringUtils.stringToList(stringRep));
    }

    @Test
    public void ListToStringEmpty() {
        Assert.assertEquals("[]",StringUtils.listToString(Arrays.asList()));
    }

    @Test
    public void StringToListEmpty() {
        Assert.assertEquals(Arrays.asList(),StringUtils.stringToList("[]"));
    }
}
