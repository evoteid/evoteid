package com.controller;

import com.data.voter.SingleVoter;
import com.exceptions.NoSuchVotingException;
import com.exceptions.PKIClosedException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.keys.KeyService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.Optional;

import static org.mockito.Mockito.when;

public class KeysControllerTest {

    private ControllerTestHelper helper;

    private RequestContextHolder requestContextHolder;

    private KeysController keysController;

    private KeyService mockKeyService;

    private String key = "key";

    String keyBody;

    @Before
    public void setup() {
        requestContextHolder = new FakeRequestContextHolder();
        mockKeyService = Mockito.mock(KeyService.class);
        keysController = new KeysController(mockKeyService, requestContextHolder);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(keysController);

        keyBody = new JSONObject()
                .put("publicKey", key)
                .toString();
    }

    @Test
    public void voterTriesToSetKeyForOtherVoter() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        SingleVoter voter = new SingleVoter().setName("u").setId(differentVoterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setContent(keyBody)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void voterTriesToSetKeyForClosedVotingExceptionIsPropagated() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        when(mockKeyService.setPublicKeyOfVoterInVoting(voterId,votingsId,key)).thenThrow(new PKIClosedException("Some exception"));

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setContent(keyBody)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
    }

    @Test
    public void voterTriesToSetKeyForNotExistingVotingExceptionIsPropagated() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        when(mockKeyService.setPublicKeyOfVoterInVoting(voterId,votingsId,key)).thenThrow(new NoSuchVotingException("Some exception"));

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setContent(keyBody)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void addingCustomKeyWorks() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        when(mockKeyService.setPublicKeyOfVoterInVoting(voterId,votingsId,key)).thenReturn(true);

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setContent(keyBody)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }

    @Test
    public void voterTriesToGetKeyFromOtherVoter() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        SingleVoter voter = new SingleVoter().setName("u").setId(differentVoterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getPublicKeyForVotingReturnsNotFoundIfNotThere() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        when(mockKeyService.getPublicKeyOfVoterInVoting(voterId,votingsId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void getPublicKeyForVotingReturnsKeyIfThere() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        String publicKey = "thePublicKey";

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        when(mockKeyService.getPublicKeyOfVoterInVoting(voterId,votingsId)).thenReturn(Optional.of(publicKey));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/publicKeys/voters/"+voterId)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"publicKey\":\"thePublicKey\"}",response.getContentAsString());
    }
}
