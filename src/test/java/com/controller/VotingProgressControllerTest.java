package com.controller;

import com.data.votingvoterprogress.VotingVoterProgress;
import com.service.context.FakeRequestContextHolder;
import com.services.data.votingvoterprogress.VotingVoterProgressService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class VotingProgressControllerTest {

    @MockBean
    private VotingVoterProgressService votingVoterProgressService;

    private VotingProgressController votingProgressController;

    private FakeRequestContextHolder requestContextHolder;

    private ControllerTestHelper helper;

    @Before
    public void setup() {
        requestContextHolder = new FakeRequestContextHolder();
        votingProgressController = new VotingProgressController(votingVoterProgressService);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(votingProgressController);
    }

    @Test
    public void shouldReturnNotFoundIfVotingDoesNotExist() throws Exception {
        long votingId = 42;
        long voterId = 27;

        when(votingVoterProgressService.getVotingProgressForVoter(votingId,voterId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingId+"/progress/voters/"+voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void shouldReturnStatusFoundIfVotingDoesExist() throws Exception {
        long votingId = 42;
        long voterId = 27;

        when(votingVoterProgressService.getVotingProgressForVoter(votingId,voterId)).thenReturn(Optional.of(new VotingVoterProgress()));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingId+"/progress/voters/"+voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }
}
