package com.controller;

import com.data.proofs.SingleVoteNIZKProof;
import com.data.proofs.VoteNIZKProofs;
import com.data.voter.SingleVoter;
import com.exceptions.DuplicatedVoteException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.proofs.VoteNIZKProofService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class VoteNIZKPControllerTest {

    private RequestContextHolder requestContextHolder;

    private ControllerTestHelper helper;

    private VoteNIZKPController voteNIZKPController;

    @Mock
    private VoteNIZKProofService mockVoteNIZKProofService;

    @Before
    public void setup() {
        requestContextHolder = new FakeRequestContextHolder();
        mockVoteNIZKProofService = Mockito.mock(VoteNIZKProofService.class);
        voteNIZKPController = new VoteNIZKPController(mockVoteNIZKProofService, requestContextHolder);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(voteNIZKPController);
    }

    @Test
    public void voterTriesToSpecifyZeroKnowledgeProofForOtherVoter() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        SingleVoter voter = new SingleVoter().setName("u").setId(differentVoterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/voteZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void invalidData() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/voteZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .setContent("invalidContent")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void conflictIsReturnedIfServiceReturnError() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        when(mockVoteNIZKProofService.setNIZKProofs(eq(votingsId),eq(voterId),any())).thenThrow(new DuplicatedVoteException("dummy message"));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/voteZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .setContent(new VoteNIZKProofs().toJson().toString())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
        Assert.assertEquals("dummy message",response.getContentAsString());
    }

    @Test
    public void createdIsReturnedIfEverythingWorked() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        when(mockVoteNIZKProofService.setNIZKProofs(eq(votingsId),eq(voterId),any())).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/voteZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .setContent(new VoteNIZKProofs().toJson().toString())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals("Zero knowledge proof for voting was added.",response.getContentAsString());
    }

    @Test
    public void getVoteNIKZProofsReturnsNotFoundIfNotThere() throws Exception {
        long votingsId = 1;
        when(mockVoteNIZKProofService.getNIZKProofs(votingsId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/voteZeroKnowledgeProofs")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
    }

    @Test
    public void getVoteNIKZProofsReturnsValidData() throws Exception {
        long votingsId = 1;
        List<VoteNIZKProofs> proofs = new ArrayList();
        proofs.add(new VoteNIZKProofs()
                        .setFromVoterId(1)
                        .setFromVoterName("v1")
                        .addProof(new SingleVoteNIZKProof()
                                    .setA("a1")
                                    .setB("b1")
                                    .setC("c1")
                                    .setR("r1")
                                    .setMessageIndex(0))
                        .addProof(new SingleVoteNIZKProof()
                                .setA("a2")
                                .setB("b2")
                                .setC("c2")
                                .setR("r2")
                                .setMessageIndex(1)));

        proofs.add(new VoteNIZKProofs()
                    .setFromVoterId(2)
                    .setFromVoterName("v2")
                    .addProof(new SingleVoteNIZKProof()
                            .setA("a3")
                            .setB("b3")
                            .setC("c3")
                            .setR("r3")
                            .setMessageIndex(0))
        );

        when(mockVoteNIZKProofService.getNIZKProofs(votingsId)).thenReturn(Optional.of(proofs));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/voteZeroKnowledgeProofs")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assert.assertEquals("{\"allProofs\":[{\"proofs\":[{\"a\":\"a1\",\"b\":\"b1\",\"r\":\"r1\",\"c\":\"c1\",\"messageIndex\":0},{\"a\":\"a2\",\"b\":\"b2\",\"r\":\"r2\",\"c\":\"c2\",\"messageIndex\":1}],\"fromVoterId\":1,\"fromVoterName\":\"v1\"},{\"proofs\":[{\"a\":\"a3\",\"b\":\"b3\",\"r\":\"r3\",\"c\":\"c3\",\"messageIndex\":0}],\"fromVoterId\":2,\"fromVoterName\":\"v2\"}]}",response.getContentAsString());
    }


}
