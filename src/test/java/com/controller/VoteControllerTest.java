package com.controller;

import com.data.dataforkeyderivation.DataForKeyDerivation;
import com.data.voter.SingleVoter;

import com.data.votes.SingleVote;
import com.data.votes.Votes;
import com.data.votings.EligibleVoterWithKey;
import com.data.votings.VotingCalculationParameters;
import com.exceptions.VotingClosedException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.vote.VoteService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class VoteControllerTest {

    private RequestContextHolder requestContextHolder;

    private ControllerTestHelper helper;

    private VoteController voteController;

    @MockBean
    private VoteService voteService;

    @Before
    public void setup() {
        requestContextHolder = new FakeRequestContextHolder();
        voteController = new VoteController(voteService, requestContextHolder);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(voteController);
    }


    @Test
    public void voterTriesToVoteForOtherVoter() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        SingleVoter voter = new SingleVoter().setName("u").setId(differentVoterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/votes/voters/"+voterId)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void invalidData() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/votes/voters/"+voterId)
                .setUser(voter)
                .setContent("invalidContent")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void getAllVotesWorks() throws Exception {
        Long votingsId = 1L;

        Votes votes = Mockito.mock(Votes.class);
        JSONObject jsonObject = Mockito.mock(JSONObject.class);
        when(jsonObject.toString()).thenReturn("dummyVotes");
        when(votes.toJson()).thenReturn(jsonObject);

        when(voteService.getAllVotesForVoting(votingsId)).thenReturn(votes);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/votes")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("dummyVotes", response.getContentAsString());
    }

    @Test
    public void validDataShouldStore() throws Exception {
        Long votingsId = 1L;
        Long voterId = 4L;

        JSONObject jsonObject = new JSONObject()
                .put("fromVoterId",voterId)
                .put("fromVoterName","u")
                .put("alpha","alpha")
                .put("beta","beta")
                .put("signature", "signature");

        ArgumentCaptor<SingleVote> argumentCaptor = ArgumentCaptor.forClass(SingleVote.class);
        when(voteService.setVoteForVoter(eq(voterId),eq(votingsId),argumentCaptor.capture())).thenReturn(true);

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/votes/voters/"+voterId)
                .setContent(jsonObject.toString())
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals("alpha",argumentCaptor.getValue().getAlpha());
        Assert.assertEquals("beta",argumentCaptor.getValue().getBeta());
        Assert.assertEquals("signature",argumentCaptor.getValue().getSignature());
    }

    @Test
    public void shouldReturnConflictIfError() throws Exception {
        Long votingsId = 1L;
        Long voterId = 4L;

        SingleVoter user = new SingleVoter().setName("u").setId(voterId);
        when(voteService.setVoteForVoter(anyLong(),anyLong(),any())).thenThrow(new VotingClosedException("Voting closed."))
                .thenReturn(false);

        JSONObject jsonObject = new JSONObject()
                .put("fromVoterId",voterId)
                .put("fromVoterName","u")
                .put("alpha","alpha")
                .put("beta","beta")
                .put("signature", "signature");

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/votes/voters/"+voterId)
                .setContent(jsonObject.toString())
                .setUser(user)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
    }

    @Test
    public void shouldReturnNotFoundIfVotingDoesNotExist() throws Exception {
        long voteId = 42;
        String name = "dummy";

        SingleVoter voter = new SingleVoter().setName(name).setId(0L);
        when(voteService.getDataForKeyDerivation(voteId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+voteId+"/keyCalculationData")
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void shouldReturnPermissionDeniedIfVoterIsNotInEligibleVotersList() throws Exception {
        long voteId = 42;
        long notAllowedVoterId = 27;

        DataForKeyDerivation dataForKeyDerivation = new DataForKeyDerivation()
                .addEligibleVoter(new EligibleVoterWithKey().setId(1L))
                .addEligibleVoter(new EligibleVoterWithKey().setId(2L))
                .addEligibleVoter(new EligibleVoterWithKey().setId(3L));

        when(voteService.getDataForKeyDerivation(voteId)).thenReturn(Optional.of(dataForKeyDerivation));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+voteId+"/keyCalculationData")
                .setVoterId(notAllowedVoterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void shouldReturnDataIfVoterIsVotingAdministrator() throws Exception {
        long voteId = 42;
        long voterId = 27;

        DataForKeyDerivation dataForKeyDerivation = new DataForKeyDerivation()
                .setVotingCalculationParameters(new VotingCalculationParameters())
                .setVotingAdministratorId(voterId);

        when(voteService.getDataForKeyDerivation(voteId)).thenReturn(Optional.of(dataForKeyDerivation));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+voteId+"/keyCalculationData")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }

    @Test
    public void shouldReturnDataIfAllows() throws Exception {
        long voteId = 42;
        long allowedVoterId = 1L;

        DataForKeyDerivation dataForKeyDerivation = new DataForKeyDerivation()
                .setVotingCalculationParameters(new VotingCalculationParameters())
                .addEligibleVoter(new EligibleVoterWithKey().setId(0L))
                .addEligibleVoter(new EligibleVoterWithKey().setId(allowedVoterId))
                .addEligibleVoter(new EligibleVoterWithKey().setId(2L));

        when(voteService.getDataForKeyDerivation(voteId)).thenReturn(Optional.of(dataForKeyDerivation));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+voteId+"/keyCalculationData")
                .setVoterId(allowedVoterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(dataForKeyDerivation.toJson().toString(),response.getContentAsString());
    }
}
