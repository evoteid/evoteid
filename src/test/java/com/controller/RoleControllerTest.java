package com.controller;

import com.data.roles.Roles;
import com.data.voter.SingleVoter;
import com.data.voter.Voters;
import com.enums.Role;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.roles.RoleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class RoleControllerTest {

    private ControllerTestHelper helper;

    private RequestContextHolder requestContextHolder;

    private RoleService mockRoleService;


    @Before
    public void setup() {
        mockRoleService = Mockito.mock(RoleService.class);
        requestContextHolder = new FakeRequestContextHolder();
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(new RoleController(mockRoleService,requestContextHolder));
    }

    @Test
    public void getRolesReturnsPermissionDeniedIfCurrentVoterIsNotKnown() throws Exception {
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/roles")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getRolesReturnsPermissionDeniedIfNotAdminOrVotingAdmin() throws Exception {
        long voterId = 27;
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(false);
        when(mockRoleService.hasRole(voterId, Role.REGISTRAR)).thenReturn(false);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/roles")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void votingAdminCanGetRoles() throws Exception {
        long voterId = 27;
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(false);
        when(mockRoleService.hasRole(voterId, Role.REGISTRAR)).thenReturn(true);
        when(mockRoleService.getAllVotersWithTheirRoles()).thenReturn(new Voters());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/roles")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }

    @Test
    public void getRolesReturnsRightData() throws Exception {

        Roles roles1 = new Roles();
        Roles roles2 = new Roles()
                .addRole(Role.REGISTRAR)
                .addRole(Role.ADMINISTRATOR);

        SingleVoter voter1 = new SingleVoter().setName("u1").setRoles(roles1).setId(1L);
        SingleVoter voter2 = new SingleVoter().setName("u2").setRoles(roles2).setId(2L);

        long voterId = 27;
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        Voters voters = new Voters()
                .addVoter(voter1)
                .addVoter(voter2);
        when(mockRoleService.getAllVotersWithTheirRoles()).thenReturn(voters);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/roles")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"voters\":[{\"roles\":[],\"name\":\"u1\",\"id\":1},{\"roles\":[\"Registrar\",\"Administrator\"],\"name\":\"u2\",\"id\":2}]}",
                response.getContentAsString());
    }

    @Test
    public void getAllAvailableRolesWorks() throws Exception {
        when(mockRoleService.getAllAvailableRoles()).thenReturn(new Roles().addRole(Role.ADMINISTRATOR).addRole(Role.VOTING_ADMINISTRATOR));

        long voterId = 27;
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/roles")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"roles\":[\"Administrator\",\"Voting_Administrator\"]}",
                response.getContentAsString());
    }

    @Test
    public void addRolesReturnsPermissionDeniedIfCurrentVoterIsNotKnown() throws Exception {
        long voterId = 27;
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+voterId+"/roles/dummy")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void addRolesReturnsPermissionDeniedIfNotAdmin() throws Exception {
        long voterId = 27;
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(false);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+voterId+"/roles/dummy")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void addRolesReturnsInvalidRequestIfRoleDoesNotExist() throws Exception {
        long voterId = 27;
        String roleName = "invalidRole";
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+voterId+"/roles/"+roleName)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void addRoleWorks() throws Exception {
        long voterId = 27;
        String roleName = Role.ADMINISTRATOR.getRoleName();

        when(mockRoleService.addRoleToVoter(voterId,Role.ADMINISTRATOR)).thenReturn(true);

        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+voterId+"/roles/"+roleName)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(mockRoleService,times(1)).addRoleToVoter(voterId,Role.ADMINISTRATOR);

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("Role \'Administrator\' was added to user with id \'27\'",response.getContentAsString());
    }

    @Test
    public void addRoleInternalErrorPropagated() throws Exception {
        long voterId = 27;
        String roleName = Role.ADMINISTRATOR.getRoleName();

        when(mockRoleService.addRoleToVoter(voterId,Role.ADMINISTRATOR)).thenReturn(false);

        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+voterId+"/roles/"+roleName)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(mockRoleService,times(1)).addRoleToVoter(voterId,Role.ADMINISTRATOR);

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
    }

    @Test
    public void deleteRoleReturnsPermissionDeniedIfNotAuthenticated() throws Exception {
        long voterId = 27;
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(false);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+voterId+"/roles/dummy")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void deleteRoleReturnsPermissionDeniedIfNotAdmin() throws Exception {
        long voterId = 27;
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(false);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+voterId+"/roles/dummy")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void deleteRoleReturnsInvalidRequestIfRoleDoesNotExist() throws Exception {
        long voterId = 27;
        String roleName = "invalidRole";
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+voterId+"/roles/"+roleName)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void deleteRoleWorks() throws Exception {
        long voterId = 27;
        String roleName = Role.ADMINISTRATOR.getRoleName();

        when(mockRoleService.deleteRoleFromVoter(voterId,Role.ADMINISTRATOR)).thenReturn(true);

        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+voterId+"/roles/"+roleName)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(mockRoleService,times(1)).deleteRoleFromVoter(voterId,Role.ADMINISTRATOR);

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("Role \'Administrator\' was removed from user with id \'27\'",response.getContentAsString());
    }

    @Test
    public void deleteRoleInternalErrorPropagated() throws Exception {
        long voterId = 27;
        String roleName = Role.ADMINISTRATOR.getRoleName();

        when(mockRoleService.deleteRoleFromVoter(voterId,Role.ADMINISTRATOR)).thenReturn(false);
        when(mockRoleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+voterId+"/roles/"+roleName)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(mockRoleService,times(1)).deleteRoleFromVoter(voterId,Role.ADMINISTRATOR);

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
    }

}
