package com.controller;

import com.data.voter.SingleVoter;

import com.enums.HttpErrorMessages;
import com.services.autentication.AuthenticationService;
import com.services.data.voters.VotersService;
import com.services.token.JwtTokenCreatorService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class AuthenticationControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private JwtTokenCreatorService tokenService;

    @MockBean
    private VotersService votersService;

    @Autowired
    private AuthenticationController authenticationController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(authenticationController)
                .build();
    }

    @Test
    public void shouldReturnErrorMessageIfUserNotThere() throws Exception {

        String voterName = "James Bond";
        String password = "007";

        when(votersService.getVoter(voterName)).thenReturn(Optional.empty());
        when(authenticationService.authenticate(voterName, password)).thenReturn(true);

        String body = new JSONObject()
                .put("username",voterName)
                .put("password",password)
                .toString();

        MvcResult result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(),result.getResponse().getStatus());
    }

    @Test
    public void shouldPassValidRequestToAuthenticationServiceIfVoterExists() throws Exception {
        String voterName = "James Bond";
        String password = "007";

        when(votersService.getVoter(voterName)).thenReturn(Optional.of(new SingleVoter()));
        when(tokenService.createTokenForUser(eq(voterName))).thenReturn(Optional.of("dummy Token"));
        when(authenticationService.authenticate(voterName, password)).thenReturn(true);

        String body = new JSONObject()
                .put("username",voterName)
                .put("password",password)
                .toString();

        MvcResult result = this.mockMvc.perform(
                post("/api/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body)
        ).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals("{\"token\":\"dummy Token\"}",result.getResponse().getContentAsString());

        verify(authenticationService, times(1)).authenticate(voterName,password);
    }

    @Test
    public void getRequestNotSupported() throws Exception {
        MvcResult result;

        result = this.mockMvc.perform(
                get("/api/authenticate")
        ).andReturn();

        Assert.assertEquals(HttpStatus.METHOD_NOT_ALLOWED.value(),result.getResponse().getStatus());
    }

    @Test
    public void missingUsernameParameter() throws Exception {
        MvcResult result;

        String bodyWithMissingUsername = new JSONObject()
                .put("password","secret")
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyWithMissingUsername)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),result.getResponse().getStatus());
        Assert.assertEquals(HttpErrorMessages.MISSING_USERNAME_FIELD.toString(),response);
    }

    @Test
    public void missingPasswordParameter() throws Exception {
        MvcResult result;

        String bodyWithMissingPassword = new JSONObject()
                .put("username","name")
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(bodyWithMissingPassword)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),result.getResponse().getStatus());
        Assert.assertEquals(HttpErrorMessages.MISSING_PASSWORD_FIELD.toString(),response);
    }

    @Test
    public void illegalCharacterInUsername() throws Exception {
        MvcResult result;

        String illegalUsername = "user(name";
        String legalPassword = "secret";

        String body = new JSONObject()
                .put("username",illegalUsername)
                .put("password",legalPassword)
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),result.getResponse().getStatus());
        Assert.assertEquals(HttpErrorMessages.ILLEGAL_CHARACTER_IN_USERNAME_OR_PASSWORD.toString(),response);

    }

    @Test
    public void illegalCharacterInPassword() throws Exception {
        MvcResult result;

        String illegalVoterName = "voterName";
        String legalPassword = "se&ret";

        String body = new JSONObject()
                .put("username",illegalVoterName)
                .put("password",legalPassword)
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),result.getResponse().getStatus());
        Assert.assertEquals(HttpErrorMessages.ILLEGAL_CHARACTER_IN_USERNAME_OR_PASSWORD.toString(),response);
    }

    @Test
    public void wrongLoginCredentialsShouldReturnError() throws Exception {
        MvcResult result;

        String voterName = "username";
        String password = "password";

        when(authenticationService.authenticate(voterName,password)).thenReturn(false);

        String body = new JSONObject()
                .put("username",voterName)
                .put("password",password)
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());

        Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(),result.getResponse().getStatus());
        Assert.assertEquals(HttpErrorMessages.WRONG_USERNAME_AND_PASSWORD.toString(),response);

    }

    @Test
    public void internalServerErrorShouldBeReturnedIfTokenCouldNotBeGenerated() throws Exception {
        MvcResult result;

        Long id = 27L;
        String voterName = "Luke Skywalker";
        String password = "password";

        when(votersService.getVoter(voterName)).thenReturn(Optional.of(
                new SingleVoter()
                        .setName(voterName)
                        .setId(id)));
        when(authenticationService.authenticate(voterName,password)).thenReturn(true);
        when(tokenService.createTokenForUser(eq(voterName))).thenReturn(Optional.empty());

        String body = new JSONObject()
                .put("username",voterName)
                .put("password",password)
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),result.getResponse().getStatus());

    }

    @Test
    public void JwtTokenShouldBeReturnedIfEverythingIsOk() throws Exception {
        MvcResult result;

        Long id = 27L;
        String voterName = "Luke Skywalker";
        String password = "password";
        String dummyToken = "dummyToken";

        when(votersService.getVoter(voterName)).thenReturn(Optional.of(
                new SingleVoter()
                        .setName(voterName)
                        .setId(id)));
        when(authenticationService.authenticate(voterName,password)).thenReturn(true);
        when(tokenService.createTokenForUser(eq(voterName))).thenReturn(Optional.of(dummyToken));

        String body = new JSONObject()
                .put("username",voterName)
                .put("password",password)
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());
        String token = new JSONObject(response).getString("token");


        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(dummyToken,token);

    }

    @Test
    public void JwtTokenShouldBeReturnedWithKeyIfEverythingIsOkAndKeyIsThere() throws Exception {
        MvcResult result;

        Long id = 27L;
        String voterName = "Luke Skywalker";
        String password = "password";
        String dummyTokenWithKey = "dummyTokenWithKey";
        String dummyKey = "dummyKey";

        when(votersService.getVoter(voterName)).thenReturn(Optional.of(
                new SingleVoter()
                        .setName(voterName)
                        .setPublicKey(dummyKey)
                        .setId(id)));
        when(authenticationService.authenticate(voterName,password)).thenReturn(true);
        when(tokenService.createTokenForUser(eq(voterName))).thenReturn(Optional.of(dummyTokenWithKey));

        String body = new JSONObject()
                .put("username",voterName)
                .put("password",password)
                .toString();

        result = this.mockMvc.perform(
                post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        ).andReturn();

        String response = new String(result.getResponse().getContentAsByteArray());
        String token = new JSONObject(response).getString("token");


        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(dummyTokenWithKey,token);
    }

}
