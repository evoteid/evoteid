package com.data.votingvoterprogress;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingVoterProgressTest {

    @Test
    public void returnValidJsonObject() {
        int phaseNumber = 27;
        boolean createdSecretShares = true;
        boolean voted = false;
        boolean participatedInDecryption = true;

        VotingVoterProgress votingVoterProgress = new VotingVoterProgress()
                .setPhaseNumber(phaseNumber)
                .setCreatedSecretShares(createdSecretShares)
                .setVoted(voted)
                .setParticipatedInDecryption(participatedInDecryption);

        JSONObject object = votingVoterProgress.toJson();

        Assert.assertEquals(phaseNumber,object.getInt("phaseNumber"));
        Assert.assertEquals(createdSecretShares,object.getBoolean("createdSecretShares"));
        Assert.assertEquals(voted,object.getBoolean("voted"));
        Assert.assertEquals(participatedInDecryption,object.getBoolean("participatedInDecryption"));
    }
}
