package com.data.votingvoterprogress;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingsVoterProgressTest {

    @Test
    public void returnValidJsonObject() {
        VotingsVoterProgress votingsVoterProgress = new VotingsVoterProgress()
                .addVotingProgress(new VotingVoterProgress().setVotingId(27L))
                .addVotingProgress(new VotingVoterProgress().setVotingId(42L));

        JSONObject object = votingsVoterProgress.toJson();

        Assert.assertEquals(2,object.getJSONArray("votingsVoterProgress").length());
        Assert.assertEquals(27L,object.getJSONArray("votingsVoterProgress").getJSONObject(0).get("votingId"));
        Assert.assertEquals(42L,object.getJSONArray("votingsVoterProgress").getJSONObject(1).get("votingId"));
    }
}
