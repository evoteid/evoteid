package com.data.decryptionfactor;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class DecryptionFactorsTest {

    @Test
    public void returnValidJsonObject() {
        SingleDecryptionFactor singleDecryptionFactor1 = new SingleDecryptionFactor().setDecryptionFactor("d1");
        SingleDecryptionFactor singleDecryptionFactor2 = new SingleDecryptionFactor().setDecryptionFactor("d2");
        SingleDecryptionFactor singleDecryptionFactor3 = new SingleDecryptionFactor().setDecryptionFactor("d3");

        DecryptionFactors decryptionFactors = new DecryptionFactors()
                .addDecryptionFactor(singleDecryptionFactor1)
                .addDecryptionFactor(singleDecryptionFactor2)
                .addDecryptionFactor(singleDecryptionFactor3);

        JSONObject object = decryptionFactors.toJson();

        Assert.assertEquals(3,object.getJSONArray("decryptionFactors").length());
        Assert.assertEquals("d1",object.getJSONArray("decryptionFactors").getJSONObject(0).getString("decryptionFactor"));
        Assert.assertEquals("d2",object.getJSONArray("decryptionFactors").getJSONObject(1).getString("decryptionFactor"));
        Assert.assertEquals("d3",object.getJSONArray("decryptionFactors").getJSONObject(2).getString("decryptionFactor"));
    }
}
