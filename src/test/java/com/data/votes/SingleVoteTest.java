package com.data.votes;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class SingleVoteTest {

    @Test
    public void returnValidJsonObject() {
        long fromVoterId = 27;
        String fromVoterName = "voterName";
        String alpha = "alpha";
        String beta = "beta";

        SingleVote singleVote = new SingleVote()
                .setFromVoterId(fromVoterId)
                .setFromVoterName(fromVoterName)
                .setAlpha(alpha)
                .setBeta(beta);

        JSONObject object = singleVote.toJson();

        Assert.assertEquals(fromVoterId,object.getLong("fromVoterId"));
        Assert.assertEquals(fromVoterName,object.getString("fromVoterName"));
        Assert.assertEquals(alpha,object.getString("alpha"));
        Assert.assertEquals(beta,object.getString("beta"));
    }
}
