package com.data.votings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class EligibleVoterWithKeyTest {

    @Test
    public void returnValidJsonObject() {
        String userName = "voter1";
        String key = "key";
        Long id = 27L;
        Long position = 42L;
        EligibleVoterWithKey eligibleVoter = new EligibleVoterWithKey()
                .setName(userName)
                .setId(27L)
                .setPosition(42L)
                .setPublicKey(key);
        JSONObject json = eligibleVoter.toJson();
        Assert.assertEquals(id,json.get("id"));
        Assert.assertEquals(position,json.get("position"));
        Assert.assertEquals(userName,json.get("name"));
        Assert.assertEquals(key,json.get("publicKey"));
    }
}
