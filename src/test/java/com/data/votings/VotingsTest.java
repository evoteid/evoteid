package com.data.votings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingsTest {

    @Test
    public void returnValidJsonObject() {
        String titleOne = "titleOne";
        String titleTwo = "titleTwo";
        String titleThree = "titleThree";
        Votings votings = new Votings()
                .addVoting(new Voting().setTitle(titleOne))
                .addVoting(new Voting().setTitle(titleTwo))
                .addVoting(new Voting().setTitle(titleThree));

        JSONObject jsonObject = votings.toJson();
        Assert.assertEquals(titleOne,jsonObject.getJSONArray("votings").getJSONObject(0).getString("title"));
        Assert.assertEquals(titleTwo,jsonObject.getJSONArray("votings").getJSONObject(1).getString("title"));
        Assert.assertEquals(titleThree,jsonObject.getJSONArray("votings").getJSONObject(2).getString("title"));
    }
}
