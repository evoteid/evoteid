package com.data.votings;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingTest {

    @Test
    public void returnValidJsonObject() {
        long id = 27;
        String title = "title";
        String description = "desc";
        String op1Name = "foo";
        String op2Name = "bar";
        String op3Name = "baz";
        int phase = 0;

        long votingEndTime = 42L;

        VotingOption op1 = new VotingOption().setOptionName(op1Name);
        VotingOption op2 = new VotingOption().setOptionName(op2Name);
        VotingOption op3 = new VotingOption().setOptionName(op3Name);

        Voting voting = new Voting()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setPhase(phase)
                .setTimeVotingIsFinished(votingEndTime)
                .addVotingOption(op1)
                .addVotingOption(op2)
                .addVotingOption(op3);

        JSONObject object = voting.toJson();
        JSONArray options = object.getJSONArray("options");

        Assert.assertEquals(id,object.getInt("id"));
        Assert.assertEquals(title,object.getString("title"));
        Assert.assertEquals(description,object.getString("description"));
        Assert.assertEquals(phase,object.getInt("phase"));
        Assert.assertEquals(votingEndTime,object.getLong("timeVotingIsFinished"));
        Assert.assertEquals(op1Name,options.getJSONObject(0).getString("optionName"));
        Assert.assertEquals(op2Name,options.getJSONObject(1).getString("optionName"));
        Assert.assertEquals(op3Name,options.getJSONObject(2).getString("optionName"));
    }
}
