package com.data.voter;

import com.data.roles.Roles;
import com.enums.Role;
import org.junit.Assert;
import org.junit.Test;

public class VotersTest {

    @Test
    public void returnValidJson() {

        SingleVoter singleVoterWithRoles1 = new SingleVoter().setName("u1").setRoles(new Roles().addRole(Role.ADMINISTRATOR));
        SingleVoter singleVoterWithRoles2 = new SingleVoter().setName("u2").setRoles(new Roles().addRole(Role.REGISTRAR).addRole(Role.VOTING_ADMINISTRATOR));

        Voters voters = new Voters();
        voters
                .addVoter(singleVoterWithRoles1)
                .addVoter(singleVoterWithRoles2);

        Assert.assertEquals("{\"voters\":[{\"roles\":[\"Administrator\"],\"name\":\"u1\"},{\"roles\":[\"Registrar\",\"Voting_Administrator\"],\"name\":\"u2\"}]}",
                voters.toJsonWithRoles().toString());

        Assert.assertEquals(2,voters.toJsonWithRoles().getJSONArray("voters").length());
        Assert.assertEquals(singleVoterWithRoles1.toJsonWithRoles().toString(),voters.toJsonWithRoles().getJSONArray("voters").get(0).toString());
        Assert.assertEquals(singleVoterWithRoles2.toJsonWithRoles().toString(),voters.toJsonWithRoles().getJSONArray("voters").get(1).toString());
    }
}
