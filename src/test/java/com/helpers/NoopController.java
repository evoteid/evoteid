package com.helpers;

import org.springframework.boot.autoconfigure.condition.ConditionalOnNotWebApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ConditionalOnNotWebApplication
public class NoopController {

    @GetMapping("/**")
    public @ResponseBody
    String matchAll() {
        return RESPONSEMESSAGE;
    }

    public static final String RESPONSEMESSAGE = "This is the noop controller";
}
