package com.helpers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class NoopControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void noopControllerNotActiveInWebAppContext() throws Exception {
        MvcResult result = mockMvc.perform(get("/something")).andReturn();
        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),result.getResponse().getStatus());
    }

    @Test
    public void noopControllerActiveInStandaloneSetup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(new NoopController()).build();
        MvcResult result = mockMvc.perform(get("/something")).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
    }
}
