package com.service.hash;

import com.services.hash.HashService;
import com.services.hash.HashServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HashServiceImplTest {

    private HashService hashService;

    @Before
    public void setup() {
        hashService = new HashServiceImpl();
    }

    @Test
    public void createValidHash(){
        String testMessage = "testMessage";
        String hash = hashService.createSHA256Hash(testMessage);
        String expected = "d9920dc69e7b8352ea5774041afeaf8eeebd1c4985bae1368c2a5559c12bcb56";
        Assert.assertEquals(expected,hash);
    }
}
