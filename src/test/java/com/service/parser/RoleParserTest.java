package com.service.parser;

import com.data.roles.Roles;
import com.enums.Role;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoSuchRoleException;
import com.exceptions.parser.ParserException;
import com.services.parser.RoleParser;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class RoleParserTest {

    @Test
    (expected = MissingFieldException.class)
    public void invalidJsonThrowsError() throws ParserException {
        String validJsonNoRole = "{}";
        Roles roles = RoleParser.parse(validJsonNoRole);
        Assert.assertEquals(Lists.emptyList(),roles.getRoles());
    }

    @Test
    public void validJsonCanBeParsedEmpty() throws ParserException {
        String validJsonNoRole = "{\"roles\": []}";
        Roles roles = RoleParser.parse(validJsonNoRole);
        Assert.assertEquals(Lists.emptyList(),roles.getRoles());
    }

    @Test
    public void validJsonCanBeParsedOneRole() throws ParserException {
        String validJsonOneRole = "{\"roles\": [\""+Role.ADMINISTRATOR.getRoleName()+"\"]}";
        Roles roles = RoleParser.parse(validJsonOneRole);
        Assert.assertEquals(Arrays.asList(Role.ADMINISTRATOR),roles.getRoles());
    }

    @Test
    public void validJsonCanBeParsedMultipleRoles() throws ParserException {
        String validJsonMultipleRoles = "{\"roles\": [\""+Role.ADMINISTRATOR.getRoleName()+"\",\""+Role.REGISTRAR.getRoleName()+"\"]}";
        Roles roles  = RoleParser.parse(validJsonMultipleRoles);
        Assert.assertEquals(Arrays.asList(Role.ADMINISTRATOR, Role.REGISTRAR),roles.getRoles());
    }

    @Test
    (expected = NoSuchRoleException.class)
    public void invalidRoleThrowsException() throws ParserException {
        String invalidJson = "{\"roles\": [\"invalidRole\"]}";
        RoleParser.parse(invalidJson);
    }
}
