package com.service.parser;

import com.data.proofs.DecryptionNIZKProof;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.ParserException;
import com.services.parser.DecryptionNIZKProofParser;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DecryptionNIZKProofParserTest {

    private JSONObject jsonObject;

    @Before
    public void setup() {
        this.jsonObject = new JSONObject()
                .put("a","a1")
                .put("b","b1")
                .put("r","r1")
                .put("signature","sign");
    }

    @Test
    public void canParseValidJson() throws ParserException {
        String validJsonString = jsonObject.toString();
        DecryptionNIZKProof proofs = DecryptionNIZKProofParser.parse(validJsonString);

        Assert.assertEquals("a1",proofs.getA());
        Assert.assertEquals("b1",proofs.getB());
        Assert.assertEquals("r1",proofs.getR());
        Assert.assertEquals("sign",proofs.getSignature());
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingAFieldThrowsException() throws ParserException {
        jsonObject.remove("a");
        String invalidJsonString = jsonObject.toString();
        DecryptionNIZKProofParser.parse(invalidJsonString);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingBFieldThrowsException() throws ParserException {
        jsonObject.remove("b");
        String invalidJsonString = jsonObject.toString();
        DecryptionNIZKProofParser.parse(invalidJsonString);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingRFieldThrowsException() throws ParserException {
        jsonObject.remove("r");
        String invalidJsonString = jsonObject.toString();
        DecryptionNIZKProofParser.parse(invalidJsonString);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingSignatureFieldThrowsException() throws ParserException {
        jsonObject.remove("signature");
        String invalidJsonString = jsonObject.toString();
        DecryptionNIZKProofParser.parse(invalidJsonString);
    }
}
