package com.service.parser;

import com.data.votings.NewlyCreatedVoting;
import com.data.votings.Voting;
import com.data.votings.VotingCalculationParameters;
import com.exceptions.parser.*;
import com.services.parser.VotingParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VotingsParserTest {

    private JSONObject validJsonObject;

    @Before
    public void setup() {
        JSONArray jsonVoterKeysArray = new JSONArray();
        jsonVoterKeysArray.put(new JSONObject().put("id",3).put("name","voter1").put("position",0).put("isTrustee",true).put("canVote",false));
        jsonVoterKeysArray.put(new JSONObject().put("id",4).put("name","voter2").put("position",1).put("isTrustee",true).put("canVote",true));
        jsonVoterKeysArray.put(new JSONObject().put("id",5).put("name","voter3").put("position",2).put("isTrustee",false).put("canVote",true));

        JSONArray jsonOptionsArray = new JSONArray();
        jsonOptionsArray.put(new JSONObject().put("optionName","Foo Option 1").put("optionPrimeNumber",2L));
        jsonOptionsArray.put(new JSONObject().put("optionName","Foo Option 2").put("optionPrimeNumber",3L));
        jsonOptionsArray.put(new JSONObject().put("optionName","Foo Option 3").put("optionPrimeNumber",5L));

        JSONObject calculationParameters = new JSONObject()
                .put("primeP","31")
                .put("primeQ","37")
                .put("generatorG","2");

        validJsonObject = new JSONObject()
                .put("title","Foo Title")
                .put("description","Foo Description")
                .put("options",jsonOptionsArray)
                .put("eligibleVoters",jsonVoterKeysArray)
                .put("securityThreshold",2)
                .put("numberOfEligibleVoters",3)
                .put("phase",5)
                .put("id",42)
                .put("calculationParameters",calculationParameters);

    }

    @Test
    public void validJsonCanBeParsed() throws Exception {
        String validVoting = validJsonObject.toString();
        NewlyCreatedVoting voting = VotingParser.parse(validVoting);

        Assert.assertEquals("Foo Title",voting.getTitle());
        Assert.assertEquals("Foo Description",voting.getDescription());
        Assert.assertEquals(3,voting.getVotingOptions().size());
        Assert.assertEquals("Foo Option 1",voting.getVotingOptions().get(0).getOptionName());
        Assert.assertEquals("Foo Option 2",voting.getVotingOptions().get(1).getOptionName());
        Assert.assertEquals("Foo Option 3",voting.getVotingOptions().get(2).getOptionName());
        Assert.assertEquals(Long.valueOf(2),voting.getVotingOptions().get(0).getOptionPrimeNumber());
        Assert.assertEquals(Long.valueOf(3),voting.getVotingOptions().get(1).getOptionPrimeNumber());
        Assert.assertEquals(Long.valueOf(5),voting.getVotingOptions().get(2).getOptionPrimeNumber());

        Assert.assertEquals(2,voting.getSecurityThreshold());
        Assert.assertEquals(3,voting.getNumberOfEligibleVoters());
        Assert.assertEquals(3,voting.getEligibleVoters().size());
        Assert.assertEquals(new Long(3),voting.getEligibleVoters().get(0).getId());
        Assert.assertEquals(new Long(4),voting.getEligibleVoters().get(1).getId());
        Assert.assertEquals(new Long(5),voting.getEligibleVoters().get(2).getId());
        Assert.assertEquals("voter1",voting.getEligibleVoters().get(0).getName());
        Assert.assertEquals("voter2",voting.getEligibleVoters().get(1).getName());
        Assert.assertEquals("voter3",voting.getEligibleVoters().get(2).getName());
        Assert.assertEquals(new Long(0),voting.getEligibleVoters().get(0).getPosition());
        Assert.assertEquals(new Long(1),voting.getEligibleVoters().get(1).getPosition());
        Assert.assertEquals(new Long(2),voting.getEligibleVoters().get(2).getPosition());

        Assert.assertFalse(voting.getEligibleVoters().get(0).getCanVote());
        Assert.assertTrue(voting.getEligibleVoters().get(1).getCanVote());
        Assert.assertTrue(voting.getEligibleVoters().get(2).getCanVote());

        Assert.assertTrue(voting.getEligibleVoters().get(0).getIsTrustee());
        Assert.assertTrue(voting.getEligibleVoters().get(1).getIsTrustee());
        Assert.assertFalse(voting.getEligibleVoters().get(2).getIsTrustee());

        //Phase is not part of json votings input
        Assert.assertEquals(5,voting.getPhase());
        //Id is not part of json votings input
        Assert.assertEquals(42,voting.getId().intValue());

        VotingCalculationParameters calculationParameters = voting.getVotingCalculationParameters();
        Assert.assertEquals("31",calculationParameters.getPrimeP());
        Assert.assertEquals("37",calculationParameters.getPrimeQ());
        Assert.assertEquals("2",calculationParameters.getGeneratorG());


    }

    @Test(expected = NoContentException.class)
    public void emptyThrowsException() throws ParserException {
        String invalidContent = "";
        Voting voting = VotingParser.parse(invalidContent);
    }

    @Test(expected = MissingFieldException.class)
    public void missingTitleThrowsException() throws ParserException {
        validJsonObject.remove("title");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingDescriptionThrowsException() throws ParserException {
        validJsonObject.remove("description");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = TooFewVotingOptionsException.class)
    public void missingOptionsThrowsException() throws ParserException {
        validJsonObject.remove("options");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingSecurityThresholdThrowsException() throws ParserException {
        validJsonObject.remove("securityThreshold");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = TooFewEligiblieVotersException.class)
    public void missingUserIdsThrowsException() throws ParserException {
        validJsonObject.remove("eligibleVoters");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingNumberOfVotersThrowsException() throws ParserException {
        validJsonObject.remove("numberOfEligibleVoters");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = TooFewVotingOptionsException.class)
    public void emptyOptionsThrowsException() throws ParserException {
        validJsonObject.remove("options");
        validJsonObject.put("options",new JSONArray());
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = TooFewVotingOptionsException.class)
    public void tooFewOptionsThrowsException() throws ParserException {
        validJsonObject.getJSONArray("options").remove(2);
        validJsonObject.getJSONArray("options").remove(1);
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = OptionWithoutNameException.class)
    public void optionWithoutName() throws ParserException {
        validJsonObject.getJSONArray("options").put(new JSONObject());
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = OptionWithoutNameException.class)
    public void optionWithEmptyName() throws ParserException {
        validJsonObject.getJSONArray("options").put(new JSONObject().put("optionName",""));
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = OptionWithoutPrimeNumberException.class)
    public void optionWithNoPrimeNumber() throws ParserException {
        validJsonObject.getJSONArray("options").getJSONObject(0).remove("optionPrimeNumber");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void userWithoutId() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).remove("id");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = InvalidFieldFormatException.class)
    public void userWithInvalidId() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("id","notNumber");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void userWithEmptyId() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("id","");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void userWithoutName() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).remove("name");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = InvalidFieldFormatException.class)
    public void userWithEmptyName() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("name","");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void userWithoutCanVoteField() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).remove("canVote");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void userWithoutIsTrustee() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).remove("isTrustee");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void userWithEmptyPosition() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("position","");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = InvalidFieldFormatException.class)
    public void userWithInvalidPositionFormat() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("position","nan");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = IllegalVoterPositionException.class)
    public void userWithNegativePosition() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("position",-1);
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = IllegalVoterPositionException.class)
    public void userWithTooHighPosition() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("position",3);
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = IllegalVoterPositionException.class)
    public void userWithDuplicatedHighPosition() throws ParserException {
        validJsonObject.getJSONArray("eligibleVoters").getJSONObject(0).put("position",1);
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingCalculationParametersThrowsException() throws ParserException {
        validJsonObject.remove("calculationParameters");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingPrimePsThrowsException() throws ParserException {
        validJsonObject.getJSONObject("calculationParameters").remove("primeP");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingPrimeQsThrowsException() throws ParserException {
        validJsonObject.getJSONObject("calculationParameters").remove("primeQ");
        VotingParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingGeneratorGThrowsException() throws ParserException {
        validJsonObject.getJSONObject("calculationParameters").remove("generatorG");
        VotingParser.parse(validJsonObject.toString());
    }

}
