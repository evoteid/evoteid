package com.service.votingssummary;

import com.data.voter.VoterEntityWithIdSetter;
import com.models.helper.VotingEntityWithSetters;
import com.models.singlevotingvoter.*;
import com.models.user.UserEntity;
import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingOptionEntity;
import com.models.voting.VotingsRepository;
import com.services.votingssummary.VotingsSummaryService;
import com.services.votingssummary.VotingsSummaryServiceImpl;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.Mockito.when;

public class VotingsSummaryServiceTest {

    private long votingId = 27l;

    VotingsRepository mockVotingsRepository;

    SingleVotingVoterRepository singleVotingVoterRepository;

    private VotingsSummaryService votingsSummaryService;

    VotingEntity votingEntity;

    @Before
    public void setup() {
        mockVotingsRepository = Mockito.mock(VotingsRepository.class);
        singleVotingVoterRepository = Mockito.mock(SingleVotingVoterRepository.class);

        this.votingsSummaryService = new VotingsSummaryServiceImpl(mockVotingsRepository, singleVotingVoterRepository);

        votingEntity = new VotingEntityWithSetters().setId(votingId);
        addEligibleVotersData(votingEntity);
        addVotingsMetaData(votingEntity);
        addKeyGenerationData(votingEntity);
        addVoteData(votingEntity);
        addDecryptionData(votingEntity);

        when(mockVotingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));
    }

    @Test
    public void returnsEmptyIfNotThere() {
        when(mockVotingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.empty());
        Assert.assertFalse(votingsSummaryService.createVotingsSummary(votingId).isPresent());
    }

    @Test
    public void votingMetaDataIsIncludedInVotingDescription() {
        when(mockVotingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        Optional<JSONObject> jsonObjectOptional = votingsSummaryService.createVotingsSummary(votingId);
        Assert.assertTrue(jsonObjectOptional.isPresent());
        Assert.assertEquals("primeQ",jsonObjectOptional.get().getJSONObject("meta").getJSONObject("calculationParameters").getString("primeQ"));
        Assert.assertEquals("primeP",jsonObjectOptional.get().getJSONObject("meta").getJSONObject("calculationParameters").getString("primeP"));
        Assert.assertEquals("generatorG",jsonObjectOptional.get().getJSONObject("meta").getJSONObject("calculationParameters").getString("generatorG"));
        Assert.assertEquals("fantastic title",jsonObjectOptional.get().getJSONObject("meta").getJSONObject("info").getString("title"));
        Assert.assertEquals("fantastic description",jsonObjectOptional.get().getJSONObject("meta").getJSONObject("info").getString("description"));
        Assert.assertEquals(3,jsonObjectOptional.get().getJSONObject("meta").getJSONObject("info").getInt("securityThreshold"));

        Assert.assertEquals("o0",jsonObjectOptional.get().getJSONObject("meta").getJSONArray("options").getJSONObject(0).get("optionName"));
        Assert.assertEquals("o1",jsonObjectOptional.get().getJSONObject("meta").getJSONArray("options").getJSONObject(1).get("optionName"));
        Assert.assertEquals("o2",jsonObjectOptional.get().getJSONObject("meta").getJSONArray("options").getJSONObject(2).get("optionName"));

        Assert.assertEquals(2l,jsonObjectOptional.get().getJSONObject("meta").getJSONArray("options").getJSONObject(0).get("optionPrimeNumber"));
        Assert.assertEquals(3l,jsonObjectOptional.get().getJSONObject("meta").getJSONArray("options").getJSONObject(1).get("optionPrimeNumber"));
        Assert.assertEquals(5l,jsonObjectOptional.get().getJSONObject("meta").getJSONArray("options").getJSONObject(2).get("optionPrimeNumber"));
    }

    @Test
    public void eligibleVotersAreIncludedInVotingDescription() {
        when(mockVotingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        Optional<JSONObject> jsonObjectOptional = votingsSummaryService.createVotingsSummary(votingId);
        Assert.assertTrue(jsonObjectOptional.isPresent());

        Assert.assertEquals("key0",jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(0).get("publicKey"));
        Assert.assertTrue(jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(0).getBoolean("canVote"));
        Assert.assertFalse(jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(0).getBoolean("isTrustee"));
        Assert.assertEquals(0L,jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(0).getLong("position"));
        Assert.assertEquals(10L,jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(0).getLong("id"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(0).getString("name"));

        Assert.assertEquals("key1",jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(1).get("publicKey"));
        Assert.assertTrue(jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(1).getBoolean("canVote"));
        Assert.assertFalse(jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(1).getBoolean("isTrustee"));
        Assert.assertEquals(1L,jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(1).getLong("position"));
        Assert.assertEquals(11L,jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(1).getLong("id"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(1).getString("name"));

        Assert.assertEquals("key2",jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(2).get("publicKey"));
        Assert.assertFalse(jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(2).getBoolean("canVote"));
        Assert.assertTrue(jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(2).getBoolean("isTrustee"));
        Assert.assertEquals(2L,jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(2).getLong("position"));
        Assert.assertEquals(12L,jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(2).getLong("id"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("eligibleVoters").getJSONObject(2).getString("name"));
    }

    @Test
    public void keyGenerationDataIsIncludedInVotingDescription() {
        Optional<JSONObject> jsonObjectOptional = votingsSummaryService.createVotingsSummary(votingId);
        Assert.assertTrue(jsonObjectOptional.isPresent());

        Assert.assertEquals(10l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getLong("fromVoterId"));

        Assert.assertEquals("c0",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("commitments").getJSONObject(0).getString("commitment"));
        Assert.assertEquals(0l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("commitments").getJSONObject(0).getLong("coefficientNumber"));
        Assert.assertEquals("commSign0",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("commitments").getJSONObject(0).getString("signature"));

        Assert.assertEquals("c1",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("commitments").getJSONObject(1).getString("commitment"));
        Assert.assertEquals(1l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("commitments").getJSONObject(1).getLong("coefficientNumber"));
        Assert.assertEquals("commSign1",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("commitments").getJSONObject(1).getString("signature"));

        Assert.assertEquals("c3",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(1).getJSONArray("commitments").getJSONObject(0).getString("commitment"));
        Assert.assertEquals(0l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(1).getJSONArray("commitments").getJSONObject(0).getLong("coefficientNumber"));
        Assert.assertEquals("commSign3",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(1).getJSONArray("commitments").getJSONObject(0).getString("signature"));

        Assert.assertEquals("c4",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(1).getJSONArray("commitments").getJSONObject(1).getString("commitment"));
        Assert.assertEquals(1l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(1).getJSONArray("commitments").getJSONObject(1).getLong("coefficientNumber"));
        Assert.assertEquals("commSign4",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(1).getJSONArray("commitments").getJSONObject(1).getString("signature"));

        Assert.assertEquals("s00",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(0).getJSONArray("secretShare").getString(0));
        Assert.assertEquals("s01",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(0).getJSONArray("secretShare").getString(1));
        Assert.assertEquals(10l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(0).getLong("fromVoterId"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(0).getString("fromVoterName"));
        Assert.assertEquals("secretSign0",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(0).getString("signature"));

        Assert.assertEquals("s10",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(1).getJSONArray("secretShare").getString(0));
        Assert.assertEquals("s11",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(1).getJSONArray("secretShare").getString(1));
        Assert.assertEquals(11l,jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(1).getLong("fromVoterId"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(1).getString("fromVoterName"));
        Assert.assertEquals("secretSign1",jsonObjectOptional.get().getJSONArray("keyGenerationPhase").getJSONObject(0).getJSONArray("secretShares").getJSONObject(1).getString("signature"));
    }

    @Test
    public void votingDataIsIncludedInVotingDescription() {
        Optional<JSONObject> jsonObjectOptional = votingsSummaryService.createVotingsSummary(votingId);
        Assert.assertTrue(jsonObjectOptional.isPresent());

        Assert.assertEquals(10l,jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONObject("data").getLong("fromVoterId"));
        Assert.assertEquals(11l,jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONObject("data").getLong("fromVoterId"));
        Assert.assertEquals(12l,jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(2).getJSONObject("data").getLong("fromVoterId"));

        Assert.assertEquals("alpha0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONObject("data").getString("alpha"));
        Assert.assertEquals("alpha1",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONObject("data").getString("alpha"));
        Assert.assertEquals("alpha2",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(2).getJSONObject("data").getString("alpha"));

        Assert.assertEquals("beta0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONObject("data").getString("beta"));
        Assert.assertEquals("beta1",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONObject("data").getString("beta"));
        Assert.assertEquals("beta2",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(2).getJSONObject("data").getString("beta"));

        Assert.assertEquals("sign0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONObject("data").getString("signature"));
        Assert.assertEquals("sign1",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONObject("data").getString("signature"));
        Assert.assertEquals("sign2",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(2).getJSONObject("data").getString("signature"));

        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONObject("data").getString("fromVoterName"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONObject("data").getString("fromVoterName"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(2).getJSONObject("data").getString("fromVoterName"));

        Assert.assertEquals(0L,jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONArray("proofs").getJSONObject(0).getLong("messageIndex"));
        Assert.assertEquals("a0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONArray("proofs").getJSONObject(0).getString("a"));
        Assert.assertEquals("b0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONArray("proofs").getJSONObject(0).getString("b"));
        Assert.assertEquals("c0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONArray("proofs").getJSONObject(0).getString("c"));
        Assert.assertEquals("r0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONArray("proofs").getJSONObject(0).getString("r"));
        Assert.assertEquals("sign0",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(0).getJSONArray("proofs").getJSONObject(0).getString("signature"));

        Assert.assertEquals(1L,jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONArray("proofs").getJSONObject(1).getLong("messageIndex"));
        Assert.assertEquals("a4",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONArray("proofs").getJSONObject(1).getString("a"));
        Assert.assertEquals("b4",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONArray("proofs").getJSONObject(1).getString("b"));
        Assert.assertEquals("c4",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONArray("proofs").getJSONObject(1).getString("c"));
        Assert.assertEquals("r4",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONArray("proofs").getJSONObject(1).getString("r"));
        Assert.assertEquals("sign4",jsonObjectOptional.get().getJSONArray("votingPhase").getJSONObject(1).getJSONArray("proofs").getJSONObject(1).getString("signature"));
    }

    @Test
    public void decryptionDataIsIncludedInVotingDescription() {
        Optional<JSONObject> jsonObjectOptional = votingsSummaryService.createVotingsSummary(votingId);
        Assert.assertTrue(jsonObjectOptional.isPresent());

        Assert.assertEquals("d0",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("data").getString("decryptionFactor"));
        Assert.assertEquals("sign0",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("data").getString("signature"));
        Assert.assertEquals(10l,jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("data").getLong("fromVoterId"));
        Assert.assertEquals(0,jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("data").getLong("fromVoterPosition"));
        Assert.assertEquals("***",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("data").getString("fromVoterName"));

        Assert.assertEquals("a0",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("proof").getString("a"));
        Assert.assertEquals("b0",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("proof").getString("b"));
        Assert.assertEquals("r0",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("proof").getString("r"));
        Assert.assertEquals("sign0",jsonObjectOptional.get().getJSONArray("decryptionPhase").getJSONObject(0).getJSONObject("proof").getString("signature"));
    }

    private void addVotingsMetaData(VotingEntity votingEntity) {
        VotingOptionEntity votingOption0 = new VotingOptionEntity()
                .setOptionName("o0")
                .setOptionPrimeNumber(2l);

        VotingOptionEntity votingOption1 = new VotingOptionEntity()
                .setOptionName("o1")
                .setOptionPrimeNumber(3l);

        VotingOptionEntity votingOption2 = new VotingOptionEntity()
                .setOptionName("o2")
                .setOptionPrimeNumber(5l);

        VotingCalculationParametersEntity votingCalculationParametersEntity = new VotingCalculationParametersEntity()
                .setPrimeQ("primeQ")
                .setPrimeP("primeP")
                .setGeneratorG("generatorG");

        votingEntity
                .setTitle("fantastic title")
                .setDescription("fantastic description")
                .setSecurityThreshold(3)
                .setCalculationParametersEntity(votingCalculationParametersEntity)
                .addVotingOption(votingOption0)
                .addVotingOption(votingOption1)
                .addVotingOption(votingOption2);
    }

    private void addEligibleVotersData(VotingEntity votingEntity) {
        UserEntity userEntity1 = new VoterEntityWithIdSetter().setId(10l).setUsername("foo");
        UserEntity userEntity2 = new VoterEntityWithIdSetter().setId(11l).setUsername("bar");
        UserEntity userEntity3 = new VoterEntityWithIdSetter().setId(12l).setUsername("baz");

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity)
                .setCanVote(true)
                .setIsTrustee(false)
                .setPublicKey("key0")
                .setVoterNumber(0L)
                .setCreatedSecretSharesToTrue();

        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity)
                .setCanVote(true)
                .setIsTrustee(false)
                .setPublicKey("key1")
                .setVoterNumber(1L)
                .setCreatedSecretSharesToTrue();

        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity)
                .setCanVote(false)
                .setIsTrustee(true)
                .setPublicKey("key2")
                .setVoterNumber(2L)
                .setCreatedSecretSharesToTrue();

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3);
    }

    private void addKeyGenerationData(VotingEntity votingEntity) {
        SingleVotingVoterEntity singleVotingVoterEntity0 = new SingleVotingVoterEntity(votingEntity.getAllEligibleVoters().get(0).getUserEntity(),votingEntity)
                .addCommitments(new CommitmentEntity().setCommitment("c0").setForCoefficientNumber(0l).setSignature("commSign0"))
                .addCommitments(new CommitmentEntity().setCommitment("c1").setForCoefficientNumber(1l).setSignature("commSign1"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(10L).setSecretShare("[s00,s01]").setSignature("secretSign0"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(11L).setSecretShare("[s10,s11]").setSignature("secretSign1"))
                .setCreatedSecretSharesToTrue();

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(votingEntity.getAllEligibleVoters().get(1).getUserEntity(),votingEntity)
                .addCommitments(new CommitmentEntity().setCommitment("c3").setForCoefficientNumber(0l).setSignature("commSign3"))
                .addCommitments(new CommitmentEntity().setCommitment("c4").setForCoefficientNumber(1l).setSignature("commSign4"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(10L).setSecretShare("[s30,s31]").setSignature("secretSign3"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(11L).setSecretShare("[s40,s41]").setSignature("secretSign4"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(12L).setSecretShare("[s50,s51]").setSignature("secretSign5"));

        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(votingEntity.getAllEligibleVoters().get(2).getUserEntity(),votingEntity)
                .addCommitments(new CommitmentEntity().setCommitment("c6").setForCoefficientNumber(0l).setSignature("commSign6"))
                .addCommitments(new CommitmentEntity().setCommitment("c7").setForCoefficientNumber(1l).setSignature("commSign7"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(10L).setSecretShare("[s60,s61]").setSignature("secretSign6"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(11L).setSecretShare("[s70,s71]").setSignature("secretSign7"))
                .addSecretShare(new SecretShareEntity().setFromVoterId(12L).setSecretShare("[s80,s81]").setSignature("secretSign8"));

        when(singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(10l,votingId)).thenReturn(Optional.of(singleVotingVoterEntity0));
        when(singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(11l,votingId)).thenReturn(Optional.of(singleVotingVoterEntity1));
        when(singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(12l,votingId)).thenReturn(Optional.of(singleVotingVoterEntity2));

        when(singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(10l,votingId)).thenReturn(Optional.of(singleVotingVoterEntity0));
        when(singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(11l,votingId)).thenReturn(Optional.of(singleVotingVoterEntity1));
        when(singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(12l,votingId)).thenReturn(Optional.of(singleVotingVoterEntity2));
    }

    private void addVoteData(VotingEntity votingEntity) {
        votingEntity.getAllEligibleVoters().get(0).setAlpha("alpha0").setBeta("beta0").setVoteSignature("sign0").setVotedToTrue();
        votingEntity.getAllEligibleVoters().get(1).setAlpha("alpha1").setBeta("beta1").setVoteSignature("sign1").setVotedToTrue();
        votingEntity.getAllEligibleVoters().get(2).setAlpha("alpha2").setBeta("beta2").setVoteSignature("sign2").setVotedToTrue();

        VoteNIZKPEntity voteNIZKPEntity0 = new VoteNIZKPEntity().setA("a0").setB("b0").setC("c0").setR("r0").setMessageIndex(0).setSignature("sign0");
        VoteNIZKPEntity voteNIZKPEntity1 = new VoteNIZKPEntity().setA("a1").setB("b1").setC("c1").setR("r1").setMessageIndex(1).setSignature("sign1");
        VoteNIZKPEntity voteNIZKPEntity2 = new VoteNIZKPEntity().setA("a2").setB("b2").setC("c2").setR("r2").setMessageIndex(2).setSignature("sign2");

        VoteNIZKPEntity voteNIZKPEntity3 = new VoteNIZKPEntity().setA("a3").setB("b3").setC("c3").setR("r3").setMessageIndex(0).setSignature("sign3");
        VoteNIZKPEntity voteNIZKPEntity4 = new VoteNIZKPEntity().setA("a4").setB("b4").setC("c4").setR("r4").setMessageIndex(1).setSignature("sign4");
        VoteNIZKPEntity voteNIZKPEntity5 = new VoteNIZKPEntity().setA("a5").setB("b5").setC("c5").setR("r5").setMessageIndex(2).setSignature("sign5");

        VoteNIZKPEntity voteNIZKPEntity6 = new VoteNIZKPEntity().setA("a6").setB("b6").setC("c6").setR("r6").setMessageIndex(0).setSignature("sign6");
        VoteNIZKPEntity voteNIZKPEntity7 = new VoteNIZKPEntity().setA("a7").setB("b7").setC("c7").setR("r7").setMessageIndex(1).setSignature("sign7");
        VoteNIZKPEntity voteNIZKPEntity8 = new VoteNIZKPEntity().setA("a8").setB("b8").setC("c8").setR("r8").setMessageIndex(2).setSignature("sign8");

        votingEntity.getAllEligibleVoters().get(0)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity0)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity1)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity2);

        votingEntity.getAllEligibleVoters().get(1)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity3)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity4)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity5);

        votingEntity.getAllEligibleVoters().get(2)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity6)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity7)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity8);
    }

    private void addDecryptionData(VotingEntity votingEntity) {
        votingEntity.getAllEligibleVoters().get(0).setDecryptionFactor("d0").setParticipatedInDecryptionToTrue();
        votingEntity.getAllEligibleVoters().get(1).setDecryptionFactor("d1").setParticipatedInDecryptionToTrue();
        votingEntity.getAllEligibleVoters().get(2).setDecryptionFactor("d2").setParticipatedInDecryptionToTrue();

        votingEntity.getAllEligibleVoters().get(0).setDecryptionFactorSignature("sign0");
        votingEntity.getAllEligibleVoters().get(1).setDecryptionFactorSignature("sign1");
        votingEntity.getAllEligibleVoters().get(2).setDecryptionFactorSignature("sign2");

        DecryptionNIZKPEntity decryptionNIZKPEntity0 = new DecryptionNIZKPEntity().setA("a0").setB("b0").setR("r0").setSignature("sign0");
        DecryptionNIZKPEntity decryptionNIZKPEntity1 = new DecryptionNIZKPEntity().setA("a1").setB("b1").setR("r1").setSignature("sign1");
        DecryptionNIZKPEntity decryptionNIZKPEntity2 = new DecryptionNIZKPEntity().setA("a2").setB("b2").setR("r2").setSignature("sign2");

        votingEntity.getAllEligibleVoters().get(0).setDecryptionNIZKProof(decryptionNIZKPEntity0);
        votingEntity.getAllEligibleVoters().get(1).setDecryptionNIZKProof(decryptionNIZKPEntity1);
        votingEntity.getAllEligibleVoters().get(2).setDecryptionNIZKProof(decryptionNIZKPEntity2);
    }
}
