package com.service.context;

import com.data.voter.SingleVoter;

import com.services.context.RequestContextHolder;

import java.util.Optional;

public class FakeRequestContextHolder implements RequestContextHolder {

    private SingleVoter currentVoter;

    @Override
    public void setCurrentVoter(SingleVoter currentVoter) {
        this.currentVoter = currentVoter;
    }

    @Override
    public Optional<SingleVoter> getCurrentVoter() {
        return Optional.ofNullable(currentVoter);
    }
}
