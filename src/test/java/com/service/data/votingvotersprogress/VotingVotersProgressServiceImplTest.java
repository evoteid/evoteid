package com.service.data.votingvotersprogress;

import com.data.votingvoterprogress.VotingVoterProgress;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.voting.VotingEntity;
import com.services.data.votingvoterprogress.VotingVoterProgressService;
import com.services.data.votingvoterprogress.VotingVoterProgressServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.Mockito.when;

public class VotingVotersProgressServiceImplTest {

    private VotingVoterProgressService service;

    private SingleVotingVoterRepository singleVotingVoterRepository;

    private Long voterId = 27L;

    private Long votingId = 42L;

    private int phaseNumber = 5;

    @Before
    public void setup() {
        singleVotingVoterRepository = Mockito.mock(SingleVotingVoterRepository.class);
        service = new VotingVoterProgressServiceImpl(singleVotingVoterRepository);
    }

    @Test
    public void returnEmptyIfVotingsRepositoryReturnsEmpty() {
        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.empty());
        Assert.assertFalse(service.getVotingProgressForVoter(votingId,voterId).isPresent());
    }

    @Test
    public void returnRightDataIfEntityIsThere() {
        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity()
                .setPhaseNumber(phaseNumber);

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity)
                .setCreatedSecretSharesToTrue()
                .setParticipatedInDecryptionToTrue()
                .setCreatedOwnKeyToTrue();

        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        VotingVoterProgress progress = service.getVotingProgressForVoter(votingId,voterId).get();
        Assert.assertEquals(phaseNumber,progress.toJson().getInt("phaseNumber"));
        Assert.assertTrue(progress.toJson().getBoolean("createdSecretShares"));
        Assert.assertFalse(progress.toJson().getBoolean("voted"));
        Assert.assertTrue(progress.toJson().getBoolean("participatedInDecryption"));
        Assert.assertTrue(progress.toJson().getBoolean("usesCustomKey"));
    }
}
