package com.service.data.roles;

import com.data.roles.Roles;
import com.data.voter.Voters;
import com.enums.Role;
import com.models.role.RoleRepository;
import com.models.role.RolesEntity;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.services.data.roles.RoleService;
import com.services.data.roles.RoleServiceImpl;
import org.assertj.core.util.Lists;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class RoleServiceImplTest {

    @Autowired
    private UserRepository voterRepository;

    @Autowired
    private RoleRepository roleRepository;

    private RoleService roleService;

    @Before
    public void setup() {
        voterRepository.deleteAll();
        roleService = new RoleServiceImpl(roleRepository,voterRepository);
    }

    @Test
    public void noVoterRespondsEmpty() {
        Long notExisting = 24L;
        Assert.assertFalse(roleService.getVoterWithRoles(notExisting).isPresent());
    }

    @Test
    public void voterWithNoRoleIsOk() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        Assert.assertEquals(Lists.emptyList(),roleService.getVoterWithRoles(id).get().getRoles().getRoles());
    }

    @Test
    public void voterWithRolesIsOk() {
        UserEntity userEntity = new UserEntity()
                .setUsername("dummy")
                .setRoles(new RolesEntity()
                    .setAdministratorRole(true)
                    .setRegistrarRole(true));

        Long id = voterRepository.save(userEntity).getId();
        Assert.assertEquals("dummy",roleService.getVoterWithRoles(id).get().getName());
        Assert.assertEquals(2,roleService.getVoterWithRoles(id).get().getRoles().getRoles().size());
        Assert.assertTrue(roleService.getVoterWithRoles(id).get().getRoles().getRoles().contains(Role.ADMINISTRATOR));
        Assert.assertTrue(roleService.getVoterWithRoles(id).get().getRoles().getRoles().contains(Role.REGISTRAR));
    }

    @Test
    public void findVoterByUsernameWorksIfEmpty() {
        String notExistingName = "nonExisting";
        Assert.assertFalse(roleService.getVoterWithRoles(notExistingName).isPresent());
    }

    @Test
    public void findVoterByUsernameWorks() {
        String voterName = "dummy";
        UserEntity userEntity = new UserEntity()
                .setUsername(voterName)
                .setRoles(new RolesEntity()
                        .setAdministratorRole(true)
                        .setRegistrarRole(true));

        voterRepository.save(userEntity).getId();
        Assert.assertEquals(voterName,roleService.getVoterWithRoles(voterName).get().getName());
        Assert.assertEquals(2,roleService.getVoterWithRoles(voterName).get().getRoles().getRoles().size());
        Assert.assertTrue(roleService.getVoterWithRoles(voterName).get().getRoles().getRoles().contains(Role.ADMINISTRATOR));
        Assert.assertTrue(roleService.getVoterWithRoles(voterName).get().getRoles().getRoles().contains(Role.REGISTRAR));
    }

    @Test
    public void addRoleToVoterWithVoterNotThereReturnsFalse() {
        Long missingVoterId = 27L;
        Assert.assertFalse(roleService.addRoleToVoter(missingVoterId,Role.ADMINISTRATOR));
    }

    @Test
    public void addRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        roleService.addRoleToVoter(id,Role.ADMINISTRATOR);
        Assert.assertEquals(Arrays.asList(Role.ADMINISTRATOR),roleService.getVoterWithRoles(id).get().getRoles().getRoles());
    }

    @Test
    public void addRoleTwiceWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        roleService.addRoleToVoter(id,Role.ADMINISTRATOR);
        roleService.addRoleToVoter(id,Role.ADMINISTRATOR);
        Assert.assertEquals(Arrays.asList(Role.ADMINISTRATOR),roleService.getVoterWithRoles(id).get().getRoles().getRoles());
    }

    @Test
    public void deleteRoleReturnsFalseIfVoterNotThere() {
        Long missingVoterId = 27L;
        Assert.assertFalse(roleService.deleteRoleFromVoter(missingVoterId,Role.ADMINISTRATOR));
    }

    @Test
    public void deleteRoleWorksEvenIfRoleNotThere() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        Assert.assertTrue(roleService.deleteRoleFromVoter(id,Role.ADMINISTRATOR));
    }

    @Test
    public void deleteRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        roleService.addRoleToVoter(id,Role.ADMINISTRATOR);
        roleService.deleteRoleFromVoter(id,Role.ADMINISTRATOR);
        Assert.assertEquals(0,roleService.getVoterWithRoles(id).get().getRoles().getRoles().size());
    }

    @Test
    public void containsWithNoVoterReturnsFalse() {
        Long missingVoterId = 27L;
        Assert.assertFalse(roleService.hasRole(missingVoterId,Role.ADMINISTRATOR));
    }

    @Test
    public void containsReturnsFalseIfNotThere() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        Assert.assertFalse(roleService.hasRole(id,Role.ADMINISTRATOR));
    }

    @Test
    public void containsReturnsTrueIfThere() {
        UserEntity userEntity = new UserEntity();
        Long id = voterRepository.save(userEntity).getId();
        roleService.addRoleToVoter(id,Role.ADMINISTRATOR);
        Assert.assertTrue(roleService.hasRole(id,Role.ADMINISTRATOR));
    }

    @Test
    public void getAllVotersWithRolesWorks() {
        UserEntity userEntityOne = new UserEntity();
        UserEntity userEntityTwo = new UserEntity();
        UserEntity userEntityThree = new UserEntity();

        Long userEntityIdOne = voterRepository.save(userEntityOne).getId();
        Long userEntityIdTwo = voterRepository.save(userEntityTwo).getId();
        Long userEntityIdThree = voterRepository.save(userEntityThree).getId();

        roleService.addRoleToVoter(userEntityIdOne,Role.VOTING_ADMINISTRATOR);
        roleService.addRoleToVoter(userEntityIdTwo,Role.ADMINISTRATOR);
        roleService.addRoleToVoter(userEntityIdTwo,Role.REGISTRAR);
        roleService.addRoleToVoter(userEntityIdThree,Role.ADMINISTRATOR);

        Voters allVoters = roleService.getAllVotersWithTheirRoles();

        JSONObject allUsersJson = allVoters.toJsonWithRoles();
        JSONArray jsonArray = allUsersJson.getJSONArray("voters");
        Assert.assertEquals(3,jsonArray.length());

        for(int i=0;i<jsonArray.length();i++) {
            if(jsonArray.getJSONObject(i).get("id").equals(userEntityIdOne)) {
                Assert.assertEquals("[\"Voting_Administrator\"]",jsonArray.getJSONObject(i).getJSONArray("roles").toString());
            }
            if(jsonArray.getJSONObject(i).get("id").equals(userEntityIdTwo)) {
                Assert.assertEquals("[\"Administrator\",\"Registrar\"]",jsonArray.getJSONObject(i).getJSONArray("roles").toString());
            }
            if(jsonArray.getJSONObject(i).get("id").equals(userEntityIdThree)) {
                Assert.assertEquals("[\"Administrator\"]",jsonArray.getJSONObject(i).getJSONArray("roles").toString());
            }
        }
    }

    @Test
    public void getAllVotersByIdWithRolesWorks() {
        UserEntity userEntityOne = new UserEntity();
        UserEntity userEntityTwo = new UserEntity();
        UserEntity userEntityThree = new UserEntity();

        Long userEntityIdOne = voterRepository.save(userEntityOne).getId();
        Long userEntityIdTwo = voterRepository.save(userEntityTwo).getId();
        Long userEntityIdThree = voterRepository.save(userEntityThree).getId();

        roleService.addRoleToVoter(userEntityIdOne,Role.VOTING_ADMINISTRATOR);
        roleService.addRoleToVoter(userEntityIdTwo,Role.ADMINISTRATOR);
        roleService.addRoleToVoter(userEntityIdTwo,Role.REGISTRAR);
        roleService.addRoleToVoter(userEntityIdThree,Role.ADMINISTRATOR);

        Voters allVoters = roleService.getAllVotersByIdWithTheirRoles(Arrays.asList(userEntityIdOne,userEntityIdThree));

        Assert.assertEquals(2,allVoters.getVoters().size());
        Assert.assertEquals(userEntityIdOne,allVoters.getVoters().get(0).getId());
        Assert.assertEquals(userEntityIdThree,allVoters.getVoters().get(1).getId());
    }

    @Test
    public void getAllAvailableRolesWorks() {
        Roles roles = roleService.getAllAvailableRoles();
        Assert.assertEquals(Arrays.asList(Role.ADMINISTRATOR,Role.VOTING_ADMINISTRATOR,Role.REGISTRAR),roles.getRoles());
    }
}


