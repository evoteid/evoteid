package com.service.data.proofs;

import com.data.proofs.DecryptionNIZKProof;
import com.enums.Phases;
import com.exceptions.IllegalVoteException;
import com.models.decryption.DecryptionRepository;
import com.models.singlevotingvoter.DecryptionNIZKPEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.voting.VotingEntity;
import com.services.data.proofs.DecryptionNIZKProofService;
import com.services.data.proofs.DecryptionNIZKProofServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DecryptionNIZKProofServiceImplTest {

    private SingleVotingVoterRepository mockSingleShareRepository;

    private DecryptionNIZKProofService decryptionNIZKProofService;

    private DecryptionRepository decryptionRepository;

    @Before
    public void setup() {
        mockSingleShareRepository = Mockito.mock(SingleVotingVoterRepository.class);
        decryptionRepository = Mockito.mock(DecryptionRepository.class);
        decryptionNIZKProofService = new DecryptionNIZKProofServiceImpl(mockSingleShareRepository,decryptionRepository);
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsReturnsNoSuchVotingIfVotingNotExisting() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;
        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.empty());
        decryptionNIZKProofService.setNIZKProofs(votingId,voterId, new DecryptionNIZKProof());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsThrowsExceptionIfAlreadySpecified() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();

        DecryptionNIZKPEntity decryptionNIZKPEntity = new DecryptionNIZKPEntity().setProofAdded(true);
        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity)
                .setDecryptionNIZKProof(decryptionNIZKPEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        decryptionNIZKProofService.setNIZKProofs(votingId,voterId, new DecryptionNIZKProof());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsThrowsExceptionIfVotingClosed() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity().setPhaseNumber(42);

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        decryptionNIZKProofService.setNIZKProofs(votingId,voterId, new DecryptionNIZKProof());
    }

    @Test
    public void savingNIZKProofInvokesTheRightCall() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity()
                .setPhaseNumber(Phases.DECRYPTION_PHASE.getNumber());

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        when(decryptionRepository.setDecryptionProof(votingId,voterId,"a1","b1","r1","sign")).thenReturn(1);
        boolean successful = decryptionNIZKProofService.setNIZKProofs(votingId,voterId,new DecryptionNIZKProof()
                .setA("a1")
                .setB("b1")
                .setR("r1")
                .setSignature("sign"));

        verify(decryptionRepository,times(1)).setDecryptionProof(votingId,voterId,"a1","b1","r1","sign");
        Assert.assertTrue(successful);
    }

}
