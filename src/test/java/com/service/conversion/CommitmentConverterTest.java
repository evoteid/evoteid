package com.service.conversion;

import com.data.commitments.SingleCommitment;
import com.models.singlevotingvoter.CommitmentEntity;
import com.services.conversion.CommitmentConverter;
import org.junit.Assert;
import org.junit.Test;

public class CommitmentConverterTest {

    private long forCoefficientNumber = 0l;

    private String commitmentValue = "c";

    private String signature = "sign";

    @Test
    public void commitmentEntityToCommitmentWorks() {
        CommitmentEntity commitmentEntity = new CommitmentEntity()
                .setForCoefficientNumber(0l)
                .setCommitment(commitmentValue)
                .setSignature(signature);

        SingleCommitment commitment = CommitmentConverter.toData(commitmentEntity);

        Assert.assertEquals(forCoefficientNumber, commitment.getCoefficientNumber().longValue());
        Assert.assertEquals(commitmentValue, commitment.getCommitment());
        Assert.assertEquals(signature, commitment.getSignature());
    }

    @Test
    public void commitmentToCommitmentEntityWorks() {
        SingleCommitment commitment = new SingleCommitment()
                .setCoefficientNumber(0l)
                .setCommitment(commitmentValue)
                .setSignature(signature);

        CommitmentEntity commitmentEntity = CommitmentConverter.toEntity(commitment);

        Assert.assertEquals(forCoefficientNumber, commitmentEntity.getForCoefficientNumber());
        Assert.assertEquals(commitmentValue, commitmentEntity.getCommitment());
        Assert.assertEquals(signature, commitmentEntity.getSignature());
    }
}
