package com.service.conversion;

import com.data.votingvoterprogress.VotingVoterProgress;
import com.models.helper.VotingEntityWithSetters;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.user.UserEntity;
import com.models.voting.VotingEntity;
import com.services.conversion.VotingVoterProgressConverter;
import org.junit.Assert;
import org.junit.Test;

public class VotingVoterProgressConverterTest {

    @Test
    public void toDataTest() {
        long votingId = 27;
        int phaseNumber = 7;

        VotingEntity votingEntity = new VotingEntityWithSetters().setId(votingId).setPhaseNumber(phaseNumber);
        UserEntity userEntity = new UserEntity();

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity)
                .setParticipatedInDecryptionToTrue()
                .setCreatedSecretSharesToTrue()
                .setCreatedOwnKeyToTrue();


        VotingVoterProgress progress = VotingVoterProgressConverter.toData(singleVotingVoterEntity);
        Assert.assertEquals("{\"participatedInDecryption\":true,\"usesCustomKey\":true,\"phaseNumber\":7,\"votingId\":27,\"createdSecretShares\":true,\"voted\":false}",progress.toJson().toString());
    }
}
