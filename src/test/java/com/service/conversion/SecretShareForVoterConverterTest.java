package com.service.conversion;

import com.data.secretshares.SingleSecretShareForVoter;
import com.models.singlevotingvoter.SecretShareEntity;
import com.services.conversion.SecretShareForVoterConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class SecretShareForVoterConverterTest {

    @Test
    public void secretShareEntityToSecretShareDataWorks() {
        SecretShareEntity secretShareEntity = new SecretShareEntity()
                .setFromVoterId(27l)
                .setFromVotername("name")
                .setSignature("sign")
                .setSecretShare("[a,b,c]");

        SingleSecretShareForVoter share = SecretShareForVoterConverter.toData(secretShareEntity);
        Assert.assertEquals(27l,share.getFromVoterId());
        Assert.assertEquals("name",share.getFromVoterName());
        Assert.assertEquals("sign",share.getSignature());
        Assert.assertEquals(Arrays.asList("a","b","c"),share.getSecretShare());
    }
}
