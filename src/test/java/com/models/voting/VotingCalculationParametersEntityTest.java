package com.models.voting;

import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VotingCalculationParametersEntityTest {

    @Autowired
    private VotingsRepository votingsRepository;

    @Test()
    public void getCalculationParametersWorks() {
        String largeP = "178400903080225611711164807715027039791119806599816933981173994138923013065117816547452806678577891690415378292895826261793152443896402436036598290694339831529402945937382657400200494947438672665854507960350532581120228460046692838422604100098789178807368844582295956839918225572945735160045183648809269289573";
        String largeQ = "140042793474967019240045331289823420495710409376286716669186275914292636952595842649018027190625292833216481897237125213911545851467742664992026409337044943604339777465930181133465425836126069969644098960897342913332381343425877873857982002773600269254442935956244691067615220608423201878906688966868530475493";
        String largeGenerator = "143139376393189172673336384679636238486575973249772393557996715181623665461745339041147149760301829736878002212350233605383662876668664726195742384388279698238378138378781101119441494421706551814951312009237913504173815049276442518496761150011314568262577499401827320019014188449987865502071715761635932777269";

        VotingCalculationParametersEntity calculationParametersEntity = new VotingCalculationParametersEntity()
                .setPrimeP(largeP)
                .setPrimeQ(largeQ)
                .setGeneratorG(largeGenerator);

        VotingEntity votingEntity = new VotingEntity()
                .setCalculationParametersEntity(calculationParametersEntity);

        Long id = votingsRepository.save(votingEntity).getId();
        VotingCalculationParametersEntity retParams = votingsRepository.findById(id).get().getCalculationParametersEntity();
        Assert.assertEquals(largeP,retParams.getPrimeP());
        Assert.assertEquals(largeQ,retParams.getPrimeQ());
        Assert.assertEquals(largeGenerator,retParams.getGeneratorG());
    }
}
