package com.models.voting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VotingsRepositoryTest {

    @Autowired
    private VotingsRepository votingsRepository;

    @Test
    public void findOneReturnsEmptyIfNotThere() {
        Optional<VotingEntity> votingEntityOptional = votingsRepository.findById(1L);
        Assert.assertFalse(votingEntityOptional.isPresent());
    }

    @Test
    public void findOneReturnsObjectIfThere() {
        VotingEntity votingsEntity = new VotingEntity();
        Long id = votingsRepository.save(votingsEntity).getId();
        Optional<VotingEntity> votingEntityOptional = votingsRepository.findById(id);
        Assert.assertTrue(votingEntityOptional.isPresent());
    }

    @Test
    public void findOneWithInvalidIdShouldNotBeThere() {
        long notThere = 1;
        Assert.assertFalse(votingsRepository.findById(notThere).isPresent());
    }

    @Test
    public void findOneWithValidIdShouldBeThere() {
        Long validId = votingsRepository.save(new VotingEntity()).getId();
        Assert.assertTrue(votingsRepository.findById(validId).isPresent());
    }

    @Test
    public void votingsArrayCanBeStoredAndRetrievedBack() {
        String title1 = "Title 1";
        String title2 = "Title 2";

        VotingEntity votingEntityOne = new VotingEntity().setTitle(title1);
        VotingEntity votingEntityTwo = new VotingEntity().setTitle(title2);

        votingsRepository.save(votingEntityOne).getId();
        votingsRepository.save(votingEntityTwo).getId();

        Iterator<VotingEntity> retrievedIterator = votingsRepository.findAllByOrderByIdAsc().iterator();

        VotingEntity retEntityOne = retrievedIterator.next();
        VotingEntity retEntityTwo = retrievedIterator.next();

        Assert.assertEquals(title1,retEntityOne.getTitle());
        Assert.assertEquals(title2,retEntityTwo.getTitle());
    }

    @Test
    public void updateVotingPhaseNumberWorks() {
        int oldPhaseNumber = 27;
        int newPhaseNumber = 42;

        VotingEntity votingEntityOne = new VotingEntity().setTitle("title").setPhaseNumber(oldPhaseNumber);
        VotingEntity savedEntity = votingsRepository.save(votingEntityOne);

        int numberOfUpdatedRows = votingsRepository.updatePhaseNumber(savedEntity.getId(),newPhaseNumber);

        int storedPhaseNumber = votingsRepository.findById(savedEntity.getId()).get().getPhaseNumber();
        Assert.assertEquals(1,numberOfUpdatedRows);
        Assert.assertEquals(newPhaseNumber,storedPhaseNumber);
    }
}
