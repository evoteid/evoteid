package com.models.user;

import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void retrieveNonExisting() {
        Optional<UserEntity> entityOptional = userRepository.findById(new Long(1));
        Assert.assertFalse(entityOptional.isPresent());
    }

    @Test
    public void findByUserNameReturnsNullIfNotThere() {
        Optional<UserEntity> entityOptional = userRepository.findOneByUsername("Not There");
        Assert.assertFalse(entityOptional.isPresent());
    }

    @Test
    public void findByUserNameReturnsCorrectIfThere() {
        String userName = "user";
        UserEntity userEntity = new UserEntity().setUsername(userName);
        userRepository.save(userEntity).getId();
        Optional<UserEntity> entityOptional = userRepository.findOneByUsername(userName);
        Assert.assertTrue(entityOptional.isPresent());
    }

    @Test
    public void findByUserNameAndEmailReturnsNullIfNotThere() {
        Optional<UserEntity> entityOptional = userRepository.findOneByUsernameAndEmail("Not There","Mail");
        Assert.assertFalse(entityOptional.isPresent());
    }

    @Test
    public void findByUserNameAndEmailReturnsCorrectIfThere() {
        String userName = "user";
        String email = "userMail";
        UserEntity userEntity = new UserEntity().setUsername(userName).setEmail(email);
        userRepository.save(userEntity).getId();
        Optional<UserEntity> entityOptional = userRepository.findOneByUsernameAndEmail(userName,email);
        Assert.assertTrue(entityOptional.isPresent());
    }

    @Test
    public void updateWorks() {
        String userName = "user";
        String key = "key";

        UserEntity userEntity = new UserEntity().setUsername(userName);
        userRepository.save(userEntity);

        userEntity.setPublicKey(key);
        Long id = userRepository.save(userEntity).getId();

        Optional<UserEntity> entityOptional = userRepository.findById(id);
        Assert.assertEquals(userName,entityOptional.get().getUsername());
        Assert.assertEquals(key,entityOptional.get().getPublicKey());
    }

    @Test
    public void getAllWorks() {
        UserEntity e1 = new UserEntity().setUsername("i1").setPublicKey("k1");
        UserEntity e2 = new UserEntity().setUsername("i2").setPublicKey("k2");
        UserEntity e3 = new UserEntity().setUsername("i3").setPublicKey("k3");

        userRepository.save(e1);
        userRepository.save(e2);
        userRepository.save(e3);

        Iterable<UserEntity> eIter = userRepository.findAll();
        List<UserEntity> keyEntities = StreamSupport.stream(eIter.spliterator(),false).collect(Collectors.toList());
        Assert.assertEquals(3,keyEntities.size());
    }

    @Test
    public void findByMultipleUsernamesWorks() {
        UserEntity e1 = new UserEntity().setUsername("i1");
        UserEntity e2 = new UserEntity().setUsername("i2");
        UserEntity e3 = new UserEntity().setUsername("i3");

        userRepository.save(e1);
        userRepository.save(e2);
        userRepository.save(e3);

        List<UserEntity> entities = userRepository.findByUsernameIn(Arrays.asList("i1","i3"));
        Assert.assertEquals(2,entities.size());
    }

    @Test
    public void findByMultipleUsernamesWorksEmptyList() {
        UserEntity e1 = new UserEntity().setUsername("i1");
        UserEntity e2 = new UserEntity().setUsername("i2");
        UserEntity e3 = new UserEntity().setUsername("i3");

        userRepository.save(e1);
        userRepository.save(e2);
        userRepository.save(e3);

        List<UserEntity> entities = userRepository.findByUsernameIn(Arrays.asList("i5"));
        Assert.assertEquals(Lists.emptyList(),entities);
    }

    @Test
    public void getAllWorksAlsoIfEmpty() {
        Iterable<UserEntity> eIter = userRepository.findAll();
        List<UserEntity> keyEntities = StreamSupport.stream(eIter.spliterator(),false).collect(Collectors.toList());
        Assert.assertEquals(0,keyEntities.size());
    }

    @Test
    public void saveAllWorks() {
        String voterName1 = "voter1";
        String voterName2 = "voter2";
        String voterName3 = "voter3";

        List<String> allNames = Arrays.asList(voterName1,voterName2,voterName3);

        List<UserEntity> entities = Arrays.asList(
                new UserEntity().setUsername(voterName1),
                new UserEntity().setUsername(voterName2),
                new UserEntity().setUsername(voterName3)
        );

        Iterable<UserEntity> savedEntities = userRepository.saveAll(entities);
        int count = 0;
        for(UserEntity userEntity : savedEntities) {
            Assert.assertNotNull(userEntity.getId());
            Assert.assertTrue(allNames.contains(userEntity.getUsername()));
            count++;
        }
        Assert.assertEquals(3,count);
    }

}
