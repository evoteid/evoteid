package com.models.singlevotingvoter;

import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DecryptionNIZKPEntityTest {

    @Autowired
    SingleVotingVoterRepository votingVoterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    VotingsRepository votingsRepository;

    @After
    public void tearDown() {
        votingVoterRepository.deleteAll();
        userRepository.deleteAll();
        votingsRepository.deleteAll();
    }

    @Test
    public void savingAndRetrievingWorks() {
        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();

        long userId = userRepository.save(userEntity).getId();
        long votingId = votingsRepository.save(votingEntity).getId();

        DecryptionNIZKPEntity proof = new DecryptionNIZKPEntity()
                .setA("a1")
                .setB("b1")
                .setR("r1");

        SingleVotingVoterEntity entity = new SingleVotingVoterEntity(userEntity, votingEntity);
        entity.setDecryptionNIZKProof(proof);

        votingVoterRepository.save(entity);

        SingleVotingVoterEntity returnedEntity = votingVoterRepository.findOneByUserAndVotingWithNIZKPFetched(userId,votingId).get();

        Assert.assertEquals("a1",returnedEntity.getDecryptionNIZKProof().getA());
        Assert.assertEquals("b1",returnedEntity.getDecryptionNIZKProof().getB());
        Assert.assertEquals("r1",returnedEntity.getDecryptionNIZKProof().getR());
    }
}
