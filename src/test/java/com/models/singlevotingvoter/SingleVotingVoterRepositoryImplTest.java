package com.models.singlevotingvoter;

import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SingleVotingVoterRepositoryImplTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Test
    public void returnsEmptyIfEmptyTable() {
        Assert.assertFalse(singleVotingVoterRepository
                .findOneByUserAndVotingWithCommitmentsFetched(27,42).isPresent());
    }

    @Test
    public void returnsEmptyIfUserIdDoesNotMatch() {
        UserEntity userEntity = new UserEntity();
        long userId = userRepository.save(userEntity).getId();
        long wrongUserId = userId+1;

        VotingEntity votingEntity = new VotingEntity();
        long votingId= votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterRepository.save(singleVotingVoterEntity);
        Assert.assertFalse(singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(wrongUserId,votingId).isPresent());
    }

    @Test
    public void returnsEmptyIfVotingIdDoesNotMatch() {
        UserEntity userEntity = new UserEntity();
        long userId = userRepository.save(userEntity).getId();

        VotingEntity votingEntity = new VotingEntity();
        long votingId = votingsRepository.save(votingEntity).getId();
        long wrongVotingId = votingId+1;

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterRepository.save(singleVotingVoterEntity);
        Assert.assertFalse(singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(userId,wrongVotingId).isPresent());
    }

    @Test
    public void returnsDataIfVotingIdDoesMatch() {
        UserEntity userEntity = new UserEntity();
        long userId = userRepository.save(userEntity).getId();

        VotingEntity votingEntity = new VotingEntity();
        long votingId = votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterRepository.save(singleVotingVoterEntity);
        Assert.assertTrue(singleVotingVoterRepository.findOneByUserAndVotingWithCommitmentsFetched(userId,votingId).isPresent());
    }

    @Test
    public void commitmentsCanBeFetched() {
        UserEntity userEntity = new UserEntity();
        long userId = userRepository.save(userEntity).getId();

        VotingEntity votingEntity = new VotingEntity();
        long votingId = votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterEntity.addCommitments(new CommitmentEntity().setCommitment("c1"));
        singleVotingVoterEntity.addCommitments(new CommitmentEntity().setCommitment("c2"));
        singleVotingVoterEntity.addCommitments(new CommitmentEntity().setCommitment("c3"));
        singleVotingVoterRepository.save(singleVotingVoterEntity);
        Assert.assertEquals(Arrays.asList("c1","c2","c3"),singleVotingVoterRepository
                .findOneByUserAndVotingWithCommitmentsFetched(userId,votingId)
                .get()
                .getCommitments()
                .stream()
                .map(c -> c.getCommitment())
                .collect(Collectors.toList()));
    }

    @Test
    public void sharesCanBeFetched() {
        UserEntity userEntity = new UserEntity();
        long userId = userRepository.save(userEntity).getId();

        VotingEntity votingEntity = new VotingEntity();
        long votingId = votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterEntity.addSecretShare(new SecretShareEntity().setSecretShare("s1"));
        singleVotingVoterEntity.addSecretShare(new SecretShareEntity().setSecretShare("s2"));
        singleVotingVoterEntity.addSecretShare(new SecretShareEntity().setSecretShare("s3"));
        singleVotingVoterRepository.save(singleVotingVoterEntity);
        Assert.assertEquals(new HashSet<>(Arrays.asList("s1","s2","s3")),singleVotingVoterRepository
                .findOneByUserAndVotingWithSecretSharesFetched(userId,votingId)
                .get()
                .getSecretShares()
                .stream()
                .map(c -> c.getSecretShare())
                .collect(Collectors.toSet()));
    }

    @Test
    public void findOneWorks() {
        UserEntity userEntity = new UserEntity();
        long userId = userRepository.save(userEntity).getId();

        VotingEntity votingEntity = new VotingEntity();
        long votingId = votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterEntity
                .setAlpha("alpha")
                .setBeta("beta").setVotedToTrue()
                .setDecryptionFactor("decryptionFactor");

        singleVotingVoterRepository.save(singleVotingVoterEntity);

        Optional<SingleVotingVoterEntity> optEntity = singleVotingVoterRepository.findOne(userId,votingId);
        Assert.assertTrue(optEntity.get().getVoted());
        Assert.assertEquals("alpha",optEntity.get().getAlpha());
        Assert.assertEquals("beta",optEntity.get().getBeta());
        Assert.assertEquals("decryptionFactor",optEntity.get().getDecryptionFactor());
    }
}
