package com.models.helper;

import com.models.voting.VotingEntity;

public class VotingEntityWithSetters extends VotingEntity {

    private Long id;

    @Override
    public Long getId() {
        return id;
    }

    public VotingEntityWithSetters setId(Long id) {
        this.id = id;
        return this;
    }



}
