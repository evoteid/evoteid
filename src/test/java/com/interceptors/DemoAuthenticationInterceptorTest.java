package com.interceptors;

import com.data.voter.SingleVoter;

import com.helpers.NoopController;
import com.services.context.RequestContextHolder;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCreatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class DemoAuthenticationInterceptorTest {

    private final String protectedUtl = "/something";
    private final String unprotectedUrl = "/api/authenticate";

    private JwtTokenCoderService mockTokenService;

    private JwtTokenCreatorService mockTokenCreatorService;

    private RequestContextHolder mockRequestContextHolder;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockTokenService = Mockito.mock(JwtTokenCoderService.class);
        mockTokenCreatorService = Mockito.mock(JwtTokenCreatorService.class);
        mockRequestContextHolder = Mockito.mock(RequestContextHolder.class);

        mockMvc = MockMvcBuilders.standaloneSetup(new NoopController())
                .addInterceptors(new DemoAuthenticationInterceptor(mockTokenService,mockTokenCreatorService,mockRequestContextHolder))
                .build();
    }

    @Test
    public void unprotectedUrlShouldPassThrough() throws Exception {
        MvcResult result = mockMvc.perform(get(unprotectedUrl)).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
    }

    @Test
    public void protectedUrlWithoutJwtTokenShouldNotPass() throws Exception {
        MvcResult result = mockMvc.perform(get(protectedUtl)).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals("JwtToken to access protected URI \"/something\" is missing.",result.getResponse().getContentAsString());
    }

    @Test
    public void protectedUrlWithInvalidJwtTokenShouldNotPass() throws Exception {
        when(mockTokenService.getVoterFromToken(eq("dummy"))).thenReturn(Optional.empty());

        MvcResult result = mockMvc.perform(
                get(protectedUtl)
                .header("Authorization","dummy")).andReturn();
        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("JwtToken to access protected URI \"/something\" is invalid.",result.getResponse().getContentAsString());
    }

    @Test
    public void protectedUrlValidJwtTokenShouldPass() throws Exception {
        when(mockTokenService.getVoterFromToken(eq("dummy"))).thenReturn(Optional.of(new SingleVoter().setName("foo")));

        MvcResult result = mockMvc.perform(
                get(protectedUtl)
                        .header("Authorization","dummy")).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
    }

    @Test
    public void protectedUrlValidJwtTokenAndBearerPrefixShouldPass() throws Exception {
        when(mockTokenService.getVoterFromToken(eq("dummy"))).thenReturn(Optional.of(new SingleVoter().setName("foo")));

        MvcResult result = mockMvc.perform(
                get(protectedUtl)
                        .header("Authorization","Bearer dummy")).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
    }

    @Test
    public void validRequestShouldSetCurrentUserInContext() throws Exception {
        String userName = "foo";
        String publicKey = "publicKey";
        SingleVoter returnedVoter = new SingleVoter().setName("foo").setPublicKey("publicKey");
        when(mockTokenService.getVoterFromToken(eq("dummy"))).thenReturn(Optional.of(returnedVoter));

        mockMvc.perform(
                get(protectedUtl)
                        .header("Authorization","dummy")).andReturn();

        ArgumentCaptor<SingleVoter> argumentCaptor = ArgumentCaptor.forClass(SingleVoter.class);
        verify(mockRequestContextHolder,times(1)).setCurrentVoter(argumentCaptor.capture());
        SingleVoter storedVoter = argumentCaptor.getValue();
        Assert.assertEquals(userName,storedVoter.getName());
        Assert.assertEquals(publicKey,storedVoter.getPublicKey().get());
    }

    @Test
    public void newJwtTokenIsSent() throws Exception {
        Long voterId = 42L;
        when(mockTokenService.getVoterFromToken(eq("dummy"))).thenReturn(Optional.of(new SingleVoter().setName("foo").setId(42L)));
        when(mockTokenCreatorService.createTokenForUser(eq(voterId))).thenReturn(Optional.of("newDummyToken"));

        MvcResult result = mockMvc.perform(
                get(protectedUtl)
                        .header("Authorization","dummy")).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
        Assert.assertEquals("newDummyToken",result.getResponse().getHeader("RefreshToken"));
    }

}
