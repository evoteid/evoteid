package com.interceptors;

import com.data.voter.SingleVoter;
import com.helpers.NoopController;
import com.services.context.RequestContextHolder;
import com.services.data.voters.VotersService;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCreatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class AuthenticationInterceptorTest {

    private static final String UNPROTECTED_URL = "/api/authenticate";

    private static final String CURRENT_VOTER_URL = "/api/currentVoterToken";

    private static final String REGISTER_CURRENT_VOTER_URL = "/api/registerCurrentVoter";

    @Autowired
    private MockMvc mockMvc;

    private JwtTokenCoderService mockTokenService;

    private JwtTokenCreatorService mockTokenCreatorService;

    private RequestContextHolder mockRequestContextHolder;

    private VotersService mockVotersService;

    @Before
    public void setup() {
        mockTokenService = Mockito.mock(JwtTokenCoderService.class);
        mockTokenCreatorService = Mockito.mock(JwtTokenCreatorService.class);
        mockRequestContextHolder = Mockito.mock(RequestContextHolder.class);
        mockVotersService = Mockito.mock(VotersService.class);

        mockMvc = MockMvcBuilders.standaloneSetup(new NoopController())
                .addInterceptors(new AuthenticationInterceptor(
                        mockTokenService,
                        mockTokenCreatorService,
                        mockVotersService,
                        mockRequestContextHolder
                        ))
                .build();
    }

    @Test
    public void homeOrganizationNullForbidden() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy"))
                .andReturn();
        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("Can't determine which organization the voter belongs to.",result.getResponse().getContentAsString());
    }

    @Test
    public void nonEXAMPLE_COMmembersForbidden() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","Hogwarts")).andReturn();
        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),result.getResponse().getStatus());
        Assert.assertEquals("At the moment only members from ETH Zurich can access the tool.",result.getResponse().getContentAsString());
    }

    @Test
    public void emailMustBeSpecified() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")).andReturn();
        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("Can't determine the e-mail address of the voter.",result.getResponse().getContentAsString());
    }

    @Test
    public void inValidEmailReturnsForbidden() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","invalid")).andReturn();
        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("The mail address of voter is invalid.",result.getResponse().getContentAsString());
    }

    @Test
    public void givenNameMustBeSpecified() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("surname","foo")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId"))
                .andReturn();

        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("Can't determine the name voter.",result.getResponse().getContentAsString());
    }

    @Test
    public void surNameMustBeSpecified() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","foo")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId"))
                .andReturn();

        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("Can't determine the name voter.",result.getResponse().getContentAsString());
    }

    @Test
    public void principalNameMustBeSpecified() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("surname","foo")
                .requestAttr("givenName","foo")
                .requestAttr("uniqueID","uniqueId"))
                .andReturn();

        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("Can't determine voter id.",result.getResponse().getContentAsString());
    }

    @Test
    public void uniqueIDMustBeSpecified() throws Exception {
        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("surname","foo")
                .requestAttr("givenName","foo")
                .requestAttr("principalName","princFoo"))
                .andReturn();

        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),result.getResponse().getStatus());
        Assert.assertEquals("Can't determine voter id.",result.getResponse().getContentAsString());
    }

    @Test
    public void registeredVoterGetsToken() throws Exception {
        String newToken = "New token";
        Long voterId = 27L;
        when(mockVotersService.upsertVotersData("uniqueId","Foo Bar","foo@bar")).thenReturn(Optional.of(new SingleVoter().setId(voterId)));
        when(mockTokenCreatorService.createTokenForUser(voterId)).thenReturn(Optional.of(newToken));

        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")
        ).andReturn();

        verify(mockTokenCreatorService,times(1)).createTokenForUser(voterId);
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
        Assert.assertEquals(newToken,result.getResponse().getHeader("RefreshToken"));
    }

    @Test
    public void currentVoterIsSetInContextIfSuccessful() throws Exception {
        String newToken = "New token";
        Long voterId = 27L;
        when(mockVotersService.upsertVotersData("uniqueId","Foo Bar","foo@bar")).thenReturn(Optional.of(new SingleVoter().setId(voterId)));
        when(mockTokenCreatorService.createTokenForUser(voterId)).thenReturn(Optional.of(newToken));

        mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")
        ).andReturn();

        ArgumentCaptor<SingleVoter> captor = ArgumentCaptor.forClass(SingleVoter.class);
        verify(mockRequestContextHolder,times(1)).setCurrentVoter(captor.capture());
        Assert.assertEquals(27L,captor.getValue().getId().longValue());
    }
    @Test
    public void returnsUnregisteredIfNotKnown() throws Exception {
        when(mockVotersService.upsertVotersData("uniqueId","Foo Bar","foo@bar")).thenReturn(Optional.empty());

        MvcResult result = mockMvc.perform(get("/dummy")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")).andReturn();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),result.getResponse().getStatus());
        Assert.assertEquals("The voter is not registered yet.",result.getResponse().getHeader("ErrorMessage"));
        Assert.assertEquals("{\"voterName\":\"Foo Bar\"}",result.getResponse().getContentAsString());
    }

    @Test
    public void unprotectedUrlShouldPassThrough() throws Exception {
        MvcResult result = mockMvc.perform(get(UNPROTECTED_URL)
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
    }

    @Test
    public void registerCurrentVoterUrlShouldPassThrough() throws Exception {
        MvcResult result = mockMvc.perform(get(REGISTER_CURRENT_VOTER_URL)
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")).andReturn();

        ArgumentCaptor<SingleVoter> argumentCaptor = ArgumentCaptor.forClass(SingleVoter.class);
        verify(mockRequestContextHolder,times(1)).setCurrentVoter(argumentCaptor.capture());

        Assert.assertEquals("Foo Bar", argumentCaptor.getValue().getName());
        Assert.assertEquals("foo@bar", argumentCaptor.getValue().getEmail());
        Assert.assertEquals("uniqueId", argumentCaptor.getValue().getUniqueId().get());
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
    }

    @Test
    public void currentVoterUrlShouldPassThrough() throws Exception {
        MvcResult result = mockMvc.perform(get(CURRENT_VOTER_URL)
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")).andReturn();

        ArgumentCaptor<SingleVoter> argumentCaptor = ArgumentCaptor.forClass(SingleVoter.class);
        verify(mockRequestContextHolder,times(1)).setCurrentVoter(argumentCaptor.capture());

        Assert.assertEquals("Foo Bar", argumentCaptor.getValue().getName());
        Assert.assertEquals("foo@bar", argumentCaptor.getValue().getEmail());
        Assert.assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
        Assert.assertEquals(NoopController.RESPONSEMESSAGE,result.getResponse().getContentAsString());
    }

    @Test
    public void adminTokenIsRefreshedIfVoterIsAdmin() throws Exception {
        long id = 27L;
        when(mockTokenCreatorService.createTokenForUser("admin")).thenReturn(Optional.of("New admin token"));

        when(mockTokenService.getVoterFromToken("New token")).thenReturn(Optional.of(new SingleVoter().setId(id)));
        when(mockTokenService.getVoterFromToken("Admin Token")).thenReturn(Optional.of(new SingleVoter().setName("admin")));
        when(mockTokenService.getVoterFromToken("New admin token")).thenReturn(Optional.of(new SingleVoter().setName("admin")));

        MvcResult result = mockMvc.perform(get("/dummy")
                .header("Authorization","Admin Token")
                .requestAttr("homeOrganization","example.com")
                .requestAttr("mail","foo@bar")
                .requestAttr("givenName","Foo")
                .requestAttr("surname","Bar")
                .requestAttr("principalName","princFoo")
                .requestAttr("uniqueID","uniqueId")).andReturn();

        ArgumentCaptor<SingleVoter> captor = ArgumentCaptor.forClass(SingleVoter.class);
        verify(mockRequestContextHolder,times(1)).setCurrentVoter(captor.capture());
        Assert.assertEquals("admin",captor.getValue().getName());
        Assert.assertEquals("New admin token",result.getResponse().getHeader("RefreshToken"));
    }
}
