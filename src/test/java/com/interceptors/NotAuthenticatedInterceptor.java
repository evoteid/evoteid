package com.interceptors;


import com.services.context.RequestContextHolder;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCreatorService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NotAuthenticatedInterceptor extends DemoAuthenticationInterceptor {

    public NotAuthenticatedInterceptor(JwtTokenCoderService jwtTokenCoderService, JwtTokenCreatorService jwtTokenCreatorService, RequestContextHolder requestContextHolder) {
        super(jwtTokenCoderService, jwtTokenCreatorService, requestContextHolder);
    }

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        response.getWriter().write("Unauthorized");
        return false;
    }
}
