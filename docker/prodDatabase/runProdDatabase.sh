sudo docker stop databaseContainer
sudo docker rm databaseContainer

sudo docker stop databaseTestContainer
sudo docker rm databaseTestContainer

sudo docker rmi mysql

sudo docker volume ls -qf dangling=true
sudo docker volume rm $(sudo docker volume ls -qf dangling=true)

sudo docker run -d \
      -p 3306:3306 \
     --name databaseContainer \
     -e MYSQL_ROOT_PASSWORD=root123 \
     -e MYSQL_DATABASE=evoting_app_db \
     -e MYSQL_USER=app_user \
     -e MYSQL_PASSWORD=test123 \
        mysql:latest

        sudo docker run -d \
              -p 3307:3306 \
             --name databaseTestContainer \
             -e MYSQL_ROOT_PASSWORD=root123 \
             -e MYSQL_DATABASE=evoting_test_db \
             -e MYSQL_USER=app_user \
             -e MYSQL_PASSWORD=test123 \
                mysql:latest

sleep 10

