sudo docker stop backendProdContainer
sudo docker rm backendProdContainer
sudo docker rmi -f evotingbackendprod

sudo docker build -t evotingbackendprod --file docker/prodBackend/dockerFileBackendProd .
sudo docker run --name backendProdContainer --net=host -p 8080:8080 -t -d evotingbackendprod